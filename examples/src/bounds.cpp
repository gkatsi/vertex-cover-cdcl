#include <cstdlib>
#include <iostream>

#include "dimacs.hpp"
#include <Cmdline.hpp>
#include <Graph.hpp>

#include <Prop.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/utils.hpp>

const int nbenches = 71;

const char* benches[nbenches] = {"data/DIMACS_cliques/MANN_a27.clq",
    "data/DIMACS_cliques/brock200_1.clq", "data/DIMACS_cliques/brock200_2.clq",
    "data/DIMACS_cliques/brock200_3.clq", "data/DIMACS_cliques/brock200_4.clq",
    "data/DIMACS_cliques/brock400_1.clq", "data/DIMACS_cliques/brock400_2.clq",
    "data/DIMACS_cliques/brock400_3.clq", "data/DIMACS_cliques/brock400_4.clq",
    "data/DIMACS_cliques/brock800_1.clq" //};
    ,
    "data/DIMACS_cliques/brock800_2.clq", "data/DIMACS_cliques/brock800_3.clq",
    "data/DIMACS_cliques/brock800_4.clq", "data/DIMACS_cliques/c-fat200-1.clq",
    "data/DIMACS_cliques/c-fat200-2.clq", "data/DIMACS_cliques/c-fat200-5.clq",
    "data/DIMACS_cliques/c-fat500-1.clq", "data/DIMACS_cliques/c-fat500-10.clq",
    "data/DIMACS_cliques/c-fat500-2.clq", "data/DIMACS_cliques/c-fat500-5.clq",
    "data/DIMACS_cliques/hamming10-2.clq",
    "data/DIMACS_cliques/hamming10-4.clq", "data/DIMACS_cliques/hamming6-2.clq",
    "data/DIMACS_cliques/hamming6-4.clq", "data/DIMACS_cliques/hamming8-2.clq",
    "data/DIMACS_cliques/hamming8-4.clq",
    "data/DIMACS_cliques/johnson16-2-4.clq",
    "data/DIMACS_cliques/johnson32-2-4.clq",
    "data/DIMACS_cliques/johnson8-2-4.clq",
    "data/DIMACS_cliques/johnson8-4-4.clq", "data/DIMACS_cliques/keller4.clq",
    "data/DIMACS_cliques/keller5.clq", "data/DIMACS_cliques/keller6.clq",
    "data/DIMACS_cliques/p_hat1000-1.clq",
    "data/DIMACS_cliques/p_hat1000-2.clq",
    "data/DIMACS_cliques/p_hat1000-3.clq",
    "data/DIMACS_cliques/p_hat1500-1.clq",
    "data/DIMACS_cliques/p_hat1500-2.clq",
    "data/DIMACS_cliques/p_hat1500-3.clq", "data/DIMACS_cliques/p_hat300-1.clq",
    "data/DIMACS_cliques/p_hat300-2.clq", "data/DIMACS_cliques/p_hat300-3.clq",
    "data/DIMACS_cliques/p_hat500-1.clq", "data/DIMACS_cliques/p_hat500-2.clq",
    "data/DIMACS_cliques/p_hat500-3.clq", "data/DIMACS_cliques/p_hat700-1.clq",
    "data/DIMACS_cliques/p_hat700-2.clq", "data/DIMACS_cliques/p_hat700-3.clq",
    "data/DIMACS_cliques/san1000.clq", "data/DIMACS_cliques/san200_0.7_1.clq",
    "data/DIMACS_cliques/san200_0.7_2.clq",
    "data/DIMACS_cliques/san200_0.9_1.clq",
    "data/DIMACS_cliques/san200_0.9_2.clq",
    "data/DIMACS_cliques/san200_0.9_3.clq",
    "data/DIMACS_cliques/san400_0.5_1.clq",
    "data/DIMACS_cliques/san400_0.7_1.clq",
    "data/DIMACS_cliques/san400_0.7_2.clq",
    "data/DIMACS_cliques/san400_0.7_3.clq",
    "data/DIMACS_cliques/san400_0.9_1.clq",
    "data/DIMACS_cliques/sanr200_0.7.clq",
    "data/DIMACS_cliques/sanr200_0.9.clq",
    "data/DIMACS_cliques/sanr400_0.5.clq",
    "data/DIMACS_cliques/sanr400_0.7.clq", "data/DIMACS_cliques/MANN_a45.clq",
    "data/DIMACS_cliques/MANN_a81.clq", "data/DIMACS_cliques/MANN_a9.clq",
    "data/frb30-15-mis/frb30-15-1.mis", "data/frb30-15-mis/frb30-15-2.mis",
    "data/frb30-15-mis/frb30-15-3.mis", "data/frb30-15-mis/frb30-15-4.mis",
    "data/frb30-15-mis/frb30-15-5.mis"};

int main(int argc, char* argv[])
{

    Cmdline cmd("VertexCover", ' ', "0.0");
    cmd.parse(argc, argv);

    if (cmd.print_par()) {
        std::cout << " c instance   = " << cmd.get_filename() << std::endl;
        std::cout << " c seed       = " << cmd.get_seed() << std::endl;
        std::cout << " c cutoff     = time: ";
        if (cmd.get_time() > 0) {
            std::cout << cmd.get_time() << " s";
        } else {
            std::cout << "none";
        }
        std::cout << std::endl;
        std::cout << " c restarts   = " << cmd.get_restart() << std::endl;
    }

    usrand(cmd.get_seed());

    int ninst = nbenches;
    Graph* G;

    if (cmd.get_filename() == "random") {
        ninst = 2;
        G = new Graph[ninst];

        G[0].initialise(cmd.get_numnodes());
        G[0].initialise_random(cmd.get_numedges());

        G[1].initialise(G[0]);
        G[1].flip();
    } else {

        if (cmd.get_filename() == "all") {

            G = new Graph[2 * ninst];

            double starttime = minicsp::cpuTime();
            std::cout << "read graphs";
            std::cout.flush();

            for (int i = 0; i < ninst; ++i) {

                // std::cout << "read " << (i+1) << "-th graph: " << benches[i]
                // << std::endl;

                read_graph(benches[i],
                    [&](int nv, int ne) { G[i].initialise(nv); },
                    [&](int v1, int v2) {
                        G[i].add_undirected(v1 - 1, v2 - 1);
                    },
                    true);

                std::cout << ".";
                std::cout.flush();
            }

            double endtime = minicsp::cpuTime();
            std::cout << " -> ok! (" << (endtime - starttime) << ")"
                      << std::endl;

            starttime = minicsp::cpuTime();
            std::cout << "flip graphs";
            std::cout.flush();

            for (int i = 0; i < ninst; ++i) {
                G[ninst + i].initialise(G[i]);
                G[ninst + i].flip();

                std::cout << ".";
                std::cout.flush();
            }
            ninst *= 2;

            endtime = minicsp::cpuTime();
            std::cout << " -> ok! (" << (endtime - starttime) << ")"
                      << std::endl;

        } else {

            ninst = 2;

            G = new Graph[ninst];

            read_graph(cmd.get_filename().c_str(),
                [&](int nv, int ne) { G[0].initialise(nv); },
                [&](int v1, int v2) { G[0].add_undirected(v1 - 1, v2 - 1); },
                true);

            G[1].initialise(G[0]);
            G[1].flip();
        }
    }

    DegreeSorted<Graph>** rG = new DegreeSorted<Graph>*[ninst];

    for (int i = 0; i < ninst; ++i) {
        if (cmd.get_clique()) {

            std::cout << "flip " << benches[i] << std::endl;
            G[i].flip();
        }

        rG[i] = new DegreeSorted<Graph>(G[i]);
        rG[i]->set_modulo_weights(100);

        if (cmd.print_ins())
            std::cout << rG[i] << std::endl;
    }

    int bound[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    double cputime[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    double starttime, endtime;
    int delta = 0;

    std::cout << "clique cover bitset ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset();
        bound[0] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[0] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[0]) / double(ninst)
              << std::setw(20) << cputime[0] << std::endl;
		
    std::cout << "cc bitset qrepair   ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset_quick_repair(delta);
        bound[9] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[9] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[9]) / double(ninst)
              << std::setw(20) << cputime[9] << std::endl;
		
    std::cout << "cc bitset repair    ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset_repair(delta);
        bound[5] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[5] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[5]) / double(ninst)
              << std::setw(20) << cputime[5] << std::endl;

    std::cout << "cc bitset random 3  ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset_rand(3);
        bound[6] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[6] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[6]) / double(ninst)
              << std::setw(20) << cputime[6] << std::endl;

    std::cout << "cc bitset random 10 ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset_rand(10);
        bound[7] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[7] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[7]) / double(ninst)
              << std::setw(20) << cputime[7] << std::endl;

    std::cout << "cc bitset random 100";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_clique_cover_bitset_rand(100);
        bound[8] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[8] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[8]) / double(ninst)
              << std::setw(20) << cputime[8] << std::endl;

    std::cout << "clique cover dsatur ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->dsatur_cc();
        bound[1] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[1] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[1]) / double(ninst)
              << std::setw(20) << cputime[1] << std::endl;

    std::cout << "  coloration bitset ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->min_coloration_bitset();
        bound[2] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[2] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[2]) / double(ninst)
              << std::setw(20) << cputime[2] << std::endl;

    std::cout << "  coloration dsatur ";
    std::cout.flush();
    starttime = minicsp::cpuTime();
    for (int i = 0; i < ninst; ++i) {
        int gbound = rG[i]->dsatur_col();
        bound[3] += gbound;
        // std::cout << ".";
        // std::cout.flush();
    }
    endtime = minicsp::cpuTime();
    cputime[3] = (endtime - starttime);
    std::cout << std::setw(10) << double(bound[3]) / double(ninst)
              << std::setw(20) << cputime[3] << std::endl;

    std::cout << cmd.print_sol() << std::endl;

    if (cmd.print_sol()) {
        for (int i = 0; i < ninst; ++i) {

            int greedycc = rG[i]->min_clique_cover_bitset();
            int repaircc = rG[i]->min_clique_cover_bitset_quick_repair(delta);
            int dsaturcc = rG[i]->dsatur_cc();

            std::cout << std::setw(4) << greedycc << " " << std::setw(4)
                      << repaircc << " " << std::setw(4) << dsaturcc << " "
                      << benches[(i % nbenches)] << std::endl;
        }
    }

    delete[] G;
    for (int i = 0; i < ninst; ++i) {
        delete rG[i];
    }
    delete[] rG;
}
