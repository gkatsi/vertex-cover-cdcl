//make coloring
// ./bin/coloring data/DIMACS/MIS/graphe.vc
//options: 	-c
//			--cliques nombre_de_cliques_voulu
//			--triangle
//kirtania: ssh -Y fgauthie@kirtania.laas.fr

#include <cstdlib>
#include <iostream>
#include <chrono>

#include "dimacs.hpp"
#include "mtx.hpp"
#include "snap.hpp"
#include <Cmdline.hpp>
#include <Graph.hpp>

#include <BrancherLB.hpp>
#include <Prop.hpp>
#include <Clik.hpp>
#include <LBcont.hpp>
#include <Cliques.hpp>
#include <Triangle.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/core/cons.hpp>
#include <minicsp/core/utils.hpp>

//used in B&B
bool as(std::vector<int> v, int col) {
	for (int i = 0; i != v.size(); i++) {
		if (v[i] == col) return true;
	}
	return false;
}

// recalcule objectif (contraintes))
int branch_and_bound(minicsp::Solver& s, Graph G,
    std::vector<minicsp::cspvar>& vars, Cmdline& cmd,
	int lb, int& ub)
{
    bool sat{true};
    int nbcol = ub;
    int couleur;
    std::vector<int> cols;
	int numsol = 0;
	while (sat && (nbcol > lb || nbcol == 0)) {
		
		std::cout << "try solving" << std::endl;
		sat = s.solve();
		std::cout << sat << std::endl;
		
		if (sat) {
			++numsol;
			nbcol = 0;
			cols.resize(0);
			// calcule nbcol
			for (int i = 0; i < G.size(); i++) {
				couleur = s.cspModelValue(vars[i]);
			
				// debug
				//std::cout << couleur << " ";
				// ~debug
				if (!as(cols, couleur)) {
					cols.resize(cols.size()+1, couleur);
					nbcol++;
					std::cout << couleur << " ";
				}
			}
			// print actual solution
			double timenow = minicsp::cpuTime();
			std::cout << "Solution " << nbcol << " conflicts " << s.conflicts
				<< " time " << timenow << std::endl;
	    	if (cmd.print_sol()) {
					std::cout << "\n";
					for (int i = 0; i < G.size(); i++) {
	        	std::cout << s.cspModelValue(vars[i]) << " ";
	    		}
					std::cout << "\n";
		  	}
			
	 	   if (cmd.get_dumpsol()) {
	     	   	std::cout << "\nraw: ";
	       		for (int i = 0; i != s.nVars(); ++i)
	            	std::cout << (s.modelValue(minicsp::Lit(i)) == minicsp::l_True ? 1
	                	                                                           : 0)
	                    	  << " ";
	        	std::cout << "\n";
	    	}
		
			ub = nbcol;

			// change les num de couleurs
			/*for (int i = 0; i < G.size(); i++) {
				minicsp::Lit consCol = minicsp::Lit(s.cspvarleqi(vars[i],nbcol));
				s.uncheckedEnqueue(consCol);
			}*/

			// demande meilleure solution
			for (int i = 0; i < G.size(); i++) {	
				minicsp::Lit consCol = minicsp::Lit(s.cspvarleqi(vars[i],nbcol-1));
				if (vars[i].min(s) == nbcol) {
					sat = false;
				} else if (vars[i].max(s) <= nbcol-1) {
				} else if (vars[i].min(s) != vars[i].max(s)) {
					//std::cout << "Domaine : " << vars[i].min(s) << " " << vars[i].max(s) << std::endl;
					s.uncheckedEnqueue(consCol);
				}
			}
			// debug
				//std::cout << "\n";
			// ~debug
		}
   }
   return nbcol;
}

int main(int argc, char* argv[])
{
//gère options
    Cmdline cmd("VertexCover", ' ', "0.0");
    cmd.parse(argc, argv);

    if (cmd.print_cmd()) {
        for (int i = 0; i < argc; ++i)
            std::cout << argv[i] << " ";
        std::cout << std::endl;
    }

    if (cmd.print_par()) {
        std::cout << " c instance    = " << cmd.get_filename();
        if (cmd.get_clique())
            std::cout << " (as clique)\n";
        else
            std::cout << " (as vertex cover)\n";
        std::cout << " c seed        = " << cmd.get_seed() << std::endl;
        std::cout << " c learning    = ";
        switch (cmd.get_learning()) {
        case vc_options::NONE:
            std::cout << "no\n";
            break;
        case vc_options::NAIVE:
            std::cout << "yes unminimized\n";
            break;
        case vc_options::FAST:
            std::cout << "yes minimized (fast)\n";
            break;
        case vc_options::GREEDY:
            std::cout << "yes minimized (greedy)\n";
            break;
        }
        std::cout << " c lower bound = " << cmd.get_boundalgo() << std::endl;
        std::cout << " c pruning     = ";
        switch (cmd.get_pruning()) {
        case vc_options::NONE:
            std::cout << "no\n";
            break;
        case vc_options::NAIVE:
            std::cout << "Babel\'s rule unminimized";
            break;
        case vc_options::FAST:
            std::cout << "Babel\'s rule minimized (fast)";
            break;
        case vc_options::GREEDY:
            std::cout << "Babel\'s rule minimized (greedy)";
            break;
        }
        if (cmd.get_pruning() != vc_options::NONE) {
            if (cmd.get_deferred())
                std::cout << " (deferred)\n";
            else
                std::cout << " (immediate)\n";
        }
        std::cout << " c maxsat      = " << cmd.get_maxsat() << "\n";
        std::cout << " c dominance   = "
                  << (cmd.get_dominance()
                             ? (cmd.get_dominance() == 2 ? "cliques"
                                                         : "low degree")
                             : "none")
                  << std::endl;
        std::cout << " c buss        = " << (cmd.get_buss() ? "yes" : "no")
                  << std::endl;
        std::cout << " c cutoff      = time: ";
        if (cmd.get_time() > 0) {
            std::cout << cmd.get_time() << " s";
        } else {
            std::cout << "none";
        }
        std::cout << std::endl;
        std::cout << " c restarts    = " << cmd.get_restart() << std::endl;
        std::cout << " c branching   = " << cmd.get_branch_name() << std::endl;
        if (cmd.get_branch() == SERIAL) {
            std::cout << " c branch seq  = " << cmd.get_branch_sequence()
                      << std::endl;
            std::cout << " c branch sched= " << cmd.get_branch_schedule()
                      << std::endl;
        }
    }

    //usrand(cmd.get_seed());
		usrand(12345);
//création graphe
    Graph G;
		
	

    if (cmd.get_filename() == "random") {
        G.initialise(cmd.get_numnodes(), true, !cmd.get_clique());
        G.initialise_random(cmd.get_numedges());	
    } else {
        try {
            if (cmd.get_filename().find("snap") > cmd.get_filename().size()) {
                if (cmd.get_filename().find("mtx")
                    > cmd.get_filename().size()) {
                    dimacs::read_graph(cmd.get_filename().c_str(),
                        [&](int nv, int ne) {
                            G.initialise(nv, true, !cmd.get_clique());
                        },
                        [&](int v1, int v2) {
                            G.add_undirected(v1 - 1, v2 - 1);
                        },
                        [&](int v, int w) {
                            G.set_weight(v - 1, w);
                        },
                        false);
                } else {
                    mtx::read_graph(cmd.get_filename().c_str(),
                        [&](int nv, int ne) {
                            G.initialise(nv, true, !cmd.get_clique());
                        },
                        [&](int v1, int v2) {
                            if (!G.exists_edge(v1 - 1, v2 - 1))
                                G.add_undirected(v1 - 1, v2 - 1);
                        },
                        true);
                }
            } else {
                snap::read_graph(cmd.get_filename().c_str(),
                    [&](int nv, int ne) {
                        G.initialise(nv, true, !cmd.get_clique());
                    },
                    [&](int v1, int v2) {
                        if (!G.exists_edge(v1 - 1, v2 - 1))
                            G.add_undirected(v1 - 1, v2 - 1);
                    },
                    true);
            }
        } catch (std::runtime_error const& e) {
            std::cout << "Error while reading graph: " << e.what() << "\n";
            return 1;
        }
    }

		usrand(cmd.get_seed());

/*
    if (cmd.get_sorted() == "hash")
        G.hashsort(true);

    if (cmd.get_complement()) {
        G.flip();
    }

    if (cmd.get_sorted() != "no") {
        // else
        G.sort(cmd.get_sorted() == std::string("min"));
    }
*/
//pas de rG
//reecrire format classique
    if (cmd.print_dimacs() != "") {
        std::ofstream outfile(cmd.print_dimacs().c_str(), std::ofstream::out);
        G.print_dimacs(outfile);
        outfile.close();
    } else if (cmd.print_mts() != "") {
        std::ofstream outfile(cmd.print_mts().c_str(), std::ofstream::out);
        G.print_mts(outfile);
        outfile.close();
    } else {
        if (cmd.print_ins()) {
            std::cout << G << std::endl;
            std::cout << G.matrix[0] << std::endl;
        }
        if (cmd.print_par()) {
            std::cout << " c nodes       = " << G.size() << std::endl;
            std::cout << " c edges       = " << G.num_edges << std::endl;
            std::cout << " c weight      = " << G.total_weight << std::endl;
            if (cmd.get_clique())
                std::cout << " c lower bound = " << cmd.get_lb()
                          << "\n";
            else
                std::cout << " c upper bound = "
                          << G.total_weight - cmd.get_lb() << "\n";
        }
//upper bound
		// DualProblem dual(&G);
				
				

				
				
		DegreeSorted<Graph> dsG(G);
		DualProblem dual(&dsG);
		close_type ct = close_type::WEAK;
		dsG.dsatur_col(&dual, ct);
		

		int& ub = dual.dual_cost;
		

//cree solver
        minicsp::Solver s;
        setup_signal_handlers(&s);
        if (cmd.get_restart() == "no")
            s.restarting = false;
        if (cmd.get_learning() == vc_options::NONE)
            s.learning = false;
        s.setrandomseed(cmd.get_seed());

        s.trace = cmd.get_trace();
        s.debugclauses = cmd.get_trace();
        s.allow_clause_dbg = false;
//cree var
		int nv = G.size();
	//vars de coloration
        std::vector<minicsp::cspvar> vars(nv);
        for (int i = 0; i != nv; ++i) {
            vars[i] = minicsp::cspvar(s.newCSPVar(1,ub));
        }
		
		std::vector<minicsp::Lit> eq(nv*nv);
		if (cmd.get_triangle()) {
			//vars d'égalité couleurs
			for (int i = 0; i != nv; i++) {
				for (int j = i+1; j != nv; j++) {
					if (!G.exists_edge(i,j)) {
						eq[nv*j + i] = minicsp::Lit(s.newVar());
					} else {
						eq[nv*j+i] = lit_Undef;
					}
				}
			}
			//attention seulement défini pour j>i
			// A AMELIORER

			std::cout << cmd.get_debugsol().empty() << std::endl;
			exit(1);

        	if (!cmd.get_debugsol().empty()) {
           		std::ifstream dfs(cmd.get_debugsol());
           		if (!dfs) {
                	std::cout << "could not open debug solution file \'"
                    	      << cmd.get_debugsol() << "\'\n";
                	exit(1);
            	}
            	bool val;
            	for (dfs >> val; dfs; dfs >> val)
                	s.debug_solution_lits.push_back(minicsp::lbool(val));
            	std::cout << "Read solution file with "
                	      << s.debug_solution_lits.size() << " literals\n";
            	assert(s.debug_solution_lits.size()
                	== static_cast<unsigned>(s.nVars()));
            	for (int i = 0; i != s.nVars(); ++i)
                	std::cout << (s.debug_solution_lits[i] == minicsp::l_True ? 1
                    	                                                      : 0)
                        	  << " ";
            	std::cout << "\n";
        	}
		}
//options
/*
        vc_options options{
            cmd.get_bound(), cmd.get_threshold(),
            vc_options::explanation_level(cmd.get_learning()),
            vc_options::explanation_level(cmd.get_pruning()),
            cmd.get_deferred(), cmd.get_dominance(), cmd.checked(),
            cmd.weighted(), rG.total_weight - cmd.get_lb(),
            order_type(cmd.get_greedy_order()), cmd.get_maxsat(), cmd.get_exp(),
            cmd.get_compcaching(), cmd.get_dynprog(), !cmd.get_clique(),
            cmd.get_buss()
            // true
        };
*/
/*MODIFICATION
	ici on crée toutes nos contraintes (pour chaques arretes on doit avoir une couleur (int) differente) */
//creer tableau de contraintes
	int n = G.size();
	//contraintes de coloration
	for (int i = 0; i != n; i++) {
		for (int j = i+1; j != n; j++) {
			if (G.exists_edge(i, j)) { // besoin ou j i ?
				post_neq(s, vars[i], vars[j], 0);
			}
		}
	}
	
	vec<Lit> re;
	if (cmd.get_triangle()) {
		//contraintes de lien coloration-eq
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				if (!G.exists_edge(i,j))
					// post_eqneq_re(s, vars[i], vars[j], 0, eq[n*j + i]);
					post_neq_re(s, vars[i], vars[j], 0, eq[n*j + i]);
			}
		}

	
		//contraintes de triangles
		/*int nE;
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				for (int k = j+1; k < n; k++) {
					nE = 0;
					nE += G.exists_edge(i, j);
					nE += G.exists_edge(i, k);
					nE += G.exists_edge(j, k);
					if (nE < 2) {
						if (nE < 1) {
							cons_triangle *tr = new cons_triangle(s, eq[n*k+j],
												eq[n*k+i],
												eq[n*j+i]);
							s.addConstraint(tr);
						} else {
							if (G.exists_edge(i,j)) {
								re.clear();
								re.push(~eq[n*k+i]);
								re.push(~eq[n*k+j]);
								s.addClause(re);
							} else if (G.exists_edge(i,k)) {
								re.clear();
								re.push(~eq[n*j+i]);
								re.push(~eq[n*k+j]);
								s.addClause(re);
							} else if (G.exists_edge(k,j)) {
								re.clear();
								re.push(~eq[n*k+i]);
								re.push(~eq[n*j+i]);
								s.addClause(re);
							}
						}
					}
					// debug
						//std::cout << "triangle : " << i+1 << j+1 << k+1 << std::endl;
						//std::cout << nE << " edges" << std::endl;
					// ~debug
				}
			}
		}*/
	}

// debug
	// std::cout << G << std::endl;

// creation lb
	cons_coloring cons = cons_coloring(G, s, vars, nv);
	cons.initialise(cmd.get_cliques());

	// std::cout << "Lower Bound : " << cons.lb << std::endl;
	//
	// for (int i = 0; i < cons.lb; i++) {
	// 	std::cout << " " << cons.clikk[i];
	// }
	// std::cout << std::endl;
	
	// coloration clique
	/*for (int i = 0; i < cons.lb; i++) {
		vars[cons.clikk[i]].assign(s, i+1, NO_REASON);
		//std::cout << cons.clikk[i]+1  << " ";
	}*/
	//std::cout << std::endl;
	std::cout << "Lower Bound : " << cons.lb << std::endl;

	std::cout << "Upper Bound : " << ub << std::endl;

//LB en continue
	std::unique_ptr<minicsp::cons> lbc;
	Brancher *br = NULL;
	if (cmd.get_triangle()) {
		lbc = std::make_unique<cons_eqvar>(G, s, vars, eq, nv, ub);
		br = new Brancher(s, eq, nv, G);
		br->use();
	} else {
		lbc = std::make_unique<cons_lbc>(G, s, vars, nv, ub);
	}

//appel resolution
    int nbcolopti{0};
		
		
  	if (!cmd.get_debugsol().empty()) {
     		std::ifstream dfs(cmd.get_debugsol());
     		if (!dfs) {
          	std::cout << "could not open debug solution file \'"
              	      << cmd.get_debugsol() << "\'\n";
          	exit(1);
      	}
      	bool val;
      	for (dfs >> val; dfs; dfs >> val)
          	s.debug_solution_lits.push_back(minicsp::lbool(val));
      	std::cout << "Read solution file with "
          	      << s.debug_solution_lits.size() << " literals\n";
      	assert(s.debug_solution_lits.size()
          	== static_cast<unsigned>(s.nVars()));
      	for (int i = 0; i != s.nVars(); ++i)
          	std::cout << (s.debug_solution_lits[i] == minicsp::l_True ? 1
              	                                                      : 0)
                  	  << " ";
      	std::cout << "\n";
  	}
		
    nbcolopti = branch_and_bound(s, G, vars, cmd, cons.lb, ub);
	//std::cout << "Nombre de couleurs: \n" << nbcolopti << "\n";
	double timenow = minicsp::cpuTime();
	std::cout << "Fini " << nbcolopti << " conflicts " << s.conflicts << " time " << timenow << std::endl;
	//std::cout << "UB finale : " << ub << " LB finale : " << cons.lb << std::endl;
	delete br;
	}
}
