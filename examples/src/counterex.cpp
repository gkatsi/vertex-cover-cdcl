//make coloring
// ./bin/coloring data/DIMACS/MIS/graphe.vc
//options: 	-c
//			--triangle
//			--cliques
//kirtania: ssh -Y fgauthie@kirtania.laas.fr

#include <cstdlib>
#include <iostream>

#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/core/cons.hpp>
#include <minicsp/core/utils.hpp>


using namespace minicsp;

int main(int argc, char* argv[])
{
	
        Solver s;
				
        std::vector<cspvar> vars(3);
				vars[0] = cspvar(s.newCSPVar(3,3));
				vars[1] = cspvar(s.newCSPVar(2,2));
				vars[2] = cspvar(s.newCSPVar(1,4));
	
				post_alldiff(s, vars);
				
        for (int i = 0; i != 3; ++i) {
						std::cout << vars[i].min(s) << " " << vars[i].max(s) << std::endl;
        }
	
	
}
