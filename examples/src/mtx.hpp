#ifndef __VC_MTX_HH
#define __VC_MTX_HH

#include <assert.h>
#include <fstream>
#include <sstream>
#include <vector>

namespace mtx
{

template <typename setsize, typename add_edge>
void read_graph(const char* fn, setsize ss, add_edge ae, bool strict = false)
{
    using std::cerr;
    try {
        std::ifstream ifs(fn);
        if (!ifs)
            throw std::runtime_error("Could not open file for reading");
        bool gotheader{false};
        int ln{1};
        int nv{0}, nvb{0}, ne{0}, reade{0};
        std::vector<int> node;
        for (std::string line; getline(ifs, line); ++ln) {
            if (line[0] == '%') {
                continue;
            }
            std::istringstream iss(line);
            if (!gotheader) {
                std::string edge;
                iss >> nv >> nvb >> ne;
                if (!iss || nv != nvb) {
                    cerr << "ERROR: could not parse header at line " << ln
                         << "\n";
                    exit(1);
                }
                ss(nv, ne);
                gotheader = true;
            } else {
                // edge
                int x, y;
                iss >> x >> y;

                ++reade;

                if (x == y)
                    continue;
								
                ae(x, y);
            }
        }
        if (strict && reade < ne) {
            cerr << "ERROR: " << ne << " edges declared, but only " << reade
                 << " edges read\n";
            exit(1);
        }
    } catch (std::exception& e) {
        std::cout.flush();
        cerr << "ERROR: " << e.what() << std::endl;
        exit(1);
    }
}
}

#endif
