#ifndef __VC_SNAP_HH
#define __VC_SNAP_HH

#include <assert.h>
#include <fstream>
#include <sstream>
#include <vector>

namespace snap
{

template <typename setsize, typename add_edge>
void read_graph(const char* fn, setsize ss, add_edge ae, bool strict = false)
{
    using std::cerr;
    try {
        std::ifstream ifs(fn);
        if (!ifs)
            throw std::runtime_error("Could not open file for reading");
        bool gotheader{false};
        std::string lt; // line type
        int ln{1};
        int nv{0}, ne{0}, maxval{0}, reade{0}, cpt{1};
        std::vector<int> node;
        for (std::string line; getline(ifs, line); ++ln) {
            if (line[0] == '#') {
                continue;
            }
            std::istringstream iss(line);
            if (!gotheader) {
                std::string edge;
                iss >> lt >> edge >> nv >> ne >> maxval;
                if (!iss || lt != "p" || edge != "edge") {
                    cerr << "ERROR: could not parse header at line " << ln
                         << "\n";
                    exit(1);
                }
                ss(nv, ne);

                assert(maxval <= 1000000);

                maxval++;

                node.assign(maxval, -1);

                gotheader = true;
            } else {
                // edge
                int x, y;
                iss >> x >> y;

                ++reade;

                if (x == y)
                    continue;

                assert((x < maxval) && (y < maxval));

                if (node[x] == -1) {
                    node[x] = cpt;
                    cpt++;
                }

                if (node[y] == -1) {
                    node[y] = cpt;
                    cpt++;
                }

                ae(node[x], node[y]);
            }
        }
        if (strict && reade < ne) {
            cerr << "ERROR: " << ne << " edges declared, but only " << reade
                 << " edges read\n";
            exit(1);
        }
    } catch (std::exception& e) {
        std::cout.flush();
        cerr << "ERROR: " << e.what() << std::endl;
        exit(1);
    }
}
}

#endif
