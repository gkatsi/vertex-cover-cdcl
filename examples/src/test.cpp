
#include <stdlib.h>
#include <fstream>

 

#include <graph.hpp>

//  test git + clang-format
using namespace std;



int main(int argc, char **argv)
{
	int n = 20;
	
	int m = 75;

	Graph G;

	G.initialise(n);
	
	G.initialise_random(m);

	DegreeSorted<Graph> rG(G);

	std::cout << rG << std::endl
		<< std::endl
			<< "clique cover = " << rG.min_clique_cover_bitset()
				<< std::endl;

	std::cout << std::endl
		<< "coloration   = " << rG.min_coloration_bitset()
			<< std::endl;
				
				
				
	BitSet e(0,300,BitSet::full);
	BitSet d(0,300,BitSet::empt);
	
	e.remove_interval(40,70);
	e.remove_interval(10,15);
	e.remove_interval(140,200);
	e.remove_interval(202,207);
	e.remove_interval(220,300);
		
	std::cout << e << " " << e.size() << std::endl;
	
	int themax = 0;
	
	e.clear_up_to(100, d, themax);
	
	
	std::cout << e << " " << e.size() << std::endl;
	std::cout << d << " " << d.size() << std::endl;
	
}



