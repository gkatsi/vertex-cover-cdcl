#include <cstdlib>
#include <iostream>

#include "dimacs.hpp"
#include "mtx.hpp"
#include "snap.hpp"
#include <Cmdline.hpp>
#include <Graph.hpp>

#include <Brancher.hpp>
#include <Prop.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/utils.hpp>

struct bnb_state
{
    minicsp::Solver& s;
    DegreeSorted<Graph>& G;
    Cmdline& cmd;
    std::vector<minicsp::Lit> vars;

    int numsol{0};
    weight_type obj{G.total_weight};
    std::vector<int> bestsol;

    cons_vc* cons;

    bnb_state(minicsp::Solver& solver, DegreeSorted<Graph>& graph,
        Cmdline& cmdline, std::vector<minicsp::Lit>& v, vc_options options)
        : s(solver)
        , G(graph)
        , cmd(cmdline)
        , vars(v)
    {
        cons = post_vc(s, G, vars, options,
            [&](std::vector<int> const& vcsol, std::vector<int> const& clqsol) {
                solution_callback(vcsol, clqsol);
            });
    }
    ~bnb_state() { cons->solution_callback = nullptr; }

    weight_type branch_and_bound();
    void solution_callback(
        std::vector<int> const& implvc, std::vector<int> const& implclq);
};

void bnb_state::solution_callback(
    std::vector<int> const& implvc, std::vector<int> const& implclq)
{
    ++numsol;
    obj = 0;
    bestsol.clear();
    if (implclq.empty() && implvc.empty()) {
        for (int i = 0; i != G.capacity; ++i)
            if (s.modelValue(vars[i]) == minicsp::l_False) {
                bestsol.push_back(i);
                obj += G.weight[i];
            }
    } else {
        for (int i = 0; i != G.capacity; ++i)
            if (s.value(vars[i]) == minicsp::l_False) {
                bestsol.push_back(i);
                obj += G.weight[i];
            }
        for (int v : implclq) {
            bestsol.push_back(v);
            obj += G.weight[v];
        }
    }

    double timenow = minicsp::cpuTime();
    std::cout << "Solution " << numsol << ", conflicts = " << s.conflicts
              << ", depth = " << s.decisionLevel() << ", time = " << timenow
              << ", size = " << obj;

    if (cmd.print_sol()) {
        std::cout << ", sol = ";
        for (int x : bestsol)
            std::cout << (x) << " ";
    }

    std::cout << std::endl;

    if (cmd.get_dumpsol()) {
        std::cout << "\nraw: ";
        for (int i = 0; i != s.nVars(); ++i)
            std::cout << (s.modelValue(minicsp::Lit(i)) == minicsp::l_True ? 1
                                                                           : 0)
                      << " ";
        std::cout << "\n";
    }

    if (!G.check_solution(bestsol, !cmd.get_clique())) {
        std::cout << "WRONG IS!" << std::endl;
        exit(1);
    }
}

weight_type bnb_state::branch_and_bound()
{
    bool sat{true};
    std::vector<int> bestsol;
    std::vector<int> dummy1, dummy2;

    while (sat) {
        sat = s.solve();
        if (sat) {
            cons->sync_graph(s);
            solution_callback(dummy1, dummy2);

            if (!cons->set_ub(G.total_weight - obj))
                sat = false;
        }
    }

    return obj;
}

std::unique_ptr<Brancher> make_brancher(BranchingHeuristic b, Cmdline& cmd,
    minicsp::Solver& s, DegreeSorted<Graph>& rG,
    std::vector<minicsp::Lit> const& vars, cons_vc& cons);

std::vector<std::unique_ptr<Brancher>> make_serial_brancher_sequence(
    std::string const& seq, Cmdline& cmd, minicsp::Solver& s,
    DegreeSorted<Graph>& rG, std::vector<minicsp::Lit> const& vars,
    cons_vc& cons)
{
    std::vector<std::unique_ptr<Brancher>> rv;
    std::string str;
    for (size_t beg = 0, end = seq.find(',', beg); beg != seq.npos;
         beg = (end != seq.npos ? end + 1 : end), end = seq.find(',', beg)) {
        str = seq.substr(beg, end - beg);
        if (str.empty())
            throw std::runtime_error("empty brancher");

        auto bh = [&]() {
            auto i = brancher_names.find(str);
            if (i == brancher_names.end())
                throw std::runtime_error("Unknown brancher");
            return i->second;
        }();

        if (bh == SERIAL)
            throw std::runtime_error("Brancher inception");

        rv.emplace_back(make_brancher(bh, cmd, s, rG, vars, cons));
    }
    return rv;
}

std::vector<int> make_serial_brancher_schedule(std::string const& seq)
{
    std::vector<int> rv;
    std::string str;
    for (size_t beg = 0, end = seq.find(',', beg); beg != seq.npos;
         beg = (end != seq.npos ? end + 1 : end), end = seq.find(',', beg)) {
        str = seq.substr(beg, end - beg);
        if (str.empty())
            throw std::runtime_error("empty schedule cutoff");
        rv.push_back(std::stoi(str));
    }
    return rv;
}

std::unique_ptr<Brancher> make_brancher(BranchingHeuristic b, Cmdline& cmd,
    minicsp::Solver& s, DegreeSorted<Graph>& rG,
    std::vector<minicsp::Lit> const& vars, cons_vc& cons)
{
    switch (b) {
    case MAXDEGREE:
        return std::make_unique<MaxDBrancher>(s, rG, vars, cons);
    case MINDEGREE:
        return std::make_unique<MinDBrancher>(s, rG, vars, cons);
    case MINDOMMAXDEG:
        return std::make_unique<MinDomMaxDegBrancher>(s, rG, vars, cons);
    case MINDOMMINDEG:
        return std::make_unique<MinDomMinDegBrancher>(s, rG, vars, cons);
    case MINDOMTIMEDEG:
        return std::make_unique<MinDomTimeDegBrancher>(s, rG, vars, cons);
    case MINDOMOVERDEG:
        return std::make_unique<MinDomOverDegBrancher>(s, rG, vars, cons);
    case MINDOMTIMEWEICLQ:
        return std::make_unique<ScoreBrancher<ScoreDomTimeWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomTimeWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMOVERWEICLQ:
        return std::make_unique<ScoreBrancher<ScoreDomOverWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomOverWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMTIMEWEIIS:
        return std::make_unique<ScoreBrancher<ScoreDomTimeAntiWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomTimeAntiWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMOVERWEIIS:
        return std::make_unique<ScoreBrancher<ScoreDomOverAntiWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomOverAntiWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMTIMEPWEICLQ:
        return std::make_unique<ScoreBrancher<ScoreDomTimeProdWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomTimeProdWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMOVERPWEICLQ:
        return std::make_unique<ScoreBrancher<ScoreDomOverProdWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomOverProdWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMTIMEPWEIIS:
        return std::make_unique<ScoreBrancher<ScoreDomTimeProdAntiWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomTimeProdAntiWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case MINDOMOVERPWEIIS:
        return std::make_unique<ScoreBrancher<ScoreDomOverProdAntiWei, Within,
            std::less<double>>>(
            s, rG, vars, cons, ScoreDomOverProdAntiWei{rG, cons}, Within(cmd.heuristic_tolerance()));
    case LASTCLQ:
        return std::make_unique<RightmostCliqueBrancher>(s, rG, vars, cons);
    case MAXLB:
        return std::make_unique<MaxLBBrancher>(s, rG, vars, cons);
    case MAXLBOVERDEG:
        return std::make_unique<MaxLBOverDegBrancher>(s, rG, vars, cons);
    case MAXLBTIMEDEG:
        return std::make_unique<MaxLBTimeDegBrancher>(s, rG, vars, cons);
    case LEX:
        s.polarity_mode = minicsp::Solver::polarity_user;
        s.phase_saving = false;
        s.restarting = false;
        return std::make_unique<LexBrancher>(s, rG, vars, cons);
    case MINNW:
        return std::make_unique<ScoreBrancher<ScoreNeighborWeight,
            std::equal_to<int>, std::less<int>>>(
            s, rG, vars, cons, ScoreNeighborWeight{rG}, std::equal_to<int>());
    case MAXNW:
        return std::make_unique<ScoreBrancher<ScoreNeighborWeight,
            std::equal_to<int>, std::greater<int>>>(
            s, rG, vars, cons, ScoreNeighborWeight{rG}, std::equal_to<int>());
    case SERIAL: {
        auto branchers = make_serial_brancher_sequence(
            cmd.get_branch_sequence(), cmd, s, rG, vars, cons);
        auto schedule
            = make_serial_brancher_schedule(cmd.get_branch_schedule());
        return std::make_unique<SerialBrancher>(
            s, rG, vars, cons, std::move(branchers), std::move(schedule));
    }
    case VSIDS:
    case VECLEX: // fallthrough. We never handled it
        return {};
    default:
        throw std::runtime_error("Unknown brancher");
    }
}

int main(int argc, char* argv[])
{
    Cmdline cmd("VertexCover", ' ', "0.0");
    cmd.parse(argc, argv);

    if (cmd.print_cmd()) {
        for (int i = 0; i < argc; ++i)
            std::cout << argv[i] << " ";
        std::cout << std::endl;
    }

    if (cmd.print_par()) {
        std::cout << " c instance    = " << cmd.get_filename();
        if (cmd.get_clique())
            std::cout << " (as clique)\n";
        else
            std::cout << " (as vertex cover)\n";
        std::cout << " c seed        = " << cmd.get_seed() << std::endl;
        std::cout << " c learning    = ";
        switch (cmd.get_learning()) {
        case vc_options::NONE:
            std::cout << "no\n";
            break;
        case vc_options::NAIVE:
            std::cout << "yes unminimized\n";
            break;
        case vc_options::FAST:
            std::cout << "yes minimized (fast)\n";
            break;
        case vc_options::GREEDY:
            std::cout << "yes minimized (greedy)\n";
            break;
        }
        std::cout << " c lower bound = " << cmd.get_boundalgo() << std::endl;
        std::cout << " c pruning     = ";
        switch (cmd.get_pruning()) {
        case vc_options::NONE:
            std::cout << "no\n";
            break;
        case vc_options::NAIVE:
            std::cout << "Babel\'s rule unminimized";
            break;
        case vc_options::FAST:
            std::cout << "Babel\'s rule minimized (fast)";
            break;
        case vc_options::GREEDY:
            std::cout << "Babel\'s rule minimized (greedy)";
            break;
        }
        if (cmd.get_pruning() != vc_options::NONE) {
            if (cmd.get_deferred())
                std::cout << " (deferred)\n";
            else
                std::cout << " (immediate)\n";
        }
        std::cout << " c maxsat      = " << cmd.get_maxsat() << "\n";
        std::cout << " c dominance   = "
                  << (cmd.get_dominance()
                             ? (cmd.get_dominance() == 2 ? "cliques"
                                                         : "low degree")
                             : "none")
                  << std::endl;
        std::cout << " c buss        = " << (cmd.get_buss() ? "yes" : "no")
                  << std::endl;
        std::cout << " c cutoff      = time: ";
        if (cmd.get_time() > 0) {
            std::cout << cmd.get_time() << " s";
        } else {
            std::cout << "none";
        }
        std::cout << std::endl;
        std::cout << " c restarts    = " << cmd.get_restart() << std::endl;
        std::cout << " c branching   = " << cmd.get_branch_name() << std::endl;
        if (cmd.get_branch() == SERIAL) {
            std::cout << " c branch seq  = " << cmd.get_branch_sequence()
                      << std::endl;
            std::cout << " c branch sched= " << cmd.get_branch_schedule()
                      << std::endl;
        }
    }

    usrand(cmd.get_seed());

    Graph G;

    if (cmd.get_filename() == "random") {
        G.initialise(cmd.get_numnodes(), true, !cmd.get_clique());
        G.initialise_random(cmd.get_numedges());
    } else {
        try {
            if (cmd.get_filename().find("snap") > cmd.get_filename().size()) {
                if (cmd.get_filename().find("mtx")
                    > cmd.get_filename().size()) {
                    dimacs::read_graph(cmd.get_filename().c_str(),
                        [&](int nv, int ne) {
                            G.initialise(nv, true, !cmd.get_clique());
                        },
                        [&](int v1, int v2) {
                            G.add_undirected(v1 - 1, v2 - 1);
                        },
                        [&](int v, weight_type w) {
                            G.set_weight(v - 1, w);
                        },
                        false);
                } else {
                    mtx::read_graph(cmd.get_filename().c_str(),
                        [&](int nv, int ne) {
                            G.initialise(nv, true, !cmd.get_clique());
                        },
                        [&](int v1, int v2) {
                            if (!G.exists_edge(v1 - 1, v2 - 1))
                                G.add_undirected(v1 - 1, v2 - 1);
                        },
                        true);
                }
            } else {
                snap::read_graph(cmd.get_filename().c_str(),
                    [&](int nv, int ne) {
                        G.initialise(nv, true, !cmd.get_clique());
                    },
                    [&](int v1, int v2) {
                        if (!G.exists_edge(v1 - 1, v2 - 1))
                            G.add_undirected(v1 - 1, v2 - 1);
                    },
                    true);
            }
        } catch (std::runtime_error const& e) {
            std::cout << "Error while reading graph: " << e.what() << "\n";
            return 1;
        }
    }

    if (cmd.get_sorted() == "hash")
        G.hashsort(true);

    if (cmd.get_complement()) {
        G.flip();
    }

    if (cmd.get_sorted() != "no") {
        // else
        G.sort(cmd.get_sorted() == std::string("min"));
    }

    if (cmd.weighted())
        G.set_modulo_weights(200);

    DegreeSorted<Graph> rG(G);

    if (cmd.print_dimacs() != "") {
        std::ofstream outfile(cmd.print_dimacs().c_str(), std::ofstream::out);
        rG.print_dimacs(outfile);
        outfile.close();
    } else if (cmd.print_mts() != "") {
        std::ofstream outfile(cmd.print_mts().c_str(), std::ofstream::out);
        rG.print_mts(outfile);
        outfile.close();
    } else {
        if (cmd.print_ins()) {
            std::cout << rG << std::endl;
            std::cout << rG.matrix[0] << std::endl;
        }
        if (cmd.print_par()) {
            std::cout << " c nodes       = " << rG.size() << std::endl;
            std::cout << " c edges       = " << rG.num_edges << std::endl;
            std::cout << " c weight      = " << rG.total_weight << std::endl;
            if (cmd.get_clique())
                std::cout << " c lower bound = " << cmd.get_lb()
                          << "\n";
            else
                std::cout << " c upper bound = "
                          << G.total_weight - cmd.get_lb() << "\n";
        }

        minicsp::Solver s;
        setup_signal_handlers(&s);
        if (cmd.get_restart() == "no")
            s.restarting = false;
        if (cmd.get_learning() == vc_options::NONE)
            s.learning = false;
        s.setrandomseed(cmd.get_seed());

        s.trace = cmd.get_trace();
        s.debugclauses = cmd.get_trace();
        s.allow_clause_dbg = false;

        std::vector<minicsp::Lit> vars(G.size());
        for (int i = 0; i != G.size(); ++i) {
            vars[i] = minicsp::Lit(s.newVar(true));
        }

        if (!cmd.get_debugsol().empty()) {
            std::ifstream dfs(cmd.get_debugsol());
            if (!dfs) {
                std::cout << "could not open debug solution file \'"
                          << cmd.get_debugsol() << "\'\n";
                exit(1);
            }
            bool val;
            for (dfs >> val; dfs; dfs >> val)
                s.debug_solution_lits.push_back(minicsp::lbool(val));
            std::cout << "Read solution file with "
                      << s.debug_solution_lits.size() << " literals\n";
            assert(s.debug_solution_lits.size()
                == static_cast<unsigned>(s.nVars()));
            for (int i = 0; i != s.nVars(); ++i)
                std::cout << (s.debug_solution_lits[i] == minicsp::l_True ? 1
                                                                          : 0)
                          << " ";
            std::cout << "\n";
        }

        vc_options options{
            cmd.get_bound(), cmd.get_threshold(),
            vc_options::explanation_level(cmd.get_learning()),
            vc_options::explanation_level(cmd.get_pruning()),
            cmd.get_deferred(), cmd.get_dominance(), cmd.checked(),
            rG.is_weighted(), rG.total_weight - cmd.get_lb(),
            order_type(cmd.get_greedy_order()), cmd.get_maxsat(), cmd.get_exp(),
            cmd.get_compcaching(), cmd.get_dynprog(), !cmd.get_clique(),
            cmd.get_buss(), cmd.get_prune_to_is()
        };

        bnb_state state{s, rG, cmd, vars, options};
        auto cons = state.cons;

        std::unique_ptr<Brancher> brancher
            = make_brancher(cmd.get_branch(), cmd, s, rG, vars, *cons);
        if (cmd.get_branch() != VSIDS) {
            brancher->use();
            brancher->config = cmd.get_branch_config();
            brancher->cutoff = cmd.get_branch_switch_cutoff();
        }

        weight_type obj{0};
        obj = state.branch_and_bound();

        std::cout << "Optimum " << obj << "\n";

        if (cmd.print_sta()) {
            printStats(s);
            std::cout << "VC propagator stats:\n\tFails: " << cons->fails
                      << "\n\tAverage explanation size: "
                      << cons->clauselits / static_cast<double>(cons->fails)
                      << "\n\tbefore minimization: "
                      << (cons->clauselits + cons->removedlits)
                    / static_cast<double>(cons->fails)
                      << "\n\tdifference: "
                      << cons->removedlits / static_cast<double>(cons->fails)
                      << "\n";
            if (cmd.get_bound() != vc_options::GREEDYdual)
                std::cout << "\tdelta (dsatur-gcc): "
                          << ((double)(cons->delta)
                                 / (double)(std::max(cons->numcalls, 1)))
                          << std::endl;
            if (cmd.get_branch() != VSIDS && cmd.get_branch() != LEX)
                brancher->printStats();
            if (cmd.checked()) {
                std::cout << "\tNum successful bound checks: "
                          << cons->numchecks << std::endl;
            }
        }
        if (cmd.get_maxsat() >= vc_options::STAT) {
            std::cout << std::setw(3) << "lvl" << std::setw(8) << "#"
                      << std::setw(8) << "bound" << std::setw(8) << "# fails\n"
                      << std::endl;
            for (unsigned i = 0; i < rG.capacity; ++i) {
                if (cons->num_maxsat_calls[i] > 0)
                    std::cout
                        << std::setw(3) << i << std::setw(8)
                        << cons->num_maxsat_calls[i] << std::setw(8)
                        << (double)((int)(((double)(cons->maxsat_bound[i]))
                               / ((double)(cons->num_maxsat_calls[i])) * 100.0))
                            / 100.0
                        << std::setw(8) << cons->maxsat_early_fails[i] << " ("
                        << (cons->maxsat_early_fails[i] * 100.0
                               / (double)(cons->num_maxsat_calls[i])) << " %)"
                        << std::endl;
                else
                    break;
            }
        }
        if (cmd.get_compcaching()) {
            std::cout << std::setw(3) << "lvl" << std::setw(8) << "#"
                      << std::setw(8) << "bound" << std::setw(8) << "# fails\n"
                      << std::endl;
            for (unsigned i = 0; i < rG.capacity; ++i) {
                if (cons->num_cc_calls[i] > 0)
                    std::cout
                        << std::setw(3) << i << std::setw(8)
                        << cons->num_cc_calls[i] << std::setw(8)
                        << (double)((int)(((double)(cons->cc_bound[i]))
                               / ((double)(cons->num_cc_calls[i])) * 100.0))
                            / 100.0
                        << std::setw(8) << cons->cc_early_fails[i] << " ("
                        << (cons->cc_early_fails[i] * 100.0
                               / (double)(cons->num_cc_calls[i])) << " %)"
                        << std::endl;
                else
                    break;
            }
        }
        if (cmd.get_dynprog()) {
            std::cout << std::setw(3) << "lvl" << std::setw(8) << "#"
                      << std::setw(8) << "bound" << std::setw(8) << "# fails\n"
                      << std::endl;
            for (unsigned i = 0; i < rG.capacity; ++i) {
                if (cons->num_dp_calls[i] > 0)
                    std::cout
                        << std::setw(3) << i << std::setw(8)
                        << cons->num_dp_calls[i] << std::setw(8)
                        << (double)((int)(((double)(cons->dp_bound[i]))
                               / ((double)(cons->num_dp_calls[i])) * 100.0))
                            / 100.0
                        << std::setw(8) << cons->dp_early_fails[i] << " ("
                        << (cons->dp_early_fails[i] * 100.0
                               / (double)(cons->num_dp_calls[i])) << " %)"
                        << std::endl;
                else
                    break;
            }
        }
    }
}
