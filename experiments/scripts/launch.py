#! /usr/bin/env python

import threading
# from subprocess import STDOUT, check_output
import subprocess
import sys
from threading import Thread
import os
import signal


def kill( process ):
    if process.poll() == None:
        os.kill( process.pid, signal.SIGKILL )

def limit( process, cutoff ):
    t = threading.Timer( cutoff, kill, [process] )
    t.start()
    return t
    
class ExperimentRunner(Thread):
    def __init__ (self, jobs, timelimit):
        Thread.__init__(self)
        self.job = jobs;
        self.timelimit = timelimit
        print 'init thread'
        

    def run(self):
        while 1:
            global GLOBAL_COUNTER
            idx = GLOBAL_COUNTER
            GLOBAL_COUNTER += 1
            
            if idx >= len(self.job):
                break
                
            
            cmd = []
            stout = 'dump'
            sterr = 'dump'
            for word in self.job[idx].split():
                if stout == None:
                    stout = word
                elif sterr == None:
                    sterr = word
                elif word == '>' :
                    stout = None
                elif word == '2>' :
                    sterr = None
                else:
                    cmd.append(word)
                    
                    
            print 'run job %i/%i: %s'%(idx+1, len(self.job), ' '.join(cmd))
            
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            clock = limit( process, self.timelimit )
            # out,err = process.communicate()
            outfile = open(stout, 'w')
            process.stdout.flush()
	    for line in process.stdout :
		outfile.write(line)
	    outfile.write(process.stdout.read()+'\n')
            process.wait()
	    outfile.close()
            
            err = process.stderr.read()
            if len(err) > 0 :
                errfile = open(sterr, 'w')
                errfile.write(process.stderr.read()+'\n')
                errfile.close()
            # output = check_output(self.job[idx], stderr=STDOUT, timeout=self.timelimit)



GLOBAL_COUNTER = 0


if __name__ == "__main__":
    
    if len(sys.argv) < 2 :
        print 'usage: ./launch.py <list of commands> [cutoff in seconds]'
        sys.exit(1)
        
    cutoff = 5
    if len(sys.argv) > 2 :
        cutoff = float(sys.argv[2])
        
    parallel = 3
    if len(sys.argv) > 3 :
        parallel = int(sys.argv[3])

    jobs = [line[:-1] for line in open(sys.argv[1], 'r') if len(line)>2]

    ER = [ExperimentRunner(jobs, cutoff) for i in range(parallel)]
    for er in ER:
        er.start()






