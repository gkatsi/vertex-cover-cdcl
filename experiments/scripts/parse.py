#! /usr/bin/env python

from __future__ import print_function
import sys
import subprocess
import math


show_missing_runs = False

root = '../data'
folders = ['DIMACS_cliques', 'DIMACS_MIS', 'frb30-15-mis'] #, 'snap']
ptypes = ['clq', 'mis', 'mis'] #, 'mis']
common = ['--print_par', '--print_sta', '--print_cmd']
exe = '../bin/vc'

option = {}

option['bound'] = ['-b', 'greedy']
option['pruning'] = ['-b', 'greedy', '-p']
option['clique'] = ['-b', 'greedy', '-d', '2']
option['clqNpru'] = ['-b', 'greedy', '-d', '2', '-p']
option['learning0'] = ['-b', 'greedy', '-l', '-m', '0']
option['pruNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-p']
option['learning1'] = ['-b', 'greedy', '-l', '-m', '1']
option['pruNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-p']
option['learning2'] = ['-b', 'greedy', '-l', '-m', '2']
option['pruNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-p']
option['learning3'] = ['-b', 'greedy', '-l', '-m', '3']
option['pruNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-p']
option['clqNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-d', '2']
option['clqNpruNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-p', '-d', '2']
option['clqNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-d', '2']
option['clqNpruNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-p', '-d', '2']
option['clqNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-d', '2']
option['clqNpruNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2']
option['clqNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-d', '2']
option['clqNpruNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-p', '-d', '2']

option['mindegree'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree']
option['lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq']
option['maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'maxlb']
option['mindegree2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'this2vsids']
option['lastclq2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['maxlb2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['vsids2mindegree'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['vsids2lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']
option['vsids2maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']

option['dynamic'] = ['-b', 'dynamic', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['quickr'] = ['-b', 'quickr', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['repair'] = ['-b', 'repair', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']

option['dsatur'] = ['-b', 'dsatur', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']



caption = dict([('lb','Lower Bound'), ('ub','Upper Bound'), ('gap','Gap')]) 

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

root = 'results'


def pretty((ty, val), best, prec):
    if ty == 'w' :
        return '\\cellcolor{red!30}{%.1f}'%(float(int(val*(float(10**prec))))/float(10**prec))
    elif ty == 'u' :
        return '-'
    elif ty == 'g' :
        if (ty,val) == best :
            return '\\cellcolor{TealBlue!30}{[%.1f]}'%(float(int(val*(float(10**prec))))/float(10**prec))
        else :
            return '[%.1f]'%(float(int(val*(float(10**prec))))/float(10**prec))
    elif ty == 'a' :
        if (ty,val) == best :
            return '\\cellcolor{TealBlue!30}{%.1fs}'%(float(int(val*(float(10**prec))))/float(10**prec))
        else :
            return '%.1fs'%(float(int(val*(float(10**prec))))/float(10**prec))


def prettyprint(ub, lb, tub, tlb, best_ub, best_lb, best_tub, best_tlb):
    if lb == -1 :
        return "-- & --"
    elif ub == lb :
        #optimal
        if best_tub*1.1 >= tub or best_tub+.2 >= tub :
            return '\\cellcolor{TealBlue!30}{\\textbf{%i}} & \\cellcolor{TealBlue!30}{%.1f}'%(ub, float(int(tub*(float(10))))/float(10))
        else : 
            return '\\textbf{%i} & %.1f'%(ub, float(int(tub*(float(10))))/float(10))
    else:
        if best_lb == lb :
            if ub == -1 :
                if best_tlb*1.1 >= tlb or best_tlb+.2 >= tlb :
                    return '\\cellcolor{TealBlue!30}{{\\tiny [\\textbf{%.1f}, --]}} & \\cellcolor{TealBlue!30}{{\\tiny [%.1f, --]}}'%(float(int(lb*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10))
                else :
                    return '{\\tiny [\\textbf{%.1f}, --]} & {\\tiny [%.1f, --]}'%(float(int(lb*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10))                
            else : 
                if best_tlb*1.1 >= tlb or best_tlb+.2 >= tlb :
                    return '\\cellcolor{TealBlue!30}{{\\tiny [\\textbf{%.1f}, %.1f]}} & \\cellcolor{TealBlue!30}{{\\tiny [%.1f, %.1f]}}'%(float(int(lb*(float(10))))/float(10),float(int(ub*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10),float(int(tub*(float(10))))/float(10))
                else :
                    return '{\\tiny [\\textbf{%.1f}, %.1f]} & {\\tiny [%.1f, %.1f]}'%(float(int(lb*(float(10))))/float(10),float(int(ub*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10),float(int(tub*(float(10))))/float(10))
        else :
            if best_ub == ub :
                return '{\\tiny [%.1f, \\textbf{%.1f}]} & {\\tiny [%.1f, %.1f]}'%(float(int(lb*(float(10))))/float(10),float(int(ub*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10),float(int(tub*(float(10))))/float(10))
            else :
                return '{\\tiny [%.1f, %.1f]} & {\\tiny [%.1f, %.1f]}'%(float(int(lb*(float(10))))/float(10),float(int(ub*(float(10))))/float(10),float(int(tlb*(float(10))))/float(10),float(int(tub*(float(10))))/float(10))


def integrate(data, precision, epsilon=0):
    curve = []
    y=0

    lastx = 0
    lasty = 0
    maxy = float(sum(data))
    maxx = float(len(data))

    if maxy > 0 :
        for x in range(len(data)):
            y+=data[x]
    
            nx = float(x)/maxx
            ny = float(y)/maxy

            delta = math.sqrt((ny-lasty)*(ny-lasty) + (nx-lastx)*(nx-lastx))
    
            # print('curve[%i]=%f (der=%f, delta=(%f-%f)^2+(%f-%f)^2=%f)'%(x,y,data[x],ny,lasty,nx,lastx,delta))
    
            if epsilon<delta and data[x]>0 :
                # print('add')
        
                curve.append((y,float(x)/(10.0**precision)))
                lasty = ny
                lastx = nx
            # else:
            #     print('skip')
    
        # if maxy != curve[-1][0] :
        #     curve.append((maxy, maxx))
    else:
        curve = [(0,0)]
      
    return curve


  
def plot(curve, problem, b):
  styles = [' [a] \n', ' [b] \n', ' [c] \n', ' [d] \n', ' [e] \n', ' [f] \n', ' [g] \n', ' [h] \n', ' [i] \n', ' [j] \n', ' [k] \n', ' [l] \n', ' [m] \n', ' [n] \n', ' [o] \n']
  osolvers = sorted([s for s in curve.keys()])
  return '\cactus{%s (%s)}{%s}{{%s}}'%(problem, caption[b],', '.join(osolvers),','.join(['{%s%s}'%(style.join([str(pair) for pair in curve[solver]]),style) for solver,style in zip(osolvers, styles)]))

    
  

        

class ExperimentData:

  def __init__(self, key, solvers=None, problems=None):
      
    print('init db [%s]'%key)
      
    keyfile = open('%s/%s.txt'%(root,key))
      
    line = keyfile.readline()
    self.solvers = line[:-1].split()
    
    line = keyfile.readline()
    parsed_problems = line[:-1].split()
    
    self.problem_set = set(parsed_problems)
    if problems is not None :        
        self.problem_set &= set(problems)
        self.problems = sorted([s for s in self.problem_set])
    else :
        self.problems = parsed_problems
        
    self.bench = {}.fromkeys(self.problems)
    for problem in parsed_problems:
        line = keyfile.readline()
        if problem in self.problem_set:
            self.bench[problem] = line[:-1].split()
        
    line = keyfile.readline()
    self.seeds = [int(s) for s in line[:-1].split()]
        
        
    
    self.solver_set = set(self.solvers)
    
    if solvers is not None :        
        self.solver_set &= set(solvers)
        self.solvers = sorted([s for s in self.solver_set])
        
    self.seed_set = set(self.seeds)
    
    self.bench_set = {}
    for problem in self.problems:
        self.bench_set[problem] = set(self.bench[problem])
    
    self.key = key
    # self.key = 'nohup'
    
    self.numruns = {}.fromkeys(self.problems)
    self.path = {}.fromkeys(self.problems)
    for problem in self.problems :
      self.path[problem] = {}.fromkeys(self.bench[problem])
      self.numruns[problem] = {}.fromkeys(self.solvers)
      for solver in self.solvers :
          self.numruns[problem][solver] = {}.fromkeys(self.bench[problem])
          for bench in self.bench[problem] :
              self.numruns[problem][solver][bench] = 0
              

    
    
  def read_minicsp_file(self, problem, bench, solver, seed):
      f = 'results/%s-%s-%s-%s-%i.res'%(self.key, problem, bench, solver, seed)
      try :
        infile = open(f)
      except IOError:
        if show_missing_runs :
            eprint('missing run: %s %s %i (%s)'%(solver, bench, seed, problem))
            cmdline = [exe, self.path[problem][bench], '--seed', str(seed)]
            if problem == 'clq':
              cmdline.append('-c')
            cmdline.extend(common)
            cmdline.extend(option[solver])
            eprint('%s > %s'%(' '.join([str(c) for c in cmdline]), f))
      else:
        self.numruns[problem][solver][bench] += 1
        n,m = 0,0

        for line in infile:
          if line.find('c instance') >= 0:
            path = line.split('=')[1]
            # print path[path.rfind('/')+1:path.rfind('.')],
          elif line.find('c nodes') >= 0:
            n = int(line.split('=')[1])
            # print n,
          elif line.find('c edges') >= 0:
            m = int(line.split('=')[1])
            self.param[problem][bench] = (n, m)
            # print m,
          elif line.find('Solution') >= 0:
            data = line.split(',')
            c = int(data[1].split('=')[1])
            t = float(data[2].split('=')[1])
            o = int(data[3].split('=')[1])

            if t>self.horizon:
              self.horizon = t

            if o<self.bound['lb'][problem][bench][0] :
              self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
            if o>self.bound['lb'][problem][bench][1] :
              self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)

            # print('[%s: %i, %i, %f]'%(solver,o,c,t))
            self.table['lb'][problem][bench][solver][seed].append((o,c,t))
          elif line.find('VC lower bound') >= 0:
            data = line.split(',')
            c = int(data[2].split('=')[1])
            t = float(data[3].split('=')[1])
            o = int(data[1].split('=')[1])

            if t>self.horizon:
              self.horizon = t

            if o<self.bound['ub'][problem][bench][0] :
              self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
            if o>self.bound['ub'][problem][bench][1] :
              self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)

            # print('[%s: %i, %i, %f]'%(solver,o,c,t))
            self.table['ub'][problem][bench][solver][seed].append((o,c,t))
          elif line[:7] == 'Optimum' :

            o = int(line.split()[1])
            c,t = 0,0
            for line in infile :
                if line[:9] == 'conflicts' :
                    c = int(line.split()[2])
                    break
            for line in infile :
                if line[:3] == 'CPU' :
                    t = float(line.split()[3])
                    break
            if t>self.horizon:
              self.horizon = t
            if o<self.bound['ub'][problem][bench][0] :
              self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
            if o>self.bound['ub'][problem][bench][1] :
              self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)

            # print('[%s: %i, %i, %f]'%(solver,o,c,t))
            self.table['ub'][problem][bench][solver][seed].append((o,c,t))
          elif line.find(exe) >= 0:
             self.path[problem][bench] = line.split()[1]
        
        infile.close()



      
  def read_parasols_file(self, problem, bench, solver, seed):
      if seed == 11648255 :
      
          f = 'results/%s-%s-%s-%s-%i.res'%(self.key, problem, bench, solver, seed)
          try :
            infile = open(f)
          except IOError:
            if show_missing_runs:
                # eprint('missing run: %s %s %i (%s)'%(solver, bench, seed, problem))
                cmdline = [exe, self.path[problem][bench], '--seed', str(seed)]
                if problem == 'clq':
                  cmdline.append('-c')
                cmdline.extend(common)
                cmdline.extend(option[solver])
                eprint('%s > %s'%(' '.join(cmdline), f))
          else:
            self.numruns[problem][solver][bench] += 1
            for line in infile:
              # print(line)
              if line.find('initial colouring') >= 0:
                  # print('bound')
                  data = line.split()
                  c = 0
                  t = float(data[1])/1000.0
                  o = int(data[5])

                  if t>self.horizon:
                    self.horizon = t

                  if o<self.bound['ub'][problem][bench][0] :
                    self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
                  if o>self.bound['ub'][problem][bench][1] :
                    self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)

                  self.table['ub'][problem][bench][solver][seed].append((o,c,t))
              elif line.find('found') >= 0:
                # print('sol')
                data = line.split()
                c = 0
                t = float(data[1])/1000.0
                o = int(data[3])

                if t>self.horizon:
                  self.horizon = t

                if o<self.bound['lb'][problem][bench][0] :
                  self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
                if o>self.bound['lb'][problem][bench][1] :
                  self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)
              
                self.table['lb'][problem][bench][solver][seed].append((o,c,t))
              elif line.find(exe) >= 0:
                 self.path[problem][bench] = line.split()[1]
              elif line[:2] != '--' and len(line) > 1 and line.find('slurm')<0 and line.find('srun')<0 and line.find('Segmentation fault')<0:
                  
                  # print('%s %s -->'%(f,line))
                  
                  # print('end')
                  data = line.split()
                  c = int(data[1])
                  o = int(data[0])
                  
                  # print(' o=%i c=%i'%(o,c))
                  
                  
                  
                  skip = True
                  for line in infile :
                      # print(line)
                      if not skip :
                          # print('time')
                          data = line.split()
                          t = float(data[0])/1000.0
                          break
                      else :
                          # print('skip')
                          skip = False
              
                  if o<self.bound['ub'][problem][bench][0] :
                    self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
                  if o>self.bound['ub'][problem][bench][1] :
                    self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)
                
                  self.table['ub'][problem][bench][solver][seed].append((o,c,t))
                  break

            # print
            infile.close()


  def read_mwclq_file(self, problem, bench, solver, seed):
      if seed == 11648255 :
      
          f = 'results/%s-%s-%s-%s-%i.res'%(self.key, problem, bench, solver, seed)
          try :
            infile = open(f)
          except IOError:
            if show_missing_runs:
                # eprint('missing run: %s %s %i (%s)'%(solver, bench, seed, problem))
                cmdline = [exe, self.path[problem][bench], '--seed', str(seed)]
                if problem == 'clq':
                  cmdline.append('-c')
                cmdline.extend(common)
                cmdline.extend(option[solver])
                eprint('%s > %s'%(' '.join(cmdline), f))
          else:
            self.numruns[problem][solver][bench] += 1
            for line in infile:
                
              if line[:2] == 'lb' :
                  data = line.split()
                  c = int(data[3])
                  t = float(data[2])
                  o = int(data[1])
                  
                  if t>self.horizon:
                    self.horizon = t

                  if o<self.bound['lb'][problem][bench][0] :
                    self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
                  if o>self.bound['lb'][problem][bench][1] :
                    self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)
                    
                  self.table['lb'][problem][bench][solver][seed].append((o,c,t))
                    
              elif line[:2] == 'ub' :
                  data = line.split()
                  c = int(data[3])
                  t = float(data[2])
                  o = int(data[1])
                  
                  if len(self.table['lb'][problem][bench][solver][seed]) > 0 :
                      if o < self.table['lb'][problem][bench][solver][seed][-1][0] :
                          break
                          
                  if t>self.horizon:
                    self.horizon = t

                  if o<self.bound['ub'][problem][bench][0] :
                    self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
                  if o>self.bound['ub'][problem][bench][1] :
                    self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)
                    
                  self.table['ub'][problem][bench][solver][seed].append((o,c,t))
            
            # if len(self.table['lb'][problem][bench][solver][seed]) == 0 and len(self.table['ub'][problem][bench][solver][seed]) > 0 :
                

            # print
            infile.close()
            
            
  def read_IncMaxCLQ_file(self, problem, bench, solver, seed):
      if seed == 11648255 :
      
          f = 'results/%s-%s-%s-%s-%i.res'%(self.key, problem, bench, solver, seed)
          
          try :
            infile = open(f)
          except IOError:
            if show_missing_runs:
                eprint('missing run: %s %s %i (%s)'%(solver, bench, seed, problem))
          else :
            self.numruns[problem][solver][bench] += 1
            last_lb = -1
            for line in infile :
                
                if len(line) >= 16:
                
                  if line[:16] == 'Current MaxCliqu' :
                      if len(line)>23 :
                          data = line[23:-2].split()
                          if len(data)>1 :
                      
                              c = 0
                              t = float(data[1])/1000.0
                              o = int(data[0])

                              if t>self.horizon:
                                self.horizon = t
                                
                              if o > last_lb :
                                  if o<self.bound['lb'][problem][bench][0] :
                                    self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
                                  if o>self.bound['lb'][problem][bench][1] :
                                    self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)

                                  self.table['lb'][problem][bench][solver][seed].append((o,c,t))
                                  last_lb = o
                  elif line[:16] == 'initial clique s' :

                      if len(line)>17 :
                          data = line[17:].split()
                          if len(data)>1 :
                      
                              c = 0
                              t = 0.0
                              o = int(data[1])
                              

                              if t>self.horizon:
                                self.horizon = t

                              if o > last_lb :
                                  if o<self.bound['lb'][problem][bench][0] :
                                    self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
                                  if o>self.bound['lb'][problem][bench][1] :
                                    self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)

                                  self.table['lb'][problem][bench][solver][seed].append((o,c,t))
                                  last_lb = o                            

                  elif line[:16] == 'Max Clique Size:' :

                      data = line.split()
                      c = 0
                      t = 0
                      if len(self.table['lb'][problem][bench][solver][seed]) > 0 :
                          t = self.table['lb'][problem][bench][solver][seed][-1][2]
                      o = int(data[3])

                      if t>self.horizon:
                        self.horizon = t

                      if o<self.bound['ub'][problem][bench][0] :
                        self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
                      if o>self.bound['ub'][problem][bench][1] :
                        self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)

                      self.table['ub'][problem][bench][solver][seed].append((o,c,t))
                
            infile.close()
                    
          # try :
          #   infile = open(f)
          # except IOError:
          #   if show_missing_runs:
          #       eprint('missing run: %s %s %i (%s)'%(solver, bench, seed, problem))
          #       if bench ==
          #       # cmdline = [exe, self.path[problem][bench], '--seed', str(seed)]
          #       # if problem == 'clq':
          #       #   cmdline.append('-c')
          #       # cmdline.extend(common)
          #       # cmdline.extend(option[solver])
          #       # eprint('%s > %s'%(' '.join(cmdline), f))
          # else:
          #   self.numruns[problem][solver][bench] += 1
          #   for line in infile:
          #     print(line)
          #     if line[:22] == 'Current MaxClique Size' :
          #
          #         print('LB')
          #
          #         data = line[23:].split()
          #         c = 0
          #         t = float(data[1])
          #         o = int(data[0])
          #
          #         if t>self.horizon:
          #           self.horizon = t
          #
          #         if o<self.bound['lb'][problem][bench][0] :
          #           self.bound['lb'][problem][bench] = (o, self.bound['lb'][problem][bench][1])
          #         if o>self.bound['lb'][problem][bench][1] :
          #           self.bound['lb'][problem][bench] = (self.bound['lb'][problem][bench][0], o)
          #
          #         self.table['lb'][problem][bench][solver][seed].append((o,c,t))
          #
          #     elif line[:16] == 'Max Clique Size:' :
          #
          #         print('UB')
          #
          #         data = line.split()
          #         c = 0
          #         t = 0
          #         if len(self.table['lb'][problem][bench][solver][seed]) > 0 :
          #             t = self.table['lb'][problem][bench][solver][seed][-1][2]
          #         o = int(data[3])
          #
          #         if t>self.horizon:
          #           self.horizon = t
          #
          #         if o<self.bound['ub'][problem][bench][0] :
          #           self.bound['ub'][problem][bench] = (o, self.bound['ub'][problem][bench][1])
          #         if o>self.bound['ub'][problem][bench][1] :
          #           self.bound['ub'][problem][bench] = (self.bound['ub'][problem][bench][0], o)
          #
          #         self.table['ub'][problem][bench][solver][seed].append((o,c,t))
          #
          #   # print
          #   infile.close()
        
        
  def read_datafiles(self):
      
    if not hasattr(self,'horizon'):
        
      print('read files')
      
      self.table = {}.fromkeys(['ub','lb'])
      self.bound = {}.fromkeys(['ub','lb'])
      self.table['lb'] = {}.fromkeys(self.problems)
      self.table['ub'] = {}.fromkeys(self.problems)
      self.bound['lb'] = {}.fromkeys(self.problems)
      self.bound['ub'] = {}.fromkeys(self.problems)
      self.param = {}.fromkeys(self.problems)
      self.horizon = 0
    
      for problem in self.problems:
        self.table['lb'][problem] = {}.fromkeys(self.bench_set[problem])
        self.table['ub'][problem] = {}.fromkeys(self.bench_set[problem])
        self.bound['lb'][problem] = {}.fromkeys(self.bench_set[problem])
        self.bound['ub'][problem] = {}.fromkeys(self.bench_set[problem])
        self.param[problem] = {}.fromkeys(self.bench_set[problem])
      
        for bench in self.bench_set[problem]:
          self.table['lb'][problem][bench] = {}.fromkeys(self.solvers)
          self.bound['lb'][problem][bench] = (sys.maxint, 0)
          self.table['ub'][problem][bench] = {}.fromkeys(self.solvers)
          self.bound['ub'][problem][bench] = (sys.maxint, 0)
        
          for solver in self.solvers:
            # print(solver
            self.table['lb'][problem][bench][solver] = {}.fromkeys(self.seeds)
            self.table['ub'][problem][bench][solver] = {}.fromkeys(self.seeds)
          
            for seed in self.seeds:
              self.table['lb'][problem][bench][solver][seed] = []
              self.table['ub'][problem][bench][solver][seed] = []

 
              if solver == 'parasols' :
                  self.read_parasols_file(problem, bench, solver, seed)
              elif solver == 'mwclq' :
                  self.read_mwclq_file(problem, bench, solver, seed)
              elif solver == 'IncMaxCLQ' :
                  self.read_IncMaxCLQ_file(problem, bench, solver, seed)
              else :
                  self.read_minicsp_file(problem, bench, solver, seed)
              
              if len(self.table['lb'][problem][bench][solver][seed]) == 0 :
                  self.table['lb'][problem][bench][solver][seed].append((None,None,None))
              if len(self.table['ub'][problem][bench][solver][seed]) == 0 :
                  self.table['ub'][problem][bench][solver][seed].append((None,None,None))
      # sys.exit(1)
      
                    
                  
  def find_buggy(self):
      print('find buggy runs')
      for problem in self.problems:
        for bench in self.bench_set[problem]:
          for solver in self.solvers:
            if solver != 'parasols' :
                for seed in self.seeds:      
                    if self.table['lb'][problem][bench][solver][seed][0][0] == None or self.table['ub'][problem][bench][solver][seed][0][0] == None :
                        f = 'results/%s-%s-%s-%s-%i.res'%(self.key, problem, bench, solver, seed)
                        cmdline = [exe, self.path[problem][bench], '--seed', str(seed)]
                        if problem == 'clq':
                          cmdline.append('-c')
                        cmdline.extend(common)
                        cmdline.extend(option[solver])
                        print('%s > %s 2> %s'%(' '.join(cmdline), f, f.replace('.res','.err')))
                    # else:
                    #     print('%s, %s, %s, %i, %s'%(problem, bench, solver, seed, str(self.table['ub'][problem][bench][solver][seed][0][0])))
                    #

    
  def get_cactus(self, precision, b, step):
    
    self.read_datafiles()
      
    
    horizon = int(self.horizon*(10.0**precision))+1
    
    curve = {}.fromkeys(self.problems)
    for problem in self.problems:
      curve[problem] = {}.fromkeys(self.solvers)
      for solver in self.solvers:
        curve[problem][solver] = [0]*horizon
      
    for problem in self.problems:
      for bench in self.bench[problem]:
        if self.param[problem][bench] != None :
            # print(bench)
            n,m = self.param[problem][bench]
            l,u = self.bound[b][problem][bench]
            for solver in self.solvers:
              # print(solver+' '+str(self.numruns[problem][solver][bench]))
          
              # nseeds = len([s for s in self.seeds if len(self.table[b][problem][bench][solver][s])>0])
          
              for seed in self.seeds:
                ly = 0
                for o,c,t in self.table[b][problem][bench][solver][seed]:
                    if o is not None :
                          x = int(t*(10.0**precision))
                          if b == 'lb' :
                              y = float(o - l + 1)/float(u - l + 1)
                          else :
                              y = float(u - o + 1)/float(u - l + 1)
                          
                          if y > 1 :
                              print('%s, %s, %f in [%f,%f]'%(solver,bench,o,l,u))
                              sys.exit(1)
                      
                          curve[problem][solver][x] += (y - ly)/float(len(self.bench[problem]) * self.numruns[problem][solver][bench])
                          ly = y
              # print('\n')
        # else :
        #     print('no run on %s'%bench)

    for problem in self.problems:
      for solver in self.solvers:
        print('%s, %s: %f'%(problem, solver, sum(curve[problem][solver])))
        # print(curve[problem][solver])
        curve[problem][solver] = integrate(curve[problem][solver],precision,step)
        # print(curve[problem][solver])
      
    return curve
    
  def get_gap_cactus(self, precision, step):
    
    self.read_datafiles()
      
    horizon = int(self.horizon*(10.0**precision))+1
    
    curve = {}.fromkeys(self.problems)
    for problem in self.problems:
      curve[problem] = {}.fromkeys(self.solvers)
      for solver in self.solvers:
        curve[problem][solver] = [0]*horizon
      
    for problem in self.problems:
      for bench in self.bench[problem]:
        if self.param[problem][bench] != None :
            # print(bench+'\n')
            n,m = self.param[problem][bench]
            l,u = (self.bound['ub'][problem][bench][0]-self.bound['lb'][problem][bench][1],self.bound['ub'][problem][bench][1]-self.bound['lb'][problem][bench][0])
            for solver in self.solvers:
              # print(solver)
          
              nseeds = len([s for s in self.seeds if (len(self.table['lb'][problem][bench][solver][s])+len(self.table['ub'][problem][bench][solver][s]))>0])
          
              for seed in self.seeds:
                bounds = [(c,t,o,'l') for o,c,t in self.table['lb'][problem][bench][solver][seed] if o is not None]
                bounds.extend([(c,t,o,'u') for o,c,t in self.table['ub'][problem][bench][solver][seed] if o is not None])
                obounds = sorted(bounds)
                gaps = []
            
                # print('\n'.join([str(g) for g in obounds]))
                # # print('\n')
                #
                lb,ub = self.bound['lb'][problem][bench][0],self.bound['ub'][problem][bench][1]
                for c,t,o,b in obounds:
                    if b == 'u' :
                        if o < ub :
                            ub = o
                    else:
                        if o > lb :
                            lb = o
                    gaps.append(((ub-lb),c,t))
                        
                ly = 0
                # print((l,u))
                # print('\n'.join([str(g) for g in gaps]))
                # print('\n')
                for o,c,t in gaps:
                  x = int(t*(10.0**precision))
                  y = float(u - o + 1)/float(u - l + 1)
                  curve[problem][solver][x] += (y - ly)/float(len(self.bench[problem]) * self.numruns[problem][solver][bench])
                  # print((y,x))
                  ly = y
        # else :
        #     print('no run on %s'%bench)

    for problem in self.problems:
      for solver in self.solvers:   
        curve[problem][solver] = integrate(curve[problem][solver],precision,step)
      
    return curve
    
    
  def get_average(self, bench, solver, b):
    vals = [self.table[b][problem][bench][solver][s][-1][0] for s in self.seeds if len(self.table[b][problem][bench][solver][s])>0]
    rvals = [v for v in vals if v is not None]
    total = sum(rvals)
    if len(rvals) > 0 :
        return float(total)/float(len(rvals))
    return None
    
  # def print_table(self, problem, precision, b):
  #
  #   self.read_datafiles()
  #
  #   rstr = '\\begin{table}\n{\\caption{%s (%s)}\\scriptsize\n\\begin{tabular}{c|%s}\n& %s\\\\\n\\hline\n'%(problem, caption[b], 'r'*len(self.solvers), ' & '.join(['\\rotatebox[origin=c]{90}{%s}'%s for s in self.solvers]))
  #   for bench in self.bench[problem]:
  #     avgs = [self.get_average(bench,solver,b) for solver in self.solvers]
  #     # print(avgs)
  #     if b == 'lb' :
  #         best = max([a if a is not None else (-sys.maxint - 1) for a in avgs])
  #     else :
  #         best = min([a if a is not None else sys.maxint for a in avgs])
  #     rstr += '\\texttt{%s} & %s \\\\\n'%(bench.replace('_','\_'), ' & '.join([pretty(avg,best,precision) for avg in avgs]))
  #
  #   return rstr+'\\end{tabular}\n}\n\\end{table}'
    
    
  def print_table(self, problem, precision, solvers=None):

    self.read_datafiles()
    
    if solvers == None :
        solvers = self.solvers

    rstr = '\\begin{table}\n{\\caption{%s}\\scriptsize\n\\tabcolsep=2pt\n\\begin{tabular}{c|%s}\n& %s\\\\\n\\hline\n'%(problem, 'r'*len(solvers), ' & '.join(['\\rotatebox[origin=c]{90}{%s}'%s for s in solvers]))
    for bench in self.bench[problem]:
        rstr += '\\texttt{%s} & '%(bench.replace('_','\_'))
        
        cells = []
        for solver in solvers :
            
            cell = ('u', 0)

            lbs = [self.table['lb'][problem][bench][solver][s][-1][0] for s in self.seeds if self.table['lb'][problem][bench][solver][s][-1][0] is not None]
            ubs = [self.table['ub'][problem][bench][solver][s][-1][0] for s in self.seeds if self.table['ub'][problem][bench][solver][s][-1][0] is not None]


            bestub = self.bound['ub'][problem][bench][0]
            bestlb = self.bound['lb'][problem][bench][1]

            if len(lbs) != 0 and len(ubs) != 0 :
                
                avgub = float(sum(ubs))/float(len(ubs))
                avglb = float(sum(lbs))/float(len(lbs))
                
                if len(lbs) == self.numruns[problem][solver][bench] and len(ubs) == self.numruns[problem][solver][bench] :
                    if avgub == avglb :
                        if avgub != bestub :
                            cell = ('w', avgub)
                        else :
                            tlbs = [self.table['lb'][problem][bench][solver][s][-1][2] for s in self.seeds if self.table['lb'][problem][bench][solver][s][-1][2] is not None]
                            cell = ('a', float(sum(tlbs))/float(len(tlbs)))
                    else :
                        cell = ('g', (avgub-avglb))
                else :
                    # cell = '(%i) %s'%(len(ubs),pretty_gap(avgub-avglb, precision))
                    cell = ('g', (avgub-avglb))
            cells.append(cell)
                    
        best = min(cells)
            
        rstr += ' & '.join([pretty(cell, best, precision) for cell in cells])
        
        
  def print_fancy_table(self, problem, precision, solvers=None):

    self.read_datafiles()
    
    if solvers == None :
        solvers = self.solvers

    rstr = '\\begin{table}\n{\\caption{%s}\\scriptsize\n\\tabcolsep=5pt\n\\begin{tabular}{c%s}\n& %s\\\\\n'%(problem, ''.join(['||rr' for s in solvers]), ' & '.join(['\multicolumn{2}{c}{%s}'%s for s in solvers]))
    rstr += '& %s \\\\\n\\hline'%(' & '.join(['objective & cputime' for s in solvers]))
    for bench in self.bench[problem]:
        row = '\\texttt{%s} & '%(bench.replace('_','\_'))
        
        cells_ub = []
        cells_lb = []
        cells_tub = []
        cells_tlb = []
        
        empty = True
        
        for solver in solvers :

            lbs = [self.table['lb'][problem][bench][solver][s][-1][0] for s in self.seeds if self.table['lb'][problem][bench][solver][s][-1][0] is not None]
            ubs = [self.table['ub'][problem][bench][solver][s][-1][0] for s in self.seeds if self.table['ub'][problem][bench][solver][s][-1][0] is not None]
            tlbs = [self.table['lb'][problem][bench][solver][s][-1][2] for s in self.seeds if self.table['lb'][problem][bench][solver][s][-1][2] is not None]
            tubs = [self.table['ub'][problem][bench][solver][s][-1][2] for s in self.seeds if self.table['ub'][problem][bench][solver][s][-1][2] is not None]
            
            
            # print(lbs)
            
            bestub = self.bound['ub'][problem][bench][0]
            bestlb = self.bound['lb'][problem][bench][1]

            if len(lbs) != 0 :
                
                avglb = float(sum(lbs))/float(len(lbs))
                avgtlb = float(sum(tlbs))/float(len(tlbs))
                
                cells_lb.append(avglb)
                cells_tlb.append(avgtlb)
                
                empty = False
                
            else :
                cells_lb.append(-1.0)
                cells_tlb.append(-1.0)
                
            if len(ubs) != 0 :
            
                avgub = float(sum(ubs))/float(len(ubs))
                avgtub = float(sum(tubs))/float(len(tubs))
                
                cells_ub.append(avgub)
                cells_tub.append(avgtub)
                
                empty = False
                
            else :
                cells_ub.append(-1.0)
                cells_tub.append(-1.0)
                
        
        if not empty :
            rstr += row
            best_lb = max(cells_lb)
            ubs = [u for l,u in zip(cells_lb,cells_ub) if l==best_lb and u!=-1.0]
            best_ub = -2
            best_tub = -2
            if len(ubs)>0:
                best_ub = min(ubs)
                best_tub = min([t for t,u in zip(cells_tub,cells_ub) if u==best_ub])
                
            best_tlb = min([t for t,l in zip(cells_tlb,cells_lb) if l==best_lb])
            
            
            rstr += ' & '.join([prettyprint(ub, lb, tub, tlb, best_ub, best_lb, best_tub, best_tlb) for ub, lb, tub, tlb in zip(cells_ub,cells_lb,cells_tub,cells_tlb)])
        
            rstr += '\\\\\n'

    return rstr+'\\end{tabular}\n}\n\\end{table}'



    

if __name__ == "__main__":
  
  if len(sys.argv) != 2:
    eprint('usage: ./parse.py <key>')
    exit(1)

  # ed = ExperimentData(sys.argv[1], ['bound', 'clqNpruNlearn2', 'mindegree', 'lastclq', 'maxlb', 'mindegree2vsids', 'lastclq2vsids', 'maxlb2vsids', 'vsids2mindegree', 'vsids2lastclq', 'vsids2maxlb', 'parasols'])
  # ed = ExperimentData(sys.argv[1], ['bound', 'clqNpruNlearn2', 'mindegree', 'lastclq', 'maxlb', 'mindegree2vsids', 'lastclq2vsids', 'maxlb2vsids', 'parasols'])
  # ed = ExperimentData(sys.argv[1], ['dsatur', 'dynamic', 'vsids2mindegree'], ['clq'])
  # ed = ExperimentData(sys.argv[1], ['lp00', 'lp01', 'lp02', 'lp03', 'lp10', 'lp11', 'lp12', 'lp13', 'lp20', 'lp21', 'lp22', 'lp23', 'lp30', 'lp31', 'lp32', 'lp33'], ['clq'])
  # ed = ExperimentData(sys.argv[1], ['repair', 'lp32'], ['clq', 'mis'])
  # ed = ExperimentData(sys.argv[1], ['dynprim', 'repprim', 'dynmaxsatprim', 'repmaxsatprim', 'dyndpprim', 'repdpprim', 'dynmsatdpprim', 'repmsatdpprim', 'dynmsatdpdefprim', 'repmsatdpdefprim', 'dynco', 'repco', 'dynmaxsatco', 'repmaxsatco', 'dyndpco', 'repdpco', 'dynmsatdpco', 'repmsatdpco', 'dynmsatdpdefco', 'repmsatdpdefco'], ['clq', 'mis'])
    
  # ed = ExperimentData(sys.argv[1], ['dynprim', 'dynmaxsatprim', 'dyncogr', 'dynmaxsatcogr', 'dynprdd', 'dynmaxsatprdd', 'dyncodd', 'dynmaxsatcodd', 'dynamic', 'parasols'])

  # ed = ExperimentData(sys.argv[1], ['dyn2prim', 'dyn2maxsatprim', 'dynprdd', 'dynmaxsatprdd', 'dyn2cogr', 'rep2prim', 'rep2maxsatprim', 'repprdd', 'repmaxsatprdd'])
  # ed = ExperimentData(sys.argv[1], ['dyn2prim', 'dynmaxsatprim', 'dynprdd', 'dynmaxsatprdd', 'dyn2cogr', 'rep2prim', 'repmaxsatprim', 'repprdd', 'repmaxsatprdd'])
  
  #MAXSAT / REPAIR
  # ed = ExperimentData(sys.argv[1], ['parasols', 'repprdd', 'repmaxsatprdd', 'repbussprdd', 'repmaxsatbussprdd'])
  ed = ExperimentData(sys.argv[1], ['rep2prdd', 'rep2norprdd', 'rep2maxsatprdd', 'parasols', 'IncMaxCLQ'])
  # ed = ExperimentData(sys.argv[1], ['parasols', 'IncMaxCLQ'])
  
  # ed.read_datafiles()
 #
 #  sys.exit(1)

  #DOMOVERDEG
  # ed = ExperimentData(sys.argv[1], ['dyn2prim', 'dynprdd', 'repmaxsatprim', 'repmaxsatprdd'])

  # ed.read_datafiles()
  #
  # print(ed.table['lb']['clq']['brock800_3']['dynprdd'][11648255])
  #
  # sys.exit(1)

  P = 3
  
  for b in ['ub', 'lb'] :
      cactus = ed.get_cactus(P,b,.02)

      if show_missing_runs :
          ed.find_buggy()

      for problem in ed.problems:
        print('compute curve (%s,%s)'%(problem,b))
        cactusfile = open('cactus_%s_%s.tex'%(problem, b), 'w')
        cactusfile.write(plot(cactus[problem], problem, b))
        cactusfile.close()

      # for problem in ed.problems:
      #   print('compute table (%s,%s)'%(problem,b))
      #   tablefile = open('table_%s_%s.tex'%(problem, b), 'w')
      #   tablefile.write(ed.print_table(problem, 1, b))
      #   tablefile.close()

  cactus = ed.get_gap_cactus(P,.02)

  for problem in ed.problems:
    print('compute curve (%s,gap)'%(problem))
    cactusfile = open('cactus_%s_gap.tex'%problem, 'w')
    cactusfile.write(plot(cactus[problem], problem, 'gap'))
    cactusfile.close()
    
  for problem in ed.problems:
    print('compute table (%s)'%(problem))
    tablefile = open('table_%s.tex'%(problem), 'w')
    tablefile.write(ed.print_fancy_table(problem, 1))
    tablefile.close()


  
  




