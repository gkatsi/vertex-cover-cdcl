#! /usr/bin/env python


# print ', '.join(["'lp%i%i'"%(i,j) for i in range(4) for j in range(4)])
#
# import sys
#
# sys.exit(1)


import subprocess

root = '../data'
folders = ['DIMACS_cliques', 'DIMACS_MIS', 'frbs'] #, 'snap']
ptypes = ['clq', 'mis', 'mis'] #, 'mis']

common = ['--print_par', '--print_sta', '--print_cmd']

exe = '../bin/vc'

option = {}

option['greedy'] = ['-b', 'greedy']
option['repair'] = ['-b', 'repair']
option['quickr'] = ['-b', 'quickr']
option['random'] = ['-b', 'random']
option['dsatur'] = ['-b', 'dsatur']

option['greedylearn'] = ['-b', 'greedy', '-l']
option['repairlearn'] = ['-b', 'repair', '-l']
option['quickrlearn'] = ['-b', 'quickr', '-l']
option['randomlearn'] = ['-b', 'random', '-l']
option['dsaturlearn'] = ['-b', 'dsatur', '-l']

option['greedylearnmin'] = ['-b', 'greedy', '-l', '-m', '2']
option['repairlearnmin'] = ['-b', 'repair', '-l', '-m', '2']
option['quickrlearnmin'] = ['-b', 'quickr', '-l', '-m', '2']
option['randomlearnmin'] = ['-b', 'random', '-l', '-m', '2']
option['dsaturlearnmin'] = ['-b', 'dsatur', '-l', '-m', '2']

option['greedylearnfmin'] = ['-b', 'greedy', '-l', '-m', '1']
option['repairlearnfmin'] = ['-b', 'repair', '-l', '-m', '1']
option['quickrlearnfmin'] = ['-b', 'quickr', '-l', '-m', '1']
option['randomlearnfmin'] = ['-b', 'random', '-l', '-m', '1']
option['dsaturlearnfmin'] = ['-b', 'dsatur', '-l', '-m', '1']


option['bound'] = ['-b', 'greedy']
option['pruning'] = ['-b', 'greedy', '-p']
option['clique'] = ['-b', 'greedy', '-d', '2']
option['clqNpru'] = ['-b', 'greedy', '-d', '2', '-p']
option['learning0'] = ['-b', 'greedy', '-l', '-m', '0']
option['pruNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-p']
option['learning1'] = ['-b', 'greedy', '-l', '-m', '1']
option['pruNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-p']
option['learning2'] = ['-b', 'greedy', '-l', '-m', '2']
option['pruNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-p']
option['learning3'] = ['-b', 'greedy', '-l', '-m', '3']
option['pruNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-p']
option['clqNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-d', '2']
option['clqNpruNlearn0'] = ['-b', 'greedy', '-l', '-m', '0', '-p', '-d', '2']
option['clqNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-d', '2']
option['clqNpruNlearn1'] = ['-b', 'greedy', '-l', '-m', '1', '-p', '-d', '2']
option['clqNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-d', '2']
option['clqNpruNlearn2'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2']
option['clqNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-d', '2']
option['clqNpruNlearn3'] = ['-b', 'greedy', '-l', '-m', '3', '-p', '-d', '2']

option['mindegree'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree']
option['lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq']
option['maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'maxlb']
option['mindegree2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'this2vsids']
option['lastclq2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['maxlb2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['vsids2mindegree'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['vsids2lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']
option['vsids2maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']

option['dynamic'] = ['-b', 'dynamic', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['quickr'] = ['-b', 'quickr', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['repair'] = ['-b', 'repair', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']

option['dsatur'] = ['-b', 'dsatur', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']


option['lp00'] = ['-b', 'dynamic', '-l', '0', '-p', '0', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp01'] = ['-b', 'dynamic', '-l', '0', '-p', '1', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp02'] = ['-b', 'dynamic', '-l', '0', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp03'] = ['-b', 'dynamic', '-l', '0', '-p', '3', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp10'] = ['-b', 'dynamic', '-l', '1', '-p', '0', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp11'] = ['-b', 'dynamic', '-l', '1', '-p', '1', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp12'] = ['-b', 'dynamic', '-l', '1', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp13'] = ['-b', 'dynamic', '-l', '1', '-p', '3', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp20'] = ['-b', 'dynamic', '-l', '2', '-p', '0', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp21'] = ['-b', 'dynamic', '-l', '2', '-p', '1', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp22'] = ['-b', 'dynamic', '-l', '2', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp23'] = ['-b', 'dynamic', '-l', '2', '-p', '3', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp30'] = ['-b', 'dynamic', '-l', '3', '-p', '0', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp31'] = ['-b', 'dynamic', '-l', '3', '-p', '1', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp32'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['lp33'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']


option['repair'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']


option['maxsat'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this', '--maxsat', '2']

option['deferred'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this', '--deferred']


option['dynprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2']
option['repprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2']
option['dynmaxsatprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['repmaxsatprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['dyndpprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['repdpprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['dynmsatdefprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['repmsatdefprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['dynmsatdpprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['repmsatdpprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['dynmsatdpdefprim'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['repmsatdpdefprim'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyncogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2']
option['repcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2']
option['dynmaxsatcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['repmaxsatcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['dyndpcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['repdpcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['dynmsatdefcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['repmsatdefcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['dynmsatdpcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['repmsatdpcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['dynmsatdpdefcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['repmsatdpdefcogr'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']


option['dynprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2']
option['repprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2']
option['dynmaxsatprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['repmaxsatprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['dyndpprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['repdpprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['dynmsatdefprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['repmsatdefprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['dynmsatdpprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['repmsatdpprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['dynmsatdpdefprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['repmsatdpdefprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyncodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2']
option['repcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2']
option['dynmaxsatcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['repmaxsatcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3']
option['dyndpcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['repdpcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog']
option['dynmsatdefcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['repmsatdefcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--deferred']
option['dynmsatdpcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['repmsatdpcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3']
option['dynmsatdpdefcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['repmsatdpdefcodd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyn2prdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2']
option['rep2prdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2']
option['dyn2maxsatprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['rep2maxsatprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['dyn2dpprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['rep2dpprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['dyn2msatdefprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['rep2msatdefprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['dyn2msatdpprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['rep2msatdpprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['dyn2msatdpdefprdd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['rep2msatdpdefprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyn2codd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2']
option['rep2codd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2']
option['dyn2maxsatcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['rep2maxsatcodd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['dyn2dpcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['rep2dpcodd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['dyn2msatdefcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['rep2msatdefcodd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['dyn2msatdpcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['rep2msatdpcodd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['dyn2msatdpdefcodd'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['rep2msatdpdefcodd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyn2prim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2']
option['rep2prim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2']
option['dyn2maxsatprim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['rep2maxsatprim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['dyn2dpprim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['rep2dpprim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['dyn2msatdefprim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['rep2msatdefprim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['dyn2msatdpprim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['rep2msatdpprim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['dyn2msatdpdefprim'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['rep2msatdpdefprim'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']

option['dyn2cogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2']
option['rep2cogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2']
option['dyn2maxsatcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['rep2maxsatcogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3']
option['dyn2dpcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['rep2dpcogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog']
option['dyn2msatdefcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['rep2msatdefcogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--maxsat', '3', '--deferred']
option['dyn2msatdpcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['rep2msatdpcogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3']
option['dyn2msatdpdefcogr'] = ['-b', 'dynamic', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']
option['rep2msatdpdefcogr'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--dynprog', '--maxsat', '3', '--deferred']


option['repbussprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--buss']
option['repmaxsatbussprdd'] = ['-b', 'repair', '-l', '3', '-p', '3', '-d', '2', '--maxsat', '3', '--buss']

option['rep2norprdd'] = ['-b', 'repair', '-l', '3', '-p', '2', '-d', '2', '--restart', 'no']


branch = {}
branch['clq'] = {}
branch['mis'] = {}


branch['clq']['cogr'] = '--complement --branch maxdegree --branchconfig vsids2this'
branch['clq']['prim'] = '-c --branch mindegree --branchconfig vsids2this'
branch['mis']['cogr'] = '--complement -c --branch mindegree --branchconfig vsids2this'
branch['mis']['prim'] = '--branch maxdegree --branchconfig vsids2this'


branch['clq']['codd'] = '--complement --branch mindomoverdeg --branchconfig vsids2this'
branch['clq']['prdd'] = '-c --branch mindomtimedeg --branchconfig vsids2this'
branch['mis']['codd'] = '--complement -c --branch mindomtimedeg --branchconfig vsids2this'
branch['mis']['prdd'] = '--branch mindomoverdeg --branchconfig vsids2this'




# slvs = ['dynprim', 'repprim', 'dynmaxsatprim', 'repmaxsatprim', 'dyndpprim', 'repdpprim', 'dynmsatdpprim', 'repmsatdpprim', 'dynmsatdpdefprim', 'repmsatdpdefprim', 'dynco', 'repco', 'dynmaxsatco', 'repmaxsatco', 'dyndpco', 'repdpco', 'dynmsatdpco', 'repmsatdpco', 'dynmsatdpdefco', 'repmsatdpdefco']
# slvs = ['dynprim', 'dynmaxsatprim', 'dyncogr', 'dynmaxsatcogr', 'dynprdd', 'dynmaxsatprdd', 'dyncodd', 'dynmaxsatcodd']

# slvs = ['dynmsatdpprim', 'dynmsatdpco']

# slvs = ['dynprim']


IncMaxCLQ = ['solvers/IncMaxCLQictai13Linux']
mwclq = ['solvers/mwc/mwclq']
parasols = ['solvers/parasols/build/esclarmonde.10g/solve_max_clique', 'ccod', 'dynex', '--print-incumbents']

class DataSet:

  def __init__(self):

    self.problems = [t for t in set(ptypes)]

    self.path = {}
    self.ptype = {}
    self.extension = {}
    
    self.bench = {}.fromkeys(self.problems)
    
    for typ in self.problems:
      self.bench[typ] = []
    
    
    for fold,typ in zip(folders,ptypes) :
      
      ls = subprocess.Popen(['ls', '-1', '%s/%s/'%(root,fold)], stdout=subprocess.PIPE)

      for line in ls.stdout:
        extidx = line.rfind('.')
        ext = line[extidx+1:-1]
        if ext != 'co' :
            b = line[:extidx]
            self.bench[typ].append(b)
            self.extension[b] = ext
            self.path[b] = '%s/%s/%s.%s'%(root,fold,b,ext)
        else : 
            print('bench=%s, ext=%s'%(b,ext))
        
      ls.wait()
      


  def print_command_lines(self, key, solvers, seeds=None, iproblems=None, ibenches=None):
    
    if seeds == None:
      seeds = [1234]
    
    problems = self.problems
    if iproblems != None:
      problems = iproblems
          
    job_id = 0
    outfiles = []
    
    for problem in problems:
      
      benches = self.bench[problem]
      if ibenches != None:
        benches = ibenches
      
      for seed in seeds:  
          for bench in benches:
        
              for solver in solvers:
                    
                    cmdline= None

                    outfile = 'results/%s-%s-%s-%s-%i.res'%(key, problem, bench, solver, seed)
                    errfile = outfile
                    errfile.replace('.res','.err')
                    
                    if solver == 'parasols' :
                        if seed == seeds[0] :
                            cmdline = [cmd for cmd in parasols]
                            if problem == 'mis' :
                                cmdline.append('--complement')
                            cmdline.append(self.path[bench])
                    elif solver == 'mwclq' :
                        if seed == seeds[0] :
                            cmdline = [cmd for cmd in mwclq]
                            if problem == 'mis' :
                                cmdline.append(self.path[bench]+'.co')
                            else :
                                cmdline.append(self.path[bench])
                    elif solver == 'IncMaxCLQ' :
                        if seed == seeds[0] :
                            cmdline = [cmd for cmd in IncMaxCLQ]
                            if problem == 'mis' :
                                cmdline.append(self.path[bench]+'.co')
                            else :
                                cmdline.append(self.path[bench])                        
                    else :    
                        cmdline = [exe, self.path[bench], '--seed', str(seed)]
                        cmdline.append(branch[problem][solver[-4:]])
                        #
                        # if problem == 'clq':
                        #     if solver.find('prim') >= 0 :
                        #         cmdline.append('-c --branch mindegree')
                        #     else :
                        #         cmdline.append('--complement --branch maxdegree')
                        # else :
                        #     if solver.find('prim') >= 0 :
                        #         cmdline.append('--branch maxdegree')
                        #     else :
                        #         cmdline.append('--complement -c --branch mindegree')
                        cmdline.extend(common)
                        cmdline.extend(option[solver])
    
                    strcmd = ' '.join(cmdline)
    
                    print '%s > %s 2> %s'%(strcmd, outfile, errfile)
                    
                    job_id += 1
                    job_file = open('jobs/job_%s'%job_id, 'w')
                    job_file.write(strcmd+'\n')
                    job_file.close()
                    
                    
                    outfiles.append(outfile)
                    
    mapfile = open('mapfile', 'w')
    mapfile.write('\n'.join(outfiles)+'\n')
    mapfile.close()
    
    slurmfile = open('rslurm', 'w')
    slurmfile.write('#!/bin/sh\n#SBATCH --ntasks=1 --time=01:00:00 --array=1-%i\nsrun -u sh jobs/job_${SLURM_ARRAY_TASK_ID}\n'%job_id)
    slurmfile.close()


option['lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq']
option['maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'maxlb']
option['mindegree2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'this2vsids']
option['lastclq2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['maxlb2vsids'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'this2vsids']
option['vsids2mindegree'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'mindegree', '--branchconfig', 'vsids2this']
option['vsids2lastclq'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']
option['vsids2maxlb'] = ['-b', 'greedy', '-l', '-m', '2', '-p', '-d', '2', '--branch', 'lastclq', '--branchconfig', 'vsids2this']


# slvs = ['greedy', 'dsatur', 'greedylearn', 'dsaturlearn', 'greedylearnmin', 'dsaturlearnmin', 'greedylearnfmin', 'dsaturlearnfmin']
#slvs = ['bound', 'pruning', 'clique', 'learning2', 'pruNlearn2', 'clqNpru', 'clqNlearn2', 'clqNpruNlearn2']

keyword = 'long'
# slvs = ['lastclq', 'maxlb', 'mindegree2vsids', 'lastclq2vsids', 'maxlb2vsids', 'vsids2mindegree', 'vsids2lastclq', 'vsids2maxlb']
# slvs = ['deferred']
# slvs = ['lp12', 'lp13', 'lp21', 'lp22', 'lp23', 'lp31', 'lp32']
sds = [11648255] #,21648256,31648257,41648258,51648259]
pbs = ['clq','mis']

slvs = ['rep2prdd', 'rep2norprdd', 'rep2maxsatprdd', 'parasols', 'IncMaxCLQ']
# slvs = ['parasols', 'mwclq']
# slvs = ['IncMaxCLQ']
# slvs = ['repbussprdd', 'repmaxsatbussprdd']


d = DataSet()

# d.print_command_lines('nohup', slvs, [11648255,21648256,31648257,41648258,51648259], ['mis'])
# d.print_command_lines('nohup', ['mindegree'], [11648255,21648256,31648257,41648258,51648259], ['clq','mis'])
# d.print_command_lines('nohup', ['parasols'], [11648255], ['clq','mis'])

d.print_command_lines(keyword, slvs, sds, pbs)

solvers = set([])
try :
    keyfile = open('results/%s.txt'%keyword, 'r')
    line = keyfile.readline()
    solvers = set(line[:-1].split())
except :
    print('new key file')

# print('%s + %s'%(' '.join(solvers), ' '.join(slvs)))

for solver in slvs :
    solvers.add(solver)
osolvers = sorted([s for s in solvers])

# print(' ==> %s'%(' '.join(osolvers)))

problems = set([])
try :
    line = keyfile.readline()
    oproblems = line[:-1].split()
    problems = set(oproblems)
except :
    pass
# pbs = set(ptypes)

bench = {}.fromkeys(pbs)
for problem in pbs:
    bench[problem] = set([])
    try :
        line = keyfile.readline()
        bench[problem] = set(line[:-1].split())
    except :
        pass
    for b in d.bench[problem]:
        bench[problem].add(b)
    

for problem in pbs:
    problems.add(problem)
oproblems = sorted([p for p in problems])

obench = {}.fromkeys(problems)
for problem in problems:
    obench[problem] = sorted([b for b in bench[problem]])
    
seeds = set([])
try:
    line = keyfile.readline()
    seeds = set([int(s) for s in line[:-1].split()])
except :
    pass

for seed in sds:
    seeds.add(seed)
oseeds = sorted([s for s in seeds])
    
oproblems = sorted([s for s in problems])

try:
    keyfile.close()
except :
    pass

keyfile = open('results/%s.txt'%keyword, 'w')
keyfile.write(' '.join(osolvers)+'\n')
keyfile.write(' '.join(oproblems)+'\n')
for problem in oproblems :
    keyfile.write(' '.join(obench[problem])+'\n')
keyfile.write(' '.join([str(s) for s in oseeds])+'\n')

keyfile.close()






