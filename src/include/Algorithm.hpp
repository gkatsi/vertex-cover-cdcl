#ifndef __ALGORITHM_HPP
#define __ALGORITHM_HPP

#include <vector>
#include <tuple>

//--------------------------------------------------
// erase elements from a vector/vec, subject to a Pred
template<typename T, typename A, typename Pred>
void erase_if(std::vector<T, A>& v, Pred p)
{
    v.erase( remove_if(begin(v), end(v), p), end(v) );
}

// extract a minimal set of elements that satisfy predicate 'p' from a
// vector 'maybe' and return that set. The predicate gets two vectors
// as arguments and tests their union.
//
// note: maybe is copied
template <typename T, typename Pred>
std::vector<T> minimize(std::vector<T> maybe, Pred p)
{
    std::vector<T> crit;
    while (!maybe.empty()) {
        T e{std::move(maybe.back())};
        maybe.pop_back();
        if (p(maybe, crit))
            crit.emplace_back(e);
    }
    return crit;
}

/* Same as std::copy_if, but the predicate returns two bools: the
   first tells whether to copy, the second whether we should stop
   asking and just copy everything else. */
template <typename InIt, typename OutIt, typename Pred>
OutIt copy_if_early_stop(InIt b, InIt e, OutIt o, Pred p)
{
    while (b != e) {
        bool copy, cont;
        std::tie(copy, cont) = p(*b);
        if (copy) {
            *o = *b;
            ++o;
        }
        ++b;
        if (!cont)
            return std::copy(b, e, o);
    }
    return o;
}

#endif
