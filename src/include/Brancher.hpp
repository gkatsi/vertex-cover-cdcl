#ifndef __BRANCHER_HPP
#define __BRANCHER_HPP

#include <Graph.hpp>
#include <Prop.hpp>
#include <minicsp/core/solver.hpp>

struct Brancher {
    minicsp::Solver& s;
    DegreeSorted<Graph>& rG;
    std::vector<minicsp::Lit> _x;
    cons_vc& constraint;

    // parameters
    BranchingConfig config{This};
    uint64_t cutoff{0};

    // stats
    int64_t numdecisions{0}, numchoices{0};

    Brancher(minicsp::Solver& ps, DegreeSorted<Graph>& prG,
        std::vector<minicsp::Lit> const& px, cons_vc& pvc)
        : s(ps)
        , rG(prG)
        , _x(px)
        , constraint(pvc)
    {
    }

    void use()
    {
        s.varbranch = minicsp::VAR_USER;
        s.user_brancher = [this](std::vector<minicsp::Lit>& cand) {
            if ((config == This2VSIDS && s.decisions > cutoff)
                || (config == VSIDS2This && s.decisions < cutoff))
                return;

            constraint.sync_graph(s);

            if (rG.node.size == 0)
                return;

            select_candidates(cand);
            ++numdecisions;
            numchoices += cand.size();
        };
    }

    void printStats()
    {
        std::cout << "Branching heuristic stats:\n\tDecisions: " << numdecisions
                  << "\n\tAverage # tied vertices: "
                  << numchoices / (double)(numdecisions) << "\n";
    }

    virtual void select_candidates(std::vector<minicsp::Lit>& cand) = 0;
};

struct MaxDBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        auto& maxdegree = rG.node_of_degree[rG.max_degree];
        cand.resize(maxdegree.size());
        std::transform(maxdegree.begin(), maxdegree.end(), cand.begin(),
            [&](int v) { return ~_x[v]; });
    }
};

struct MinDBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        auto& mindegree = rG.node_of_degree[rG.min_degree];
        cand.resize(mindegree.size());
        std::transform(mindegree.begin(), mindegree.end(), cand.begin(),
            [&](int v) { return ~_x[v]; });
    }
};

// struct MinDegreeMinDomainBrancher : public Brancher {
//     using Brancher::Brancher;
//
//     void select_candidates(std::vector<minicsp::Lit>& cand) override
//     {
//         auto& mindegree = rG.node_of_degree[rG.min_degree];
//         cand.resize(mindegree.size());
//
//                              auto best_clique = 0;
//                              for( auto v : mindegree ) {
//
//                              }
//
//         std::transform(mindegree.begin(), mindegree.end(), cand.begin(),
//             [&](int v) { return ~_x[v]; });
//     }
// };

struct RightmostCliqueBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            rG.min_clique_cover_bitset(
                &constraint.dual, close_type::WEAK, order_type::DYNAMIC_DEGREE);
        }

        auto& rclq = constraint.dual.partition[constraint.dual.sorted.back()];
        cand.resize(rclq.size());
        std::transform(begin(rclq), end(rclq), cand.begin(),
            [&](int v) { return ~_x[v]; });
    }
};

struct MinDomMinDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        auto& rclq = constraint.dual.partition[constraint.dual.sorted.back()];

        auto min_degree = rG.size();
        for (auto v : rclq) {
            auto d{rG.degree(v)};

            if (d < min_degree) {
                cand.clear();
                min_degree = d;
            }
            if (d == min_degree) {
                cand.push_back(~_x[v]);
            }
        }
    }
};


struct MinDomMaxDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        auto& rclq = constraint.dual.partition[constraint.dual.sorted.back()];

        auto max_degree = rG.size();
        for (auto v : rclq) {
            auto d{rG.degree(v)};

            if (d > max_degree) {
                cand.clear();
                max_degree = d;
            }
            if (d == max_degree) {
                cand.push_back(~_x[v]);
            }
        }
    }
};

// minimize |dom(v)| * deg(v)
struct MinDomTimeDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        auto best = (rG.size() * rG.size());
        for (auto v : rG.node) {
            auto d{rG.degree(v)
                * constraint.dual
                      .partition_sz[constraint.dual.get_partition(v)]};

            if (d < best) {
                cand.clear();
                best = d;
            }
            if (d == best) {
                cand.push_back(~_x[v]);
            }
        }
    }
};

// minimize |dom(v)| * (w(N(v)) + w(v))
struct MinDomTimeWeiBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        unsigned int best = -1;
        for (auto v : rG.node) {
            auto d{(rG.weight[v] + rG.neighbor_weight[v])
                // * constraint.dual
                //       .partition_sz[constraint.dual.get_partition(v)]};
              * constraint.dual
                    .partition_sz[constraint.dual.partition_of[v][0]]};

            if (d < best) {
                cand.clear();
                best = d;
            }
            if (d <= (int)((double)best*1.05)) {
                cand.push_back(~_x[v]);
            }
        }
    }
};

// minimize |dom(v)| / deg(v)
struct MinDomOverDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        auto best_dom = rG.size();
        auto best_deg = 0;
        for (auto v : rG.node) {
            auto deg{rG.degree(v)};
            auto dom{
                // constraint.dual.partition_sz[constraint.dual.get_partition(v)]};
                constraint.dual
                    .partition_sz[constraint.dual.partition_of[v][0]]};

            if (dom * best_deg < deg * best_dom) {
                cand.clear();
                best_dom = dom;
                best_deg = deg;
            }
            if (dom * best_deg == deg * best_dom) {
                cand.push_back(~_x[v]);
            }
        }
    }
};

// minimize |dom(v)| / (w(N(v)) + w(v))
struct MinDomOverWeiBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0) {
            // this only happens on restart
            constraint.compute_dual();
        }

        auto best_dom = rG.size();
        auto best_deg = 0;
        auto best_npa = rG.size();

        for (auto v : rG.node) {

            assert(rG.weight[v] > 0);
            assert(rG.neighbor_weight[v] >= 0);

            auto deg{rG.weight[v] + rG.neighbor_weight[v]};

            auto dom{0};
            for (auto i : constraint.dual.partition_of[v]) {
                dom += constraint.dual.partition_sz[i];
            }

            auto npa{constraint.dual.partition_of[v].size()};

            auto self = dom * best_deg * best_npa;
            auto best = deg * best_dom * npa;

            // std::cout << dom << "/" << npa << "/" << deg << " <? " <<
            // best_dom << "/" << best_npa << "/" << best_deg << "?";

            // int threshold = (int)((double)best * .975);
            if (self < best) {
                cand.clear();
                best_dom = dom;
                best_deg = deg;
                best_npa = npa;

                best = self;
                // std::cout << " yes!" << std::endl;
            }
            // else {
            //              std::cout << " no\n";
            //             }
            //
            int ok = (int)((double)best * 1.05);

            // std::cout << dom << "/" << npa << "/" << deg << " <=? " <<
            // best_dom << "/" << best_npa << "/" << best_deg << "*1.05?";

            if (self <= ok) {
                cand.push_back(~_x[v]);

                // std::cout << " yes!" << std::endl;
            }
            // else {
            //              std::cout << " no\n";
            //             }
        }
        // std::cout << std::endl << cand.size() << std::endl << std::endl;
        // exit(1);
    }
};

struct MaxLBBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0 || !constraint.have_vertex_lbs()) {
            // this only happens on restart
            rG.min_clique_cover_bitset(
                &constraint.dual, close_type::WEAK, order_type::DYNAMIC_DEGREE);
            auto& rclq
                = constraint.dual.partition[constraint.dual.sorted.back()];
            cand.resize(rclq.size());
            std::transform(begin(rclq), end(rclq), cand.begin(),
                [&](int v) { return ~_x[v]; });
            return;
        }

        using std::begin;
        using std::end;
        auto maxv = *std::max_element(
            begin(rG.node), end(rG.node), [&](int u, int v) {
                return constraint.vertex_lb(u) < constraint.vertex_lb(v);
            });
        auto maxlb = constraint.vertex_lb(maxv);
        for (auto v : rG.node)
            if (constraint.vertex_lb(v) == maxlb)
                cand.push_back(~_x[v]);
    }
};

struct MaxLBOverDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0 || !constraint.have_vertex_lbs()) {
            // this only happens on restart
            rG.min_clique_cover_bitset(
                &constraint.dual, close_type::WEAK, order_type::DYNAMIC_DEGREE);
            auto& rclq
                = constraint.dual.partition[constraint.dual.sorted.back()];
            cand.resize(rclq.size());
            std::transform(begin(rclq), end(rclq), cand.begin(),
                [&](int v) { return ~_x[v]; });
            return;
        }

        using std::begin;
        using std::end;
        auto maxv = *std::max_element(
            begin(rG.node), end(rG.node), [&](int u, int v) {
                return constraint.vertex_lb(u)*rG.degree(v) < constraint.vertex_lb(v)*rG.degree(u);
            });
        auto maxlb = constraint.vertex_lb(maxv);
        auto mindeg = rG.degree(maxv);
        for (auto v : rG.node)
            if (constraint.vertex_lb(v)*mindeg == maxlb*rG.degree(v))
                cand.push_back(~_x[v]);
    }
};

struct MaxLBTimeDegBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (constraint.dual.dual_cost == 0 || !constraint.have_vertex_lbs()) {
            // this only happens on restart
            rG.min_clique_cover_bitset(
                &constraint.dual, close_type::WEAK, order_type::DYNAMIC_DEGREE);
            auto& rclq
                = constraint.dual.partition[constraint.dual.sorted.back()];
            cand.resize(rclq.size());
            std::transform(begin(rclq), end(rclq), cand.begin(),
                [&](int v) { return ~_x[v]; });
            return;
        }

        using std::begin;
        using std::end;
        auto maxv = *std::max_element(
            begin(rG.node), end(rG.node), [&](int u, int v) {
                return constraint.vertex_lb(u)*rG.degree(u) < constraint.vertex_lb(v)*rG.degree(v);
            });
        auto maxlb = constraint.vertex_lb(maxv);
        auto mindeg = rG.degree(maxv);
        for (auto v : rG.node)
            if (constraint.vertex_lb(v)*mindeg == maxlb*rG.degree(v))
                cand.push_back(~_x[v]);
    }
};


struct LexBrancher : public Brancher
{
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        using std::begin;
        using std::end;
        auto v = *std::min_element(begin(rG.node), end(rG.node));
        cand.clear();
        cand.push_back(_x[v]);
    }
};

struct VectorLexBrancher : public Brancher {
    using Brancher::Brancher;

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        using std::begin;
        using std::end;
        auto lit = *std::find_if(begin(_x), end(_x),
            [&](auto l) { return s.value(l) == minicsp::l_Undef; });
        cand.clear();
        cand.push_back(lit);
    }
};

template <typename F, typename Tolerance, typename Compare> struct ScoreBrancher : public Brancher {
    F f;
    Tolerance close_enough;
    Compare comp;

    ScoreBrancher(minicsp::Solver& ps, DegreeSorted<Graph>& prG,
        std::vector<minicsp::Lit> const& px, cons_vc& pvc, F f,
        Tolerance close_enough, Compare comp = Compare())
        : Brancher(ps, prG, px, pvc)
        , f(f)
        , close_enough(close_enough)
        , comp(comp)
    {
    }

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        using std::begin;
        using std::end;
        auto i = std::min_element(begin(rG.node), end(rG.node),
            [&](auto v, auto u) { return comp(f(v), f(u)); });
        auto nw = f(*i);
        for (auto v : rG.node)
            if (close_enough(f(v), nw)) {
                // std::cout << v << " (" << f(v) << ") ";
                cand.push_back(~_x[v]);
            }
        // std::cout << " " << cand.size() << std::endl;
    }
};

struct ScoreNeighborWeight {
    DegreeSorted<Graph>& rG;
    ScoreNeighborWeight(DegreeSorted<Graph>& rG)
        : rG(rG)
    {
    }
    int operator()(int v) { return rG.neighbor_weight[v]; }
};

struct ScoreDomTimeWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomTimeWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.neighbor_weight[v];

        return (double)(dom * (wei + nwe));
    }
};

struct ScoreDomOverWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomOverWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.neighbor_weight[v];

        return double(dom) / (double)(wei + nwe);
    }
};

struct ScoreDomTimeProdWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomTimeProdWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.neighbor_weight[v];

        return (double)(dom * wei * nwe);
    }
};

struct ScoreDomOverProdWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomOverProdWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.neighbor_weight[v];

        return double(dom) / (double)(wei * nwe);
    }
};

struct ScoreDomTimeAntiWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomTimeAntiWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.total_weight - rG.neighbor_weight[v];

        return (double)(dom * (wei + nwe));
    }
};

struct ScoreDomOverAntiWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomOverAntiWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.total_weight - rG.neighbor_weight[v];

        return double(dom) / (double)(wei + nwe);
    }
};

struct ScoreDomTimeProdAntiWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomTimeProdAntiWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.total_weight - rG.neighbor_weight[v];

        return (double)(dom * wei * nwe);
    }
};

struct ScoreDomOverProdAntiWei {
    DegreeSorted<Graph>& rG;
    cons_vc& constraint;
    ScoreDomOverProdAntiWei(DegreeSorted<Graph>& rG, cons_vc& c)
        : rG(rG), constraint(c)
    {
    }
    double operator()(int v)
    {
        auto dom
            = constraint.dual.partition_sz[constraint.dual.partition_of[v][0]];
        auto wei = rG.weight[v];
        auto nwe = rG.total_weight - rG.neighbor_weight[v];

        return double(dom) / (double)(wei * nwe);
    }
};

struct Within {
    double tolerance;

    Within(double t)
        : tolerance(t)
    {
    }
    int operator()(double f, double b) { return f <= tolerance*b; }
};

struct SerialBrancher : public Brancher {
    std::vector<std::unique_ptr<Brancher>> branchers;
    std::vector<int> schedule;
    int cutoff;

    SerialBrancher(minicsp::Solver& ps, DegreeSorted<Graph>& prG,
        std::vector<minicsp::Lit> const& px, cons_vc& pvc,
        std::vector<std::unique_ptr<Brancher>> br, std::vector<int> sch)
        : Brancher(ps, prG, px, pvc)
        , branchers(std::move(br))
        , schedule(std::move(sch))
    {
        // reverse so we can pop_back() whatever we do not use
        if (branchers.size() != schedule.size() + 1)
            throw std::runtime_error(
                "Schedule size does not match number of branchers");
        std::reverse(begin(branchers), end(branchers));
        std::reverse(begin(schedule), end(schedule));
        if (!schedule.empty())
            cutoff = schedule.back();
        else
            cutoff = -1;
    }

    void select_candidates(std::vector<minicsp::Lit>& cand) override
    {
        if (cutoff > 0 && s.decisions > cutoff) {
            schedule.pop_back();
            branchers.pop_back();
            if (!schedule.empty())
                cutoff = s.decisions + schedule.back();
            else
                cutoff = -1;
        }
        auto* br = branchers.back().get();
        if (br)
            br->select_candidates(cand);
    }
};

#endif
