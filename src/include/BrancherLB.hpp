#ifndef __BRANCHERLB
#define __BRANCHERLB

#include <Graph.hpp>
#include <Prop.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>

struct Brancher {
    minicsp::Solver& s;
    std::vector<minicsp::Lit>& eq;
	int n;
	Graph& G;

    // parameters
    BranchingConfig config{This};
    uint64_t cutoff{0};

    // stats
    int64_t numdecisions{0}, numchoices{0};

    Brancher(minicsp::Solver& s, std::vector<minicsp::Lit>& eq, int n, Graph& G)
        : s(s) , eq(eq), n(n), G(G) {}

    void use()
    {
        s.varbranch = minicsp::VAR_USER;
        s.user_brancher = [this](std::vector<minicsp::Lit>& cand) {
			select_candidates(cand);
		};
    }

    void select_candidates(std::vector<minicsp::Lit>& cand) {
		cand.clear();
		cand.reserve(n*n);
        	for (int i = 0; i != n; i++) {
				for (int j = i+1; j != n; j++) {
					if (!G.exists_edge(i,j)) {
						if (s.value(eq[j*n+i]) == minicsp::l_Undef)
							cand.push_back(eq[j*n+i]);
					}
				}
			}
    }
};
#endif
