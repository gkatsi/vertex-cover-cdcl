#ifndef __CLIK
#define __CLIK

#include <Graph.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/core/cons.hpp>
#include <minicsp/mtl/Heap.h>
#include <minicsp/mtl/Vec.h>

#include <vector>

class cons_coloring : minicsp::cons
{
private:
	Graph& G;
	minicsp::Solver& s;
	std::vector<minicsp::cspvar>& vars;
	int n;
	
	std::vector<int> clik();
	// Sommets par degrés décroissants
	std::vector<int> nodes;
	// rank[i] donne le rang en degrès du noeux i (entre 1 et nv)
	std::vector<int> rank;
	// degrés des noeux
	std::vector<int> degs;
	
	void echanger(int i, int j);
	void quickSort(int deb, int pivot, int fin);
	void countingSort();
	
	void rankInit();
	
	void cliks(int k);

	//variables used by BK
	bool first;
	int nbF;
	int nb;

	void BK(std::vector<int>& R, std::vector<int> P, std::vector<int> X);
	void traiterClique(std::vector<int>& R);
	void BKCliks();
public:
	cons_coloring(Graph& G, minicsp::Solver& s, std::vector<minicsp::cspvar>& vars, int n);
	// clique
	std::vector<int> clikk;
	// taille de la meilleure clique
	int lb{0};
	void initialise(int k);
};

#endif
