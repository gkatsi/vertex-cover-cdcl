#ifndef __CLI
#define __CLI

#include <Graph.hpp>
#include <Bitset.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/mtl/Heap.h>
#include <minicsp/mtl/Vec.h>

#include <vector>

using namespace ::minicsp;

class cons_eqvar : public minicsp::cons
{
private:
	Solver& s;
	std::vector<cspvar>& vars;
	std::vector<Lit>& eq;
	Graph& G;
	int n;
	int& ub;

	//Backtracking
	std::vector<std::pair<int,int>> v;
	btptr indptr;
	void sync_graph(bool ajout, int ind_eq);
	void backtrack(int bk);
	
	// vertices (ranked by degree once done)
	std::vector<int> nodes;
	// degrees of vertices
	std::vector<int> degs;
	// clique found
	std::vector<int> clique;
	BitSet dispo;
	//reason
	vec<Lit> r;

	void calcul_clique();
	void counting_sort();
	void rank_init();
	void clik();
	void explain();
public:
	cons_eqvar(Graph& G, Solver& s, std::vector<cspvar>& vars, std::vector<Lit>& eq, int n, int& ub);
	Clause* wake_advised(Solver& s, Lit l, void* hint) override;
	Clause* propagate(Solver& s) override;
};

#endif
