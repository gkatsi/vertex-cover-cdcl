

#ifndef __CMDLINE_HPP
#define __CMDLINE_HPP

#include <tclap/CmdLine.h>
#include <map>

enum BranchingHeuristic {
    VSIDS,
    MINDEGREE,
    MAXDEGREE,
    MINDOMMINDEG,
    MINDOMMAXDEG,
    MINDOMTIMEDEG,
    MINDOMOVERDEG,
    MINDOMTIMEWEICLQ,
    MINDOMOVERWEICLQ,
    MINDOMTIMEWEIIS,
    MINDOMOVERWEIIS,
    MINDOMTIMEPWEICLQ,
    MINDOMOVERPWEICLQ,
    MINDOMTIMEPWEIIS,
    MINDOMOVERPWEIIS,
    LASTCLQ,
    MAXLB,
    MAXLBOVERDEG,
    MAXLBTIMEDEG,
    LEX,
    VECLEX,
    MINNW,
    MAXNW,
    SERIAL
};
extern std::map<std::string, BranchingHeuristic> brancher_names;
enum BranchingConfig { This, This2VSIDS, VSIDS2This };

class Cmdline : public TCLAP::CmdLine
{

private:
    TCLAP::UnlabeledValueArg<std::string>* fileArg;
    TCLAP::ValueArg<int>* seedArg;
    TCLAP::ValueArg<double>* timeArg;
    TCLAP::ValueArg<std::string>* boundArg;
    TCLAP::ValueArg<int>* verbosityArg;
    std::unique_ptr<TCLAP::ValueArg<int>> cliquesArg;
        TCLAP::ValueArg<std::string>* restartArg;
    std::unique_ptr<TCLAP::ValueArg<int>> learningArg;
    std::unique_ptr<TCLAP::ValueArg<int>> pruneArg;
    TCLAP::ValueArg<int>* dominanceArg;
    TCLAP::SwitchArg* cliqueArg;
    TCLAP::SwitchArg* printsolArg;
    TCLAP::SwitchArg* printparArg;
    TCLAP::SwitchArg* printmodArg;
    TCLAP::SwitchArg* printinsArg;
    TCLAP::SwitchArg* printstaArg;
    TCLAP::SwitchArg* printcmdArg;
    TCLAP::SwitchArg* checkArg;
    TCLAP::SwitchArg* weightArg;

    TCLAP::ValueArg<int>* dsaturThresholdArg;

    std::unique_ptr<TCLAP::ValueArg<int>> numnodesArg;
    std::unique_ptr<TCLAP::ValueArg<int>> numedgesArg;

    TCLAP::ValuesConstraint<std::string>* r_allowed;
    TCLAP::ValuesConstraint<std::string>* b_allowed;
    TCLAP::ValuesConstraint<int>* i_allowed;
    std::unique_ptr<TCLAP::ValuesConstraint<int>> d_allowed;

    std::unique_ptr<TCLAP::ValuesConstraint<std::string>> branch_allowed;
    std::unique_ptr<TCLAP::ValueArg<std::string>> branchArg;
    std::unique_ptr<TCLAP::ValuesConstraint<std::string>> branch_config_allowed;
    std::unique_ptr<TCLAP::ValueArg<std::string>> branchConfigArg;
    std::unique_ptr<TCLAP::ValueArg<int>> branchCutoffArg;
    std::unique_ptr<TCLAP::ValueArg<std::string>> branch_sequenceArg;
    std::unique_ptr<TCLAP::ValueArg<std::string>> branch_scheduleArg;

    std::unique_ptr<TCLAP::SwitchArg> dumpsolArg;
    std::unique_ptr<TCLAP::UnlabeledValueArg<std::string>> debugsolArg;

    std::unique_ptr<TCLAP::SwitchArg> traceArg;

    std::unique_ptr<TCLAP::ValueArg<std::string>> printdimacsArg;
    std::unique_ptr<TCLAP::ValueArg<std::string>> printmtsArg;
    std::unique_ptr<TCLAP::ValueArg<int>> lbArg;

    std::unique_ptr<TCLAP::ValueArg<int>> maxsatArg;
    std::unique_ptr<TCLAP::ValueArg<float>> expArg;

    std::unique_ptr<TCLAP::ValueArg<std::string>> sortedArg;

    std::unique_ptr<TCLAP::SwitchArg> deferredArg;

        std::unique_ptr<TCLAP::SwitchArg> triangleArg;

    std::unique_ptr<TCLAP::SwitchArg> compcachingArg;

    std::unique_ptr<TCLAP::SwitchArg> bussArg;

    std::unique_ptr<TCLAP::SwitchArg> dynprogArg;

    std::unique_ptr<TCLAP::SwitchArg> complementArg;

    std::unique_ptr<TCLAP::ValueArg<float>> heuristic_toleranceArg;

    std::unique_ptr<TCLAP::ValueArg<int>> greedy_orderArg;

    std::unique_ptr<TCLAP::SwitchArg> pruneToISArg;
public:
    Cmdline(const std::string& message, const char delimiter = ' ',
        const std::string& version = "none", bool helpAndVersion = true);

    void initialise();

    ~Cmdline();

    std::string get_filename();
    int get_seed();
    int get_bound();
    std::string get_boundalgo();
    double get_time();
    int get_verbosity();
    int get_cliques();
        std::string get_restart();
    int get_learning();
    int get_pruning();
    int get_dominance();
    bool get_clique();
    bool print_sol();
    bool print_par();
    bool print_mod();
    bool print_ins();
    bool print_sta();
    bool print_cmd();
    int get_threshold();
    bool checked();
    bool weighted();

    BranchingHeuristic get_branch();
    std::string get_branch_name();
    BranchingConfig get_branch_config();
    int get_branch_switch_cutoff();

    std::string get_branch_sequence();
    std::string get_branch_schedule();

    int get_greedy_order();

    int get_numnodes();
    int get_numedges();

    bool get_dumpsol();
    std::string get_debugsol();

    bool get_trace();

    std::string print_dimacs();

    std::string print_mts();

    int get_lb();

    int get_maxsat();

    float get_exp();

    std::string get_sorted();

    bool get_deferred();

        bool get_triangle();

    bool get_compcaching();

    bool get_buss();

    bool get_dynprog();

    bool get_complement();

    float heuristic_tolerance();

    bool get_prune_to_is();
};

#endif // __CMDLINE_HPP
