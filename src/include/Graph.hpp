

#include <assert.h>
#include <iomanip>
#include <vector>

#include <Bitset.hpp>
#include <Intstack.hpp>
#include <Random.hpp>
#include <Vector.hpp>

#include <Algorithm.hpp>

#include <minicsp/mtl/Heap.h>

#ifndef __GRAPH_HPP
#define __GRAPH_HPP

#define _NOT_SO_COMPACT
// #define _DEBUG_BOUNDS_ false
// #define _VERIFY_BOUNDS_ 2
// #define _DEBUG_UNWATCH true
// #define _DEBUG_WATCH true
// #define _DEBUG_DSATUR_ true

using weight_type = int64_t;


int distance(const int x, const int y);
int pos_bias(const int x, const int y);

template <typename T>
std::ostream& vecdisplay(const std::vector<T> vec, std::ostream& os)
{

    os << "[";
    for (typename std::vector<T>::const_iterator it = vec.begin();
         it != vec.end(); ++it) {
        os << " " << *it;
    }
    os << " ]";

    return os;
}

class Graph;
class DualProblem
{
public:
    // the graph
    Graph* G;
    // number of virtual/real cliques in the current clique cover(s)
    weight_type dual_cost{0};
    // cumulative size of each "layer" of clique covers
    std::vector<int> layer_csize;
    // // weight of each "layer" of clique covers
    // std::vector<int> layer_weight;
    // whether the clique cover is sorted
    std::vector<int> sorted;
    // the current clique cover
    std::vector<BitSet> partition;
    // also, maintains its size because each call to size() is costly
    std::vector<int> partition_sz;
    // the sets of vertices that could possibly be added to the cliques
    std::vector<BitSet> candidate;
    // also, maintains its size because each call to size() is costly
    std::vector<int> candidate_sz;
    // for each partition, its contribution to the dual cost
    std::vector<weight_type> partition_weight;
    // for each node, the partitions it belongs to and how much does it
    // contribute to it
    std::vector<std::vector<int>> partition_of;
    std::vector<std::vector<weight_type>> weight_contribution;
    // helper to compute the residual weights / partition_of efficiently
    std::vector<std::vector<int>> partition_list;
    std::vector<int> singleton_partitions;
    // std::vector<int> partition_of;
    // bool is_sorted;

    DualProblem(Graph* g);
    virtual ~DualProblem();
    // void initialise(Graph* g);

    void resize(const int n);

    // insert x in the first available clique and return its index
    int insert(const int x);
    // insert x in the first available clique, return its index and the removed
    // candidate
    int insert(const int x, BitSet& delta);
    // compute all candidate size
    void close(std::vector<int>&, std::vector<weight_type>&);
    // compute all candidate sizes, but consume weight less eagerly
    void close_weak(std::vector<int>&, std::vector<weight_type>&);
    // add a new clique with unique element x
    int push(const int x, const int nx);
    // add a new clique G
    void push(const Graph& G);

    void repair(BitSet& util_set);
    // try to remove the ith clique by moving its vertices to subsequent
    // cliques -- explore the cliques in the order given by "sorted"
    void remove_partition(std::vector<int>::iterator cl, BitSet& util_set);
    // update the clique cover in response to "removed_nodes", adds the cliques
    // that have changes in "changed_partitions"
    void update(BitSet& removed_nodes, BitSet& util_set);
    void clear();
    void to_size(const int k);
    // void clear(const int actual_size);
    // set the order of the clique indices in "sorted" by non-increasing
    // candidate-size (ties broken by non-decreasing partition-size)
    void sort();
    // move nodes to lower cliques greedily
    void percolate(BitSet& delta);
    void check_conflict_set(std::vector<int>& cset, std::vector<BitSet>& part,
        bool cons, bool verbose);

    int get_partition(const int v) const;
    int num_partition() const;
    void new_layer();
    // void new_clique();
		
    void check(const char* msg, const bool covering = false);

private:
    int _insert(const int x);
};

class MaxsatAlgo
{

public:
    MaxsatAlgo(Graph& G);
    virtual ~MaxsatAlgo();

    /* Stores the conflicting set(s). There are |conflict| conflicting sets.
     * Conflict 0 goes from clique.end()-1 to conflict[0]+1, for i>0 conflict i
     * goes from conflict[i-1] to conflict[i]+1. Cliques that are not covered
     * are not part of conflicting set */
    std::vector<int> clique;
    std::vector<std::vector<int>::iterator> conflict;

    // f[i][j] = vertices of clique j that would be forbidden if clique i is
    // chosen (i.e., adjacent to every node in cique i)
    std::vector<std::vector<std::vector<int>>> forbidden;

    // nn[v][i] = number of non-neighbors of v in clique i
    std::vector<std::vector<int>> num_nneighbors;

    // co[v] = i such that v is in clique i
    std::vector<int> partition_of;
    // std::vector<std::vector<Pair>> partition_of;

    // s[v][i] = vertices of clique i that are not neighbors with v
    std::vector<std::vector<std::vector<int>>> support;

    // stack for GAC-like processing
    std::vector<int> inconsistent;
    // propagation stack starts at first_inc, from 0 to first_inc are all
    // pruned value for the current conflict set
    int first_inc{0};

    std::vector<BitSet> opartition;
    std::vector<BitSet> cpartition;
    std::vector<int> clique_size;

    // the overall set of inconsistent vertices for the current conflict
    BitSet inconsistent_set;

    // helper used to compute the support sets
    BitSet support_set;

    void compute_conflict_set(DualProblem& cc, const bool full_ac);
    void copy(DualProblem& cc);
};

class Graph
{

public:
    /** nodes */
    // the maximum capacity of the graph (number of nodes)
    size_t capacity;
    // the list of nodes of the graph, in no particular order
    IntStack node;
    // the nodes in a bitset, to help bitwise operations
    BitSet node_set;
    // node weight
    std::vector<weight_type> weight;
    // total weight of the vertices
    weight_type total_weight;

    /** edges */
    // the current number of edges (the list of edges is not updated)
    int num_edges;
    // the (original) edges of the graph
    std::vector<Pair> edges;
    // ranks stores the indices of the nodes in eachother's neighbor list so
    // that we can remove the edge efficiently
    std::vector<Pair> ranks;

    /** neighborhood */
    // the list of neighbors of each node
    std::vector<int>* neighbor;
    // the indices of the incident edges
    std::vector<int>* nb_index;

    // how matrix[v] should be read (true: neighbor, false: non-neighbors)
    bool spin{true};

    // total weight of the neighbors
    std::vector<weight_type> neighbor_weight;

#ifdef _NOT_SO_COMPACT
    BitSet* matrix;
    bool exists_edge(const int u, const int v) const;
#endif

    Graph() {}
    Graph(const int n, const bool full_nodes = true, const bool sp = true);
    Graph(const Graph& g);

    virtual void initialise(
        const int n, const bool full_nodes = true, const bool sp = true);
    virtual void initialise_random(const int m);
    virtual void initialise(const Graph& g);

    virtual ~Graph();

    // helpers
    int size() const;
    bool null() const;
    bool empty() const;
    bool full() const;
    bool has_node(int x) const;
    int degree(const int x) const { return neighbor[x].size(); }

    void flip_neighbors(const int);
    // complement graph
    void flip();
    // remove every edge and node
    void clear();

    // rename the vertices so that they are sorted by non-increasing degree
    void sort(bool non_decreasing=true);
    void hashsort(bool non_decreasing = true);

    // weight helpers
    void set_weight(const int x, const weight_type v);
    void set_random_uniform_weights(const int l, const int u);
    void set_modulo_weights(const int m);

    // node addition/removal
    void declare_node(const int x);
    void add_node(const int x);

    void rem_node(const int x);

    // edge addition/removal
    int add_undirected(const int x, const int y);
    int add_undirected_nm(const int x,
        const int y); // used only when flipping, does not change the matrix

    void rem_undirected(const int x, const int y, const int i);
    void rem_undirected(const int i);

    //
    void maximal_matching(
        std::vector<int>& matching, int& nmatch, std::vector<int>& indexlist);

    bool check_solution(std::vector<int>& is, const bool mis = true) const;
    // bool check_clique(std::vector<int>& cl) const;
    // bool check_vc(std::vector<int>& vc) const;

    // printing
    std::ostream& display(std::ostream& os) const;
    void print_dimacs(std::ostream& os) const;
    void print_mts(std::ostream& os) const;

    // verify that the coloration is correct
    bool verify_coloration(const int* coloration);
    bool verify_clique_cover(const int* cover, const int nc);
    // verify that the data structure is consistent
    void verify(const char* msg);
    double get_density() const;
		
		bool is_weighted() { return total_weight != node.size; }
};

std::ostream& operator<<(std::ostream& os, const Graph& x);

std::ostream& operator<<(std::ostream& os, const Graph* x);

enum close_type { STRONG, WEAK };
enum order_type { DYNAMIC_DEGREE, STATIC_DSATUR };

template <class GraphType> class DegreeSorted : public GraphType
{

public:
    int min_degree;
    int max_degree;

    std::vector<int>* co_neighbor;

    std::vector<int>* node_of_degree;
    int* degree_index;

    int* original_size;

    weight_type* maxweight;

    int* saturation;

    IntStack util_stack;
    std::vector<int> util_vec;
    BitSet util_set;
    BitSet _visited;

    std::vector<int> static_order;

    std::vector<weight_type> tmp_weight;

    struct SaturationDegreeGt {
        const DegreeSorted<GraphType>* graph;
        bool spin;

        bool operator()(const int x, const int y) const
        {
            int sx = graph->saturation[x];
            int sy = graph->saturation[y];
            return sx > sy
                || (sx == sy && (graph->degree(x) > graph->degree(y)));
        }
        SaturationDegreeGt(const DegreeSorted<GraphType>* g, const bool lt = 0)
            : graph(g)
            , spin(lt)
        {
        }
    };

    struct SaturationDegreeLt {
        const DegreeSorted<GraphType>* graph;
        bool spin;

        bool operator()(const int x, const int y) const
        {
            int sx = graph->saturation[x];
            int sy = graph->saturation[y];

            return sx > sy
                || (sx == sy && (graph->degree(x) < graph->degree(y)));
        }
        SaturationDegreeLt(const DegreeSorted<GraphType>* g, const bool lt = 0)
            : graph(g)
            , spin(lt)
        {
        }
    };

    Heap<SaturationDegreeGt> node_heap_col;
    Heap<SaturationDegreeLt> node_heap_cc;

    DegreeSorted();

    DegreeSorted(const int n);

    DegreeSorted(const GraphType& g);

    DegreeSorted(const DegreeSorted& g);

    virtual ~DegreeSorted();

    void initialise_memory();

    void initialise_degree();

    int real_size();

    void min_clique_cover_bitset_helper(
        DualProblem* DP, std::vector<int>& vertices, close_type ct);

    void vertices_by_degree_increasing(std::vector<int>& vs);
    void vertices_by_degree_decreasing(std::vector<int>& vs);
    void vertices_static(std::vector<int>& vs);

    void min_clique_cover_bitset(DualProblem* CC, close_type ct, order_type ot);
    void min_coloration_bitset(DualProblem* COL, close_type ct, order_type ot);

    void naive_helper(DualProblem* D, std::vector<int>& vertices);
    void min_clique_cover_naive(DualProblem* CC);
    void min_coloration_naive(DualProblem* COL);

    // tries to improve the current clique cover by reallocating the nodes of a
    // clique into later cliques
    // void repair(DualProblem* CC);
    // tries to improve the current clique cover by reallocating the nodes of
    // clique i into later cliques
    bool remove_partition(DualProblem* d, const int i);

    // template <typename ScoreFunc>
    // void min_weight_clique_cover_bitset(DualProblem* CC, ScoreFunc scoring);

    void dsatur_clc(DualProblem* CC, close_type ct);
    void dsatur_col(DualProblem* COL, close_type ct);
    template <typename Heap>
    void dsatur(DualProblem* C, Heap& heap, close_type ct);

    // return true iff the neighborhood of x is a clique, w1 and w2 are set to
    // two arbitrary non-adjacent neighbors otherwise
    bool neighboring_clique(const int x, int& w1, int& w2);
    bool non_neighboring_is(const int x, int& w1, int& w2);

    inline int codegree(const int x);

    void verify(const char* msg);

    inline int min_degree_vertex();

    void add_and_update(const int x);

    void rem_and_update(const int x);



    std::ostream& display(std::ostream& os) const;

    void check_cc(const char* msg, const bool covering);
};

template <class GraphType>
DegreeSorted<GraphType>::DegreeSorted()
    : node_heap_col(this)
    , node_heap_cc(this)
{
}

    template <class GraphType>
    DegreeSorted<GraphType>::DegreeSorted(const int n)
        : node_heap_col(this)
        , node_heap_cc(this)
    {
        GraphType::initialise(n);
        initialise_degree();
        // initialise_watchers();
    }

    template <class GraphType>
    DegreeSorted<GraphType>::DegreeSorted(const GraphType& g)
        : GraphType(g)
        , node_heap_col(this)
        , node_heap_cc(this)
    {
        initialise_degree();
        // initialise_watchers();
    }

    template <class GraphType>
    DegreeSorted<GraphType>::DegreeSorted(const DegreeSorted& g)
        : GraphType(g)
        , node_heap_col(this)
        , node_heap_cc(this)
    {
        initialise_memory();
        min_degree = g.min_degree;
        max_degree = g.max_degree;
        original_size = new int[GraphType::capacity];
        for (int i = 0; i < GraphType::capacity; ++i) {
            original_size[i] = g.original_size[i];
            degree_index[i] = g.degree_index[i];
            node_of_degree[GraphType::degree(i)].push_back(i);
        }
    }

    template <class GraphType> DegreeSorted<GraphType>::~DegreeSorted()
    {
        delete[] co_neighbor;
        delete[] node_of_degree;
        delete[] original_size;
        delete[] degree_index;
        delete[] saturation;
        delete[] maxweight;
    }

    template <class GraphType> void DegreeSorted<GraphType>::initialise_memory()
    {

        co_neighbor = new std::vector<int>[GraphType::capacity];
        node_of_degree = new std::vector<int>[GraphType::capacity];
        degree_index = new int[GraphType::capacity];
        util_stack.initialise(
            0, GraphType::capacity - 1, GraphType::capacity, false);
        _visited.initialise(0, GraphType::capacity - 1, BitSet::empt);
        saturation = new int[GraphType::capacity];
        util_set.initialise(0, GraphType::capacity - 1, BitSet::empt);
        maxweight = new weight_type[GraphType::capacity];
        tmp_weight.resize(GraphType::capacity + 1);
        tmp_weight[GraphType::capacity] = 0;
    }

    template <class GraphType> void DegreeSorted<GraphType>::initialise_degree()
    {
        initialise_memory();
        min_degree = GraphType::capacity - 1;
        max_degree = 0;
        original_size = new int[GraphType::capacity];
        for (unsigned i = 0; i < GraphType::capacity; ++i) {
            auto d = GraphType::degree(i);
            original_size[i] = d;

            assert(d <= static_cast<int>(GraphType::capacity - 1));

            if (d < min_degree)
                min_degree = d;
            if (d > max_degree)
                max_degree = d;

            degree_index[i] = node_of_degree[d].size();
            node_of_degree[d].push_back(i);
        }

        verify("init");
    }

    template <class GraphType> int DegreeSorted<GraphType>::real_size()
    {
        return GraphType::node.size - node_of_degree[0].size();
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::min_clique_cover_bitset_helper(
        DualProblem* DP, std::vector<int>& vertices, close_type ct)
    {
        while (vertices.size() > 0) {
            DP->new_layer();
            for (auto v : vertices) {
                DP->insert(v);
            }
            if (ct == close_type::STRONG)
                DP->close(vertices, tmp_weight);
            else
                DP->close_weak(vertices, tmp_weight);
        }
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::vertices_by_degree_increasing(
        std::vector<int>& vertices)
    {
        for (auto d = min_degree; d <= max_degree; ++d) {
            for (int j = node_of_degree[d].size(); --j >= 0;) {
                auto v = node_of_degree[d][j];
                vertices.push_back(v);
                tmp_weight[v] = GraphType::weight[v];
            }
        }
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::vertices_static(std::vector<int>& vertices)
    {
        vertices.insert(vertices.end(), this->node.begin(), this->node.end());
        std::sort(begin(vertices), end(vertices),
            [&](int v, int u) { return static_order[v] < static_order[u]; });
        for (int v : vertices)
            tmp_weight[v] = this->weight[v];
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::min_clique_cover_bitset(
        DualProblem* CC, close_type ct, order_type ot)
    {
        // std::cout << "\nfind CC ";

        std::vector<int>& vertices{util_vec};

        CC->clear();
        if (GraphType::null())
            return;

        if (ct == close_type::WEAK)
            CC->dual_cost = this->total_weight;

        vertices.clear();
        switch (ot) {
        case DYNAMIC_DEGREE:
            vertices_by_degree_increasing(vertices);
            break;
        case STATIC_DSATUR:
            if (!static_order.empty())
                vertices_static(vertices);
            else
                vertices_by_degree_increasing(vertices);
            break;
        }
        min_clique_cover_bitset_helper(CC, vertices, ct);
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::vertices_by_degree_decreasing(
        std::vector<int>& vertices)
    {
        for (auto d = max_degree; d >= min_degree; --d) {
            for (int j = node_of_degree[d].size(); --j >= 0;) {
                auto v = node_of_degree[d][j];
                vertices.push_back(v);
                tmp_weight[v] = GraphType::weight[v];
            }
        }
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::min_coloration_bitset(
        DualProblem* COL, close_type ct, order_type ot)
    {
        std::vector<int>& vertices{util_vec};

        COL->clear();
        if (GraphType::null())
            return;

        if (ct == close_type::WEAK)
            COL->dual_cost = this->total_weight;

        vertices.clear();
        switch (ot) {
        case DYNAMIC_DEGREE:
            vertices_by_degree_decreasing(vertices);
            break;
        case STATIC_DSATUR:
            if (!static_order.empty())
                vertices_static(vertices);
            else
                vertices_by_degree_decreasing(vertices);
            break;
        }
        min_clique_cover_bitset_helper(COL, vertices, ct);
    }

    // template <class GraphType>
    // template <typename ScoreFunc>
    // void DegreeSorted<GraphType>::min_weight_clique_cover_bitset(
    //     DualProblem* CC, ScoreFunc scoring)
    // {
    //
    //     CC->clear();
    //     if (GraphType::node.empty())
    //         return;
    //
    //     int d, j, i, x, best_score, score, best_clique;
    //
    //     for (d = min_degree; d <= max_degree; ++d) {
    //         for (j = node_of_degree[d].size(); --j >= 0;) {
    //             x = node_of_degree[d][j];
    //             best_clique = CC->dual_cost;
    //             best_score = INFTY;
    //             for (i = 0; i < CC->dual_cost; ++i) {
    //                 if (CC->candidate[i].fast_contain(x)) {
    //                     score = scoring(GraphType::weight[x], maxweight[i]);
    //                     if (score < best_score) {
    //                         best_score = score;
    //                         best_clique = i;
    //                     }
    //                 }
    //             }
    //             if (best_clique == CC->dual_cost) {
    //                 maxweight[CC->dual_cost] = GraphType::weight[x];
    //                 ++CC->dual_cost;
    //             } else if (maxweight[best_clique] < GraphType::weight[x]) {
    //                 maxweight[best_clique] = GraphType::weight[x];
    //             }
    //
    //             CC->candidate[best_clique].intersect_with(GraphType::matrix[x]);
    //             CC->partition[best_clique].fast_add(x);
    //         }
    //     }
    //
    //     // TODO: FIX THAT
    //     // score = 0;
    //     // for (int i = 0; i < dual_cost; ++i) {
    //     //     score += maxweight[i];
    //     // }
    //     // return score;
    // }

    template <class GraphType>
    void DegreeSorted<GraphType>::naive_helper(
        DualProblem* CC, std::vector<int>& vertices)
    {
        while (vertices.size() > 0) {
            util_set.clear();
            CC->new_layer();

            for (auto v : vertices) {
                if (util_set.fast_contain(v))
                    continue;
                // CC->new_clique();
                int l = CC->layer_csize.size() - 1;
                int max_cl = CC->layer_csize[l];
                CC->resize(max_cl + 1);

                CC->sorted.push_back(max_cl);
                ++CC->layer_csize[l];
                CC->candidate[max_cl].setminus_with(util_set);

                bool empty_candidates{false};
                do {
                    util_set.fast_add(v);
                    CC->partition[max_cl].fast_add(v);
                    CC->partition_list[max_cl].push_back(v);
                    ++CC->partition_sz[max_cl];
                    CC->candidate[max_cl].intersect_with(this->matrix[v]);
                    empty_candidates = CC->candidate[max_cl].empty();
                    if (!empty_candidates)
                        v = CC->candidate[max_cl].min();
                } while (!empty_candidates);
            }
            CC->close(vertices, tmp_weight);
        }
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::min_clique_cover_naive(DualProblem* CC)
    {
        std::vector<int>& vertices{util_vec};

        CC->clear();
        if (GraphType::null())
            return;

        for (auto d = min_degree; d <= max_degree; ++d) {
            for (int j = node_of_degree[d].size(); --j >= 0;) {
                auto v = node_of_degree[d][j];
                vertices.push_back(v);
                tmp_weight[v] = GraphType::weight[v];
            }
        }

        naive_helper(CC, vertices);

        util_vec.clear();
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::min_coloration_naive(DualProblem* CC)
    {
        std::vector<int>& vertices{util_vec};

        CC->clear();
        if (GraphType::null())
            return;

        for (auto d = max_degree; d >= min_degree; --d) {
            for (int j = node_of_degree[d].size(); --j >= 0;) {
                auto v = node_of_degree[d][j];
                vertices.push_back(v);
                tmp_weight[v] = GraphType::weight[v];
            }
        }

        naive_helper(CC, vertices);

        util_vec.clear();
    }

    // #define _DEBUG_DSATUR_ true

    template <class GraphType>
    void DegreeSorted<GraphType>::dsatur_clc(DualProblem* dual, close_type ct)
    {
        dsatur(dual, node_heap_cc, ct);
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::dsatur_col(DualProblem* dual, close_type ct)
    {
        dsatur(dual, node_heap_col, ct);
    }

    template <class GraphType>
    template <typename Heap>
    void DegreeSorted<GraphType>::dsatur(
        DualProblem* dual, Heap& node_heap, close_type ct)
    {

#ifdef _DEBUG_DSATUR_
        int count = 0;
        dual->check("before dsatur", false);
        if (_DEBUG_DSATUR_) {
            std::cout << std::endl << "start dsatur clique" << std::endl;
        }
#endif

        dual->clear();
        if (GraphType::node.empty())
            return;

        if (ct == close_type::WEAK)
            dual->dual_cost = this->total_weight;

        std::vector<int>& vertices{util_vec};
        BitSet& relevant_nodes{_visited};
        BitSet& neighbors{util_set};

        relevant_nodes.copy(GraphType::node_set);
        for (auto x : GraphType::node) {
            vertices.push_back(x);
            tmp_weight[x] = GraphType::weight[x];
        }

        int x;
        int cur_layer{0};
        bool empty_order{static_order.empty()};
        if (empty_order)
            static_order.resize(this->capacity + 1);
        int cur_vertex_order{0};
        while (vertices.size() > 0) {

#ifdef _DEBUG_DSATUR_
            if (_DEBUG_DSATUR_) {
                std::cout << "new layer" << std::endl;
            }
#endif

            for (auto x : vertices) {
                saturation[x] = 0;
                node_heap.insert(x);
            }

            dual->new_layer();
            ++cur_layer;
            while (!node_heap.empty()) {
                x = node_heap.removeMin();
                GraphType::rem_node(x);
                neighbors.clear();
                relevant_nodes.fast_remove(x);

                if (empty_order && cur_layer == 1)
                    static_order[x] = cur_vertex_order++;

#ifdef _DEBUG_DSATUR_
                std::cout << "insert " << x << std::endl;
                int cl =
#endif
                    dual->insert(x, neighbors);

                neighbors.intersect_with(relevant_nodes);

#ifdef _DEBUG_DSATUR_
                if (_DEBUG_DSATUR_) {
                    std::cout << std::setw(3) << (++count) << " "
                              << std::setw(3) << x << " <- " << std::setw(2)
                              << cl << " (" << saturation[x] << "|"
                              << GraphType::degree(x) << ")";
                    std::cout << std::endl
                              << "     " << dual->candidate[cl] << " u "
                              << neighbors << std::endl;
                }
#endif

                if (!neighbors.empty()) {
                    for (auto y : neighbors) {
                        ++saturation[y];
                        node_heap.decrease(y);
                    }
                }
            }

            for (auto i = 0; i < vertices.size(); ++i) {
                x = GraphType::node[GraphType::size()];
                GraphType::add_node(x);
            }

            if (ct == close_type::STRONG)
                dual->close(vertices, tmp_weight);
            else
                dual->close_weak(vertices, tmp_weight);

            relevant_nodes.clear();
            for (auto x : vertices) {
                relevant_nodes.fast_add(x);
            }
        }

#ifdef _DEBUG_DSATUR_
        std::cout << "dual_cost: " << dual->dual_cost
                  << " bound = " << (GraphType::total_weight - dual->dual_cost)
                  << std::endl;
        // exit(1);
#endif
    }

    template <class GraphType>
    int DegreeSorted<GraphType>::codegree(const int x)
    {
        return co_neighbor[x].size();
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::verify(const char* msg)
    {

        GraphType::verify(msg);

        for (unsigned i = 0; i < GraphType::capacity; ++i) {
            assert(original_size[i] == GraphType::degree(i) + codegree(i));
        }

        // check min/max degree
        int lb = GraphType::capacity;
        int ub = 0;
        int ne = 0;
        for (auto i = 0; i < GraphType::size(); ++i) {
            int x = GraphType::node[i];
            int d = GraphType::degree(x);
            if (d < lb)
                lb = d;
            if (d > ub)
                ub = d;

            ne += d;

            // assert(node_of_degree[d].contain(x));
            if (node_of_degree[d][degree_index[x]] != x) {
                std::cout << *this << std::endl
                          << msg << ": node_of_degree[" << d
                          << "] does not contain " << x << " (";
                vecdisplay(node_of_degree[d], std::cout);
                std::cout << ")" << std::endl;
                assert(0);
            }
        }

        if (GraphType::size() > 0) {
            if (lb != min_degree) {
                std::cout << *this << std::endl
                          << msg << ": min_degree=" << min_degree
                          << " (should be " << lb << ")" << std::endl;
                assert(0);
            }
            if (ub != max_degree) {
                std::cout << *this << std::endl
                          << msg << ": max_degree=" << max_degree
                          << " (should be " << ub << ")" << std::endl;
                assert(0);
            }
        }
        ne /= 2;

        if (ne != GraphType::num_edges) {
            std::cout << *this << std::endl
                      << msg << ": num_edges=" << GraphType::num_edges
                      << " (should be " << ne << ")" << std::endl;
            assert(0);
        }

        // check degree ordering
        for (auto d = min_degree; d <= max_degree; ++d) {
            for (unsigned i = 0; i < node_of_degree[d].size(); ++i) {
                if (GraphType::degree(node_of_degree[d][i]) != d) {
                    std::cout << *this << std::endl
                              << msg << ": degree of " << node_of_degree[d][i]
                              << " is not " << d << std::endl;
                    assert(0);
                }
            }
        }
    }

    template <class GraphType> int DegreeSorted<GraphType>::min_degree_vertex()
    {

        if (min_degree == 0) {
            for (int d = 1; d <= max_degree; d++) {
                if (node_of_degree[d].size() > 0) {
                    return node_of_degree[d].back();
                }
            }
            return -1;
        } else {
            return node_of_degree[min_degree].back();
        }
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::add_and_update(const int x)
    {

#ifdef _VERIFIED_RCG
        verify("before add");
#endif

#ifdef _DEBUG_UPDATE
        assert(x >= 0);
        assert(x < GraphType::capacity);
        assert(!GraphType::node.contain(x));
        std::cout << std::endl << *this << "\nadd node " << x << " ";
        GraphType::neighbor[x].display(std::cout);
        std::cout << std::endl;
#endif

        int i = GraphType::degree(x), y, d, l, idx;

        GraphType::add_node(x);

        degree_index[x] = node_of_degree[i].size();
        node_of_degree[i].push_back(x);

        if (max_degree < i)
            max_degree = i;
        if (min_degree > i)
            min_degree = i;

        // i = d;
        while (i--) {
            y = GraphType::neighbor[x][i];
            d = GraphType::degree(y) - 1;

            idx = degree_index[y];
            l = node_of_degree[d].back();
            node_of_degree[d].pop_back();

            if (idx != static_cast<int>(node_of_degree[d].size())) {
                node_of_degree[d][idx] = l;
                degree_index[l] = idx;
            }

            degree_index[y] = node_of_degree[d + 1].size();
            node_of_degree[d + 1].push_back(y);

            if (max_degree == d)
                max_degree = d + 1;
            if (min_degree == d && node_of_degree[d].empty()) {
                min_degree = d + 1;
            }

            // pop x from y's co-neighbor
            assert(co_neighbor[y].back() == x);
            co_neighbor[y].pop_back();
        }

#ifdef _VERIFIED_RCG_
        verify("after add");
#endif
    }

    template <class GraphType>
    void DegreeSorted<GraphType>::rem_and_update(const int x)
    {

#ifdef _VERIFIED_RCG
        verify("before rem");
#endif

#ifdef _DEBUG_UPDATE
        assert(x >= 0);
        assert(x < GraphType::capacity);
        assert(GraphType::node.contain(x));
        std::cout << std::endl << *this << "\nremove node " << x << " ";
        GraphType::neighbor[x].display(std::cout);
        std::cout << std::endl;
#endif

        int i = GraphType::degree(x), y, d, l = node_of_degree[i].back(),
            idx = degree_index[x];
        node_of_degree[i].pop_back();

        GraphType::rem_node(x);

        node_of_degree[i][idx] = l;
        degree_index[l] = idx;

        while (i--) {
            y = GraphType::neighbor[x][i];
            d = GraphType::degree(y) + 1;

            idx = degree_index[y], l = node_of_degree[d].back();
            node_of_degree[d].pop_back();
            assert((idx >= 0
                    && idx < static_cast<int>(node_of_degree[d].size()))
                || idx == static_cast<int>(node_of_degree[d].size()));

            if (idx != static_cast<int>(node_of_degree[d].size())) {
                node_of_degree[d][idx] = l;
                degree_index[l] = idx;
            }

            degree_index[y] = node_of_degree[d - 1].size();
            node_of_degree[d - 1].push_back(y);

            if (min_degree == d)
                min_degree = d - 1;

            if (max_degree == d && node_of_degree[d].empty())
                max_degree = d - 1;

            // add x to y's co-neighbor
            co_neighbor[y].push_back(x);
        }

        d = GraphType::degree(x);
        if (max_degree == d) {
            while (d >= min_degree && node_of_degree[d].empty())
                --d;
            max_degree = d;
        }
        if (min_degree == d) {
            while (d <= max_degree && node_of_degree[d].empty())
                ++d;
            min_degree = d;
        }

#ifdef _DEBUG_UPDATE
        verify("rem");
#endif

#ifdef _VERIFIED_RCG
        verify("after rem");
#endif
    }

    template <class GraphType>
    bool DegreeSorted<GraphType>::neighboring_clique(
        const int x, int& w1, int& w2)
    {
        int i, y;

        w1 = -1;
        w2 = -1;

#ifdef _DEBUG_NEIGHCLIQUE
        if (_DEBUG_NEIGHCLIQUE) {
            std::cout << "check if " << x
                      << "'s neighbors = " << GraphType::neighbor[x]
                      << " is a clique" << std::endl;
        }
#endif

        if (GraphType::neighbor[x].size() > 1) {
            util_set.copy(GraphType::matrix[x]);
            util_set.intersect_with(GraphType::node_set);

            for (i = GraphType::neighbor[x].size(); --i >= 0;) {
                y = GraphType::neighbor[x][i];
                util_set.fast_remove(y);

#ifdef _DEBUG_NEIGHCLIQUE
                if (_DEBUG_NEIGHCLIQUE) {
                    std::cout << "      -> " << y << ": "
                              << GraphType::matrix[y] << " is a super set of "
                              << util_set << "?" << std::endl;
                }
#endif

                if (!util_set.included(GraphType::matrix[y])) {
                    util_set.setminus_with(GraphType::matrix[y]);
                    w1 = util_set.min();
                    w2 = y;
                    break;
                }

                util_set.fast_add(y);
            }
        }

#ifdef _DEBUG_NEIGHCLIQUE
        if (_DEBUG_NEIGHCLIQUE) {
            if (w1 < 0) {
                std::cout << "      -> clique!" << std::endl;
            } else {
                std::cout << "      -> not a clique b/c " << w1 << " and " << w2
                          << " are not neighbors" << std::endl;
            }
        }
#endif

        return (w1 < 0);
    }

    template <class GraphType>
    bool DegreeSorted<GraphType>::non_neighboring_is(
        const int x, int& w1, int& w2
        //, const bool mis
        )
    {
        // int y;
        // int i;

        w1 = -1;
        w2 = -1;

#ifdef _DEBUG_NEIGHCLIQUE
        if (_DEBUG_NEIGHCLIQUE) {
            std::cout << "check if " << x
                      << "'s non-neighbors = " << GraphType::neighbor[x]
                      << " is an is" << std::endl;
        }
#endif

        // bool possible{false};
        // if(mis)
        //      possible = (GraphType::neighbor[x].size() > 1);
        // else
        //      possible = (GraphType::neighbor[x].size() < GraphType::size() -
        // 1);
        //
        //         if (possible) {

        if (this->neighbor[x].size() < static_cast<unsigned>(this->size()) - 1) {

            util_set.copy(GraphType::matrix[x]);
            util_set.intersect_with(GraphType::node_set);
            // now util_set contains the current nodes of the graph that are not
            // x's neighbors (including x)

            // assert(GraphType::node_set.contain(x));
            //
            // assert(!GraphType::matrix[x].contain(x));
            //
            // assert(!util_set.contain(x));

            //                                          if(mis) {
            //                                                  assert(neighbor[x].size()>0);
            //                                                  y =
            // GraphType::neighbor[x][i];
            //                                           } else {
            //                                                   assert(util_set.size()>0);
            //                                                  y =
            // util_set.get_min();
            //                                          }
            //                                          bool _continue_{true};
            //
            //                                          do {
            //                 util_set.fast_remove(y);
            //
            // #ifdef _DEBUG_NEIGHCLIQUE
            //                 if (_DEBUG_NEIGHCLIQUE) {
            //                     std::cout << "      -> " << y << ": "
            //                               << GraphType::matrix[y] << " is a
            //                               super set of "
            //                               << util_set << "?" << std::endl;
            //                 }
            // #endif
            //
            //                 // does y have a neighbor in util_set
            //                 if (!util_set.included(GraphType::matrix[y])) {
            //                     util_set.setminus_with(GraphType::matrix[y]);
            //                     w1 = util_set.min();
            //                     w2 = y;
            //                     break;
            //                 }
            //
            //                 util_set.fast_add(y);
            //
            //                                                          if(mis)
            // {
            //                                                                  if(++i>=neighbor.size())
            //                                                                          _continue_
            // =
            // false;
            //                                                                  else
            //                                                                          y
            // =
            // GraphType::neighbor[x][i];
            //                                                          } else {
            //                                                                  i
            // =
            // util_set.next(y);
            //                                                                  if(i
            // ==
            // y)
            //                                                                          _continue_
            // =
            // false;
            //                                                                  else
            //                                                                          y
            // =
            // i;
            //                                                          }
            //                                          }

            for (auto y : util_set) {
                util_set.fast_remove(y);

#ifdef _DEBUG_NEIGHCLIQUE
                if (_DEBUG_NEIGHCLIQUE) {
                    std::cout << "      -> " << y << ": "
                              << GraphType::matrix[y] << " is a super set of "
                              << util_set << "?" << std::endl;
                }
#endif

                // does y have a neighbor in util_set
                if (!util_set.included(GraphType::matrix[y])) {
                    util_set.setminus_with(GraphType::matrix[y]);
                    w1 = util_set.min();
                    w2 = y;
                    break;
                }

                util_set.fast_add(y);
            }
        }

#ifdef _DEBUG_NEIGHCLIQUE
        if (_DEBUG_NEIGHCLIQUE) {
            if (w1 < 0) {
                std::cout << "      -> clique!" << std::endl;
            } else {
                std::cout << "      -> not a clique b/c " << w1 << " and " << w2
                          << " are not neighbors" << std::endl;
            }
        }
#endif

        return (w1 < 0);
    }

    template <class GraphType>
    std::ostream& DegreeSorted<GraphType>::display(std::ostream& os) const
    {
        GraphType::display(os);

        std::cout << min_degree << ".." << max_degree << std::endl;

        int d = max_degree;

        std::vector<int> nodes;

        while (d >= min_degree) {
            if (!node_of_degree[d].empty()) {
                os << d << ": ["; //<< node_of_degree[d][0];
                // for(int i=1; i<node_of_degree[d].size; ++i) {
                //      os << " " << node_of_degree[d][i];
                // }
                for (unsigned i = 0; i < node_of_degree[d].size(); ++i) {
                    nodes.push_back(node_of_degree[d][i]);
                }
                sort(nodes.begin(), nodes.end());
                for (unsigned i = 0; i < nodes.size(); ++i) {
                    std::cout << " " << nodes[i];
                }
                nodes.clear();
                os << " ] ";
            }
            --d;
        }
        return os;
    }

    template <class GraphType>
    std::ostream& operator<<(std::ostream& os, const DegreeSorted<GraphType>& x)
    {
        return x.display(os);
    }

    template <class GraphType>
    std::ostream& operator<<(std::ostream& os, const DegreeSorted<GraphType>* x)
    {
        return x->display(os);
    }

    typedef DegreeSorted<Graph> ReversibleCompactGraph;

#endif // __GRAPH_HPP
