#ifndef __LBC
#define __LBC

#include <Graph.hpp>
#include <Bitset.hpp>
#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/mtl/Heap.h>
#include <minicsp/mtl/Vec.h>

#include <vector>

using namespace ::minicsp;

class cons_lbc : public minicsp::cons
{
private:
	Solver& s;
	std::vector<cspvar>& vars;
	Graph& G;
	int n;
	int ub;

	//couleurs non utilisées
	BitSet unused;
	//sommets candidats (voisins) lors du calcul de la clique
	BitSet dispo;
	//ensemble des sommets saturés
	std::vector<int> v;
	//reason
	vec<Lit> r;

	void calcul_clique();
	void echanger(int i, int j);
	void quick_sort(int deb, int pivot, int fin);
	void colore_clique();
	void explain();
public:
	cons_lbc(Graph& G, Solver& s, std::vector<cspvar>& vars, int n, int ub);
	Clause* propagate(Solver& s) override;
	std::vector<int> clique;
};

#endif
