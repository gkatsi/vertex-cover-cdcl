#ifndef __PROP_HPP
#define __PROP_HPP

#include <Graph.hpp>
#include <Watched.hpp>
#include <limits>
#include <minicsp/core/solver.hpp>
#include <minicsp/mtl/Heap.h>
#include <minicsp/mtl/Vec.h>
#include <vector>
#include <map>

#include <Algorithm.hpp>

// #define _TRACE_PRUNING true
// #define _PRETTY_TRACE
// #define _DEBUG_DYNPROG (s.propagations >= 496)

// using weight = int64_t;

class cons_vc;

/* A helper class for reducing allocations. */
template <typename T> class allocation_cache;
template <typename T> struct default_cache_factory {
    T* operator()(allocation_cache<T>&) const { return new T(); }
};

template <typename T> class allocation_cache
{
public:
    allocation_cache() {}

    void release(T* e) { free_list.emplace_back(e); }
    template <typename Factory = default_cache_factory<T>>
    T* get(Factory factory = default_cache_factory<T>())
    {
        if (free_list.empty())
            return factory(*this);
        else {
            T* r = free_list.back().release();
            free_list.pop_back();
            return r;
        }
    }

    template <typename Factory>
    std::shared_ptr<T> get_shared(Factory factory = default_cache_factory<T>())
    {
        T* t = get(factory);
        try {
            return std::shared_ptr<T>{t, [this](T* ptr) { release(ptr); }};
        } catch (...) {
            delete t;
            throw;
        }
    }

private:
    std::vector<std::unique_ptr<T>> free_list;
};

struct vc_explainer_shared {
    cons_vc* cons{nullptr};
    DualProblem dual;
    BitSet current_solution;
    std::vector<int> fixednodes;

    vc_explainer_shared(Graph* G)
        : dual(G)
    {
    }
};

class vc_explainer : public minicsp::explainer
{
public:
    std::shared_ptr<vc_explainer_shared> shared;
    allocation_cache<vc_explainer>* release_to;
    int v;
    int vlb;

    vc_explainer(allocation_cache<vc_explainer>* a)
        : release_to(a)
    {
    }

    virtual void explain(
        minicsp::Solver&, minicsp::Lit p, vec<minicsp::Lit>& c) override;

    virtual void use() override { assert(shared); }
    virtual void release() override
    {
        shared.reset();
        release_to->release(this);
    }
};

struct component {
    BitSet vertices;
    int size;
    int nkilledv{0};
    weight_type lb;
    double activity;

    // if true, the component has been removed and will be erased in
    // the future
    bool dead{false};

    component(BitSet v, weight_type l)
        : vertices(v)
        , size(v.size())
        , lb(l)
    {
    }
};

struct component_cache {
    // the cache itself. we cannot hold values, because we need stable
    // pointers for inv
    std::vector<std::unique_ptr<component>> cache;
    // dead components -- they cannot be removed immediately because
    // we keep pointers to them in inv
    std::vector<std::unique_ptr<component>> dead;
    // inverse map of components, i.e., for each vertex, the set of
    // components which contain it
    std::vector<std::vector<component*>> inv;

    // component cache management: activity increase and activity
    // decay, just like in minisat
    double act_inc{1.0}, act_decay{.95};

    // --------------------------------------------------
    // no default constructor, because we need to know the number of vertices
    explicit component_cache(int nv)
        : inv(nv)
    {
    }

    component& add_component(std::unique_ptr<component> c)
    {
        cache.push_back(std::move(c));
        auto& comp = cache.back();

        auto maxv = comp->vertices.max();
        if (inv.size() <= static_cast<size_t>(maxv))
            inv.resize(maxv + 1);

        for (auto v : comp->vertices)
            inv[v].push_back(comp.get());

        return *comp;
    }

    void readd_vertex(int v)
    {
        if (inv.size() > static_cast<std::size_t>(v))
            for (auto comp : inv[v])
                --comp->nkilledv;
    }

    void remove_vertex(int v)
    {
        if (inv.size() > static_cast<std::size_t>(v))
            for (auto comp : inv[v])
                ++comp->nkilledv;
    }

    // do the actual cleanup of dead components
    void gc_cache()
    {
        for (auto& vinv : inv)
            erase_if(vinv, [&](component* c) { return c->dead; });
        dead.clear();
    }

    void decay_activities() { act_inc /= act_decay; }
    void bump_activity(component& comp) { comp.activity += act_inc; }
};

struct vc_options {
    enum explanation_level { NONE, NAIVE, FAST, GREEDY };

    enum clqcover_alg {
        NAIVEdual,
        GREEDYdual,
        GREEDYWEAKdual,
        GREEDYNWEAKdual,
        REPAIRdual,
        QUICKRdual,
        RANDOMdual,
        DSATURdual,
        DSATURWEAKdual,
        DYNGREEDYdual,
        DYNWEAKdual,
        DYNDSATURdual,
        DYNAMICdual
    };

    static const std::map<std::string, clqcover_alg> clqcover_alg_names;

    static const bool MAXCLIQUE = false;
    static const bool MAXINDSET = true;

    enum maxsat_level { NOMAXSAT = 0, SIMPLE, FULL_AC, MS_PRUNE, STAT };

    int bound_algo{GREEDYdual};
    int dsatur_threshold{2};
    explanation_level explanation{GREEDY};
    explanation_level pruning{FAST};
    bool deferred_explanations{false};
    int dominance{0};
    bool check_bounds{false};
    bool weighted{false};
    weight_type ub{std::numeric_limits<weight_type>::max()};
    order_type greedy_order{DYNAMIC_DEGREE};
    int maxsat{0};
    float neighbor_ratio{0.0};
    bool component_caching{false};
    bool dynprog{false};
    bool problem{MAXINDSET};
    bool buss{false};
    bool prune_to_is{false};
};

class cons_vc : public minicsp::cons
{
    friend vc_explainer;

private:
    minicsp::Solver& s;
    std::vector<minicsp::Lit> _x;
    // map from minicsp Boolean variables to vertices. Normally they
    // have exactly the same id, but who knows what we'll do in the
    // future
    std::vector<int> revmap;

    DegreeSorted<Graph>& rG;

    minicsp::btptr coverweightptr;

    // removed nodes and backtrackble integer keeping the vector's
    // size
    std::vector<int> removed;
    minicsp::btptr removed_sizeptr;

    // all possible nodes/literals that may be part of the reason
    std::vector<int> fixednodes;
    std::vector<minicsp::Lit> fixedlits;
    minicsp::btptr fixed_sizeptr;

    // if true, wake() changed the graph, so we need to recompute the
    // lb. Note it does not need to be backtrackable.
    bool recompute_lb{true};
    // decision level of the previous call to the lower bound
    // used to know if we recompute or repair
    int decision_event_level{rG.size()};

    // cache covers computed in shallower levels
    std::vector<DualProblem> lvl_covers;

    // what it says
    component_cache ccache;

public:

    weight_type bestlb{0};

    DualProblem dual;

#ifdef _PRETTY_TRACE
    int previous_decision_level{0};
    int previous_ub{std::numeric_limits<int>::max()};
    minicsp::Lit backtrack_event{minicsp::lit_Undef};
    minicsp::Lit decision_event{minicsp::lit_Undef};
#endif

    // dynprog stuff
    std::vector<int> vertexUBForward;
    std::vector<int> vertexUBBackward;

    IntStack descendant;
    std::vector<int> sortedvertices;
    std::vector<int> vertexindex;

    int get_vertexUB(const int v);

    minicsp::Clause* dominance_preprocessing();

private:
    // a buffer for storing a second clique cover
    DualProblem bufferdual;
    // Algo and structure to compute a conflicting set
    MaxsatAlgo ms;

    // switch buffered and current clique covers
    void switch_partition();

    // caching
    vec<minicsp::Lit> reason;
    std::vector<int> prune_clqsize;
    std::vector<weight_type> maxNW;
    std::vector<uint8_t> prune_hitccs;
    std::vector<int> prune_cc;
    std::vector<int> prune_reason;
    std::vector<int> fast_minimize_fastr;
    std::vector<int> explain_crit;
    std::vector<int> dual_vc_solution;
    std::vector<int> dual_clq_solution;

    // last vertex lower bounds we have computed, and a flag to show
    // whether we have yet computed any lbs
    std::vector<weight_type> last_lbs;
    bool have_lbs{false};

    // the largest cover we allow
    weight_type ub{std::numeric_limits<weight_type>::max()};

    // a bitset containing the current cover
    BitSet current_solution;
    BitSet relevant; // used in the greedy explanation algo
    BitSet maybe; // used in the greedy explanation algo
    BitSet relevant_N; // used in the greedy explanation algo
    BitSet util_set; // passed as argument to algos
    BitSet cc_neighbors; // in explain_pruning, vc_options::FAST
    BitSet dual_sol_neighbors;

    // watch structure for clique dominance
    TwoWatchedThing<int> watchers;
    int _last_checked_watched{0};
    unsigned int _last_checked_repair{0};
    bool _first_propagation{true};

    // total weight of the full graph (to go from VC weight to IS
    // weight)
    weight_type init_weight{0};

    // keep track of explainers. One explainer per variable and a
    // cache of vc_explainer_shared (which is the heavier part,
    // allocation-wise)
    allocation_cache<vc_explainer> explainers;
    allocation_cache<vc_explainer_shared> explainers_shared;

    // --------------------------------------------------

    // propagate with various mechanisms. Each returns a conflict
    // clause if it detects a failure. It sets recompute_lb to true if
    // it changed the graph
    minicsp::Clause *propagate_dominance();
    minicsp::Clause *propagate_partition();
    minicsp::Clause *propagate_maxsat();
    minicsp::Clause *propagate_ccache();
    minicsp::Clause *propagate_dynprog();

    minicsp::Clause *handle_lb(weight_type lb);

    // a shorthand for have_meaningful_upper_bound(): if true, then
    // computing a lower bound may cause us to backtrack or
    // prune. otherwise, only dominance pruning makes sense.
    bool have_solution() const;

    // update the residual graph, the formula and our
    // backtrackable data
    minicsp::Clause* update_graph(minicsp::Solver& s, minicsp::Lit l, int node);

    void update_partition();

    // minimize the explanation by trying to find the same lower
    // bound
    // (or at least >=ub) after removing some literals
    minicsp::Clause* explain_cc(minicsp::Solver& s, weight_type lb);
    // return an explanation for a dp fail (trivial bound for now)
    minicsp::Clause* explain_dp(minicsp::Solver& s);

    // fast minimization: while lb > ub (i.e., not equal), remove
    // nodes that are in the cover. Removing them can decrease the
    // current cover by 1, increase the size of the residual graph
    // by
    // 1 and increase the clique cover by 1, lowering the lb by at
    // most 1.
    std::vector<int>& fast_minimize(
        minicsp::Solver& s, std::vector<int> const& reason, weight_type lb);

    // todo
    std::vector<int> init_degree;
    std::vector<int> greedyr;
    void greedy_minimize(minicsp::Solver& s, std::vector<int> const& reason,
        DualProblem& gdual, weight_type lb, BitSet& relevant);

    // pruning by Babel's rule (Babel, 1994): for every vertex v in
    // the residual graph and a clique cover of the residual graph,
    // let nclq(v) be the number of cliques in which non-neighbors of
    // v appear. If vc + n - (nclq(v) + 1) > ub, then v must be in the
    // cover. clqcover is the size of the cover we have already
    // computed. May return a non-NULL clause if it backprunes
    minicsp::Clause* prune(minicsp::Solver& s, DualProblem const& pdual);

    minicsp::Clause* prune_to_is(
        DualProblem const& pdual, int vtx, std::vector<int>& pruned);

    // this generates an explanation for a Babel pruning and prunes
    // using that explanation. It will use NO/FAST/GREEDY minimized
    // explanations (but not FULL). vlb is the bound computed if we
    // did not include v in the cover. May return a non-nullptr Clause
    // if it finds we have to backprune a literal
    minicsp::Clause* prune_explain(
        minicsp::Solver& s, DualProblem const& pdual, int v, int vlb);

    // helper used by prune_explain or
    // vc_explain::explain_refcounted() to generate an actual (not
    // deferred) explanation
    void explain_pruning(minicsp::Solver& s, int v, int vlb,
        vec<minicsp::Lit>& cl, DualProblem const& pdual, vc_explainer* expl);

    // prune using the dp bounds
    int dp_prune(minicsp::Solver& s);
    void dp_prune_explain(minicsp::Solver& s, int v);

    // buss rule
    minicsp::Clause* propagate_buss();
    minicsp::Clause* buss_rule_mis();
    minicsp::Clause* buss_rule_clq();
    /* Dominance. Given a clique cover CC and a vertex v, if the sum of the
     * maximum weights of any node in the intersections of the cliques and v's
     * neighborhood is less than or equal to w(v), then v can safely be put in
     * the IS */
    minicsp::Clause* dominance_rule(DualProblem const& pdual);
    /* Low degree vertices dominance. Remove vertices of degree <= 1. Returns a
     * conflict clause if the upper bound is reached */
    minicsp::Clause* low_degree_pruning(minicsp::Solver& s);
    /* Same thing for cliques. Remove vertices of degree >= |V|-1. Returns a
     * conflict clause if the upper bound is reached */
    minicsp::Clause* high_degree_pruning(minicsp::Solver& s);
    /* Clique dominance. If N(v) is a clique, then v is dominated by N(v) since
     * it has no outward edge. This method initialises the watch structure on
     * the first call, and update incrementally on subsequent calls. Returns a
     * conflict clause if the upper bound is reached */
    minicsp::Clause* clique_dom_pruning(minicsp::Solver& s);
    // The incremental part
    minicsp::Clause* clique_update(minicsp::Solver& s);
    /* Set v to true (not in the vcover) and
     * exlain it by its removed neighborood */
    minicsp::Clause* rem_neighbor_explain(minicsp::Solver& s, const int v);
    /* Set v to true (not in the vcover) and
     * exlain it by its removed non-neighborood */
    minicsp::Clause* rem_non_neighbor_explain(minicsp::Solver& s, const int v);
    /* Set v to false (in the vcover) and
     * exlain it by its neighborood in the VC */
    minicsp::Clause* rem_neighbor_invc_explain(const int v);
    /* Set v to false (in the vcover) and
     * exlain it by its non-neighborood that went in the VC*/
    minicsp::Clause* rem_non_neighbor_invc_explain(const int v);
    // for debugging
    void verify_watchers(const char* msg);
    void check_cc(const char* msg, const bool covering);

    // small helpers for constructing clauses
    minicsp::Lit node_to_lit(minicsp::Solver& s, int u);
    void nodeset_to_clause(
        minicsp::Solver& s, std::vector<int> const& ns, vec<minicsp::Lit>& cl);

    template <bool SPIN> struct NeighborWeight {
        const Graph* graph;

        bool operator()(const int x, const int y) const
        {
            return SPIN ^ (graph->neighbor_weight[x]-graph->weight[x] > graph->neighbor_weight[y]-graph->weight[y]) ;
        }
        NeighborWeight(const Graph* g)
            : graph(g)
        {
        }
    };

    template <bool SPIN> struct NonNeighborWeight {
        const Graph* graph;

        bool operator()(const int x, const int y) const
        {
            return SPIN ^ (graph->neighbor_weight[x]+graph->weight[x] > graph->neighbor_weight[y]+graph->weight[y]) ;
        }
        NonNeighborWeight(const Graph* g)
            : graph(g)
        {
        }
    };

    Heap<NeighborWeight<true>> neighweight_heap;
    Heap<NonNeighborWeight<true>> nonneighweight_heap;

public:
    void dsatur();
    void dsatur_weak();
    void greedy();
    void greedy_weak();
    void naive();
    // if the graph is full/empty compute the dual and return true, return false
    // otherwise
    bool trivial_partition();
    // compute the dual
    void compute_dual();

    // get a solution from the dual, if possible
    void solution_from_dual(DualProblem& dual);

private:
    // construct a component from a learnt clause. Called directly
    // from the solver, hence the vec
    void new_component(minicsp::Solver& s, vec<minicsp::Lit> const& clause);

    // // various versions of computing lbs with the cache
    // minicsp::Clause *lb_ccache_cover(minicsp::Solver& s);
    // minicsp::Clause *lb_ccache_disjoint(minicsp::Solver& s);
    //
    // // Keep only the most useful components
    // void reduce_cache();

    // construct a reverse map from clique to conflicting set
    void maxsat_reverse_map(std::vector<int>& cc);

public:
    cons_vc(minicsp::Solver& s, DegreeSorted<Graph>& G,
        std::vector<minicsp::Lit> const& x, vc_options opt,
        std::function<void(std::vector<int> const&, std::vector<int> const&)>
            solution_callback);
    virtual ~cons_vc();
    minicsp::Clause* wake_advised(
        minicsp::Solver& s, minicsp::Lit l, void* hint) override;
    minicsp::Clause* propagate(minicsp::Solver& s) override;

    bool set_ub(weight_type newub)
    {
        assert(newub < ub);
        ub = newub;
        return ub > bestlb;
    }

    /* compute an initial clique cover dual and populate the vector cliqueorder
    * with the nodes of the graph sorted in an order compatible with dual (ties
    * broken by maxdegree) */

    /* compute a bound via dynamic programming */
    // int dynprog_bound_sta(minicsp::Solver& s, const int node);
    int dynprog_bound(minicsp::Solver& s);
    int dynprog_bound_clc(minicsp::Solver& s);
    int dynprog_bound_col(minicsp::Solver& s);
    void dual_sort(
#ifdef _DEBUG_DYNPROG
        minicsp::Solver& s
#endif
        );

    // used by brancher, which wants to share the same graph
    void sync_graph(minicsp::Solver& s);

    // heuristic lower bounds for *excluding* each vertex
    weight_type vertex_lb(int u) const { return last_lbs[u]; }
    bool have_vertex_lbs() const { return have_lbs; }

    // stats
    int fails{0}, clauselits{0}, removedlits{0}, delta{0}, numcalls{0};

    std::vector<int> maxsat_bound;
    std::vector<int> maxsat_early_fails;
    std::vector<int> num_maxsat_calls;

    std::vector<int> cc_bound;
    std::vector<int> cc_early_fails;
    std::vector<int> num_cc_calls;

    std::vector<int> dp_bound;
    std::vector<int> dp_early_fails;
    std::vector<int> num_dp_calls;

    std::vector<int> dsatur_clqsize;
    std::vector<int> greedy_clqsize;
    std::vector<int> repair_clqsize;

    int numchecks{0};

    // options
    vc_options options;

    // solution callback. called when a better solution is found in
    // solution_from_dual. The arguments are vectors _uninstantiated_
    // vertices that are in the new VC or new CLQ, respectively
    std::function<void(std::vector<int> const&, std::vector<int> const&)>
        solution_callback;

#ifdef _PRETTY_TRACE
    void notify_decision(minicsp::Solver& s, minicsp::Lit l);
    void notify_backtrack(minicsp::Solver& s, minicsp::Lit l);
#endif
};

cons_vc* post_vc(minicsp::Solver& s, DegreeSorted<Graph>& G,
    std::vector<minicsp::Lit> const& v, vc_options opt,
    std::function<void(std::vector<int> const&, std::vector<int> const&)>
        solution_callback);

#endif
