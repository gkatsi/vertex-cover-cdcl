

#ifndef __RANDOM_HPP
#define __RANDOM_HPP

#define MAX_URAND 0xFFFFFFFFL

bool random_generator_is_ready();
void usrand(unsigned seed);
unsigned urand0(void);
unsigned urand(void);
int randint(int upto);
double randreal();

#endif // __RANDOM_HPP
