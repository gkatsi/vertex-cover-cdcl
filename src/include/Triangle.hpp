#ifndef __TRIANGLE
#define __TRIANGLE

#include <minicsp/core/solver.hpp>
#include <minicsp/core/solvertypes.hpp>
#include <minicsp/mtl/Heap.h>
#include <minicsp/mtl/Vec.h>

using namespace ::minicsp;

class cons_triangle : public minicsp::cons
{
	Lit e1, e2, e3;
	vec<Lit> r;
public:
	cons_triangle(Solver &s, Lit a1, Lit a2, Lit a3) : 
		e1(a1), e2(a2), e3(a3)
	{
		s.wake_on_lit(var(e1), this);
		s.wake_on_lit(var(e2), this);
		s.wake_on_lit(var(e3), this);
		r.capacity(3);
		DO_OR_THROW(cons_triangle::wake(s, lit_Undef));
	}

	Clause* wake(Solver &s, Lit e) override;

	Clause* propagate(Solver &s) override;
};
#endif
