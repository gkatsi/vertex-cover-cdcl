
#include <iostream>
#include <algorithm>
#include <cassert>
// #include <iomanip>

#ifndef __VECTOR_HPP
#define __VECTOR_HPP

const int NOVAL = (int)((~(unsigned int)0) / 2);
#define INFTY NOVAL / 2

template <int N, class T>

class Tuple
{

public:
    T data[N];

    Tuple() {}
    Tuple(T a) { data[0] = a; }
    Tuple(T a, T b)
    {
        data[0] = a;
        data[1] = b;
    }
    Tuple(T a, T b, T c)
    {
        data[0] = a;
        data[1] = b;
        data[2] = c;
    }
    Tuple(T a, T b, T c, T d)
    {
        data[0] = a;
        data[1] = b;
        data[2] = c;
        data[3] = d;
    }
    Tuple(T a, T b, T c, T d, T e)
    {
        data[0] = a;
        data[1] = b;
        data[2] = c;
        data[3] = d;
        data[4] = e;
    }

    T operator[](const int i) const { return data[i]; }
    T& operator[](const int i) { return data[i]; }

    /*!@name Printing*/
    //@{
    std::ostream& display(std::ostream& os) const
    {
        os << "<";
        if (N)
            os << data[0];
        for (unsigned int i = 1; i < N; ++i)
            os << " " << data[i];
        os << ">";
        return os;
    }
    //@}
};

typedef Tuple<2, int> Pair;

std::ostream& operator<<(std::ostream& os, const Pair& x);

std::ostream& operator<<(std::ostream& os, const Pair* x);

/**********************************************
* Vector
**********************************************/
/*! \class Vector
\brief Simple vector class
*/
// int global = 0;

template <class DATA_TYPE> int increasing_order(const void* x, const void* y)
{
    DATA_TYPE& x_ = *((DATA_TYPE*)x);
    DATA_TYPE& y_ = *((DATA_TYPE*)y);
    return (x_ < y_ ? -1 : (x_ > y_ ? 1 : 0));
}

template <class DATA_TYPE> class Vector
{
public:
    typedef DATA_TYPE* iterator;

    class LabelComparison
    {
        int* label;

    public:
        LabelComparison(int* l)
            : label(l)
        {
        }

        bool operator()(DATA_TYPE i, DATA_TYPE j)
        {
            return (label[(int)i] > label[(int)j]);
        }
    };

    /*!@name Parameters*/
    //@{
    DATA_TYPE* stack_;
    unsigned int capacity;
    unsigned int size;
    //@}

    /*!@name Constructor*/
    //@{
    Vector()
    {
        capacity = 0;
        size = 0;
        stack_ = NULL;
    }

    Vector(const Vector<DATA_TYPE>& s)
    {
        capacity = s.size;
        stack_ = new DATA_TYPE[capacity];
        for (size = 0; size < capacity; ++size)
            stack_[size] = s[size];
    }

    Vector(const int n)
    {
        capacity = n;
        size = n;
        if (capacity) {
            stack_ = new DATA_TYPE[capacity];
            std::fill(stack_, stack_ + capacity, (DATA_TYPE)0);
        } else
            stack_ = NULL;
    }
    //@}

    /*!@name Destructor*/
    //@{
    virtual ~Vector()
    {
#ifdef _DEBUG_MEMORY
        std::cout << "c delete vector: " << size << " " << capacity << " " // ;
                  << std::endl;
#endif

        delete[] stack_;
    }
    //@}

    /*!@name Initialisation*/
    //@{
    void initialise(const unsigned int c)
    {
        size = 0;
        capacity = c;
        stack_ = new DATA_TYPE[capacity];
        std::fill(stack_, stack_ + capacity, DATA_TYPE());
    }

    void initialise(const unsigned int s, const unsigned int c)
    {
        size = s;
        capacity = c;
        stack_ = new DATA_TYPE[capacity];
        std::fill(stack_, stack_ + capacity, DATA_TYPE());
    }

    void initialise(const unsigned int s, const unsigned int c, DATA_TYPE x)
    {
        size = s;
        capacity = c;
        stack_ = new DATA_TYPE[capacity];
        std::fill(stack_, stack_ + capacity, x);
    }

    void copy(Vector<DATA_TYPE>& vec)
    {
        size = vec.size;
        capacity = vec.capacity;
        stack_ = vec.stack_;
    }

    void neutralise() { stack_ = NULL; }

    void extendStack(const unsigned int l = 0)
    {
        unsigned int increment = (l ? l : (capacity + 1) << 1);
        capacity += increment;

        DATA_TYPE* new_stack = new DATA_TYPE[capacity];
        for (unsigned int i = 0; i < capacity - increment; ++i)
            new_stack[i] = stack_[i];

        delete[] stack_;
        stack_ = new_stack;

        std::fill(
            stack_ + capacity - increment, stack_ + capacity, DATA_TYPE());
    }

    void resize(const unsigned int l)
    {
        if (capacity < l) {
            DATA_TYPE* new_stack_ = new DATA_TYPE[l];
            memcpy(new_stack_, stack_, capacity);
            delete[] stack_;
            stack_ = new_stack_;
            std::fill(stack_ + capacity, stack_ + l, (DATA_TYPE)0);

            capacity = l;
        }
        size = l;
    }
    //@}

    /*!@name Accessors*/
    //@{

    void sort_wrt(int* l) { sort_wrt(l, begin(), end()); }

    void sort_wrt(int* l, Vector<DATA_TYPE>::iterator beg,
        Vector<DATA_TYPE>::iterator end)
    {

        if (l) {
            LabelComparison lcomp(l);
            std::sort(beg, end, lcomp);
        } else {
            std::sort(beg, end);
        }
    }

    void sort()
    {
        qsort(stack_, size, sizeof(DATA_TYPE), increasing_order<DATA_TYPE>);
    }

    inline iterator begin() const { return stack_; }
    inline iterator end() const { return stack_ + size; }

    inline int empty() const { return !size; }

    inline void add(DATA_TYPE x)
    {
        if (capacity == size)
            extendStack();
        stack_[size++] = x;
    }

    inline void secure(const DATA_TYPE x)
    {
        if (capacity == size)
            extendStack();
    }

    inline void fast_add(DATA_TYPE x) { stack_[size++] = x; }

    inline void push_back(DATA_TYPE x)
    {
        if (capacity == size)
            extendStack();
        stack_[size++] = x;
    }

    inline DATA_TYPE pop_until(const unsigned int level)
    {
        size = level;
        return stack_[size];
    }

    inline DATA_TYPE pop() { return stack_[--size]; }

    inline void pop(DATA_TYPE& x) { x = stack_[--size]; }

    inline void clear() { size = 0; }

    inline void remove(const unsigned int i) { stack_[i] = stack_[--size]; }

    inline void remove_elt(DATA_TYPE& elt)
    {
        unsigned int j = size;
        while (j && stack_[--j] != elt)
            ;
        stack_[j] = stack_[--size];
    }

    inline void set_back(const DATA_TYPE& x, const int k = 1)
    {
        stack_[size - k] = x;
    }

    inline DATA_TYPE& front(const int k = 0) { return stack_[k]; }

    inline const DATA_TYPE front(const int k = 0) const { return stack_[k]; }

    inline DATA_TYPE& back(const int k = 1) { return stack_[size - k]; }

    inline const DATA_TYPE back(const int k = 1) const
    {
        return stack_[size - k];
    }

    inline DATA_TYPE& operator[](const unsigned i)
    {
#ifdef _GLIBCXX_DEBUG
        assert(i >= 0 && i < size);
#endif
        return stack_[i];
    }

    inline const DATA_TYPE operator[](const unsigned i) const
    {
#ifdef _GLIBCXX_DEBUG
        assert(i >= 0 && i < size);
#endif
        return stack_[i];
    }

    inline Vector<DATA_TYPE>& operator=(const Vector<DATA_TYPE>& x)
    {
        if (x.capacity && !stack_) {
            initialise(0, x.capacity);
        } else if (capacity < x.size) {
            extendStack(x.capacity - capacity);
        }

        clear();

        for (unsigned int i = 0; i < x.size; ++i)
            add(x[i]);

        return *this;
    }
    //@}

    /*!@name Printing*/
    //@{
    std::ostream& display(std::ostream& os) const
    {
        os << "[";
        if (size)
            os << stack_[0];
        for (unsigned int i = 1; i < size; ++i)
            os << " " << stack_[i];
        os << "]";
        return os;
    }
    //@}
};

template <class DATA_TYPE>
std::ostream& operator<<(std::ostream& os, const Vector<DATA_TYPE>& x)
{
    return x.display(os);
}

template <class DATA_TYPE>
std::ostream& operator<<(std::ostream& os, const Vector<DATA_TYPE>* x)
{
    return (x ? x->display(os) : (os << "nill"));
}

#endif // __VECTOR_HPP
