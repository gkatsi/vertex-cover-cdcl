

#include <assert.h>
#include <iomanip>
#include <vector>

#include <Intstack.hpp>

#ifndef __WATCHED_HPP
#define __WATCHED_HPP


template <class T> class TwoWatchedThing
{

public:
    int N;

    IntStack watched;

    std::vector<T>* watched_by;
    std::vector<int>* watched_other;
    std::vector<int>* watched_index;

    TwoWatchedThing();
    TwoWatchedThing(const int n);

    virtual ~TwoWatchedThing();

    void initialise(const int n);

    void clear();

    void watch(const T x, const int w1, const int w2);
    void unwatch(const int ix1, const int w1);

    /*!@name Printing*/
    //@{
    std::ostream& display(std::ostream& os) const;
    //@}
};


template <class T> TwoWatchedThing<T>::TwoWatchedThing() {}

template <class T>
TwoWatchedThing<T>::TwoWatchedThing(const int n)
    : N(n)
{
    initialise(n);
    }

    template <class T> TwoWatchedThing<T>::~TwoWatchedThing()
    {
        delete[] watched_by;
        delete[] watched_other;
        delete[] watched_index;
    }

    template <class T> void TwoWatchedThing<T>::initialise(const int n)
    {

#ifdef _DEBUG_WATCH
        std::cout << "initialise watch struc [" << n << "]\n";
#endif

        watched.initialise(0, n - 1, n, false);
        watched_by = new std::vector<T>[n];
        watched_other = new std::vector<int>[n];
        watched_index = new std::vector<int>[n];
    }

    template <class T> void TwoWatchedThing<T>::clear()
    {
        for (int i = 0; i < watched.size; ++i) {
            watched_by[watched[i]].clear();
            watched_other[watched[i]].clear();
            watched_index[watched[i]].clear();
        }
    }

    template <class T>
    void TwoWatchedThing<T>::watch(const T x, const int w1, const int w2)
    {

        watched_index[w1].push_back(watched_by[w2].size());
        watched_index[w2].push_back(watched_by[w1].size());
        watched_other[w1].push_back(w2);
        watched_other[w2].push_back(w1);
        watched_by[w1].push_back(x);
        watched_by[w2].push_back(x);

        assert(!watched.contain(x));
        watched.add(x);

#ifdef _DEBUG_WATCH
        if (_DEBUG_WATCH) {
            std::cout << "add " << x << " to " << w1 << "'s and " << w2
                      << "'s list" << std::endl;
            vecdisplay(watched_by[w1], std::cout);
            std::cout << std::endl;
            vecdisplay(watched_by[w2], std::cout);
            std::cout << std::endl;
        }
#endif
    }

    template <class T>
    void TwoWatchedThing<T>::unwatch(const int ix1, const int w1)
    {

        // second watcher of x, [OK]
        int w2 = watched_other[w1][ix1];
        int x = watched_by[w1][ix1];

        assert(watched.contain(x));
        watched.remove(x);

#ifdef _DEBUG_UNWATCH

        if (_DEBUG_UNWATCH) {
            std::cout << " stop watching " << w1 << " and " << w2 << " with "
                      << watched_by[w1][ix1] << std::endl;

            std::cout << w1 << ":\n";
            vecdisplay(watched_by[w1], std::cout);
            std::cout << std::endl;
            vecdisplay(watched_index[w1], std::cout);
            std::cout << std::endl;
            vecdisplay(watched_other[w1], std::cout);
            std::cout << std::endl;

            std::cout << w2 << ":\n";
            vecdisplay(watched_by[w2], std::cout);
            std::cout << std::endl;
            vecdisplay(watched_index[w2], std::cout);
            std::cout << std::endl;
            vecdisplay(watched_other[w2], std::cout);
            std::cout << std::endl;
        }
#endif

        // elt to swap with x in w1's list, [OK]
        T l1 = watched_by[w1].back();
        watched_by[w1].pop_back();
        // other watcher of l1 (besides w1), [OK]
        int o2 = watched_other[w1].back();
        watched_other[w1].pop_back();
        // elt to swap with x in w2's list, [OK]
        T l2 = watched_by[w2].back();
        watched_by[w2].pop_back();
        // other watcher of l2 (besides w2), [OK]
        int o1 = watched_other[w2].back();
        watched_other[w2].pop_back();

        // index of x in w2's list [MUT!!]
        int ix2 = watched_index[w1][ix1];

        if (l1 != x) {
#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << " move " << l1 << " to " << watched_by[w1][ix1]
                          << "'s slot in " << w1 << "'s list" << std::endl
                          << "=>\n";
            }
#endif

            // move l1 to x's slot in w1
            watched_by[w1][ix1] = l1;
            watched_other[w1][ix1] = o2;
            int il1o2 = watched_index[w1].back();
            watched_index[w1].pop_back();
            watched_index[w1][ix1] = il1o2;

#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << w1 << ":\n";
                vecdisplay(watched_by[w1], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_index[w1], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_other[w1], std::cout);
                std::cout << std::endl;
            }
#endif

            // change its pointer in o2
            watched_index[o2][il1o2] = ix1;

#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << " and repoint its counterpart in " << o2
                          << "'s list" << std::endl;
                std::cout << o2 << ":\n";
                vecdisplay(watched_by[o2], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_index[o2], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_other[o2], std::cout);
                std::cout << std::endl;
            }
#endif
        } else
            watched_index[w1].pop_back();

        if (l2 != x) {
#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << " move " << l2 << " to " << watched_by[w2][ix2]
                          << "'s slot in " << w2 << "'s list" << std::endl
                          << "=>\n";
            }
#endif

            // move l2 to x's slot in w2
            watched_by[w2][ix2] = l2;
            watched_other[w2][ix2] = o1;
            int il2o1 = watched_index[w2].back();
            watched_index[w2].pop_back();
            watched_index[w2][ix2] = il2o1;

#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << w2 << ":\n";
                vecdisplay(watched_by[w2], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_index[w2], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_other[w2], std::cout);
                std::cout << std::endl;
            }
#endif

            // change its pointer in o1
            watched_index[o1][il2o1] = ix2;

#ifdef _DEBUG_UNWATCH
            if (_DEBUG_UNWATCH) {
                std::cout << " and repoint its counterpart in " << o1
                          << "'s list" << std::endl;
                std::cout << o1 << ":\n";
                vecdisplay(watched_by[o1], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_index[o1], std::cout);
                std::cout << std::endl;
                vecdisplay(watched_other[o1], std::cout);
                std::cout << std::endl;
            }
#endif
        } else {
            watched_index[w2].pop_back();
        }
    }

    template <class T>
    std::ostream& TwoWatchedThing<T>::display(std::ostream& os) const
    {
        for (int i = 0; i < N; ++i) {
            os << "[";
            vecdisplay(os, watched_by[i]);
            os << "]" << std::endl;
        }

        return os;
    }



#endif // __WATCHED_HPP
