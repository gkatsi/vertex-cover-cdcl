#include <Clik.hpp>
#include <minicsp/core/utils.hpp>
#include <minicsp/core/cons.hpp>

using namespace ::minicsp;

cons_coloring::cons_coloring(Graph& G, Solver& s, std::vector<cspvar>& vars, int n) : G(G), s(s), vars(vars), n(n) {}

void cons_coloring::echanger(int i, int j) {
	//i et j sont des indices du tableau
	int aux = nodes[i];
	nodes[i] = nodes[j];
	nodes[j] = aux;
}
void cons_coloring::quickSort(int deb, int pivot, int fin) {
	//deb pivot et fin sont des indices du tableau pas des nodes
	int degP = degs[nodes[pivot]];
	echanger(pivot, fin);
	int j = deb;
		//premier supérieur à pivot puis pivot
	for (int i = deb; i < fin; i++) {
		if (degs[nodes[i]] < degP) {
			echanger(i, j);
				//tester i!=j rentable ?
			j++;
		}
	}
	echanger(j, fin);
		//met pivot à sa place
	if (deb < j-1)
		quickSort(deb, deb, j-1);
			//choix pivot ?
	if (j+1 < fin)
		quickSort(j+1, j+1, fin);
			//choix pivot ?
}

void cons_coloring::countingSort() {
	std::vector<int> keys(n);
	std::vector<int> inds(n, 0);
	int ind = 0;
	for (int i = 0; i < n; i++) {
		keys[degs[i]] ++;
	}
	for (int i = n-1; i >= 0; i--) {
		inds[i] = ind;
		ind += keys[i];
	}
	for (int i = n-1; i >= 0; i--) {
		nodes[inds[degs[i]]] = i;
		inds[degs[i]] ++;
	}
	/*
	for (int i = 0; i < n; i++) {
		std::cout << nodes[i] + 1 << " ";
	}
	std::cout << std::endl;
	*/
}

void cons_coloring::rankInit() {
	for (int i = 0; i < n; i++) {
		rank[nodes[i]] = n - i;
	}
}
void cons_coloring::initialise(int k) {
	nb = k;
	if (nb == 0) {
		lb = 0;
	} else {
		nodes.resize(n, -1);
		degs.resize(n, 0);
		rank.resize(n, 0);
		for (int i = 0; i < n; i++) {
			nodes[i] = i;
				//numerote les sommets de 0 à n-1
			degs[i] = G.neighbor[i].size();
				//renseigne les degrès des sommets
		}
		//quickSort(0, 0, n-1);
			//choix pivot ?
		countingSort();
		rankInit();
		nbF = 0;
		//clikk = clik();
			//il faut encore colorer la clique
		//lb = clikk.size();
		//cliks(ancien entier); à adapter à nb
		BKCliks();
	}
}

//OLD
// gives one clique
std::vector<int> cons_coloring::clik() {
	std::vector<int> clique(0);
	BitSet dispo(0, n-1, BitSet::empt);
	//ici ajout est l'indice de nodes courant
	int ajout = 0;
	while (ajout < n) {
		clique.push_back(nodes[ajout]);
		dispo.union_with(G.matrix[nodes[ajout]]);
		ajout ++;
		while (ajout < n && dispo.fast_contain(nodes[ajout])) {
			ajout++;
		}
	}
	return clique;
}

//OLD
// gives the number of cliques asked for
void cons_coloring::cliks(int ka) {
	int depart = 0;
	std::vector<int> clique(0);
	std::vector<cspvar> clique2(0);
	BitSet dispo(0, n-1, BitSet::empt);
	int ajout;
	bool first;
	for (int k = 0; k < ka; k++) {
		clique.clear();
		dispo.intersect_with(BitSet::empt);
		ajout = depart;
		depart++; //si veux juste incrémenter
		first = true;

		while (ajout < n) {
			clique.push_back(nodes[ajout]);
			dispo.union_with(G.matrix[nodes[ajout]]);
			ajout ++;
			while (dispo.fast_contain(nodes[ajout]) && ajout < n) {
				//Si veux premier qui n'est pas dans la clique
				/*if (first) {
					depart = ajout;
					first = false;
				}*/
				ajout++;
			}
		}
		
		clique2.resize(clique.size());
		std::transform(std::begin(clique), std::end(clique), std::begin(clique2),
					   [this](int ci) { return vars[ci]; });
		if (k == 0) {
			//remplace la fonction clik
			lb = clique.size();
			for (int i = 0; i < clique.size(); i++) {
				vars[clique[i]].assign(s, i+1, NO_REASON);
				//std::cout << clique[i] + 1 << " ";
			}
		} else {
			post_alldiff(s, clique2, false);
			/*for (auto ci: clique) {
				std::cout << ci + 1 << " ";
			}*/
		}
		//std::cout << std::endl;
	}
}

void cons_coloring::BKCliks() {
	std::vector<int> R(0);
	std::vector<int> P(n);
	std::vector<int> X(0);
	R.reserve(P.size());
	for (int i = 0; i < n; i++) {
		P[i] = nodes[n-i-1];
		//std::cout << P[i]+1 << " ";
	}
	//std::cout << std::endl;
	first = true;

	BK(R, std::move(P), std::move(X));
}
void cons_coloring::traiterClique(std::vector<int>& R) {
	for (int i = 0; i < R.size(); i++) {
		for (int j = i+1; j < R.size(); j++) {
			if (G.matrix[R[i]].fast_contain(R[j])) {
				std::cout << "Problème: pas une clique" << std::endl;
			}
		}
	}
	
	if (first) {
		lb = R.size();
		first = false;
		//std::cout << "Clique coloré ";
		for (int i = 0; i < R.size(); i++) {
			vars[R[i]].assign(s, i+1, NO_REASON);
		}
		//std::cout << std::endl;
	} else {
		if (R.size() > lb)
			lb = R.size();
		std::vector<cspvar> Rclik(R.size());
		std::transform(std::begin(R), std::end(R), std::begin(Rclik),
						[this](int ci) { return vars[ci]; });
		post_alldiff(s, Rclik, false);
	}
}
void cons_coloring::BK(std::vector<int>& R, std::vector<int> P, std::vector<int> X) {
	if (P.empty() && X.empty()) {
		/*for (int i = 0; i < R.size(); i++) {
			std::cout << R[i]+1 << " ";
		}
		std::cout << std::endl;*/
		++nbF;
		traiterClique(R);
	} else {
		while (!P.empty()) {
			int v = P.back();
			//std::cout << "Sommet traité " << v+1 << std::endl;
			P.pop_back();
			/*std::cout << "P ";
			for (int i = 0; i < P.size(); i++) {
				std::cout << P[i]+1 << " ";
			}
			std::cout << std::endl;*/
			R.push_back(v);
			
			std::vector<int> P2;
			P2.reserve(degs[v]);
			for (auto pi: P) {
				if (!G.matrix[pi].fast_contain(v))
					P2.push_back(pi);
			}
			std::vector<int> X2;
			X2.reserve(degs[v]);
			for (auto xi: X) {
				if (!G.matrix[xi].fast_contain(v))
					X2.push_back(xi);
			}

			if (nb == -1 || nbF < nb)
				BK(R, std::move(P2), std::move(X2));
			
			R.pop_back();
			X.push_back(v);
		}
	}
}


/* ! Version si le BitSet n'est pas inversé ! et nodes par deg croissant
std::vector<int> cons_coloring::clik_old() {
	std::vector<int> clique(0);
	BitSet dispo(0, n-1, BitSet::full);
	//ici ajout est l'indice de nodes courant
	int ajout = n-1;
	clique.push_back(nodes[ajout]);
	dispo.intersect_with(G.matrix[nodes[ajout]]);
	while (!dispo.empty()) {
		ajout--;
			//permet pas tester celui qu'on vient d'ajouter
		while (!dispo.fast_contain(nodes[ajout])) {
				//si plante ici psk dispo non vide alors que tt parcouru
			ajout--;
		}
		clique.push_back(nodes[ajout]);
		dispo.intersect_with(G.matrix[nodes[ajout]]);
	}
	
	return clique;
}
*/
