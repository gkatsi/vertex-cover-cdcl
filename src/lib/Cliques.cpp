#include <Cliques.hpp>
#include <minicsp/core/utils.hpp>

using namespace ::minicsp;

cons_eqvar::cons_eqvar(Graph& G, Solver& s, std::vector<cspvar>& vars, std::vector<Lit>& eq, int n, int& ub) :
		G(G), s(s), vars(vars), eq(eq), n(n), ub(ub) {
	clique.resize(n, 0);
	v.resize(0);
	dispo = BitSet(0, n-1, BitSet::empt);
	
	for (int i = 0; i != n; i++) {
		for (int j = i+1; j != n; j++) {
			if (!G.exists_edge(i,j)) {
				s.wake_on_lit(var(eq[j*n+i]), this,  reinterpret_cast<void*>(j*n+i));
				s.schedule_on_lit(var(eq[j*n+i]), this);
			}
		}
	}
	indptr = s.alloc_backtrackable(sizeof(int));
	s.deref<int>(indptr) = 0;
}

void cons_eqvar::backtrack(int bk) {
	for (int i = v.size()-1; i > bk; i--) {
		G.matrix[v[i].first].remove(v[i].second);
		G.matrix[v[i].second].remove(v[i].first);
		v.pop_back();
	}
}
void cons_eqvar::sync_graph(bool ajout, int ind_eq) {
	int ind = s.deref<int>(indptr);
	backtrack(ind);
	if (ajout) {
		int x = ind_eq / n;
		int y = ind_eq % n;
		G.matrix[x].add(y);
		G.matrix[y].add(x);
		std::pair<int,int> p;
		p.first = x;
		p.second = y;
		v.push_back(p);
		//incrémenter indptr
		s.deref<int>(indptr) = ind+1;
	}
}

//TO DO: cut when needed
Clause* cons_eqvar::propagate(Solver& s) {
	calcul_clique();
	if (ub < clique.size()) {
		// Ici on aimerai créer une coupure (renvoyer une clause au lieu de NO_REASON)

		/*std::cout << "Coupure ?" << std::endl;
		for (int i = 0; i < clique.size(); i++) {
			std::cout << clique[i] << " ";
		}
		std::cout << std::endl;
		std::cout << ub << std::endl;*/
		/*r.clear();
		for (int i = 0; i < clique.size(); i++) {
			for (int j = i+1; j < clique.size(); j++) {
				if (clique[j]>clique[i]) {
					if (eq[clique[j]*n+clique[i]] != lit_Undef)
						r.push(eq[clique[j]*n+clique[i]]);
				} else {
					if (eq[clique[i]*n+clique[j]] != lit_Undef)
						r.push(eq[clique[i]*n+clique[j]]);
				}
			}
			//minicsp::Lit consub = minicsp::Lit(s.cspvarleqi(vars[clique[i]], ub));
			//r.push(consub);
		}*/
		//std::cout << r.size() << std::endl;
		/*for (int i = 0; i< r.size(); i++) {
			std::cout << toInt(r[i]) << " ";
		}*/
		//std::cout << std::endl;
		//explain();
		//Clause *c = Clause_new(r);
		//return c;
	}
	return NO_REASON;
}

Clause* cons_eqvar::wake_advised(Solver& s, Lit l, void* hint) {
	intptr_t ind_eq = reinterpret_cast<intptr_t>(hint);
	sync_graph(sign(l) == false, ind_eq);
	return NO_REASON;
}

void cons_eqvar::calcul_clique() {
	nodes.resize(n, -1);
	degs.resize(n, 0);
	for (int i = 0; i < n; i++) {
		nodes[i] = i;
			//numerote les sommets de 0 à n-1
		degs[i] = G.neighbor[i].size();
			//renseigne les degrès des sommets
	}
	counting_sort();
	clik();
}

void cons_eqvar::clik() {
	dispo.intersect_with(BitSet::empt);
	clique.clear();
	//ici ajout est l'indice de nodes courant
	int ajout = 0;
	while (ajout < n) {
		clique.push_back(nodes[ajout]);
		dispo.union_with(G.matrix[nodes[ajout]]);
		ajout ++;
		while (ajout < n && dispo.fast_contain(nodes[ajout])) {
			ajout++;
		}
	}
}

void cons_eqvar::counting_sort() {
	std::vector<int> keys(n);
	std::vector<int> inds(n, 0);
	int ind = 0;
	for (int i = 0; i < n; i++) {
		keys[degs[i]] ++;
	}
	for (int i = n-1; i >= 0; i--) {
		inds[i] = ind;
		ind += keys[i];
	}
	for (int i = n-1; i >= 0; i--) {
		nodes[inds[degs[i]]] = i;
		inds[degs[i]] ++;
	}
	/*
	for (int i = 0; i < n; i++) {
		std::cout << nodes[i] + 1 << " ";
	}
	std::cout << std::endl;
	*/
}

void cons_eqvar::explain() {
	r.clear();
	for (int i = 0; i < n; i++) {
		if (vars[i].domsize(s) == 1) {
			r.push(~Lit(s.cspvareqi(vars[i], vars[i].min(s))));
		} else {
			r.push(Lit(s.cspvarleqi(vars[i], vars[i].min(s)-1)));
			r.push(~Lit(s.cspvarleqi(vars[i], vars[i].max(s))));
			if (vars[i].domsize(s) != vars[i].max(s) - vars[i].min(s) + 1) {
				for (int j = vars[i].min(s)+1; j < vars[i].max(s); j++) {
					if (!vars[i].indomain(s, j)) {
						r.push(Lit(s.cspvareqi(vars[i], j)));
					}
				}
			}
		}
	}
}
