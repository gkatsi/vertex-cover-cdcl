
#include <Cmdline.hpp>
#include <Prop.hpp>
#include <map>

Cmdline::Cmdline(const std::string& message, const char delimiter,
    const std::string& version, bool helpAndVersion)
    : CmdLine(message, delimiter, version, helpAndVersion)
{
    initialise();
}

Cmdline::~Cmdline()
{

    delete fileArg;
    delete seedArg;
    delete boundArg;
    delete timeArg;
    delete printsolArg;
    delete printstaArg;
    delete printmodArg;
    delete printparArg;
    delete printinsArg;
    delete printcmdArg;
    delete checkArg;
    delete weightArg;
    delete verbosityArg;
    delete restartArg;
    delete dominanceArg;
    delete cliqueArg;
    delete dsaturThresholdArg;

    delete r_allowed;
    delete b_allowed;
    delete i_allowed;
}

void Cmdline::initialise()
{

    // INPUT FILE
    fileArg = new TCLAP::UnlabeledValueArg<std::string>("file", "instance file",
        true, "data/DIMACS_cliques/brock200_1.clq", "string");
    add(*fileArg);

    // TIME LIMIT
    timeArg = new TCLAP::ValueArg<double>(
        "t", "time_limit", "time limit", false, 0, "double");
    add(*timeArg);

    // VERBOSITY LEVEL
    verbosityArg = new TCLAP::ValueArg<int>(
        "v", "verbosity", "verbosity level", false, 1, "int");
    add(*verbosityArg);

    // RANDOM SEED
    seedArg = new TCLAP::ValueArg<int>(
        "s", "seed", "random seed", false, 12345, "int");
    add(*seedArg);

    // DSATUR SWITCH THRESHOLD
    dsaturThresholdArg = new TCLAP::ValueArg<int>(
        "", "threshold", "Threshold dsatur/greedy", false, 1, "int");
    add(*dsaturThresholdArg);

    // BOUND ALG
    std::vector<std::string> ballowed;
    for (auto& kp : vc_options::clqcover_alg_names)
        ballowed.push_back(kp.first);
    // ballowed.push_back("random"); // removed because "parition" is not implemented for it
    b_allowed = new TCLAP::ValuesConstraint<std::string>(ballowed);
    boundArg = new TCLAP::ValueArg<std::string>(
        "b", "bound", "bound algorithm", false, "greedy", b_allowed);
    add(*boundArg);

    // RESTART POLICY
    std::vector<std::string> rallowed;
    rallowed.push_back("no");
    rallowed.push_back("geom");
    rallowed.push_back("luby");
    r_allowed = new TCLAP::ValuesConstraint<std::string>(rallowed);
    restartArg = new TCLAP::ValueArg<std::string>(
        "r", "restart", "restart policy", false, "geom", r_allowed);
    add(*restartArg);

    // USE CLAUSE LEARNING
    std::vector<int> iallowed{0, 1, 2, 3};
    i_allowed = new TCLAP::ValuesConstraint<int>(iallowed);

    learningArg = std::make_unique<TCLAP::ValueArg<int>>("l", "learning",
        "Clause learning (0=none/1=naive expl/2=fast/3=greedy)", false, 1,
        i_allowed);
    add(*learningArg);

    // DO PRUNING
    pruneArg = std::make_unique<TCLAP::ValueArg<int>>("p", "prune",
        "Do bound-based pruning (0=none/1=naive expl/2=fast/3=greedy)", false,
        0, i_allowed);
    add(*pruneArg);

    // USE DOMINANCE
    std::vector<int> dallowed{0,1,2};
    d_allowed = std::make_unique<TCLAP::ValuesConstraint<int>>(dallowed);

    dominanceArg = new TCLAP::ValueArg<int>("d", "dominance",
        "0 -> no dominance; 1 -> low degree; 2 -> clique", false, 0,
        d_allowed.get());
    add(*dominanceArg);

    // SOLVE THE CLIQUE PROBLEM
    cliqueArg = new TCLAP::SwitchArg(
        "c", "clique", "Solve the clique problem", false);
    add(*cliqueArg);

    // TAKE WEIGHT INTO ACCOUNT
    weightArg = new TCLAP::SwitchArg(
        "", "weighted", "Minimize weighted cover", false);
    add(*weightArg);

    // CHECK BOUNDS
    checkArg = new TCLAP::SwitchArg("", "checked", "Check the bounds", false);
    add(*checkArg);

    // PRINT MODEL
    printmodArg
        = new TCLAP::SwitchArg("", "print_mod", "Print the model", false);
    add(*printmodArg);

    // PRINT CMDLINE
    printcmdArg
        = new TCLAP::SwitchArg("", "print_cmd", "Print the cmd line", false);
    add(*printcmdArg);

    // PRINT STATISTICS
    printstaArg
        = new TCLAP::SwitchArg("", "print_sta", "Print the statistics", false);
    add(*printstaArg);

    // PRINT INSTANCE
    printinsArg
        = new TCLAP::SwitchArg("", "print_ins", "Print the instance", false);
    add(*printinsArg);

    // PRINT PARAMETERS
    printparArg
        = new TCLAP::SwitchArg("", "print_par", "Print the parameters", false);
    add(*printparArg);

    // PRINT SOLUTION
    printsolArg = new TCLAP::SwitchArg(
        "", "print_sol", "Print the solution, if found", false);
    add(*printsolArg);

    // NUMBER OF NODES
    numnodesArg = std::make_unique<TCLAP::ValueArg<int>>(
        "n", "numnodes", "number of nodes (random graph)", false, 10, "int");
    add(*numnodesArg);

        // CLIQUES
        cliquesArg = std::make_unique<TCLAP::ValueArg<int>>(
                "", "cliques", "nb cliques", false, 1, "int");
        add(*cliquesArg);

    // NUMBER OF EDGES
    numedgesArg = std::make_unique<TCLAP::ValueArg<int>>(
        "e", "numedges", "number of edges (random graph)", false, 25, "int");
    add(*numedgesArg);

    // BRANCHING HEURISTIC
    std::vector<std::string> branch_allowed_v;
    for (auto& kp : brancher_names)
        branch_allowed_v.push_back(kp.first);
    branch_allowed = std::make_unique<TCLAP::ValuesConstraint<std::string>>(
        branch_allowed_v);
    branchArg = std::make_unique<TCLAP::ValueArg<std::string>>("", "branch",
        "branching heuristic", false, "vsids", branch_allowed.get());
    add(*branchArg);

    std::vector<std::string> branch_config_allowed_v{
        "this", "this2vsids", "vsids2this"};
    branch_config_allowed
        = std::make_unique<TCLAP::ValuesConstraint<std::string>>(
            branch_config_allowed_v);
    branchConfigArg = std::make_unique<TCLAP::ValueArg<std::string>>("",
        "branchconfig", "branching heuristic VSIDS switch", false, "vsids",
        branch_config_allowed.get());
    add(*branchConfigArg);

    branchCutoffArg = std::make_unique<TCLAP::ValueArg<int>>("", "branchcutoff",
        "cutoff to switch from/to VSIDS", false, 1e5, "int");
    add(*branchCutoffArg);

    branch_sequenceArg = std::make_unique<TCLAP::ValueArg<std::string>>("",
        "serial-branch-sequence", "serial sequence of branchers", false, "",
        "Comma-separated list of branchers");
    add(*branch_sequenceArg);

    branch_scheduleArg = std::make_unique<TCLAP::ValueArg<std::string>>("",
        "serial-branch-schedule", "serial sequence of brancher cutoffs", false,
        "", "Comma-separated list of integers");
    add(*branch_scheduleArg);

    // for debugging, solution dumping and reading
    dumpsolArg = std::make_unique<TCLAP::SwitchArg>(
        "", "dump_sol", "(Debug) Dump raw solutions", false);
    add(*dumpsolArg);

    debugsolArg
        = std::make_unique<TCLAP::UnlabeledValueArg<std::string>>("debug_sol",
            "(Debug) Read a correct solution from file", false, "", "string");
    add(*debugsolArg);

    printdimacsArg = std::make_unique<TCLAP::ValueArg<std::string>>(
        "", "print_dimacs", "Print graph to the specified file instead of solving",
        false, "", "filename");
    add(*printdimacsArg);

    printmtsArg = std::make_unique<TCLAP::ValueArg<std::string>>("",
        "print_mts", "Print graph to the specified file instead of solving",
        false, "", "filename");
    add(*printmtsArg);

    // minicsp tracing
    traceArg = std::make_unique<TCLAP::SwitchArg>(
        "", "trace", "(Debug) Enable minicsp tracing", false);
    add(*traceArg);

    // upper bound
    lbArg = std::make_unique<TCLAP::ValueArg<int>>("", "lb",
        "Assume given (clique) lower bound", false,
        0, "int");
    add(*lbArg);

    // USE MAXSAT BOUND
    maxsatArg = std::make_unique<TCLAP::ValueArg<int>>("", "maxsat",
        "Use maxsat bound (0 -> no, 1 -> quick, 2 -> full AC)", false, 0,
        "int");
    add(*maxsatArg);

    // RATIO OF NEIGHBORS IN THE EXPLANATION ABOVE WHICH USE NEGATIVE LITERAL
    expArg = std::make_unique<TCLAP::ValueArg<float>>("", "exp",
        "critical ratio of neighbors in the explanation (def=0)", false, 0.0,
        "float");
    add(*expArg);

    sortedArg = std::make_unique<TCLAP::ValueArg<std::string>>("", "sorted",
        "Sort the vertices by degree", false, "no", "{'min', 'max', 'no'}");
    add(*sortedArg);

    // DEFERRED EXPLANATIONS
    deferredArg = std::make_unique<TCLAP::SwitchArg>(
        "", "deferred", "Use deferred explanations", false);
    add(*deferredArg);

        // TRIANGLE USE
        triangleArg = std::make_unique<TCLAP::SwitchArg>(
                "", "triangle", "Use triangle constraints", false);
        add(*triangleArg);

    // COMPONENT CACHING
    compcachingArg = std::make_unique<TCLAP::SwitchArg>(
        "", "compcaching", "Use component caching", false);
    add(*compcachingArg);

    // USE BUSS RULE
    bussArg = std::make_unique<TCLAP::SwitchArg>(
        "", "buss", "Use Buss rule (default false)", false);
    add(*bussArg);

    // SWITCH DYNPROG BOUND
    dynprogArg = std::make_unique<TCLAP::SwitchArg>("", "dynprog",
        "Switch dynamic programming bound ON (default: OFF)", false);
    add(*dynprogArg);

    // SWITCH DYNPROG BOUND
    complementArg = std::make_unique<TCLAP::SwitchArg>(
        "", "complement", "Flip the graph", false);
    add(*complementArg);

    // TOLERANCE FOR SCORE-BASED HEURISTICS
    heuristic_toleranceArg = std::make_unique<TCLAP::ValueArg<float>>("", "heuristic_tolerance",
        "tolerance for creating ties", false, 1.0,
        "float");
    add(*heuristic_toleranceArg);

    // ORDER FOR GREEDY CLIQUE COVER
    greedy_orderArg = std::make_unique<TCLAP::ValueArg<int>>(
        "", "greedy-order", "0=dynamic, 1=static", false, 0, "int");
    add(*greedy_orderArg);

    // whether to try to do additional pruning which sets variables to
    // false (putting the vertex in the IS)
    pruneToISArg = std::make_unique<TCLAP::SwitchArg>("", "prunetois",
        "Additional pruning which puts vertices in the IS (default: OFF)",
        false);
    add(*pruneToISArg);
}

std::string Cmdline::get_restart() { return restartArg->getValue(); }

int Cmdline::get_learning() { return learningArg->getValue(); }

int Cmdline::get_pruning() { return pruneArg->getValue(); }

int Cmdline::get_dominance() { return dominanceArg->getValue(); }

bool Cmdline::get_clique() { return cliqueArg->getValue(); }

std::string Cmdline::get_filename() { return fileArg->getValue(); }

int Cmdline::get_seed() { return seedArg->getValue(); }

int Cmdline::get_threshold() { return dsaturThresholdArg->getValue(); }

std::string Cmdline::get_boundalgo() { return boundArg->getValue(); }
int Cmdline::get_bound()
{
    auto it = vc_options::clqcover_alg_names.find(boundArg->getValue());
    if (it != vc_options::clqcover_alg_names.end())
        return it->second;
    return -1;
}

int Cmdline::get_verbosity() { return verbosityArg->getValue(); }

int Cmdline::get_cliques() { return cliquesArg->getValue(); }

double Cmdline::get_time() { return timeArg->getValue(); }

bool Cmdline::checked() { return checkArg->getValue(); }

bool Cmdline::weighted() { return weightArg->getValue(); }

bool Cmdline::print_cmd() { return printcmdArg->getValue(); }

bool Cmdline::print_mod() { return printmodArg->getValue(); }

bool Cmdline::print_sol() { return printsolArg->getValue(); }

bool Cmdline::print_par() { return printparArg->getValue(); }

bool Cmdline::print_ins() { return printinsArg->getValue(); }

bool Cmdline::print_sta() { return printstaArg->getValue(); }

int Cmdline::get_numnodes() { return numnodesArg->getValue(); }

int Cmdline::get_numedges() { return numedgesArg->getValue(); }

std::map<std::string, BranchingHeuristic> brancher_names = {{"vsids", VSIDS},
    {"mindegree", MINDEGREE}, {"maxdegree", MAXDEGREE},
    {"mindommindeg", MINDOMMINDEG}, {"mindommaxdeg", MINDOMMAXDEG},
    {"mindomtimedeg", MINDOMTIMEDEG}, {"mindomoverdeg", MINDOMOVERDEG},
    {"mindomtimeweiclq", MINDOMTIMEWEICLQ},
    {"mindomoverweiclq", MINDOMOVERWEICLQ},
    {"mindomtimeweiis", MINDOMTIMEWEIIS}, {"mindomoverweiis", MINDOMOVERWEIIS},
    {"mindomtimepweiclq", MINDOMTIMEPWEICLQ},
    {"mindomoverpweiclq", MINDOMOVERPWEICLQ},
    {"mindomtimepweiis", MINDOMTIMEPWEIIS},
    {"mindomoverpweiis", MINDOMOVERPWEIIS}, {"lastclq", LASTCLQ},
    {"maxlb", MAXLB}, {"maxlboverdeg", MAXLBOVERDEG},
    {"maxlbtimedeg", MAXLBTIMEDEG}, {"lex", LEX}, {"veclex", VECLEX},
    {"minwneb", MINNW}, {"maxwneb", MAXNW}, {"serial", SERIAL}};
BranchingHeuristic Cmdline::get_branch()
{
    return brancher_names[branchArg->getValue()];
}

std::string Cmdline::get_branch_name() { return branchArg->getValue(); }

BranchingConfig Cmdline::get_branch_config()
{
    std::map<std::string, BranchingConfig> names = {
        {"this", This}, {"this2vsids", This2VSIDS}, {"vsids2this", VSIDS2This}};
    return names[branchConfigArg->getValue()];
}

int Cmdline::get_branch_switch_cutoff() { return branchCutoffArg->getValue(); }

std::string Cmdline::get_branch_sequence()
{
    return branch_sequenceArg->getValue();
}

std::string Cmdline::get_branch_schedule()
{
    return branch_scheduleArg->getValue();
}

bool Cmdline::get_dumpsol() { return dumpsolArg->getValue(); }

std::string Cmdline::get_debugsol() { return debugsolArg->getValue(); }

bool Cmdline::get_trace() { return traceArg->getValue(); }

std::string Cmdline::print_dimacs() { return printdimacsArg->getValue(); }

std::string Cmdline::print_mts() { return printmtsArg->getValue(); }

int Cmdline::get_lb() { return lbArg->getValue(); }

int Cmdline::get_maxsat() { return maxsatArg->getValue(); }

float Cmdline::get_exp() { return expArg->getValue(); }

std::string Cmdline::get_sorted() { return sortedArg->getValue(); }

bool Cmdline::get_deferred() { return deferredArg->getValue(); }

bool Cmdline::get_triangle() { return triangleArg->getValue(); }

bool Cmdline::get_compcaching() { return compcachingArg->getValue(); }

bool Cmdline::get_buss() { return bussArg->getValue(); }

bool Cmdline::get_dynprog() { return dynprogArg->getValue(); }

bool Cmdline::get_complement() { return complementArg->getValue(); }

float Cmdline::heuristic_tolerance() { return heuristic_toleranceArg->getValue(); }

int Cmdline::get_greedy_order() { return greedy_orderArg->getValue(); }

bool Cmdline::get_prune_to_is() { return pruneToISArg->getValue(); }
