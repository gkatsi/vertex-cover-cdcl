

#include <Graph.hpp>
#include <Algorithm.hpp>

// #define _TRACE_MAXSAT
// #define _CHECK_MAXSAT
// #define _CHECK_CC
// #define _DEBUG_WCOL

using std::swap;

int distance(const int x, const int y)
{
    if (x < y)
        return y - x;
    return x - y;
}

int pos_bias(const int x, const int y)
{
    if (x < y)
        return y - x;
    return 100 * (x - y);
}

DualProblem::DualProblem(Graph* g)
{
    G = g;
    auto n = G->capacity;
    resize(n);
    partition_of.resize(n);
    weight_contribution.resize(n);
    layer_csize.push_back(0);
}

DualProblem::~DualProblem() {}

void DualProblem::resize(const int m) {

    int old_size = candidate.size();
    int N = G->capacity;

    // std::cout << "INIT " << N << std::endl;

    if (m > old_size) {
        candidate.resize(m);
        candidate_sz.resize(m);
        for (unsigned i = old_size; i < m; ++i) {
            candidate[i].initialise(0, N - 1, BitSet::full);
            candidate_sz[i] = N;
        }
        partition.resize(m);
        partition_sz.resize(m);
        partition_weight.resize(m);
        for (unsigned i = old_size; i < m; ++i) {
            partition[i].initialise(0, N - 1, BitSet::empt);
            partition_sz[i] = 0;
        }
        partition_list.resize(m);
    }
}

void DualProblem::clear()
{

#ifdef _CHECK_CC
    check("before  clear");
#endif

    auto n{G->capacity};
    for (auto i : sorted) {
        candidate[i].fill();
        partition[i].clear();
        candidate_sz[i] = n;
        partition_sz[i] = 0;
        partition_weight[i] = 0;
        partition_list[i].clear();
    }
    dual_cost = 0;
    sorted.clear();
    layer_csize.resize(1);

    for (auto v : G->node) {
        partition_of[v].clear();
        weight_contribution[v].clear();
    }

#ifdef _DEBUG_WCOL
    std::cout << "CLEAR\n" << std::endl;
#endif

#ifdef _CHECK_CC
    check("after clear");
#endif
}

void DualProblem::to_size(const int k)
{

#ifdef _CHECK_CC
    check("before to_size");
#endif

    auto n{G->capacity};

    int l = layer_csize.size() - 1;
    while (sorted.size() > k) {
        auto i = sorted.back();

        candidate[i].fill();
        partition[i].clear();
        candidate_sz[i] = n;
        partition_sz[i] = 0;

        dual_cost -= partition_weight[i];
        partition_weight[i] = 0;
        if (--layer_csize[l]==0)
            --l;
        sorted.pop_back();
    }

#ifdef _CHECK_CC
    check("after to_size");
#endif
}

// void DualProblem::new_clique()
// {
//              int N =  G->capacity;
//     int l = layer_csize.size() - 1;
//     auto max_cl{layer_csize[l]};
//     auto n{max_cl + 1};
//     auto prev_size{partition.size()};
//     if (n > prev_size) {
//         candidate.resize(n);
//         candidate_sz.resize(n);
//         for (unsigned i = prev_size; i < n; ++i) {
//             candidate[i].initialise(0, N - 1, BitSet::full);
//             candidate_sz[i] = N;
//         }
//         partition.resize(n);
//         partition_sz.resize(n);
//         partition_weight.resize(n);
//         for (auto i = prev_size; i < n; ++i) {
//             partition[i].initialise(0, N - 1, BitSet::empt);
//             partition_sz[i] = 0;
//         }
//         // partition_of.resize(n);
//         partition_list.resize(n);
//         // weight_contribution.resize(n);
//     }
// }

int DualProblem::_insert(const int x)
{
    int l = layer_csize.size() - 1;
    int min_cl = layer_csize[l - 1];
    int max_cl = layer_csize[l];
    int i;
    for (i = min_cl; i < max_cl; ++i) {
        if (candidate[i].fast_contain(x))
            break;
    }

    if (i == max_cl) {
        // new clique of index max_cl+1

        // new_clique();
        sorted.push_back(max_cl);
        max_cl = ++layer_csize[l];

        resize(max_cl);
    }

#ifdef _DEBUG_WCOL
    std::cout << "color " << x << " with " << i << std::endl;
#endif

    partition[i].fast_add(x);
    partition_list[i].push_back(x);
    ++partition_sz[i];

    return i;
}

int DualProblem::insert(const int x)
{
    int i = _insert(x);
    candidate[i].intersect_with(G->matrix[x]);
    return i;
}

int DualProblem::insert(const int x, BitSet& delta)
{
    int i = _insert(x);
    candidate[i].intersect_with(G->matrix[x], delta);
    return i;
}

int DualProblem::push(const int x, const int nx)
{
#ifdef _CHECK_CC
    check("before push x");
#endif

    int l = layer_csize.size() - 1;
    int i = layer_csize[l]; // WORKS BECAUSE WE NEVER REPAIR!!
    weight_type w = G->weight[x];

    resize(layer_csize[l] + 1);
    // new_clique();
//
// int i = layer_csize[l - 1];
// while (i <= layer_csize[l]) {
//     if (partition_sz[i] == 0)
//         break;
//     ++i;
// }

#ifdef _DEBUG_WCOL
    std::cout << "color(p) " << x << " with " << i << std::endl;
#endif

    partition[i].fast_add(x);
    partition_of[x].push_back(i);
    weight_contribution[x].push_back(w);
    partition_sz[i] = 1;
    candidate[i].copy(G->matrix[x]);
    candidate_sz[i] = nx;
    partition_weight[i] = w;

    sorted.push_back(i);
    ++layer_csize[l];
    dual_cost += w;

#ifdef _CHECK_CC
    check("after push x");
#endif

    return i;
}

void DualProblem::push(const Graph& G)
{
#ifdef _CHECK_CC
    check("before push G");
#endif

    int l = layer_csize.size();
    new_layer();

    partition[layer_csize[l]].union_with(G.node_set);
    partition_sz[layer_csize[l]] = G.size();
    candidate[layer_csize[l]].fill();

    int maxu = -1;
    int scndmaxu = -1;
    for (auto u : G.node) {

#ifdef _DEBUG_WCOL
        std::cout << "color(g) " << u << " with " << layer_csize[l]
                  << std::endl;
#endif

        candidate[layer_csize[l]].intersect_with(G.matrix[u]);
        partition_of[u].push_back(layer_csize[l]);
        weight_contribution[u].push_back(G.weight[u]);
        if (maxu < 0 || G.weight[u] >= G.weight[maxu]) {
            maxu = u;
            scndmaxu = maxu;
        } else if (G.weight[u] >= G.weight[scndmaxu]) {
            scndmaxu = u;
        }
    }
    candidate_sz[layer_csize[l]] = candidate[layer_csize[l]].size();

    if (maxu > scndmaxu) {
        *(end(weight_contribution[maxu]) - 1) = scndmaxu;
    }

    sorted.push_back(layer_csize[l]);
    partition_weight[layer_csize[l]] = G.weight[maxu];

    ++layer_csize[l];
    dual_cost += G.weight[maxu];

#ifdef _CHECK_CC
    check("after push G");
#endif
}


// this does several things, all for the last computed layer
// 1/ compute candidate_sz
// 2/ compute the dual_cost
// 3/ compute the residual weights and the residual nodes
void DualProblem::close(std::vector<int>& vertices, std::vector<weight_type>& weight)
{
    vertices.clear();
    singleton_partitions.clear();
    int l = layer_csize.size() - 1;

    for (auto i = layer_csize[l - 1]; i < layer_csize[l]; ++i) {
        candidate_sz[i] = candidate[i].size();

        // std::cout << i << ": " << partition[i] << std::endl;

        if (partition_list[i].size() <= 1) {

            // #ifdef _DEBUG_WCOL
            //                                                          std::cout
            // <<
            // "singleton
            // clique:
            // "
            // <<
            // partition_list[i][0]
            // << " (" << weight[partition_list[i][0]] << ")\n";
            // #endif

            assert(partition_list[i].size() == 1);

            int v = partition_list[i][0];
            singleton_partitions.push_back(i);
            partition_of[v].push_back(i);
            weight_contribution[v].push_back(0);
            partition_weight[i] = 0;

#ifdef _DEBUG_WCOL
            std::cout << " [ " << v << "(" << weight[v] << "/" << weight[v]
                      << ") ]";
#endif

        } else {

            // TODO: compute the min and the max when inserting?

            auto maxvp = std::max_element(begin(partition_list[i]),
                end(partition_list[i]),
                [&](int u, int v) { return weight[u] < weight[v]; });

            auto maxv = *maxvp;
            *maxvp = *(end(partition_list[i]) - 1);
            *(end(partition_list[i]) - 1) = maxv;

            partition_list[i].pop_back();

            auto scndmaxw
                = weight[*std::max_element(begin(partition_list[i]),
                             end(partition_list[i]), [&](int u, int v) {
                                 return weight[u] < weight[v];
                             })];

#ifdef _DEBUG_WCOL
            std::cout << "[";
#endif

            for (auto v : partition_list[i]) {
                partition_of[v].push_back(i);
                weight_contribution[v].push_back(weight[v]);

#ifdef _DEBUG_WCOL
                std::cout << " " << v << "(" << weight[v] << ")";
#endif
            }

            partition_of[maxv].push_back(i);
            weight_contribution[maxv].push_back(scndmaxw);
            partition_weight[i] = scndmaxw;

#ifdef _DEBUG_WCOL
            std::cout << " " << maxv << "(" << scndmaxw << "/" << weight[maxv]
                      << ") ]";
#endif

            partition_list[i].clear();

            dual_cost += scndmaxw;

            if (weight[maxv] != scndmaxw) {
                vertices.push_back(maxv);
                weight[maxv] = weight[maxv] - scndmaxw;
            }
        }
    }

#ifdef _DEBUG_WCOL
    std::cout << std::endl;
#endif

    if (vertices.empty()) {
        // there will not be another layer, so singleton partitions of
        // this layer should contribute to the dual cost. Update also
        // weight_contribution and partition_weight accordingly
        for (auto i : singleton_partitions) {
            int v = partition_list[i][0];
            dual_cost += weight[v];
            weight_contribution[v].back() = weight[v];
            partition_weight[i] = weight[v];
        }
    } else {
        // we will construct another layer, so we can reuse vertices
        // that were in singleton partitions
        for (auto i : singleton_partitions) {
            vertices.push_back(partition_list[i][0]);
        }
    }

#ifdef _CHECK_CC
    check("after close");
#endif
}

void DualProblem::close_weak(
    std::vector<int>& vertices, std::vector<weight_type>& weight)
{
    singleton_partitions.clear();
    int l = layer_csize.size() - 1;
    int kept{0};

    for (auto i = layer_csize[l - 1]; i < layer_csize[l]; ++i) {
        candidate_sz[i] = candidate[i].size();

        if (partition_list[i].size() <= 1) {
            int v = partition_list[i][0];
            singleton_partitions.push_back(i);
            partition_of[v].push_back(i);
            weight_contribution[v].push_back(0);
            partition_weight[i] = 0;
        } else {
            auto minv = *std::min_element(begin(partition_list[i]),
                            end(partition_list[i]), [&](int u, int v) {
                                return weight[u] < weight[v];
                            });

            auto minw = weight[minv];
            dual_cost -= (partition_list[i].size() - 1) * minw;

            partition_weight[i] = minw;
            for (auto v : partition_list[i]) {
                partition_of[v].push_back(i);
                weight_contribution[v].push_back(minw);
                weight[v] -= minw;
                if (weight[v] > 0)
                    ++kept;
            }

            partition_list[i].clear();
        }
    }

    if (!kept) {
        // there will not be another layer, so singleton partitions of
        // this layer should contribute to the dual cost. Update also
        // weight_contribution and partition_weight accordingly
        for (auto i : singleton_partitions) {
            int v = partition_list[i][0];
            weight_contribution[v].back() = weight[v];
            partition_weight[i] = weight[v];
            weight[v] = 0;
        }
    }

    erase_if(vertices, [&](int v) { return weight[v] == 0; });

#ifdef _DEBUG_WCOL
    std::cout << std::endl;
#endif

#ifdef _CHECK_CC
    check("after close");
#endif
}

void DualProblem::repair(BitSet& possible)
{

#ifdef _CHECK_CC
    check("before repair");
#endif

    auto reduction{true};
    std::vector<int>::iterator cl;

    while (dual_cost > 1 && reduction) {
        reduction = false;

        cl = (sorted.begin() + dual_cost - 1);
        possible.copy(candidate[*cl]);

        while (!reduction && --cl >= sorted.begin()) {

            assert(partition_sz[*cl] > 0);

            if (partition[*cl].included(possible)) {
                reduction = true;
                remove_partition(cl, possible);
            } else {
                possible.union_with(candidate[*cl]);
            }
        }

        sort();
    }

#ifdef _CHECK_CC
    check("after repair");
#endif
}

void DualProblem::remove_partition(std::vector<int>::iterator cl, BitSet& delta)
{
#ifdef _CHECK_CC
    check("before rem clique");
#endif

    int k, n = G->capacity;
    auto i = *cl;

    while (partition_sz[i] > 0 && ++cl < sorted.begin() + layer_csize.back()) {
        k = *cl;

        if (partition_sz[k] > 0) {

            delta.clear();
            partition[i].setminus_with(candidate[k], delta);

            if (!delta.empty()) {
                for (auto elt : delta) {
                    for (int x = 0; x < partition_of[elt].size(); ++x) // TODO
                        if (partition_of[elt][x] == i) {
                            weight_contribution[elt][x] = k;
                            break;
                        }
                    // partition_of[elt] = k;
                    --partition_sz[i];
                    ++partition_sz[k];
                    candidate[k].intersect_with(G->matrix[elt]);
                }

                partition[k].union_with(delta);
                candidate_sz[k] = candidate[k].size();
            }
        }
    }

    assert(partition_sz[i] == 0);

    candidate[i].fill();
    candidate_sz[i] = n;

#ifdef _CHECK_CC
    check("after rem clique");
#endif
}

// #define _MONITOR_PERCOLATE

void DualProblem::percolate(BitSet& delta)
{

#ifdef _CHECK_CC
    check("before percolate");
#endif

    int i, k;
    bool reduced;

#ifdef _MONITOR_PERCOLATE
    int num_changes = 0;
#endif

    if (sorted.size() > 2)
        for (auto ic = sorted.begin(); ++ic != sorted.end();) {
            i = *ic;
            reduced = false;
            for (auto jc = sorted.begin(); jc != ic; ++jc) {
                k = *jc;
                if (partition_sz[k] > 0) {
                    delta.clear();
                    if (partition[i].setminus_with(candidate[k], delta)) {
                        reduced = true;
                        for (auto elt : delta) {

#ifdef _MONITOR_PERCOLATE
                            ++num_changes;
#endif

                            for (int x = 0; x < partition_of[elt].size();
                                 ++x) // TODO
                                if (partition_of[elt][x] == i) {
                                    weight_contribution[elt][x] = k;
                                    break;
                                }
                            // partition_of[elt] = k;
                            --partition_sz[i];
                            ++partition_sz[k];
                            candidate[k].intersect_with(G->matrix[elt]);
                        }

                        partition[k].union_with(delta);
                        candidate_sz[k] = candidate[k].size();
                    }
                }
                if (reduced) {
                    candidate[i].fill();
                    if (partition_sz[i]) {
                        for (auto elt : partition[i]) {
                            candidate[i].intersect_with(G->matrix[elt]);
                        }
                        candidate_sz[i] = candidate[i].size();
                    } else {
                        candidate_sz[i] = G->capacity;
                    }
                }
            }
        }

#ifdef _CHECK_CC
    check("after percolate");
#endif

#ifdef _MONITOR_PERCOLATE
    std::cout << "changes = " << num_changes << std::endl;
#endif
}

void DualProblem::check_conflict_set(
    std::vector<int>& cset, std::vector<BitSet>& part, bool cons, bool verbose)
{
    BitSet forbidden(0, G->capacity - 1, BitSet::empt);
    BitSet delta(0, G->capacity - 1, BitSet::empt);
    std::vector<BitSet> clq(sorted.size());

    for (auto v = clq.begin(); v < clq.end(); ++v) {
        v->initialise(0, G->capacity - 1, BitSet::empt);
    }

    forbidden.clear();
    for (auto i : cset) {
        clq[i].copy(partition[i]);
        forbidden.union_with(candidate[i]);

        if (verbose) {
            std::cout << i << ": " << clq[i] << "\t\t\t" << candidate[i]
                      << std::endl;
        }
    }

    if (verbose) {
        std::cout << "forbidden: " << forbidden << std::endl;
    }

    bool fix_point = false;
    bool consistent = true;

    auto culprit = -1;

    while (consistent && !fix_point) {
        fix_point = true;
        for (auto i : cset) {
            int sb = clq[i].size();
            clq[i].setminus_with(forbidden);
            int cs = clq[i].size();
            if (cs == 0) {

                if (verbose)
                    std::cout << " - inconsistent because c" << i
                              << " is included in " << forbidden << std::endl;

                consistent = false;
                culprit = i;
            } else if (cs < sb) {

                if (verbose)
                    std::cout << " - c" << i << " reduced to " << clq[i]
                              << std::endl;

                delta.fill();
                for (auto v : clq[i]) {
                    delta.intersect_with(G->matrix[v]);
                }
                if (!forbidden.includes(delta)) {
                    fix_point = false;
                    forbidden.union_with(delta);
                }

                if (verbose)
                    std::cout << " - forbidden = " << forbidden << std::endl;
            }
        }
    }

    if (!verbose) {
        if (consistent) {
            if (!cons) {
                std::cout << "\nwrong inconsistent!" << std::endl;
                for (auto i : cset) {
                    std::cout << clq[i] << std::endl;
                }
                exit(1);
            } else {
                for (auto i : cset) {
                    if (clq[i] != part[i]) {
                        std::cout << "\ndiscrepancy!" << std::endl;
                        std::cout << clq[i] << " != " << part[i] << std::endl;
                    }
                }
            }
        } else {
            if (cons) {
                std::cout << "\nwrong consistent!" << std::endl;
                std::cout << " c" << culprit << "=" << part[culprit]
                          << " has no support:\n";
                for (auto v : part[culprit]) {
                    std::cout << "N(" << v << ")=" << G->matrix[v] << std::endl;
                }

                check_conflict_set(cset, part, cons, true);

                exit(1);
            }
        }
    }
}

void DualProblem::sort()
{
    if (sorted.size() < dual_cost) {
        sorted.clear();
        for (auto i = 0; i < dual_cost; ++i)
            sorted.push_back(i);
    }

    std::sort(sorted.begin(), sorted.end(), [&](const int x, const int y) {
        // if (candidate_sz[x] < candidate_sz[y])
        //     return true;
        // else if (candidate_sz[x] == candidate_sz[y]) {
        //     if (partition_sz[x] > partition_sz[y])
        //         return true;
        //     else if (partition_sz[x] == partition_sz[y])
        //         return x < y;
        // }
        // return false;

        // return candidate_sz[x] < candidate_sz[y]
        //     || (candidate_sz[x] == candidate_sz[y]
        //            && (partition_sz[x] > partition_sz[y]));

        return partition_sz[x] > partition_sz[y]
            || (partition_sz[x] == partition_sz[y]
                   && (candidate_sz[x] < candidate_sz[y]));
    });

    while (dual_cost && !partition_sz[sorted[dual_cost - 1]]) {
        --dual_cost;
        sorted.pop_back();
    }

    // is_sorted = true;
}

void DualProblem::update(BitSet& removed_nodes, BitSet& delta)
{
    if (sorted.size() < dual_cost) {
        sorted.clear();
        for (int i = 0; i < dual_cost; ++i) {
            sorted.push_back(i);
        }
    }

    int ic;
    for (int k = 0; k < dual_cost; ++k) {
        ic = sorted[k];
        if (partition[ic].intersect(removed_nodes)) {
            partition[ic].setminus_with(removed_nodes);
            candidate[ic].fill();
            if (partition[ic].empty()) {
                partition_sz[ic] = 0;
                candidate_sz[ic] = G->capacity;
            } else {
                for (auto x : partition[ic]) {
                    candidate[ic].intersect_with(G->matrix[x]);
                }
                partition_sz[ic] = partition[ic].size();
                candidate_sz[ic] = candidate[ic].size();
            }
        }
    }

    sort();

    percolate(delta);

    sort();
}

int DualProblem::get_partition(const int v) const {
    return partition_of[v].back();
}

int DualProblem::num_partition() const {
    return layer_csize.back();
}

void DualProblem::new_layer()
{

#ifdef _DEBUG_WCOL
    std::cout << "\nSTART LAYER " << layer_csize.size() << std::endl;
#endif
    layer_csize.push_back(layer_csize.back());
}

void DualProblem::check(const char* msg, const bool covering)
{

    BitSet util_set(0, G->capacity - 1, BitSet::empt);
    BitSet rclique(0, G->capacity - 1, BitSet::empt);

    if (layer_csize.size() < 1) {
        std::cout << msg << " bad init, no elements in layer_csize\n";
        exit(1);
    }

    if (layer_csize[0] != 0) {
        std::cout << msg
                  << " wrong value for layer_csize[0] = " << layer_csize.back()
                  << "\n";
        exit(1);
    }

                // if(dual_cost != layer_csize.back()) {
    //      std::cout << msg << " wrong value for layer_csize[" <<
    // (layer_csize.size()-1) << "]/dual_cost (" <<
    // layer_csize.back() << "/" << dual_cost << ")\n";
    //      exit(1);
    // }

                if (layer_csize.back() != sorted.size()) {
                    std::cout << msg << " discrepancy on clique count "
                              << layer_csize.back() << " <> " << sorted.size()
                              << std::endl;
                    exit(1);
    }

    for (auto cl : sorted) {
        rclique.fast_add(cl);
    }

    auto total = 0;
    for (int i = 0; i < G->capacity; ++i) {
        if (rclique.fast_contain(i)) {
            if (partition[i].size() != partition_sz[i]) {
                std::cout << msg << " discreprancy size of partition[" << i
                          << "] " << partition[i].size() << "/"
                          << partition_sz[i] << std::endl;
                exit(1);
            }

            if (candidate[i].size() != candidate_sz[i]
                && (candidate[i].size() < G->capacity
                       || candidate_sz[i] < G->capacity)) {
                std::cout << msg << " discreprancy size of candidate[" << i
                          << "] " << candidate[i].size() << "/"
                          << candidate_sz[i] << std::endl;
                exit(1);
            }

            total += partition[i].size();

            if (!partition[i].empty()) {
                auto elt = partition[i].min();
                auto nxt = partition[i].next(elt);

                util_set.copy(G->matrix[elt]);
                // util_set.intersect_with(G->node_set);

                while (elt < nxt) {
                    if (!G->matrix[elt].contain(nxt)) {
                        std::cout << msg << " " << elt << " and " << nxt
                                  << " cannot be both in partition[" << i
                                  << "] = " << partition[i] << std::endl;
                        exit(1);
                    }
                    elt = nxt;

                    util_set.intersect_with(G->matrix[elt]);

                    nxt = partition[i].next(elt);
                }

                if (!candidate[i].empty()) {
                    nxt = candidate[i].min();

                    do {
                        elt = nxt;
                        if (!(partition[i].included(G->matrix[elt]))) {
                            std::cout << msg << " wrong candidate " << elt
                                      << ": " << partition[i]
                                      << " not subset of " << G->matrix[elt]
                                      << std::endl;
                            exit(1);
                        }
                        nxt = candidate[i].next(elt);
                    } while (elt < nxt);
                }
            } else {
                util_set.fill();
            }
            if (util_set != candidate[i]) {
                std::cout << std::endl
                          << msg << " problem with candidate[" << i << "] =\n"
                          << candidate[i] << " (" << candidate[i].size()
                          << ") should be\n"
                          << util_set << " (" << util_set.size()
                          << ")\n (partition[" << i << "] = " << partition[i]
                          << ")" << std::endl;
                exit(1);
            }
        } else {
            if (candidate[i].size() < G->capacity) {
                std::cout << msg << " problem with candidate[" << i
                          << "] = " << candidate[i] << " " << G->capacity
                          << std::endl;
                exit(1);
            }
            if (!partition[i].empty()) {
                std::cout << msg << " problem with partition[" << i
                          << "] = " << partition[i] << std::endl;
                exit(1);
            }
            if (partition_sz[i] != 0) {
                std::cout << msg << " discreprancy size of partition[" << i
                          << "] " << partition[i].size() << "/"
                          << partition_sz[i] << std::endl;
                exit(1);
            }

            if (candidate_sz[i] < G->capacity) {
                std::cout << msg << " discreprancy size of candidate[" << i
                          << "] " << candidate[i].size() << "/"
                          << candidate_sz[i] << std::endl;
                exit(1);
            }
        }
    }

    // std::cout << "TOTAL = " << total << "/" << G->size() << std::endl;

    if (covering && layer_csize.back() > 0 && total != G->size()) {
        std::cout << msg << " covering problem " << total << " <> " << G->size()
                  << std::endl
                  << G->node_set << std::endl;
        for (auto i : sorted) {
            std::cout << partition[i] << "  " << candidate[i] << std::endl;
        }

        exit(1);
    }
}

MaxsatAlgo::MaxsatAlgo(Graph& G)
{
    partition_of.resize(G.capacity);

    inconsistent_set.initialise(0, G.capacity - 1, BitSet::empt);
    support_set.initialise(0, G.capacity - 1, BitSet::empt);
}

MaxsatAlgo::~MaxsatAlgo() {}

void MaxsatAlgo::copy(DualProblem& cc)
{
    conflict.clear();
    clique.clear();

    for (auto cl : cc.sorted) {
        clique.push_back(cl);
    }
}

void MaxsatAlgo::compute_conflict_set(DualProblem& cc, const bool full_ac)
{
    // auto nconflict{0};
    auto N{cc.G->capacity};
    conflict.clear();
    clique.clear();

#ifdef _CHECK_MAXSAT
    std::vector<int> check_conflict;
#endif

#ifdef _TRACE_MAXSAT
    std::cout << "dual_cost: " << cc.dual_cost
              << " |sorted|=" << cc.sorted.size() << std::endl;
    for (auto cl : cc.sorted) {
        std::cout << " c" << cl << "=" << cc.partition[cl] << " -- "
                  << cc.candidate[cl] << std::endl;
    }
    std::cout << std::endl;
#endif

    // if (cc.dual_cost > 1) {
    // the smallest conflict set has at least two cliques

    assert(cc.dual_cost == cc.sorted.size());
    int max_cl = 0;
    int total = 0;
    for (auto cl : cc.sorted) {
        clique.push_back(cl);
        total += cc.partition_sz[cl];
        if (cl > max_cl)
            max_cl = cl;
        }
        ++max_cl;

        if (cc.dual_cost > 1) {
            int old_size = forbidden.size();

            if (old_size < max_cl) {
                clique_size.resize(max_cl);
                forbidden.resize(max_cl);

                num_nneighbors.resize(max_cl);
                support.resize(max_cl);

                opartition.resize(max_cl);
                cpartition.resize(max_cl);

                for (int i = 0; i < max_cl; ++i) {
                    forbidden[i].resize(max_cl);
                }

                for (int i = old_size; i < max_cl; ++i) {
                    num_nneighbors[i].resize(N);
                    support[i].resize(N);
                    opartition[i].initialise(0, N - 1, BitSet::empt);
                    cpartition[i].initialise(0, N - 1, BitSet::empt);
                }
            }

            // copy the clique cover
            for (auto cl : clique) {

                opartition[cl].copy(cc.partition[cl]);

                clique_size[cl] = cc.partition_sz[cl];

                for (auto v : opartition[cl]) {
                    partition_of[v] = cl;
                }
            }

            auto cur_clique{clique.end()};
            auto last_clique{clique.end()};
            while (cur_clique > (clique.begin() + 2)) {

#ifdef _TRACE_MAXSAT
            std::cout << "visited:";
            for (auto cl = cur_clique; cl != clique.end(); ++cl)
                std::cout << " " << *cl;
            std::cout << ", to visit:";
            for (auto cl = cur_clique - 1; cl >= clique.begin(); --cl)
                std::cout << " " << *cl;
            std::cout << std::endl << "start a new conflicting set\n";
            std::cout << " c" << *(cur_clique - 1) << "="
                      << cc.partition[*(cur_clique - 1)] << " -- "
                      << cc.candidate[*(cur_clique - 1)] << std::endl;
#endif

            // select the most promising clique in [clique.begin(), cur_clique[.
            // Since they are sorted by size, let's take the last
            --cur_clique;

            // quickly "propagate" to the future cliques by deleting
            // inconsistent vertices
            inconsistent_set.copy(cc.candidate[*cur_clique]);
            for (auto pcl = clique.begin(); pcl < cur_clique; ++pcl) {
                auto cl{*pcl};
                cpartition[cl].copy(opartition[cl]);
                cpartition[cl].setminus_with(inconsistent_set);
            }

            // start a new phase of propagation
            auto consistent{true};
            inconsistent.clear();
            first_inc = 0;

            // loop to add more cliques until obtaining a conflicting set
            while (consistent && cur_clique > clique.begin()) {

                // choose one heuristically [that minimize its size after
                // deletion of inconsistent vertices]
                auto best_clique = cur_clique - 1;
                auto best_size = cpartition[*best_clique].size();
#ifdef _TRACE_MAXSAT
                std::cout << "add a new clique to the conflicting set:";
                std::cout << " c" << (*best_clique) << " (" << best_size << ")";
#endif
                for (auto pcl = cur_clique - 2; pcl >= clique.begin(); --pcl) {
                    auto size = cpartition[*pcl].size();

#ifdef _TRACE_MAXSAT
                    std::cout << " c" << (*pcl);
#endif

                    if (size < best_size) {
                        best_size = size;
                        best_clique = pcl;

#ifdef _TRACE_MAXSAT
                        std::cout << " (" << best_size << ")";
#endif
                    }
                }

                --cur_clique;
                std::swap(*cur_clique, *best_clique);

#ifdef _TRACE_MAXSAT
                if (!full_ac)
                    std::cout << " -> " << *cur_clique << std::endl;
#endif

                if (best_size == 0) {
                    consistent = false;
                } else {

                    // add the best clique to the current conflicting set
                    inconsistent_set.union_with(cc.candidate[*cur_clique]);

                    if (full_ac) {
                        /* init the support structures to and from the newly
                         * added clique */
                        // for (int i = 0; i < clique.size(); ++i) {
                        //     for (int j = 0; j < clique.size(); ++j) {
                        //         if (forbidden[i][j].size() != 0) {
                        //             std::cout << "(before) forbidden[" << i
                        //                       << "][" << j
                        //                       << "]: " << forbidden[i][j].size()
                        //                       << std::endl;
                        //             exit(1);
                        //         }
                        //     }
                        // }

                        for (auto pcl = cur_clique + 1; pcl != last_clique;
                             ++pcl) {
                            for (auto v : cc.partition[*pcl]) {
                                auto cl = *cur_clique;
                                support_set.copy(cc.partition[cl]);
                                support_set.setminus_with(cc.G->matrix[v]);

                                num_nneighbors[cl][v] = support_set.size();
                                if (num_nneighbors[cl][v] == 0) {
                                    forbidden[cl][partition_of[v]].push_back(v);
                                } else {
                                    for (auto u : support_set) {
                                        support[partition_of[v]][u].push_back(
                                            v);
                                    }
                                }
                            }
                            for (auto v : cc.partition[*cur_clique]) {
                                auto cl = *pcl;
                                support_set.copy(cc.partition[cl]);
                                support_set.setminus_with(cc.G->matrix[v]);

                                num_nneighbors[cl][v] = support_set.size();
                                if (num_nneighbors[cl][v] == 0) {
                                    forbidden[cl][partition_of[v]].push_back(v);
                                } else {
                                    for (auto u : support_set) {
                                        support[partition_of[v]][u].push_back(
                                            v);
                                    }
                                }
                            }
                        }

#ifdef _TRACE_MAXSAT
                        std::cout << "\n -> c" << (*cur_clique) << "="
                                  << cc.partition[*cur_clique] << "\nconflict:";
                        for (auto pcl = last_clique - 1; pcl >= cur_clique;
                             --pcl)
                            std::cout << " " << (*pcl);
                        std::cout << std::endl;
#endif

                        /* first, informs the vertices of the new clique of
                         * every deletion that occurred on previous steps in
                         * other clique of the set */
                        for (auto vi = 0; vi < first_inc; ++vi) {
                            auto v{inconsistent[vi]};
                            auto clv{partition_of[v]};

#ifdef _TRACE_MAXSAT
                            std::cout << "pre-process " << v << " in c" << clv
                                      << std::endl;
#endif

                            if (clv == *cur_clique) {

#ifdef _TRACE_MAXSAT
                                std::cout << "in " << std::endl;
#endif

                                if (opartition[clv].fast_contain(v)) {
                                    opartition[clv].fast_remove(v);
                                }
                            } else {
                                for (auto u : support[*cur_clique][v]) {

#ifdef _TRACE_MAXSAT
                                    std::cout << " trigger " << u << ": "
                                              << num_nneighbors[clv][u]
                                              << std::endl;
#endif

                                    if (opartition[*cur_clique].fast_contain(
                                            u)) {
                                        if (--num_nneighbors[clv][u] == 0) {
                                            opartition[*cur_clique].fast_remove(
                                                u);

                                            assert(
                                                partition_of[u] == *cur_clique);

                                            inconsistent.push_back(u);
                                        }
                                    }
                                }
                            }
                        }

                        /* then add to the stack all the vertices made
                         * "unconditionally" inconsistent by the new clique, or
                         * in the new clique */
                        for (auto pcl = cur_clique + 1; pcl != last_clique;
                             ++pcl) {

                            // TODO: compute forbidden only here
                            for (auto v : forbidden[*cur_clique][*pcl]) {
                                if (opartition[*pcl].fast_contain(v)) {
                                    inconsistent.push_back(v);

                                    assert(partition_of[v] == *pcl);
                                    assert(inconsistent_set.fast_contain(v));

                                    opartition[*pcl].fast_remove(v);
                                }
                            }
                            forbidden[*cur_clique][*pcl].clear();

                            // TODO: compute forbidden only here
                            for (auto v : forbidden[*pcl][*cur_clique]) {
                                if (opartition[*cur_clique].fast_contain(v)) {
                                    inconsistent.push_back(v);

                                    assert(partition_of[v] == *cur_clique);
                                    assert(inconsistent_set.fast_contain(v));

                                    opartition[*cur_clique].fast_remove(v);
                                }
                            }
                            forbidden[*pcl][*cur_clique].clear();
                        }

                        for (int i = 0; i < clique.size(); ++i) {
                            for (int j = 0; j < clique.size(); ++j) {
                                if (forbidden[i][j].size() != 0) {
                                    std::cout << "(after) forbidden[" << i
                                              << "][" << j
                                              << "]: " << forbidden[i][j].size()
                                              << std::endl;
                                    exit(1);
                                }
                            }
                        }

#ifdef _TRACE_MAXSAT
                        std::cout << "previously inconsistent vertices:";
                        for (auto i = 0; i != first_inc; ++i)
                            std::cout << " " << inconsistent[i];
                        std::cout << std::endl;
                        std::cout << "inconsistent vertices:";
                        for (auto i = first_inc; i != inconsistent.size(); ++i)
                            std::cout << " " << inconsistent[i];
                        std::cout << std::endl;
#endif

                        // GAC-like procedure to remove "inconsistent" vertices
                        for (; consistent && first_inc < inconsistent.size();
                             ++first_inc) {
                            auto v{inconsistent[first_inc]};
                            auto clv{partition_of[v]};

#ifdef _TRACE_MAXSAT
                            std::cout << "process " << v << " in c" << clv
                                      << std::endl;
#endif

                            if (--clique_size[clv]) {
                                for (auto pcl = cur_clique; pcl != last_clique;
                                     ++pcl) {
                                    if (*pcl != clv) {
                                        // TODO: compute support only here
                                        for (auto u : support[*pcl][v]) {

#ifdef _TRACE_MAXSAT
                                            std::cout << "  - " << u << " in c"
                                                      << *pcl << std::endl;
#endif

                                            if (opartition[*pcl].fast_contain(
                                                    u)) {
                                                if (--num_nneighbors[clv][u]
                                                    == 0) {

                                                    opartition[*pcl]
                                                        .fast_remove(u);

                                                    assert(partition_of[u]
                                                        == *pcl);
                                                    assert(
                                                        !inconsistent_set
                                                             .fast_contain(u));
                                                    inconsistent_set.fast_add(
                                                        u);

                                                    inconsistent.push_back(u);
                                                }
                                            }
                                        }
                                        support[*pcl][v].clear();
                                    }
                                }
                            } else {
                                consistent = false;
                            }
                        }

#ifdef _CHECK_MAXSAT
#ifdef _TRACE_MAXSAT
                        std::cout << "\ncheck after adding c" << (*cur_clique)
                                  << std::endl;
                        for (auto pcl = cur_clique; pcl < last_clique; ++pcl) {
                            std::cout << "c" << (*pcl) << " "
                                      << opartition[*pcl] << std::endl;
                        }
                        std::cout << std::endl;
#endif
                        check_conflict.clear();
                        for (auto pcl = cur_clique; pcl < last_clique; ++pcl) {
                            check_conflict.push_back(*pcl);
                        }

                        cc.check_conflict_set(
                            check_conflict, opartition, consistent, false);
#endif
                    }
                }
                if (!consistent) {
// ++nconflict;

#ifdef _TRACE_MAXSAT
                    std::cout << " -> new conflicting set!\n" << std::endl;
#endif

                    for (auto pcl = cur_clique; pcl != last_clique; ++pcl) {
                        for (auto qcl = cur_clique; qcl != last_clique; ++qcl) {
                            if (pcl != qcl) {
                                for (auto v : cc.partition[*pcl]) {
                                    support[*qcl][v].clear();
                                }
                            }
                        }
                    }

                    last_clique = cur_clique;
                    conflict.push_back(cur_clique);
                } else {
                    // update the structure for choosing the next clique
                    for (auto pcl = clique.begin(); pcl < cur_clique; ++pcl) {
                        auto cl{*pcl};
                        // buffer.candidate[cl].copy(partition[cl]);
                        cpartition[cl].setminus_with(inconsistent_set);
                    }
                }
            }
        }

        for (auto pcl = cur_clique; pcl != last_clique; ++pcl) {
            for (auto qcl = cur_clique; qcl != last_clique; ++qcl) {
                if (pcl != qcl) {
                    for (auto v : cc.partition[*pcl]) {
                        support[*qcl][v].clear();
                    }
                }
            }
        }
    }

#ifdef _TRACE_MAXSAT
    std::cout << "BOUND=" << conflict.size() << std::endl << std::endl;
// exit(1);
#endif
}

Graph::Graph(const int n, const bool full_nodes, const bool sp)
{
    initialise(n, full_nodes, sp);
}

Graph::Graph(const Graph& g) { initialise(g); }

Graph::~Graph()
{
    delete[] neighbor;
    delete[] nb_index;

#ifdef _NOT_SO_COMPACT
    delete[] matrix;
#endif
}

void Graph::initialise(const Graph& g)
{
    initialise(g.capacity);

    spin = g.spin;
    num_edges = g.num_edges;
    node_set.copy(g.node_set);
    total_weight = g.total_weight;
    for (unsigned i = 0; i < g.ranks.size(); ++i) {
        edges.push_back(g.edges[i]);
        ranks.push_back(g.ranks[i]);
    }
    for (int i = g.size(); i < g.capacity; ++i) {
        node.remove(g.node[i]);
    }
    for (int x = 0; x < capacity; ++x) {

        weight[x] = g.weight[x];
        neighbor_weight[x] = g.neighbor_weight[x];

        for (std::vector<int>::iterator it = g.neighbor[x].begin();
             it != g.neighbor[x].end(); ++it)
            neighbor[x].push_back(*it);

        for (std::vector<int>::iterator it = g.nb_index[x].begin();
             it != g.nb_index[x].end(); ++it)
            nb_index[x].push_back(*it);

// for (unsigned i = 0; i < g.neighbor[x].size(); ++i) {
//     neighbor[x].push_back(g.neighbor[x][i]);
// }
// for (unsigned i = 0; i < g.nb_index[x].size(); ++i) {
//     nb_index[x].push_back(g.nb_index[x][i]);
// }

#ifdef _NOT_SO_COMPACT
        matrix[x].copy(g.matrix[x]);
#endif
    }
}

void Graph::initialise(const int n, const bool full_nodes, const bool sp)
{
    spin = sp;
    capacity = n;

    node.initialise(0, capacity - 1, capacity, full_nodes);

    node_set.initialise(
        0, capacity - 1, (full_nodes ? BitSet::full : BitSet::empt));

    weight.reserve(capacity);
    neighbor_weight.reserve(capacity);
    if (full_nodes)
        for (int i = 0; i < capacity; ++i) {
            weight.push_back(1);
            neighbor_weight.push_back(0);
        }

    neighbor = new std::vector<int>[capacity];
    nb_index = new std::vector<int>[capacity];

    num_edges = 0;
    total_weight = (full_nodes ? capacity : 0);

#ifdef _NOT_SO_COMPACT
    matrix = new BitSet[capacity];
    for (int i = 0; i < capacity; ++i) {
        matrix[i].initialise(
            0, capacity - 1, (spin ? BitSet::empt : BitSet::full));
        matrix[i].fast_remove(i);
    }
#endif
}

void Graph::initialise_random(const int m)
{
    // add m random edges

    int n = size();

    IntStack possible_vertex;
    IntStack possible_neighbor;
    possible_vertex.initialise(0, n - 1, n, true);
    possible_neighbor.initialise(0, n - 1, n, true);

    for (int i = 0; i < m; ++i) {
        // select at random a vertex that is not connected to all other vertices
        int x = possible_vertex[randint(possible_vertex.size)];
        int last = neighbor[x].size();
        if (last < n - 1) {
            possible_neighbor.fill();
            possible_neighbor.remove(x);
            for (int j = 0; j < last; ++j) {
                possible_neighbor.remove(neighbor[x][j]);
            }

            // select at random a vertex that is not connected to x
            int y = possible_neighbor[randint(n - 1 - last)];

            assert(x != y);
            add_undirected(x, y);

            if (neighbor[x].size() == static_cast<unsigned>(n - 1))
                possible_vertex.remove(x);
        }
    }

#ifdef _VERIFY_MCGRAPH
    verify("after init");
#endif
}

bool Graph::exists_edge(const int u, const int v) const
{
    return spin == matrix[u].fast_contain(v);
}

int Graph::size() const { return node.size; }

bool Graph::null() const { return node.size == 0; }

bool Graph::empty() const { return num_edges == 0; }

bool Graph::full() const
{
    return node.size * (node.size - 1) == 2 * num_edges;
}

void Graph::flip_neighbors(const int x)
{
    matrix[x].flip();
    matrix[x].set_max(capacity - 1);
    matrix[x].remove(x);
}

void Graph::flip()
{

    verify("before flip");

    edges.clear();
    ranks.clear();
    num_edges = 0;

    for (auto x = 0; x < capacity; ++x) {
        neighbor[x].clear();
        nb_index[x].clear();
    }

    for (auto x = 0; x < capacity; ++x) {

        if (spin) {
            flip_neighbors(x);
        }

        auto y = x, nxt = matrix[x].next(x);
        while (y != nxt) {
            y = nxt;
            add_undirected_nm(x, y);
            nxt = matrix[x].next(y);
        }

        if (!spin) {
            flip_neighbors(x);
        }
    }

    verify("after flip");
}

double Graph::get_density() const
{
    return (double)num_edges / (double)(size() * (size() - 1) / 2);
}

void Graph::sort(bool non_decreasing)
{

    verify("before sort");

    std::vector<int> sorted;
    for (int i = 0; i < size(); ++i) {
        sorted.push_back(node[i]);
    }

    std::sort(sorted.begin(), sorted.end(),
        [&](const int x, const int y) { return (degree(x) > degree(y))^non_decreasing; });

    std::vector<int> srank(capacity);
    for (int i = 0; i < size(); ++i) {
        srank[sorted[i]] = i;
    }

    ranks.clear();
    num_edges = 0;
    std::vector<Pair> old_edges;
    std::swap(edges, old_edges);

    for (auto x = 0; x < capacity; ++x) {
        neighbor[x].clear();
        nb_index[x].clear();

        if (spin) {
            matrix[x].clear();
        } else {
            matrix[x].fill();
        }
    }

    for (auto p : old_edges) {
        add_undirected(srank[p[0]], srank[p[1]]);
    }

    verify("after sort");
}

void Graph::hashsort(bool non_decreasing)
{

    verify("before hindex");

    BitSet common_neighbors(0, capacity, BitSet::empt);

    std::vector<int> count(capacity, 0);
    std::vector<int> hindex(capacity);
    for (int i = 0; i < size(); ++i) {
        hindex[node[i]] = degree(node[i]);
        std::cout << hindex[node[i]] << " ";
    }
    std::cout << std::endl;

    auto max_clique = 0;

    for (int i = 0; i < size(); ++i) {
        auto v = node[i];

        // std::cout << hindex[v] << ":";

        for (auto u : neighbor[v]) {
            common_neighbors.copy(matrix[v]);
            common_neighbors.intersect_with(matrix[u]);

            auto inter = common_neighbors.size();

            // std::cout << std::endl << matrix[v] << std::endl << matrix[u] <<
            // std::endl << common_neighbors;

            ++count[inter];

            // std::cout << " " << inter;
        }
        // std::cout << std::endl;

        auto total = 0;
        for (int j = 1; j <= hindex[v]; ++j) {
            total += count[hindex[v] - j];

            // std::cout << " " << total << "/" << (hindex[v]-j);

            if (total > hindex[v] - j) {
                hindex[v] = hindex[v] - j + 1;
                break;
            }
        }

        if (hindex[v] > max_clique) {
            max_clique = hindex[v];
        }

        std::fill(count.begin(), count.end(), 0);

        std::cout
            // << " -> "

            << hindex[v] << " ";
        // << std::endl << std::endl;
    }
    std::cout << std::endl;

    std::cout << "UB = " << (max_clique + 1) << std::endl;

    auto new_max_clique = 0;

    do {
        if (new_max_clique)
            max_clique = new_max_clique;
        new_max_clique = 0;

        for (int i = 0; i < size(); ++i) {
            auto v = node[i];

            auto max_h = 0;
            // std::cout << v << ":";
            for (auto u : neighbor[v]) {
                // std::cout << " " << hindex[u] ;
                ++count[hindex[u] - 1];

                if (hindex[u] > max_h)
                    max_h = hindex[u];
            }

            // std::cout << " : " << hindex[v] ;

            auto total = 0;
            for (int j = degree(v) - max_h + 1; j <= degree(v); ++j) {
                total += count[degree(v) - j];

                // std::cout << " " << total << "/" << (hindex[v]-j);

                if (total > degree(v) - j) {
                    if (hindex[v] > degree(v) - j + 1)
                        hindex[v] = degree(v) - j + 1;
                    // std::cout << (degree(v)-j+1) << " ";
                    break;
                }
            }

            if (hindex[v] > new_max_clique) {
                new_max_clique = hindex[v];
            }

            std::fill(count.begin(), count.end(), 0);

            std::cout << hindex[v] << " ";
        }

        std::cout << std::endl;
        std::cout << "UB = " << (new_max_clique + 1) << std::endl;
    } while (new_max_clique < max_clique);

    // exit(1);

    verify("after hindex");
}

void Graph::clear()
{
    int x;
    while (!node.empty()) {
        x = node.pop();
        neighbor[x].clear();
        nb_index[x].clear();

        if (spin) {
            matrix[x].clear();
        } else {
            matrix[x].fill();
        }
    }
    ranks.clear();
}

void Graph::declare_node(const int x)
{
    node.add(x);
    node_set.add(x);
}

void Graph::add_node(const int x)
{
    declare_node(x);

    int i = degree(x), y, e, pos;
    num_edges += i;
    total_weight += weight[x];

    while (i--) {
        y = neighbor[x][i];
        e = nb_index[x][i];

        // we add x at the back of y's neighbor list
        pos = (e & 1);

        // we change the position of x in y's neighbor list
        ranks[e / 2][1 - pos] = neighbor[y].size();

        // points to the edge from y's perspective
        nb_index[y].push_back(e ^ 1);

        // add x in y's neighbors
        neighbor[y].push_back(x);

        neighbor_weight[y] += weight[x];
    }

#ifdef _VERIFY_MCGRAPH
    verify("after add node");
#endif
}

void Graph::rem_node(const int x)
{
    node.remove(x);
    node_set.remove(x);
    auto i = degree(x);
    int y, z, rx, ex, posx, ey, posy;
    num_edges -= i;
    total_weight -= weight[x];

    while (i--) {

        y = neighbor[x][i];

        ex = nb_index[x][i];
        posx = ex & 1;

        // store the position of x in y's neighborhood
        rx = ranks[ex / 2][1 - posx];

        // replace x by z
        z = neighbor[y].back();
        neighbor[y].pop_back();
        neighbor[y][rx] = z;

        // set the new position of z in y's neighborhood
        ey = nb_index[y].back();
        nb_index[y].pop_back();
        posy = (ey & 1);
        ranks[ey / 2][posy] = rx;

        //
        nb_index[y][rx] = ey;

        //
        neighbor_weight[y] -= weight[x];
    }

#ifdef _VERIFY_MCGRAPH
    verify("after rem node");
#endif
}

bool Graph::has_node(int x) const { return node_set.fast_contain(x); }

int Graph::add_undirected_nm(const int x, const int y)
{
    assert(node.contain(x));
    assert(node.contain(y));

    nb_index[x].push_back(2 * ranks.size());
    nb_index[y].push_back(2 * ranks.size() + 1);

    Pair r(neighbor[x].size(), neighbor[y].size());
    ranks.push_back(r);

    Pair e(x, y);
    edges.push_back(e);

    neighbor[x].push_back(y);
    neighbor[y].push_back(x);

    neighbor_weight[x] += weight[y];
    neighbor_weight[y] += weight[x];

    ++num_edges;

#ifdef _VERIFY_MCGRAPH
    verify("after add edge");
#endif

    return ranks.size() - 1;
}

int Graph::add_undirected(const int x, const int y)
{

#ifdef _NOT_SO_COMPACT
    if (spin) {
        matrix[x].fast_add(y);
        matrix[y].fast_add(x);
    } else {
        matrix[x].fast_remove(y);
        matrix[y].fast_remove(x);
    }
#endif

    return add_undirected_nm(x, y);
}

void Graph::rem_undirected(const int i)
{
    Pair edge = edges[i];
    rem_undirected(edge[0], edge[1], i);
}
void Graph::rem_undirected(const int x, const int y, const int e)
{

#ifdef _VERIFY_MCGRAPH
    assert(node.contain(x));
    assert(node.contain(y));
#ifdef _NOT_SO_COMPACT
    assert(exists_edge(x, y));
    assert(exists_edge(y, x));
#endif
#endif

    int ry = ranks[e][0];
    int rx = ranks[e][1];

    int sx = neighbor[y].back();
    neighbor[y].pop_back();

    int ey = nb_index[y].back();
    nb_index[y].pop_back();
    if (x != sx) {
        int posy = (ey & 1);
        ranks[ey / 2][posy] = rx;

        neighbor[y][rx] = sx;
        nb_index[y][rx] = ey;
    }

    int sy = neighbor[x].back();
    neighbor[x].pop_back();

    int ex = nb_index[x].back();
    nb_index[x].pop_back();
    if (y != sy) {
        int posx = (ex & 1);

        ranks[ex / 2][posx] = ry;

        neighbor[x][ry] = sy;
        nb_index[x][ry] = ex;
    }

    Pair ez = edges.back();
    edges.pop_back();
    Pair rz = ranks.back();
    ranks.pop_back();

    if (static_cast<unsigned>(e) != edges.size()) {

        int s;
        for (int i = 0; i < 2; ++i) {
            s = (nb_index[ez[i]][rz[i]] & 1);
            nb_index[ez[i]][rz[i]] = 2 * e + s;
        }

        edges[e] = ez;
        ranks[e] = rz;
    }

    neighbor_weight[x] -= weight[y];
    neighbor_weight[y] -= weight[x];

#ifdef _NOT_SO_COMPACT
    if (spin) {
        matrix[x].fast_remove(y);
        matrix[y].fast_remove(x);
    } else {
        matrix[x].fast_add(y);
        matrix[y].fast_add(x);
    }
#endif

    --num_edges;

#ifdef _VERIFY_MCGRAPH
    verify("after rem edge");
#endif
}

void Graph::maximal_matching(
    std::vector<int>& matching, int& nmatch, std::vector<int>& indexlist)
{
    indexlist.clear();
    for (int i = 0; i < size(); i++)
        indexlist.push_back(i);
    std::random_shuffle(indexlist.begin(), indexlist.end());

    int u, v;
    matching.assign(capacity, -1);
    nmatch = 0;
    for (auto i = 0; i < size(); i++) {
        u = node[indexlist[i]];
        if (matching[u] == -1) {
            for (auto j = 0; j < neighbor[u].size(); j++) {
                v = neighbor[u][j];
                if (matching[v] == -1) {
                    matching[u] = v;
                    matching[v] = u;
                    nmatch++;
                    break;
                }
            }
        }
    }
}

bool Graph::check_solution(std::vector<int>& is, const bool mis) const
{
    for (auto i = 1; i < is.size(); ++i) {
        for (auto j = 0; j < i; ++j) {
            int x = is[i];
            int y = is[j];
            if (mis == exists_edge(x, y)) {
                return false;
            }
        }
    }
    return true;
}

void Graph::set_weight(const int x, const weight_type v)
{
    for (auto y : neighbor[x]) {
        neighbor_weight[y] += (v - weight[x]);
    }
		total_weight += (v - weight[x]);
    weight[x] = v;
}

void Graph::set_random_uniform_weights(const int l, const int u)
{
    total_weight = 0;
    for (auto i = 0; i < size(); ++i) {
        set_weight(node[i], l + randint(u - l + 2));
        total_weight += weight[i];
    }
}

void Graph::set_modulo_weights(const int m)
{
    total_weight = 0;
    for (auto i = 0; i < size(); ++i) {
        set_weight(node[i], (i+1) % m + 1);
        total_weight += weight[i];
    }
}

std::ostream& Graph::display(std::ostream& os) const
{

    for (auto i = 0; i < size(); ++i) {
        int x = node[i];

        os << x << ": ";
        vecdisplay(neighbor[x], os);

        os << "  " << weight[x] << " " << neighbor_weight[x] << std::endl;
    }
    return os;
}

void Graph::print_dimacs(std::ostream& os) const
{
    os << "c Generated by minicsp" << std::endl
       << "p edge " << size() << " " << edges.size() << std::endl;

    // not true during search
    assert(static_cast<int>(edges.size()) == num_edges);

    for (auto edge : edges) {
        os << "e " << (edge[0] + 1) << " " << (edge[1] + 1) << std::endl;
    }
}

void Graph::print_mts(std::ostream& os) const
{
    os << "c Generated by minicsp" << std::endl
       << "p " << size() << " " << edges.size() << std::endl;

    // not true during search
    assert(static_cast<int>(edges.size()) == num_edges);

    for (auto i = 1; i <= size(); ++i) {
        os << "v " << i << " 1\n";
    }

    for (auto edge : edges) {
        os << "e " << (edge[0] + 1) << " " << (edge[1] + 1) << std::endl;
    }
}

bool Graph::verify_coloration(const int* coloration)
{
    int x;
    for (auto i = 0; i < size(); ++i) {
        x = node[i];
        for (std::vector<int>::iterator y = neighbor[x].begin();
             y != neighbor[x].end(); ++y) {
            if (coloration[x] == coloration[*y]) {
                return false;
            }
        }
    }

    return true;
}

bool Graph::verify_clique_cover(const int* cover, const int nc)
{
    int x, y;
    for (auto i = 0; i < size(); ++i) {
        x = node[i];
        if (cover[x] >= nc) {
            std::cout << std::endl << "Wrong partition number!" << std::endl;
            return false;
        }

        for (auto j = 0; j < i; ++j) {
            y = node[j];

            if (x != y && !matrix[x].contain(y) && cover[x] == cover[y]) {
                std::cout << std::endl
                          << y << " is not in N(" << x << ")" << std::endl;
                return false;
            }
        }
    }

    return true;
}

void Graph::verify(const char* msg)
{
    for (unsigned i = 0; i < edges.size(); ++i) {
        Pair e = edges[i];
        int x = e[0];
        int y = e[1];

        Pair r = ranks[i];
        int ry = r[0];
        int rx = r[1];

        if (exists_edge(x, y) != exists_edge(y, x)) {
            std::cout << msg << " the matrix is not symmetric for " << x << ","
                      << y << std::endl;
            assert(0);
        }

        if (node.contain(y) && neighbor[x][ry] != y) {
            std::cout << msg << " " << i << "-th edge " << e << " points to "
                      << r << ", however the " << ry << "-th element of ";
            vecdisplay(neighbor[x], std::cout);
            std::cout << " is not " << y << std::endl;
            assert(0);
        }
        if (node.contain(y) && static_cast<unsigned>(nb_index[x][ry]) != 2 * i) {
            std::cout << msg << " " << i << "-th edge " << e << " points to "
                      << r << ", however the " << ry << "-th element of ";
            vecdisplay(nb_index[x], std::cout);
            std::cout << " is not " << (2 * i) << std::endl;
            assert(0);
        }

        if (node.contain(x) && neighbor[y][rx] != x) {
            std::cout << msg << " " << i << "-th edge " << e << " points to "
                      << r << ", however the " << rx << "-th element of ";
            vecdisplay(neighbor[y], std::cout);
            std::cout << " is not " << x << std::endl;
            assert(0);
        }
        if (node.contain(x)
            && static_cast<unsigned>(nb_index[y][rx]) != 2 * i + 1) {
            std::cout << msg << " " << i << "-th edge " << e << " points to "
                      << r << ", however the " << ry << "-th element of ";
            vecdisplay(nb_index[y], std::cout);
            std::cout << " is not " << (2 * i + 1) << std::endl;
            assert(0);
        }
    }

    int ecount = 0;
    for (int i = 0; i < size(); ++i) {
        int x = node[i];

        if (spin) {
            if (matrix[x].size() != neighbor[x].size()) {
                std::cout << msg << " discrepancy between matrix/list of " << x
                          << std::endl;
                assert(0);
            }
        } else {
            if (capacity - matrix[x].size() - 1 != neighbor[x].size()) {
                BitSet neigh(0, capacity - 1, BitSet::empt);
                for (auto y : neighbor[x])
                    neigh.add(y);

                std::cout << msg << " discrepancy between matrix/list of " << x
                          << "\n"
                          << std::endl
                          << matrix[x].size() << " " << matrix[x] << std::endl
                          << neighbor[x].size() << "/" << neigh.size() << " "
                          << neigh << std::endl;

                assert(0);
            }
        }

        for (unsigned j = 0; j < neighbor[x].size(); ++j) {

            ++ecount;
            int y = neighbor[x][j];
            int ey = nb_index[x][j];

            Pair e = edges[ey / 2];

            int posx = (ey & 1);

            assert(e[posx] == x);
            assert(e[1 - posx] == y);

            Pair r = ranks[ey / 2];

            assert(static_cast<unsigned>(r[posx]) == j);
        }
    }
    ecount /= 2;

    if (ecount != num_edges) {
        std::cout << " " << msg << ": num_edges = " << num_edges
                  << " real count = " << ecount << std::endl
            ;

        assert(0);
    }
}

std::ostream& operator<<(std::ostream& os, const Graph& x)
{
    return x.display(os);
}

std::ostream& operator<<(std::ostream& os, const Graph* x)
{
    return (x ? x->display(os) : os);
}
