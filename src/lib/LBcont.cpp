#include <LBcont.hpp>
#include <minicsp/core/utils.hpp>

using namespace ::minicsp;

cons_lbc::cons_lbc(Graph& G, Solver& s, std::vector<cspvar>& vars, int n, int ub) :
	s(s), G(G), vars(vars), n(n), ub(ub)
{
	r.capacity(n);
	dispo = BitSet(0, ub-1, BitSet::empt);
	unused = BitSet(1, ub, BitSet::full);
	v.resize(n, 0);
	clique.resize(n, 0);
	for (int i = 0; i < n; i++) {
		s.schedule_on_fix(vars[i], this);
	}
	DO_OR_THROW(cons_lbc::propagate(s));
}

Clause* cons_lbc::propagate(Solver& s) {
	//réinitialiser unused ?
	v.clear();
	int domsizeMin = n;
	int domsizeAct;
	//std::cout << std::endl;
	//std::cout << "n " << n << " ub " << ub << std::endl;
	for (int i = 0; i < n; i++) {
		domsizeAct = vars[i].domsize(s);
		//std::cout << "Variable " << i+1 << " Domaine " << vars[i].domsize(s) << std::endl;
		if (domsizeAct == 1) {
			//std::cout << "	Couleur enlevée " << vars[i].min(s) << std::endl;
			unused.remove(vars[i].min(s));
		} else 	if (domsizeAct < domsizeMin) {
			domsizeMin = domsizeAct;
			v.clear();
			v.push_back(i);
		} else if (domsizeAct == domsizeMin) {
			v.push_back(i);
		}
	}
	//std::cout << "Taille unused " << unused.size() << std::endl;
	if (domsizeMin == unused.size()) {
		quick_sort(0, 0, v.size()-1);
			//counting sort ?
		calcul_clique();
		colore_clique();
	} else {
		//std::cout << "Aucun sommet saturé" << std::endl;
	}
	return NO_REASON;
}

void cons_lbc::echanger(int i, int j) {
	int aux = v[i];
	v[i] = v[j];
	v[j] = aux;
}

void cons_lbc::quick_sort(int deb, int pivot, int fin) {
	//ordone v par degrés décroissant
	int degP = G.neighbor[v[pivot]].size();
	echanger(pivot, fin);
	int j = deb;
	for (int i = deb; i < fin; i++) {
		if (G.neighbor[v[i]].size() > degP) {
			echanger(i, j);
			j++;
		}
	}
	echanger(j, fin);
	if (deb < j-1)
		quick_sort(deb, deb, j-1);
	if (j+1 < fin)
		quick_sort(j+1, j+1, fin);
}

/*void cons_lbc::countingSort() {
	std::vector<int> keys(n);
	std::vector<int> inds(n, 0);
	int ind = 0;
	for (int i = 0; i < v.size(); i++) {
		keys[G.neighbor[v[i]].size()] ++;
	}
	for (int i = 0; i < v.size(); i++) {
		inds[i] = ind;
		ind += keys[i];
	}
	for (int i = 0; i < n; i++) {
		nodes[inds[degs[i]]] = i; // à modifier
		inds[degs[i]] ++;
	}
}*/

void cons_lbc::calcul_clique() {
	clique.clear();
	/*
	std::cout << "Sommets par degrés décroissants" << std::endl;
	for (int i = 0; i < v.size(); i++) {
		std::cout << v[i]+1 << " ";
	}
	std::cout << std::endl;
	*/
	dispo.reinitialise(0, n-1, BitSet::empt);
	int ajout = 0;
	while (ajout < v.size()) {
		clique.push_back(v[ajout]);
		dispo.union_with(G.matrix[v[ajout]]);
		ajout ++;
		while (ajout < v.size() && dispo.fast_contain(v[ajout])) {
			ajout++;
		}
	}
	/*
	std::cout << "Clique du graphe résiduel" << std::endl;
	for (int i = 0; i < clique.size(); i++) {
		std::cout << clique[i]+1 << " ";
	}
	std::cout << std::endl;
	*/
}

void cons_lbc::explain() {
	r.clear();
	for (int i = 0; i < n; i++) {
		if (vars[i].domsize(s) == 1) {
			r.push(~Lit(s.cspvareqi(vars[i], vars[i].min(s))));
		} else {
			r.push(Lit(s.cspvarleqi(vars[i], vars[i].min(s)-1)));
			r.push(~Lit(s.cspvarleqi(vars[i], vars[i].max(s))));
			if (vars[i].domsize(s) != vars[i].max(s) - vars[i].min(s) + 1) {
				for (int j = vars[i].min(s)+1; j < vars[i].max(s); j++) {
					if (!vars[i].indomain(s, j)) {
						r.push(Lit(s.cspvareqi(vars[i], j)));
					}
				}
			}
		}
	}
}

void cons_lbc::colore_clique() {
	explain();
	int colMin = unused.min();
	for (int i = 0; i < clique.size(); i++) {
		vars[clique[i]].assign(s, colMin, r);
		unused.remove(colMin);
		colMin = unused.next(colMin);
	}
}
