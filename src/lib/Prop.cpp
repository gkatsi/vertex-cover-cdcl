#include <Algorithm.hpp>
#include <Prop.hpp>
#include <minicsp/core/utils.hpp>

using namespace ::minicsp;

#define DBG(x) if(!(x)); else

// #define _DEBUG_GREEDY_EXPLAIN 2
// #define _DEBUG_KERNCLIQUEINCR true
// #define _DEBUG_KERNCLIQUE true
// #define _VERIF_WATCHERS true
// #define _DEBUG_REPAIR_LB true
// #define _TRACE_PRUNING
// #define _DEBUG_BUSS
#define DEBUG_PRUNE DBG(false)
#define DEBUG_DOM DBG(false)

const std::map<std::string, vc_options::clqcover_alg>
    vc_options::clqcover_alg_names
    = {{"naive", NAIVEdual}, {"greedy", GREEDYdual},
       {"greedy-weak", GREEDYWEAKdual}, {"greedyNweak", GREEDYNWEAKdual},
       {"dyngreedy", DYNGREEDYdual}, {"repair", REPAIRdual},
       {"quickr", QUICKRdual}, {"random", RANDOMdual}, {"dsatur", DSATURdual},
       {"dsatur-weak", DSATURWEAKdual}, {"dynamic", DYNAMICdual},
       {"dynweak", DYNWEAKdual}, {"dyndsatur", DYNDSATURdual}};

namespace std
{
template <typename T> ostream& operator<<(ostream& os, vector<T> const& v)
{
    os << "v{" << v.size() << "}=[";
    for (auto&& t : v)
        os << t << " ";
    os << "]";
    return os;
}
}

cons_vc::cons_vc(Solver& solver, DegreeSorted<Graph>& G,
    std::vector<Lit> const& x, vc_options opt,
    std::function<void(std::vector<int> const&, std::vector<int> const&)>
        sb)
    : s(solver)
    , _x(x)
    , rG(G)
    , ccache(G.capacity)
    , dual(&G)
    , bufferdual(&G)
    , ms(G)
    , neighweight_heap(&G)
    , nonneighweight_heap(&G)
    , options(opt)
{
    ub = options.ub;
    solution_callback = sb;
    int maxvar = var(*std::max_element(
        begin(_x), end(_x), [](Lit a, Lit b) { return var(a) < var(b); }));
    revmap.resize(maxvar + 1);
    for (size_t i = 0; i != _x.size(); ++i) {
        Lit l = _x[i];
        s.wake_on_lit(var(l), this, reinterpret_cast<void*>(i + 1));
        s.schedule_on_lit(var(l), this);
        revmap[var(_x[i])] = i;
    }

    coverweightptr = s.alloc_backtrackable(sizeof(weight_type));
    s.deref<weight_type>(coverweightptr) = 0;
    removed_sizeptr = s.alloc_backtrackable(sizeof(size_t));
    s.deref<size_t>(removed_sizeptr) = 0;
    fixed_sizeptr = s.alloc_backtrackable(sizeof(size_t));
    s.deref<size_t>(fixed_sizeptr) = 0;

    // Remark: I changed size() to capacity in case we do some preprocessing on
    // the graph
    current_solution.initialise(0, rG.capacity - 1, BitSet::empt);
    relevant.initialise(0, rG.capacity - 1, BitSet::empt);
    maybe.initialise(0, rG.capacity - 1, BitSet::empt);
    cc_neighbors.initialise(0, rG.capacity - 1, BitSet::empt);
    dual_sol_neighbors.initialise(0, rG.capacity - 1, BitSet::empt);

    watchers.initialise(rG.capacity);

    init_weight = rG.total_weight;

    util_set.initialise(0, rG.capacity - 1, BitSet::empt);

    num_maxsat_calls.resize(rG.capacity, 0);
    dsatur_clqsize.resize(rG.capacity, 0);
    greedy_clqsize.resize(rG.capacity, 0);
    repair_clqsize.resize(rG.capacity, 0);
    maxsat_bound.resize(rG.capacity, 0);
    maxsat_early_fails.resize(rG.capacity, 0);
    cc_bound.resize(rG.capacity, 0);
    cc_early_fails.resize(rG.capacity, 0);
    num_cc_calls.resize(rG.capacity, 0);
    dp_bound.resize(rG.capacity, 0);
    dp_early_fails.resize(rG.capacity, 0);
    num_dp_calls.resize(rG.capacity, 0);

    init_degree.resize(rG.capacity);
    for (auto v : rG.node)
        init_degree[v] = rG.degree(v);

    if (options.component_caching)
        s.use_clause_callback(
            [this](auto const& cl, int) { this->new_component(this->s, cl); });

    vertexUBForward.resize(rG.capacity);
    vertexUBBackward.resize(rG.capacity);

    sortedvertices.resize(rG.capacity);
    descendant.initialise(0, rG.capacity - 1, rG.capacity, false);

    vertexindex.resize(rG.capacity);

    DO_OR_THROW(cons_vc::propagate(s));

#ifdef _PRETTY_TRACE
    previous_ub = ub;

    s.use_clause_callback([this, &solver](
        auto const& cl, int) { this->backtrack_event = cl[0]; });

    s.use_decision_callback(
        [this, &solver](minicsp::Lit l) { this->decision_event = l; });

    std::cout << "[" << bestlb;
#endif
}

cons_vc::~cons_vc() {}

void cons_vc::sync_graph(Solver& s)
{
    size_t& removed_size = s.deref<size_t>(removed_sizeptr);
    size_t& fixed_size = s.deref<size_t>(fixed_sizeptr);

#ifdef _PRETTY_TRACE
    if (decision_event != lit_Undef) {
        notify_decision(s, decision_event);
    }
    bool backtracked{false};
    bool newsolution{previous_ub > ub};
#endif

    if (removed_size < removed.size()) {

#ifdef _PRETTY_TRACE
        backtracked = true;
#endif

        dual.clear();
        have_lbs = false;
        if (options.bound_algo == vc_options::REPAIRdual) {
            dual = lvl_covers[s.decisionLevel()];
        }
    }

#ifdef _PRETTY_TRACE
    if (backtrack_event != lit_Undef) {

        notify_backtrack(s, backtrack_event);

    } else if (backtracked) {
        if (!newsolution)
            std::cout << ", " << ub << "]\n"
                      << std::left << std::setw(s.decisionLevel() + 1)
                      << s.decisionLevel() << std::right << "restart ["
                      << bestlb;
        else {
            previous_ub = ub;
            std::cout << std::left << std::setw(s.decisionLevel() + 1)
                      << s.decisionLevel() << std::right << "new solution ["
                      << bestlb;
        }
    }
#endif

    while (removed_size < removed.size()) {
        auto x = removed.back();
        removed.pop_back();
        rG.add_and_update(x);

        if (current_solution.fast_contain(x)) {
            current_solution.fast_remove(x);
        }

        _last_checked_watched = removed.size();
        _last_checked_repair = removed.size();

        ccache.readd_vertex(x);
    }

    fixedlits.resize(fixed_size);
    fixednodes.resize(fixed_size);

#ifdef _PRETTY_TRACE
    previous_decision_level = s.decisionLevel();
    backtrack_event = lit_Undef;
    decision_event = lit_Undef;
#endif
}

#ifdef _PRETTY_TRACE
void cons_vc::notify_backtrack(Solver& s, Lit l)
{
    std::cout << ", " << ub << "]" << std::endl
              << std::left << std::setw(s.decisionLevel() + 1)
              << s.decisionLevel() << std::right << (sign(l) ? "+" : "-")
              << var(l) << " [" << bestlb;
}

void cons_vc::notify_decision(Solver& s, Lit l)
{
    std::cout << ", " << ub << "]" << std::endl
              << std::left << std::setw(s.decisionLevel() + 1)
              << s.decisionLevel() << std::right
              << (s.value(var(l)) == l_True ? "+" : "-") << var(l) << " ["
              << bestlb;
}
#endif

Clause* cons_vc::wake_advised(Solver& s, Lit l, void* hint)
{

    sync_graph(s);

#if 0
        cout << "branch " << var(l) + 1 << "=" << !sign(l) << " (node " << node
        << ") @dvlv " << s.decisionLevel() << "@ " << s.conflicts
        << " conflicts\n";
#endif

    int node = reinterpret_cast<size_t>(hint) - 1;

    if (!rG.has_node(node))
        return NO_REASON;

#ifdef _TRACE_PRUNING
    std::cout << "<update (" << node << ") -> ";
#endif

    DO_OR_RETURN(update_graph(s, l, node));

#ifdef _TRACE_PRUNING
    std::cout << " /update>" << std::endl;
#endif

    return NO_REASON;
}

void cons_vc::dsatur() {
    if (options.problem == vc_options::MAXCLIQUE)
        return rG.dsatur_col(&dual, close_type::STRONG);
    return rG.dsatur_clc(&dual, close_type::STRONG);
}

void cons_vc::dsatur_weak()
{
    if (options.problem == vc_options::MAXCLIQUE)
        return rG.dsatur_col(&dual, close_type::WEAK);
    return rG.dsatur_clc(&dual, close_type::WEAK);
}

void cons_vc::greedy()
{
    if (options.problem == vc_options::MAXCLIQUE)
        return rG.min_coloration_bitset(
            &dual, close_type::STRONG, options.greedy_order);
    return rG.min_clique_cover_bitset(
        &dual, close_type::STRONG, options.greedy_order);
}

void cons_vc::greedy_weak()
{
    if (options.problem == vc_options::MAXCLIQUE)
        return rG.min_coloration_bitset(
            &dual, close_type::WEAK, options.greedy_order);
    return rG.min_clique_cover_bitset(
        &dual, close_type::WEAK, options.greedy_order);
}

void cons_vc::naive()
{
    if (options.problem == vc_options::MAXCLIQUE)
        rG.min_coloration_naive(&dual);
    rG.min_clique_cover_naive(&dual);
}

bool cons_vc::trivial_partition()
{
    if (rG.null()) {
        dual.clear();
        ms.conflict.clear();
        return true;
    }

    bool trivial_type = rG.full();
    if (!trivial_type && !rG.empty())
        return false;

    bool mis = (options.problem == vc_options::MAXINDSET);

    ms.conflict.clear();
    if (mis == trivial_type) {
        // MIS and FULL or CLQ and EMPTY
        dual.clear();
        dual.push(rG);
    } else {
        // MIS and EMPTY or CLQ and FULL
        dual.clear();
        dual.new_layer();
        for (auto u : rG.node)
            dual.push(
                u, (mis ? init_degree[u] : (rG.capacity - 1 - init_degree[u])));
    }

    return true;
}

void cons_vc::compute_dual()
{
    if (!trivial_partition()) {
        switch (options.bound_algo) {
        case vc_options::NAIVEdual:
            naive();
            break;
        case vc_options::GREEDYdual:
            greedy();
            break;
        case vc_options::GREEDYWEAKdual:
            greedy_weak();
            break;
        case vc_options::REPAIRdual: {
            if (s.decisionLevel() <= 1) {

                greedy();
                auto clqcover = dual.dual_cost;

                switch_partition();
                dsatur();

                auto clqcoverd{dual.dual_cost};

                delta += (clqcover - clqcoverd);
                if (clqcoverd >= clqcover) {
                    switch_partition();
                }

                dual.sort();

            } else {

                update_partition();
                dual.repair(util_set);
            }

            decision_event_level = s.decisionLevel();

            assert(static_cast<unsigned>(dual.dual_cost) <= rG.node.size);

            if (lvl_covers.size()
                == static_cast<unsigned>(decision_event_level))
                lvl_covers.push_back(dual);
            else
                lvl_covers[decision_event_level] = dual;

        } break;
        case vc_options::DSATURdual: {
            greedy();
            auto clqcover{dual.dual_cost};
            switch_partition();

            dsatur();
            auto clqcoverd{dual.dual_cost};

            delta += (clqcover - clqcoverd);
            if (clqcoverd >= clqcover) {
                switch_partition();
            }
        } break;
        case vc_options::DSATURWEAKdual: {
            greedy_weak();
            auto clqcover{dual.dual_cost};
            switch_partition();

            dsatur_weak();
            auto clqcoverd{dual.dual_cost};

            delta += (clqcover - clqcoverd);
            if (clqcoverd >= clqcover) {
                switch_partition();
            }
        } break;
        case vc_options::GREEDYNWEAKdual: {
            greedy();
            auto clqcover{dual.dual_cost};
            switch_partition();

            greedy_weak();
            auto clqcoverd{dual.dual_cost};

            delta += (clqcover - clqcoverd);
            if (clqcoverd >= clqcover) {
                switch_partition();
            }
        } break;
        case vc_options::DYNGREEDYdual: {
            greedy();
            auto clqcover{dual.dual_cost};
            if (s.decisionLevel() < options.dsatur_threshold) {
                switch_partition();

                greedy_weak();
                auto clqcoverd{dual.dual_cost};

                delta += (clqcover - clqcoverd);
                if (clqcoverd >= clqcover) {
                    switch_partition();
                }
            }
        } break;
        case vc_options::DYNDSATURdual: {
            greedy();
            auto clqcover{dual.dual_cost};
            if (s.decisionLevel() < options.dsatur_threshold) {
                switch_partition();

                dsatur();
                auto clqcoverd{dual.dual_cost};

                delta += (clqcover - clqcoverd);
                if (clqcoverd >= clqcover) {
                    switch_partition();
                }
            }
        } break;
        case vc_options::DYNWEAKdual: {
            greedy_weak();
            auto clqcover{dual.dual_cost};
            if (s.decisionLevel() < options.dsatur_threshold) {
                switch_partition();

                rG.static_order.clear();
                dsatur_weak();
                auto clqcoverd{dual.dual_cost};

                delta += (clqcover - clqcoverd);
                if (clqcoverd >= clqcover) {
                    switch_partition();
                }
            }
        } break;
        case vc_options::DYNAMICdual: {
            greedy();
            auto clqcover{dual.dual_cost};

            switch_partition();
            greedy_weak();
            auto clqcoverw{dual.dual_cost};

            auto lvl = s.decisionLevel();

            delta += (clqcover - clqcoverw);
            if (clqcoverw >= clqcover && lvl >= options.dsatur_threshold) {
                switch_partition();
            } else {
                clqcover = clqcoverw;
            }

            if (lvl < options.dsatur_threshold) {
                switch_partition();

                dsatur_weak();
                auto clqcoverd{dual.dual_cost};

                delta += (clqcover - clqcoverd);
                if (clqcoverd >= clqcover) {
                    switch_partition();
                }
            }
        } break;
        }

        ms.conflict.clear();

        if (options.check_bounds) {
            dual.check("after lower bound", true);
            ++numchecks;
        }
    } else if(options.bound_algo == vc_options::REPAIRdual) {
      decision_event_level = s.decisionLevel();

      assert(dual.dual_cost <= rG.total_weight);

      if (lvl_covers.size()
          == static_cast<unsigned>(decision_event_level))
          lvl_covers.push_back(dual);
      else
          lvl_covers[decision_event_level] = dual;
    }
}

bool cons_vc::have_solution() const
{
    return ub < static_cast<weight_type>(rG.total_weight);
}

Clause* cons_vc::propagate(Solver& s)
{

#ifdef _TRACE_PRUNING
    std::cout << "propagate" << std::endl;
#endif

    if (!recompute_lb)
        return NO_REASON;

    recompute_lb = false;
    ++numcalls;

    auto stages = {&cons_vc::propagate_buss, &cons_vc::propagate_dominance,
        &cons_vc::propagate_partition, &cons_vc::propagate_maxsat,
        &cons_vc::propagate_ccache, &cons_vc::propagate_dynprog};

    for (auto stage : stages) {
        Clause *conflict = (this->*stage)();
        if (conflict)
            return conflict;
        if (recompute_lb)
            return NO_REASON;
    }

    return NO_REASON;
}

minicsp::Clause* cons_vc::handle_lb(weight_type lb)
{
#ifdef _PRETTY_TRACE
    // std::cout << "[" << bestlb << ".." << lb << ".." << ub << "]";
    std::cout << ".." << lb;
#endif

    if (s.decisionLevel() == 0 && lb > bestlb) {
        bestlb = lb;
        auto timenow{minicsp::cpuTime()};
        // this can happen if we prove too many things at the
        // root. After all, the problem is unsatisfiable, so the lower
        // bound is unbounded
        if (bestlb > ub)
            bestlb = ub;
        std::cout << "New VC lower bound = " << bestlb
                  << ", CLQ upper bound = " << init_weight - bestlb
                  << " |V| = " << rG.size() << ", conflicts = " << s.conflicts
                  << ", time = " << timenow << "\n";
    }
    if (bestlb >= ub) {
        reason.clear();
        Clause* r = Clause_new(reason);
        s.addInactiveClause(r);
        return r;
    }
    return NO_REASON;
}

minicsp::Clause* cons_vc::propagate_dominance()
{
    if (!options.dominance)
        return NO_REASON;

#ifdef _TRACE_PRUNING
    std::cout << "<low_degree_pruning -> ";
#endif

    Clause* conflict = NO_REASON;

    if (!options.weighted) {
        conflict = (options.problem == vc_options::MAXINDSET
                ? low_degree_pruning(s)
                : high_degree_pruning(s));

#ifdef _TRACE_PRUNING
        std::cout << " /low_degree_pruning>" << std::endl;
#endif

        if (conflict)
            return conflict;

        if (options.dominance >= 2) {

#ifdef _TRACE_PRUNING
            std::cout << "<dominance -> ";
#endif

            conflict = clique_dom_pruning(s);

#ifdef _TRACE_PRUNING
            std::cout << " /dominance>" << std::endl;
#endif

            if (conflict)
                return conflict;
        }
    }
    return NO_REASON;
}

void cons_vc::solution_from_dual(DualProblem& dual)
{
    weight_type newub = s.deref<weight_type>(coverweightptr); // copy, not reference
    newub += rG.total_weight;
    for (int l = 1, e = dual.layer_csize.size(); l != e; ++l) {
        weight_type layerub{newub};
        for (int cl = dual.layer_csize[l - 1]; cl < dual.layer_csize[l]; ++cl)
            if (dual.partition_sz[cl] == 1)
                layerub -= rG.weight[dual.partition[cl].min()];
        if (layerub >= ub)
            continue;
        ub = layerub;
        if (!solution_callback)
            continue;
        dual_vc_solution.clear();
        dual_clq_solution.clear();
        BitSet& N = dual_sol_neighbors;
        N.clear();
        for (int cl = dual.layer_csize[l - 1]; cl < dual.layer_csize[l]; ++cl)
            if (dual.partition_sz[cl] == 1) {
                auto v = dual.partition[cl].min();
                dual_clq_solution.push_back(v);
                N.union_with(rG.matrix[v]);
            }
        for (int cl = dual.layer_csize[l - 1]; cl < dual.layer_csize[l]; ++cl)
            if (dual.partition_sz[cl] > 1) {
                for (int v : dual.partition[cl]) {
                    if (!N.fast_contain(v)) {
                        dual_clq_solution.push_back(v);
                        layerub -= rG.weight[v];
                        ub = layerub;
                        N.union_with(rG.matrix[v]);
                    } else {
                        dual_vc_solution.push_back(v);
                    }
                }
            }
        solution_callback(dual_vc_solution, dual_clq_solution);
    }
}

minicsp::Clause* cons_vc::propagate_partition()
{
    // TODO: canfail

    compute_dual();
    solution_from_dual(dual);
    auto clqcover{dual.dual_cost};

    weight_type& coverweight = s.deref<weight_type>(coverweightptr);
    weight_type lb{coverweight + rG.total_weight - clqcover};
    DO_OR_RETURN(handle_lb(lb));
    if (lb >= ub)
        return explain_cc(s, lb);

    if (options.dominance > 1) {

#ifdef _TRACE_PRUNING
        std::cout << "<prune (" << clqcover << ") -> ";
#endif

        DO_OR_RETURN(dominance_rule(dual));

#ifdef _TRACE_PRUNING
        std::cout << " /prune>" << std::endl;
#endif
    }

    if (options.pruning != vc_options::NONE) {

#ifdef _TRACE_PRUNING
        std::cout << "<prune (" << clqcover << ") -> ";
#endif

        DO_OR_RETURN(prune(s, dual));

#ifdef _TRACE_PRUNING
        std::cout << " /prune>" << std::endl;
#endif
    }
    return NO_REASON;
}

minicsp::Clause* cons_vc::propagate_maxsat()
{
    if (options.maxsat == vc_options::NOMAXSAT)
        return NO_REASON;

    weight_type& coverweight = s.deref<weight_type>(coverweightptr);
    auto lb{coverweight + rG.total_weight - dual.dual_cost};

    if (dual.dual_cost > 2
        && (ub - lb < dual.dual_cost / 3 || s.decisionLevel() == 0)) {
        dual.sort();

        auto lvl = s.decisionLevel();
        ms.compute_conflict_set(dual, options.maxsat >= vc_options::FULL_AC);
        auto inc = ms.conflict.size();

        maxsat_bound[lvl] += inc;
        ++num_maxsat_calls[lvl];
        if (lb < ub && lb + static_cast<weight_type>(inc) >= ub)
            ++maxsat_early_fails[lvl];
    } else {
        ms.copy(dual);
    }

    if (ms.conflict.empty())
        return NO_REASON;

    lb += ms.conflict.size();

    DO_OR_RETURN(handle_lb(lb));
    if (lb >= ub)
        return explain_cc(s, lb);

    if (options.maxsat >= vc_options::MS_PRUNE) {

#ifdef _TRACE_PRUNING
        std::cout << "<maxsat_prune -> ";
#endif

        DO_OR_RETURN(prune(s, dual));

#ifdef _TRACE_PRUNING
        std::cout << " /maxsat_prune>" << std::endl;
#endif
    }
    return NO_REASON;
}

minicsp::Clause* cons_vc::propagate_ccache()
{
    // if (!options.component_caching)
    return NO_REASON;
    //return lb_ccache_cover(s);
    // return lb_ccache_disjoint(s);
}

minicsp::Clause* cons_vc::propagate_dynprog()
{
    if (!options.dynprog)
        return NO_REASON;

    weight_type& coverweight = s.deref<weight_type>(coverweightptr);
    auto lb{coverweight + rG.total_weight - dual.dual_cost};
    auto dpbound = (coverweight + rG.total_weight - dynprog_bound(s));
    ++num_dp_calls[s.decisionLevel()];
    dp_bound[s.decisionLevel()] += std::max(dpbound - lb, (weight_type)0);

    DO_OR_RETURN(handle_lb(dpbound));

    if (dpbound >= ub && lb < ub) {
        ++dp_early_fails[s.decisionLevel()];
        return explain_dp(s);
    }

    dp_prune(s);
    return NO_REASON;
}

Clause* cons_vc::update_graph(Solver& s, Lit l, int node)
{
    weight_type& coverweight = s.deref<weight_type>(coverweightptr);
    size_t& removed_size = s.deref<size_t>(removed_sizeptr);
    size_t& fixed_size = s.deref<size_t>(fixed_sizeptr);

    rG.rem_and_update(node);
    ccache.remove_vertex(node);

    removed.push_back(node);
    ++removed_size;
    fixednodes.push_back(node);
    fixedlits.push_back(l);
    ++fixed_size;

    if (!sign(l)) {

        // node added to the cover
        coverweight += rG.weight[node];
        current_solution.fast_add(node);
    } else {

        reason.clear();
        reason.push(~l);

        if (options.problem == vc_options::MAXINDSET) {
            // node not in cover, add all neighbors
            auto& nx = rG.neighbor[node];
            for (int u : nx) {

#ifdef _TRACE_PRUNING
                std::cout << "u" << u << " ";
#endif

                rG.rem_and_update(u);
                ccache.remove_vertex(u);

                current_solution.fast_add(u);

                coverweight += rG.weight[u];
                removed.push_back(u);
                ++removed_size;
                DO_OR_RETURN(s.enqueueFill(_x[u], reason));
            }
        } else {

            util_set.copy(rG.node_set);
            util_set.intersect_with(rG.matrix[node]);

            for (auto u : util_set) {

#ifdef _TRACE_PRUNING
                std::cout << "u" << u << " ";
#endif

                rG.rem_and_update(u);
                ccache.remove_vertex(u);

                current_solution.fast_add(u);

                coverweight += rG.weight[u];
                removed.push_back(u);
                ++removed_size;
                DO_OR_RETURN(s.enqueueFill(_x[u], reason));
            }
        }
    }
    recompute_lb = true;

    return NO_REASON;
}

void cons_vc::maxsat_reverse_map(std::vector<int>& cc)
{
    cc.clear();
    cc.resize(dual.partition.size());

    cc_neighbors.clear();

    for (unsigned i = 0, curcc = ms.conflict.size(); i != ms.clique.size();
         ++i) {
        auto it = ms.clique.begin() + i;
        if (curcc > 0 && it == ms.conflict[curcc - 1])
            --curcc;

        cc[ms.clique[i]] = curcc;
        if (options.maxsat >= vc_options::MS_PRUNE
            && curcc != ms.conflict.size())
            cc_neighbors.union_with(dual.candidate[ms.clique[i]]);
        DEBUG_PRUNE
        {
            std::cout << "clq " << ms.clique[i] << " is in conflset " << curcc
                      << "/" << ms.conflict.size() << "\n";
        }
    }
}

Clause* cons_vc::dominance_preprocessing()
{

    std::vector<int> pruned;

    std::cout << rG << std::endl;

    for (int i = rG.size() - 1; i >= 0; i = std::min(i - 1, rG.size() - 1)) {
        auto u = rG.node[i];

        std::cout << "probe " << u;

        int save = removed.size();

        rG.rem_and_update(u);
        removed.push_back(u);

        if (options.problem == vc_options::MAXINDSET) {

            util_set.copy(rG.node_set);
            util_set.intersect_with(rG.matrix[u]);

            DEBUG_DOM { std::cout << util_set << std::endl; }

            for (auto v : util_set) {
                rG.rem_and_update(v);
                removed.push_back(v);
            }

        } else {

            DEBUG_DOM { std::cout << rG.neighbor[u] << std::endl; }

            for (auto v : rG.neighbor[u]) {
                rG.rem_and_update(v);
                removed.push_back(v);
            }
        }

        if (options.problem == vc_options::MAXCLIQUE)
            rG.min_coloration_bitset(
                &dual, close_type::WEAK, options.greedy_order);
        rG.min_clique_cover_bitset(
            &dual, close_type::WEAK, options.greedy_order);

        std::cout << " " << (dual.dual_cost) << " | " << (rG.weight[u]);

        if (dual.dual_cost <= rG.weight[u]) {
            pruned.push_back(u);
            std::cout << " prune!\n";
        }
        std::cout << std::endl;

        while (removed.size() > save) {
            rG.add_and_update(removed.back());
            removed.pop_back();
        }
    }

    std::cout << pruned << std::endl;

    return NO_REASON;
}

Clause* cons_vc::dominance_rule(DualProblem const& pdual)
{
    if (rG.node.size == 0)
        return NO_REASON;

    Clause* conflict = NO_REASON;
    // auto& maxNW = prune_clqsize;

    DEBUG_DOM
    {
        std::cout << "node set = " << rG.node_set
                  << " rG.total_weight = " << rG.total_weight << "\n";
    }

    for (int i = rG.size() - 1; i >= 0; i = std::min(i - 1, rG.size() - 1)) {
        auto u = rG.node[i];

        // store the maximum weight of any neighbor of u in each
        // partition
        maxNW.clear();
        maxNW.resize(pdual.num_partition());
        // store the sum of maxUW
        weight_type threshold = 0;

        auto process_neighbor = [&](auto v) {
            for (int i = 0; i < pdual.partition_of[v].size(); ++i) {
                auto vpart = pdual.partition_of[v][i];
                auto wv = pdual.weight_contribution[v][i];

                if (wv > maxNW[vpart]) {
                    threshold += (wv - maxNW[vpart]);
                    maxNW[vpart] = wv;
                }

                DEBUG_DOM
                {
                    std::cout << v << " in clq " << vpart << " weight = " << wv
                              << "(" << threshold << ")\n";
                }

                if (threshold > rG.weight[u])
                    return true;
            }

            return false;
        };

        DEBUG_DOM
        {
            std::cout << std::endl
                      << u << " weight=" << rG.weight[u]
                      << " neighbors=" << rG.neighbor_weight[u] << " ";
        }

        if (options.problem == vc_options::MAXINDSET) {

            DEBUG_DOM { std::cout << rG.neighbor[u] << std::endl; }

            for (auto v : rG.neighbor[u])
                if (process_neighbor(v))
                    break;

        } else {
            util_set.copy(rG.node_set);
            util_set.intersect_with(rG.matrix[u]);

            DEBUG_DOM { std::cout << util_set << std::endl; }

            for (auto v : util_set) {
                if (process_neighbor(v))
                    break;
            }
        }

        if (threshold <= rG.weight[u]) {

            DEBUG_DOM
            {
                std::cout << rG.weight[u] << " >= " << threshold << "!\n";
            }

            if (options.problem == vc_options::MAXINDSET) {

                conflict = rem_neighbor_explain(s, u);

            } else {

                conflict = rem_non_neighbor_explain(s, u);
            }

        }
    }

    return conflict;
}

Clause* cons_vc::prune_to_is(
    DualProblem const& pdual, int vtx, std::vector<int>& pruned)
{
    int coverweight = s.deref<int>(coverweightptr);
    int vlb = coverweight + rG.total_weight - pdual.dual_cost;
    for (auto vpart : pdual.partition_of[vtx]) {
        if (pdual.partition_sz[vpart] != 1)
            continue;
        vlb += pdual.partition_weight[vpart];
        if (vlb < ub)
            continue;
        if (s.learning) {
            reason.clear();
            nodeset_to_clause(s, fixednodes, reason);
            pruned.push_back(vtx);
            DO_OR_RETURN(s.enqueueFill(~_x[vtx], reason));
        } else
            s.enqueue(~_x[vtx], NO_REASON);
        break;
    }
    return NO_REASON;
}

Clause* cons_vc::prune(Solver& s, DualProblem const& pdual)
{
    if (rG.node.size == 0)
        return NO_REASON;

    std::shared_ptr<vc_explainer_shared> explshared;
    // auto& vclq = pdual.partition_of; // which clique each vertex is in
    last_lbs.resize(rG.capacity);
    weight_type coverweight = s.deref<weight_type>(coverweightptr);

    auto& clqsize = prune_clqsize;
    weight_type startlb = coverweight + rG.total_weight - pdual.dual_cost;

    DEBUG_PRUNE
    {
        std::cout << "coverweight = " << coverweight
                  << " rG.node.size = " << rG.node.size
                  << " dual_cost = " << pdual.dual_cost
                  << " startlb = " << startlb << " ub = " << ub << "\n";
        std::cout << clqsize << "\n";
        std::cout << pdual.sorted << "\n";
    }

    std::vector<int>& cc = prune_cc;
    if (options.maxsat >= vc_options::MS_PRUNE && ms.conflict.size())
        maxsat_reverse_map(cc);

    std::vector<uint8_t>& hitccs = prune_hitccs;

    std::vector<int> pruned;

    if (options.prune_to_is) {
        for (auto u : rG.node) {
            DO_OR_RETURN(prune_to_is(pdual, u, pruned));
        }
        for (auto v : pruned) {
            auto confl = update_graph(s, ~_x[v], v);
            assert(!confl);
        }

        pruned.clear();
    }

    for (auto u : rG.node) {
        // the bound is coverweight + |rG| - |Nu|, where Nu is the set
        // of cliques containing non-neighbors of u (including u
        // itself). We count instead cliques completely covered by
        // neighbors of u, so the bound is coverweight + |rG| - (|N| -
        // |N'|) = (coverweight + |rG| - |N|) + |N'|, where N is the set
        // of all cliques and N' are those completely covered by
        // neighbors of u.
        //
        // For maxsat, we can start from the higher maxsat bound and
        // the reasoning for pruning is the same, but when we cover a
        // clique in a conflicting set, we cannot increase the lower
        // bound by 1, because that is already accounted for in the
        // maxsat bound. So we can only count towards the bound
        // starting from the second clique in a conflicting set.

        clqsize.clear();
        clqsize.resize(pdual.num_partition());

        if (options.maxsat >= vc_options::MS_PRUNE) {
            hitccs.clear();
            hitccs.resize(ms.conflict.size());
        }

        DEBUG_PRUNE
        {
            for (int i = 0; i != pdual.num_partition(); ++i) {
                BitSet bs(rG.node_set);
                bs.intersect_with(rG.matrix[u]);
                int ic = i;
                if (pdual.sorted.size())
                    ic = pdual.sorted[i];
                bs.intersect_with(pdual.partition[ic]);
                std::cout << "|N( " << u << " ) cap clq[ " << ic
                          << " ]| = " << bs.size() << "\n";
            }
        }

        weight_type weighthitccs{0};
        weight_type vlb = startlb;

        auto process_neighbor = [&](auto v) {
            for (auto vpart : pdual.partition_of[v]) {
                DEBUG_PRUNE
                {
                    std::cout << v << " in clq " << vpart
                              << " weight = " << pdual.partition_weight[vpart]
                              << "\n";
                }
                ++clqsize[vpart];
                if (clqsize[vpart] < pdual.partition_sz[vpart])
                    continue;
                vlb += pdual.partition_weight[vpart];
                DEBUG_PRUNE { std::cout << "\tinc vlb = " << vlb << "\n"; }

                if (options.maxsat >= vc_options::MS_PRUNE
                    && !ms.conflict.empty()) {
                    unsigned vcc = cc[pdual.get_partition(v)];
                    if (vcc < ms.conflict.size() && !hitccs[vcc]) {
                        weighthitccs += pdual.partition_weight[vpart];
                        hitccs[vcc] = true;
                    }
                }
            }
        };

        if (options.problem == vc_options::MAXINDSET) {
            for (auto v : rG.neighbor[u])
                process_neighbor(v);
        } else {
            util_set.copy(rG.node_set);
            util_set.intersect_with(rG.matrix[u]);

            for (auto v : util_set)
                process_neighbor(v);
        }

        int maxsatvlb = (vlb + ms.conflict.size() - weighthitccs);

        DEBUG_PRUNE
        {
            std::cout << "vlb( " << u << " ) = " << vlb << "\n";
            std::cout << clqsize << "\n";
            std::cout << "msvlb( " << u << " ) = " << maxsatvlb
                      << " ms.conflict.size = " << ms.conflict.size()
                      << " weighthitccs = " << weighthitccs << "\n";
        }

        if (options.maxsat >= vc_options::MS_PRUNE)
            last_lbs[u] = maxsatvlb;
        else
            last_lbs[u] = vlb;

        if (vlb >= ub
            || (options.maxsat >= vc_options::MS_PRUNE && maxsatvlb >= ub)) {

#ifdef _TRACE_PRUNING
            std::cout << "p" << u << " ";
#endif
            pruned.push_back(u);

            if (options.deferred_explanations) {
                if (!explshared) {
                    explshared = explainers_shared.get_shared(
                        [&](auto& m) { return new vc_explainer_shared(&rG); });
                    explshared->cons = this;
                    explshared->dual = pdual;
                    explshared->current_solution = current_solution;
                    explshared->fixednodes = fixednodes;
                }
                auto* expl = explainers.get(
                    [&](auto& m) { return new vc_explainer(&m); });
                expl->shared = explshared;
                expl->v = u;
                expl->vlb = vlb;
                s.enqueueDeferred(_x[u], expl);
                // update_graph(s, _x[u], u);

            } else
                DO_OR_RETURN(prune_explain(s, pdual, u, vlb));
        }

        /* If u covers opartition[c] for a clique c which is not in a
           conflicting set, then if we put u in the IS we will create
           a conflicting set that includes c. In that reduced graph,
           the lower bound will be increased by at least 1 more. */
        if (false && options.maxsat >= vc_options::MS_PRUNE && ms.conflict.size()
            && maxsatvlb == ub - 1) {
            auto uclq = pdual.get_partition(u);
                // longer computation: search among other
                // non-conflicting cliques
            auto end = ms.conflict.back();
            for (auto i = ms.clique.begin(); i != end; ++i)
                if (*i != uclq && rG.matrix[u].includes(ms.opartition[*i])
                    && !rG.matrix[u].includes(pdual.partition[*i])) {
#ifdef _TRACE_PRUNING
                    std::cout << "p" << u << " ";
#endif

                    DEBUG_PRUNE
                    {
                        std::cout << "N(" << u << ") = " << rG.matrix[u]
                                  << " contains opartition of clq " << *i
                                  << " = " << dual.partition[*i]
                                  << " with opartition " << ms.opartition[*i]
                                  << ", pruning\n";
                    }

                    pruned.push_back(u);
                    DO_OR_RETURN(prune_explain(s, pdual, u, ub));
                    break;
                }
        }
    }

    // since we pruned, we can get a different cover and cause more
    // pruning, so let it go until fixpoint
    if (!pruned.empty()) {
        for (auto v : pruned) {
            auto confl = update_graph(s, _x[v], v);
            assert(!confl);
        }
    }

    have_lbs = true;

    return NO_REASON;
}

Clause* cons_vc::prune_explain(
    minicsp::Solver& s, DualProblem const& pdual, int v, int vlb)
{
    assert(s.value(_x[v]) == l_Undef);
    if (!s.learning) {
        s.enqueue(_x[v], NO_REASON);
        return NO_REASON;
    }

    explain_pruning(s, v, vlb, reason, pdual, nullptr);

    using std::begin;
    using std::end;
    int maxlevel = reason.size() == 0
        ? 0
        : s.varLevel(*std::max_element(
              begin(reason), end(reason), [&](auto l1, auto l2) {
                  return s.varLevel(l1) < s.varLevel(l2);
              }));
    // XXX: remove the 'false' bit
    if (false && maxlevel < s.decisionLevel()) {
        reason.push(_x[v]);
        std::swap(reason[0], reason.last());
        Clause *r = Clause_new(reason);
        s.addInactiveClause(r);
        s.nonMonotoneEnqueue(_x[v], r);

        return r;
    }

    auto* res = s.enqueueFill(_x[v], reason);
    // if there was a conflict, it should have been detected before we
    // got here
    assert(!res);

    return NO_REASON;
}

void cons_vc::explain_pruning(minicsp::Solver& s, int v, int vlb,
    vec<minicsp::Lit>& cl, DualProblem const& pdual, vc_explainer* expl)
{
    if (expl)
        prune_reason = expl->shared->fixednodes;
    else
        prune_reason = fixednodes;
    if (options.pruning == vc_options::FAST) {
        // like fast_minimize, but do not let neighbors of v in the
        // reason. Also, no early stop.
        prune_reason.clear();
        auto& fn = expl ? expl->shared->fixednodes : fixednodes;

        // if we do maxsat pruning, only remove vertices that are in
        // no conflicting set
        std::copy_if(
            begin(fn), end(fn), back_inserter(prune_reason), [&](int u) {
                using std::make_pair;
                if (cc_neighbors.fast_contain(u))
                    return true;
                if (rG.matrix[v].contain(u))
                    return false;
                Lit l = _x[v];
                if (vlb >= ub && s.value(l) == l_True) {
                    vlb -= rG.weight[u];
                    return false;
                }
                return true;
            });
    }
    if (options.pruning >= vc_options::GREEDY) {
        if (expl)
            relevant.copy(expl->shared->current_solution);
        else
            relevant.copy(current_solution);
        relevant.setminus_with(rG.matrix[v]);
        greedy_minimize(
            s, prune_reason, expl ? expl->shared->dual : dual, vlb, relevant);
        prune_reason = greedyr;
    }

    nodeset_to_clause(s, prune_reason, cl);
}

void cons_vc::dp_prune_explain(minicsp::Solver& s, int v)
{

    assert(s.value(_x[v]) == l_Undef);
    if (!s.learning) {
        s.enqueue(_x[v], NO_REASON);
        // update_graph(s, _x[v], v);
        return;
    }

    nodeset_to_clause(s, fixednodes, reason);
    auto* res = s.enqueueFill(_x[v], reason);
    // if there was a conflict, it should have been detected before we
    // got here
    assert(!res);

    // update_graph(s, _x[v], v);
}

int cons_vc::dp_prune(Solver& s)
{
    if (rG.node.size == 0)
        return 0;

    auto base = s.deref<weight_type>(coverweightptr) + rG.node.size;

#ifdef _TRACE_PRUNING
    std::cout << "<dynprog -> ";
#endif

    //     for (int i = rG.size() - 1; i >= 0; i = std::min(rG.size() - 1, i -
    //     1)) {
    //
    //         auto v{rG.node[i]};
    //
    //         if (base - get_vertexUB(v) >= ub) {
    //
    // #ifdef _TRACE_PRUNING
    //             std::cout << "g" << v << " ";
    // #endif
    //
    //             dp_prune_explain(s, v);
    //         }
    //     }

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG) {
        util_set.fill();
        util_set.set_max(rG.capacity - 1);
        util_set.setminus_with(current_solution);
        util_set.setminus_with(rG.node_set);

        std::cout << util_set << std::endl;
    }
#endif

    std::vector<int> pruned;
    for (auto v : rG.node)
        if (base - get_vertexUB(v) >= ub) {

#ifdef _TRACE_PRUNING
            std::cout << "g" << v << " ";
#endif

#ifdef _DEBUG_DYNPROG
            if (_DEBUG_DYNPROG)
                std::cout << "dpprune: " << base << " - vertexUB[" << v
                          << "] = " << (base - get_vertexUB(v)) << " >= " << ub
                          << std::endl;
#endif

            pruned.push_back(v);
            dp_prune_explain(s, v);
        }

#ifdef _DEBUG_DYNPROG
        else if (_DEBUG_DYNPROG) {
            std::cout << "ok: " << base << " - vertexUB[" << v
                      << "] = " << (base - get_vertexUB(v)) << " < " << ub
                      << std::endl;
        }
#endif

    if (!pruned.empty()) {
        for (auto v : pruned) {
            auto confl = update_graph(s, _x[v], v);
            assert(!confl);
        }
    }

#ifdef _TRACE_PRUNING
    std::cout << "/dynprog>";
#endif
    return pruned.size();
}

Clause* cons_vc::rem_neighbor_explain(Solver& s, const int v)
{

    assert(s.value(_x[v]) == l_Undef);

    if (!s.learning) {
        s.enqueue(~_x[v], NO_REASON);
        update_graph(s, ~_x[v], v);
        return NO_REASON;
    }

    prune_reason.clear();

    for (auto u : rG.co_neighbor[v]) {
        prune_reason.push_back(u);
    }

    nodeset_to_clause(s, prune_reason, reason);
    auto* res = s.enqueueFill(~_x[v], reason);
    if (!res)
        update_graph(s, ~_x[v], v);
    return res;
}

Clause* cons_vc::rem_non_neighbor_explain(Solver& s, const int v)
{

    assert(s.value(_x[v]) == l_Undef);

    if (!s.learning) {
        s.enqueue(~_x[v], NO_REASON);
        update_graph(s, ~_x[v], v);
        return NO_REASON;
    }

    prune_reason.clear();

    util_set.copy(rG.matrix[v]);
    util_set.setminus_with(rG.node_set);

    for (auto u : util_set) {
        prune_reason.push_back(u);
    }

    nodeset_to_clause(s, prune_reason, reason);
    auto* res = s.enqueueFill(~_x[v], reason);
    if (!res) {
        update_graph(s, ~_x[v], v);
    }
    return res;
}

Clause* cons_vc::rem_neighbor_invc_explain(const int v)
{

    assert(s.value(_x[v]) == l_Undef);

    if (!s.learning) {
        s.enqueue(_x[v], NO_REASON);
        update_graph(s, _x[v], v);
        return NO_REASON;
    }

    prune_reason.clear();

#ifdef _DEBUG_BUSS
    std::cout << " b|c";
#endif

    for (auto u : rG.co_neighbor[v]) {
        if (current_solution.fast_contain(u)) {
            prune_reason.push_back(u);
#ifdef _DEBUG_BUSS
            std::cout << " " << u;
#endif
        }
    }

    nodeset_to_clause(s, prune_reason, reason);
    auto* res = s.enqueueFill(_x[v], reason);
    if (!res) {
        update_graph(s, _x[v], v);
    }

    assert(!res);

    return res;
}

Clause* cons_vc::rem_non_neighbor_invc_explain(const int v)
{

    assert(s.value(_x[v]) == l_Undef);

    if (!s.learning) {
        s.enqueue(_x[v], NO_REASON);
        update_graph(s, _x[v], v);
        return NO_REASON;
    }

    prune_reason.clear();

    // util_set.copy(rG.matrix[v]);
    // util_set.intersect_with(current_solution);
    if (options.problem == vc_options::MAXINDSET) {
        util_set.copy(current_solution);
        util_set.setminus_with(rG.matrix[v]);
    } else {
        // util_set.copy(current_solution);
        util_set.copy(rG.matrix[v]);
        util_set.intersect_with(current_solution);
    }

#ifdef _DEBUG_BUSS
    std::cout << " b/c " << util_set;
    std::cout << std::endl
              << rG.neighbor[v] << std::endl
              << rG.matrix[v] << std::endl
              << current_solution;
#endif

    for (auto u : util_set) {
        prune_reason.push_back(u);
    }

    nodeset_to_clause(s, prune_reason, reason);
    auto* res = s.enqueueFill(_x[v], reason);

    assert(!res);

    if (!res) {
        update_graph(s, _x[v], v);
    }
    return res;
}

Clause* cons_vc::propagate_buss()
{

#ifdef _TRACE_PRUNING
    std::cout << "<buss -> ";
#endif

    if (!options.buss)
        return NO_REASON;
    Clause* conflict = (options.problem == vc_options::MAXINDSET ? buss_rule_mis()
                                                     : buss_rule_clq());

#ifdef _TRACE_PRUNING
    std::cout << "</buss> " << std::endl;
#endif

    return conflict;
}

Clause* cons_vc::buss_rule_mis()
{
    Clause* conflict = NO_REASON;

    auto allowed_degree = ub - s.deref<weight_type>(coverweightptr) - 1;

#ifdef _DEBUG_BUSS
    std::cout << std::endl
              << "Apply Buss rule " << current_solution.size()
              << " (goal = " << allowed_degree << ")\n"; //<< rG << std::endl;
    std::cout << "decisions = " << s.decisions << "\n";
    std::cout << "rG.node = " << rG.node << "\n";
    // std::cout << "max allowed degree: " << allowed_degree << " = (" << ub << " -
// " << s.deref<int>(coverweightptr) << " - 1)\n";
#endif

    while (!conflict && rG.size() && rG.max_degree > allowed_degree) {

        auto toprune = rG.node_of_degree[rG.max_degree][0];

#ifdef _TRACE_PRUNING
        std::cout << "b" << toprune << " ";
#endif

#ifdef _DEBUG_BUSS
        std::cout << rG << std::endl;
        std::cout << "max allowed degree: " << allowed_degree << " = (" << ub
                  << " - " << s.deref<weight_type>(coverweightptr) << " - 1)\n";
        std::cout << " --remove " << toprune << " (" << rG.degree(toprune)
                  << ")";
#endif

        conflict = rem_non_neighbor_invc_explain(toprune);
        allowed_degree = ub - s.deref<weight_type>(coverweightptr) - 1;

#ifdef _DEBUG_BUSS
        std::cout << "\n";
#endif
    }

    return conflict;
}

Clause* cons_vc::buss_rule_clq()
{
    Clause* conflict = NO_REASON;

    auto allowed_degree = (rG.size() - ub + s.deref<weight_type>(coverweightptr));

#ifdef _DEBUG_BUSS
    std::cout << std::endl
              << "Apply ABuss rule " << current_solution.size()
              << " (goal = " << (ub - s.deref<weight_type>(coverweightptr) - 1)
              << ")\n"; //<< rG << std::endl;
// std::cout << "max allowed degree: " << allowed_degree << " = (" << ub << " -
// " << s.deref<int>(coverweightptr) << " - 1)\n";
#endif

    while (!conflict && rG.size() && rG.min_degree < allowed_degree) {

        auto toprune = rG.node_of_degree[rG.min_degree][0];

#ifdef _DEBUG_BUSS
        std::cout << rG << std::endl;
        std::cout << "min allowed degree: " << allowed_degree << " = ("
                  << rG.size() << " - " << ub << " + "
                  << s.deref<weight_type>(coverweightptr) << ")\n";
        std::cout << " --remove " << toprune << " (" << rG.degree(toprune)
                  << ")";
#endif

#ifdef _TRACE_PRUNING
        std::cout << "b" << toprune << " ";
#endif

        conflict = rem_neighbor_invc_explain(toprune);
        allowed_degree = (rG.size() - ub + s.deref<weight_type>(coverweightptr));

#ifdef _DEBUG_BUSS
        std::cout << "\n";
#endif
    }

    return conflict;
}

Clause* cons_vc::low_degree_pruning(Solver& s)
{
    Clause* conflict = NO_REASON;
    int i = 0;
    while (!conflict && rG.size() && rG.min_degree <= 1
        && rG.node_of_degree[rG.min_degree].size() > i) {

        auto toprune = rG.node_of_degree[rG.min_degree][i];

#ifdef _TRACE_PRUNING
        std::cout << "l" << toprune << " ";
#endif

        assert(rG.weight[toprune] == 1);
        assert(rG.neighbor_weight[toprune] <= 1);

        if (rG.weight[toprune] >= rG.neighbor_weight[toprune]) {
            conflict = rem_neighbor_explain(s, toprune);
            i = 0;
        } else
            ++i;
    }

    return conflict;
}

Clause* cons_vc::high_degree_pruning(Solver& s)
{
    Clause* conflict = NO_REASON;
    int i = 0;
    while (!conflict && rG.size() && rG.max_degree >= rG.size() - 2
        && rG.node_of_degree[rG.max_degree].size() > i) {

        auto toprune = rG.node_of_degree[rG.max_degree][i];

#ifdef _TRACE_PRUNING
        std::cout << "l" << toprune << " ";
#endif

        if (rG.weight[toprune] >= rG.neighbor_weight[toprune]) {
            conflict = rem_non_neighbor_explain(s, toprune);
            i = 0;
        } else
            ++i;
    }

    return conflict;
}

Clause* cons_vc::clique_dom_pruning(Solver& s)
{
#ifdef _VERIF_WATCHERS
    verify_watchers("start init");
#endif

    Clause* conflict = NO_REASON;
    if (_first_propagation) {
        _first_propagation = false;

        conflict = (options.problem == vc_options::MAXINDSET ? low_degree_pruning(s) : high_degree_pruning(s));

        // then go through every node and either remove it or sets its watchers
        int v, w1, w2, i = rG.size() - 1;
        while (!conflict && i >= 0) {
            v = rG.node[i];
            if (!watchers.watched.contain(v)) {

#ifdef _DEBUG_KERNCLIQUE
                if (_DEBUG_KERNCLIQUE) {
                    std::cout << " - check node " << v << " /" << std::endl;
                }
#endif

                if ((options.problem == vc_options::MAXINDSET
                            ? rG.neighboring_clique(v, w1, w2)
                            : rG.non_neighboring_is(v, w1, w2))) {

#ifdef _DEBUG_KERNCLIQUE
                    if (_DEBUG_KERNCLIQUE) {
                        std::cout << "  => remove " << v
                                  << " b/c its neighborhood N-";
                        vecdisplay(rG.co_neighbor[v], std::cout);
                        std::cout << " is a clique" << std::endl;
                    }
#endif

#ifdef _TRACE_PRUNING
                    std::cout << "d" << v << " ";
#endif

                    if (options.problem == vc_options::MAXINDSET)
                        conflict = rem_neighbor_explain(s, v);
                    else
                        conflict = rem_non_neighbor_explain(s, v);

                } else {

#ifdef _DEBUG_KERNCLIQUE
                    if (_DEBUG_KERNCLIQUE) {
                        std::cout << "  => the neighborhood of " << v
                                  << " is not a clique b/c " << w1 << " and "
                                  << w2 << " are not neighbors" << std::endl;
                    }
#endif

                    watchers.watch(v, w1, w2);

#ifdef _VERIF_WATCHERS
                    verify_watchers("after add");
#endif
                }
            }

            --i;
            if (i >= (int)rG.size())
                i = rG.size() - 1;
        }

#ifdef _VERIF_WATCHERS
        verify_watchers("end init");
#endif
    }

    if (!conflict)
        return clique_update(s);
    return conflict;
}

void cons_vc::switch_partition()
{
    using std::swap;
    swap(dual, bufferdual);
}

void cons_vc::update_partition()
{
    util_set.clear();
    while (_last_checked_repair < removed.size()) {
        util_set.fast_add(removed[_last_checked_repair++]);
    }
    dual.update(util_set, maybe);
}

Clause* cons_vc::clique_update(Solver& s)
{
    Clause* conflict = NO_REASON;
    int v, y, w1, w2;

#ifdef _VERIF_WATCHERS
    verify_watchers("start incr");
#endif

    while (!conflict && rG.num_edges
        && static_cast<unsigned>(_last_checked_watched) < removed.size()) {

        conflict = (options.problem == vc_options::MAXINDSET ? low_degree_pruning(s) : high_degree_pruning(s));

        if (!rG.size())
            break;

        bool pruning = false;

        while (!pruning
            && static_cast<unsigned>(_last_checked_watched) < removed.size()) {
            // now take the first removed node y and check the nodes for which
            // it
            // was witness
            y = removed[_last_checked_watched];

#ifdef _DEBUG_KERNCLIQUEINCR
            if (_DEBUG_KERNCLIQUEINCR) {
                std::cout << "  * check non cliques witnessed by " << y << ": ";
                vecdisplay(watchers.watched_by[y], std::cout);
                std::cout << " " << rG.num_edges << " "
                          << (watchers.watched_by[y].size()) << std::endl;
            }
#endif

            for (int q = watchers.watched_by[y].size() - 1;
                 !conflict && rG.num_edges && q >= 0; --q) {
                v = watchers.watched_by[y][q];

#ifdef _DEBUG_KERNCLIQUEINCR
                if (_DEBUG_KERNCLIQUEINCR) {
                    std::cout << "   -> " << v;
                    std::cout.flush();
                }
#endif

                // if v has itself been removed, there's no point
                if (rG.node.contain(v)) {

                    if ((options.problem == vc_options::MAXINDSET
                                ? rG.neighboring_clique(v, w1, w2)
                                : rG.non_neighboring_is(v, w1, w2))) {

#ifdef _DEBUG_KERNCLIQUEINCR
                        if (_DEBUG_KERNCLIQUEINCR) {
                            std::cout << "  remove " << v
                                      << " b/c its neighborhood N-";
                            vecdisplay(rG.co_neighbor[v], std::cout);
                            std::cout << " is a clique" << std::endl;
                        }
#endif

#ifdef _TRACE_PRUNING
                        std::cout << "d" << v << " ";
#endif

                        pruning = true;

                        if (options.problem == vc_options::MAXINDSET)
                            conflict = rem_neighbor_explain(s, v);
                        else
                            conflict = rem_non_neighbor_explain(s, v);

                    } else {

#ifdef _DEBUG_KERNCLIQUEINCR
                        if (_DEBUG_KERNCLIQUEINCR) {
                            std::cout << ":(" << w1 << "," << w2 << ")"
                                      << std::endl;
                        }
#endif

#ifdef _VERIF_WATCHERS
                        verify_watchers("before rem (incr)");
#endif

                        watchers.unwatch(q, y);

#ifdef _DEBUG_KERNCLIQUEINCR
                        if (_DEBUG_KERNCLIQUEINCR) {
                            std::cout << " unwatch ok" << std::endl;
                            // std::cout.flush();
                        }
#endif

#ifdef _VERIF_WATCHERS
                        verify_watchers("after rem (incr)");
#endif

                        watchers.watch(v, w1, w2);

#ifdef _DEBUG_KERNCLIQUEINCR
                        if (_DEBUG_KERNCLIQUEINCR) {
                            std::cout << " watch ok" << std::endl;
                        }
#endif

#ifdef _VERIF_WATCHERS
                        verify_watchers("after add (incr)");
#endif
                    }

                }
#ifdef _DEBUG_KERNCLIQUEINCR
                else if (_DEBUG_KERNCLIQUEINCR) {
                    std::cout << " not in the graph" << std::endl;
                }
#endif
            }

            ++_last_checked_watched;
        }
    }

#ifdef _VERIF_WATCHERS
    verify_watchers("end incr");
#endif

    return conflict;
}

void cons_vc::greedy_minimize(Solver& s, std::vector<int> const& reason,
    DualProblem& gdual, weight_type lb, BitSet& relevant)
{
#ifdef _DEBUG_GREEDY_EXPLAIN
    if (_DEBUG_GREEDY_EXPLAIN > 1) {
        std::cout << "greedy explain " << lb << " >= " << ub << ": trivial exp = "
                  << relevant << std::endl << "Col/ClC:\n";
    }
    for (auto cl : gdual.sorted) {
        std::cout << gdual.partition[cl] << "\t\t\t" << gdual.candidate[cl]
                  << std::endl;
    }
#endif

    greedyr.clear();

    // collect the cliques
    if (options.maxsat && ms.conflict.size() > 0) {
        for (auto pcl = ms.clique.begin(); pcl != ms.conflict.back(); ++pcl)
            greedyr.push_back(*pcl);
    } else {
        for (auto cl : gdual.sorted) {
            greedyr.push_back(cl);
        }
    }

    auto cc_size = gdual.dual_cost;

    bool empty_explanation{false};

    while (greedyr.size() > 0) {
        for (auto cl : greedyr) {
            if (gdual.candidate_sz[cl] > 0) {
                // it is possible to add a vertex to this clique, try to do it

                maybe.copy(gdual.candidate[cl]);
                maybe.intersect_with(relevant);

#ifdef _DEBUG_GREEDY_EXPLAIN
                if (_DEBUG_GREEDY_EXPLAIN > 1) {
                    std::cout << "we can remove a cl/is in " << maybe
                              << " from " << relevant << std::endl;
                }
#endif

                // as long as it works, take any element in the intersection
                // between the current nogood and the vertices that could go in
                // this clique, transfer it and update
                int elt;
                while (!maybe.empty()) {
                    elt = maybe.min();
                    if (rG.weight[elt] > gdual.partition_weight[cl]) {
                        maybe.fast_remove(elt);
                        continue;
                    }
                    relevant.remove(elt);
                    maybe.intersect_with(rG.matrix[elt]);

#ifdef _DEBUG_GREEDY_EXPLAIN
                    if (_DEBUG_GREEDY_EXPLAIN > 1) {
                        std::cout << "  --remove " << elt << ": " << maybe
                                  << std::endl;
                    }
#endif
                }
            }
        }

        // all the cliques are "full"
        greedyr.clear();

        // create ub-lb new cliques from scratch
        empty_explanation = relevant.empty();
        if (!empty_explanation && lb > ub) {
            auto v{relevant.min()};
            relevant.fast_remove(v);
            greedyr.push_back(
                gdual.push(v, ((options.problem == vc_options::MAXINDSET)
                                      ? init_degree[v]
                                      : (rG.capacity - 1 - init_degree[v]))));
            lb -= rG.weight[v];
        }
        // and try to grow them as above
    }

    gdual.to_size(cc_size);

    if (!empty_explanation) {
        for (auto v : relevant) {
            greedyr.push_back(v);
        }

        for (auto v : reason) {
            // if (s.value(_x[v]) == l_InIS) {
            if (s.value(_x[v]) == l_False) {
                relevant_N = rG.matrix[v];
                relevant_N.intersect_with(relevant);

                if (options.neighbor_ratio == 0.0) {
                    if (!relevant_N.empty())
                        greedyr.push_back(v);
                } else if ((double)relevant_N.size() / (double)init_degree[v]
                    > options.neighbor_ratio)
                    greedyr.push_back(v);
            }
        }

        // remove members of the vertex cover that are neighbors of
        // vertices in the independent set which are already in the
        // reason. This is binary resolution, we have (x_i, x_j) (where i
        // is in the independent set and j is its neighbor) and the reason
        // (..., x_i, ~x_j, ...), we can remove x_j from the reason. The
        // solver would do this anyway in some cases, but this will do it
        // even if ~x_i has been assigned later than x_j
        BitSet ISn;
        bool init{false};
        for (auto v : greedyr) {
            if (s.value(_x[v]) != l_False)
                continue;
            if (!init) {
                init = true;
                ISn = rG.matrix[v];
            } else
                ISn.union_with(rG.matrix[v]);
        }

        erase_if(greedyr, [&](int v) {
            return (s.value(_x[v]) == l_True && ISn.contain(v));
        });
    }

#ifdef _DEBUG_GREEDY_EXPLAIN
    std::cout << "reason size: " << reason.size()
              << " cover size: " << current_solution.size()
              << " reduced size: " << greedyr.size() << std::endl;
#endif
}

std::vector<int>& cons_vc::fast_minimize(
    Solver& s, std::vector<int> const& reason, weight_type lb)
{
    // this needs some rethinking for weighted covers
    std::vector<int>& fastr = fast_minimize_fastr;
    if (lb == ub) {
        fastr = reason;
        return fastr;
    }
    fastr.clear();
    copy_if_early_stop(
        begin(reason), end(reason), back_inserter(fastr), [&](int v) {
            using std::make_pair;
            Lit l = _x[v];
            if (s.value(l) == l_True) {
                lb -= rG.weight[v];
                if (lb < ub) {
                    return make_pair(true, false);
                } else if (lb == ub) {
                    return make_pair(false, false);
                } else {
                    return make_pair(false, true);
                }
            } else {
                return make_pair(true, true);
            }
        });
    return fastr;
}

Clause* cons_vc::explain_cc(Solver& s, weight_type lb)
{
    std::vector<int>& crit = explain_crit;

    crit = [&]() {
        if (options.explanation == vc_options::FAST)
            return fast_minimize(s, fixednodes, lb);
        else if (options.explanation == vc_options::GREEDY) {
            relevant.copy(current_solution);
            greedy_minimize(s, fixednodes, dual, lb, relevant);
            return greedyr;
        }
        return fixednodes;
    }();

    nodeset_to_clause(s, crit, reason);
    for (int i = 0; i != reason.size(); ++i)
        assert(s.value(reason[i]) != l_Undef);

    // update stats
    ++fails;
    clauselits += reason.size();
    removedlits += fixednodes.size() - crit.size();

    Clause* r = Clause_new(reason);
    s.addInactiveClause(r);
    return r;
}

Clause* cons_vc::explain_dp(Solver& s)
{
    nodeset_to_clause(s, fixednodes, reason);
    Clause* r = Clause_new(reason);
    s.addInactiveClause(r);
    return r;
}

Lit cons_vc::node_to_lit(Solver& s, int u)
{
    auto lit = _x[u];
    auto val = s.value(lit);
    if (val == l_True)
        return ~lit;
    else if (val == l_False)
        return lit;
    else
        return lit_Undef;
}

void cons_vc::nodeset_to_clause(
    minicsp::Solver& s, std::vector<int> const& ns, vec<Lit>& cl)
{
    cl.clear();
    for (auto v : ns) {
        Lit l = node_to_lit(s, v);
        assert(l != lit_Undef);
        cl.push(l);
    }
}

void cons_vc::new_component(minicsp::Solver& s, vec<minicsp::Lit> const& cl)
{
    using std::begin;
    using std::end;

    // clv = VC \cup IS, cover = VC. Positive literals in the clause
    // (meaning sign(v) == false) are negative in the trail, so they
    // correspond to vertices in the IS
    BitSet clv(rG.capacity);
    BitSet cover(rG.capacity);
    for (Lit l : cl) {
        int v = revmap[var(l)];
        clv.fast_add(v);
        if (!sign(l)) {
            clv.union_with(rG.matrix[v]);
            cover.union_with(rG.matrix[v]);
        } else
            cover.fast_add(v);
    }
    // V \setminus (VC \cup IS)
    BitSet compv(0, rG.capacity - 1, BitSet::full);
    compv.setminus_with(clv);

                //TODO is this correct?
    weight_type coverweight = cover.size();
    weight_type complb{ub - coverweight};

    auto newcomp = std::make_unique<component>(compv, complb);
    for (auto v : newcomp->vertices)
        if (!rG.node_set.fast_contain(v))
            ++newcomp->nkilledv;
    auto& comp = ccache.add_component(std::move(newcomp));

    ccache.bump_activity(comp);
    ccache.decay_activities();

    // // should add some parameters to control these
    // if (ccache.cache.size() > 10 * rG.capacity)
    //     reduce_cache();
    if (ccache.dead.size() > ccache.cache.size())
        ccache.gc_cache();
}

// /* A component that covers part of the residual graph can be used to
//    potentially augment the lb as follows. Let
//
//    n be the number of vertices in the reduced graph
//
//    n' the number of vertices covered by the component
//
//    c the number of cliques in the cover
//
//    c' the number of cliques covered by the component
//
//    lb the effective lb of the component
//
//    then oldlb = coverweight + n - c
//
//    and newlb = coverweight + (n - n') - (c - c') + lb
//
//    So compared to oldlb, newlb is lower by n' and higher by
//    c'+lb.
//
//    This routine will choose the first component that increases the
//    lower bound and try to use other components in rG setminus
//    C. Unfortunately, it happens often that newlb > c'+lb for all
//    components, so this is pretty much useless.
// */
// Clause *cons_vc::lb_ccache_cover(Solver& s)
// {
//     static const bool trace_cc{false};
//
//     static int run{0};
//     ++run;
//     ++num_cc_calls[s.decisionLevel()];
//
//     BitSet available(rG.node_set);
//
//     if (trace_cc) {
//         std::cout << "\n------------------------------\n";
//         std::cout << "dual start run = " << run << " @dlvl "
//                   << s.decisionLevel() << " with " << available.size()
//                   << " available vertices = " << available
//                   << " cache.size = " << ccache.cache.size() << "\n";
//         std::cout << "cover nclq = " << dual.dual_cost << "\n";
//     }
//
//     BitSet compunion(0, rG.capacity, BitSet::empt),
//         tempunion(0, rG.capacity, BitSet::empt),
//         tempavail(0, rG.capacity, BitSet::empt);
//     BitSet dead_cliques{dual.dual_cost};
//     std::vector<int> tokill;
//
//     weight_type nclq{dual.dual_cost};
//     weight_type coverweight = s.deref<weight_type>(coverweightptr);
//     weight_type curlb{coverweight + rG.total_weight - nclq}, origlb{curlb};
//     for (auto& compptr : ccache.cache) {
//         auto& comp = *compptr;
//
//         BitSet diff{comp.vertices};
//         diff.setminus_with(available);
//         weight_type elb = comp.lb - diff.size();
//         if (elb <= 0)
//             continue;
//
//         int ncovered{0};
//         tempunion.copy(compunion);
//         tempunion.union_with(comp.vertices);
//         for (int i = 0; i != dual.dual_cost; ++i) {
//             if (dead_cliques.fast_contain(i))
//                 continue;
//             if (tempunion.includes(dual.partition[i])) {
//                 ++ncovered;
//                 tokill.push_back(i);
//             }
//         }
//         tempavail.copy(available);
//         tempavail.setminus_with(comp.vertices);
//
//         weight_type newlb = coverweight + tempavail.size() - (nclq - ncovered) + elb;
//         if (newlb <= curlb)
//             continue;
//         curlb = newlb;
//         if (trace_cc) {
//             std::cout << "Component " << comp.vertices
//                       << " size = " << comp.vertices.size()
//                       << " lb = " << comp.lb << " diff = " << diff
//                       << " diffsz = " << diff.size() << " elb = " << elb
//                       << " ncovered = " << ncovered << "\n";
//             std::cout << "Improved lb to " << curlb << " origlb = " << origlb
//                       << " ub = " << ub << "\n";
//         }
//         for (int clq : tokill)
//             dead_cliques.fast_add(clq);
//         available.copy(tempavail);
//         compunion.copy(tempunion);
//         ccache.bump_activity(comp);
//     }
//     if (trace_cc)
//         std::cout << "increase = " << (curlb - origlb);
//     cc_bound[s.decisionLevel()] += (curlb - origlb);
//     if (curlb >= ub) {
//         if (trace_cc)
//             std::cout << " causes FAIL @dlvl " << s.decisionLevel() << "\n";
//         ++cc_early_fails[s.decisionLevel()];
//     }
//     if (trace_cc)
//         std::cout << "\n";
//
//     DO_OR_RETURN(handle_lb(curlb));
//     // only return the actual bound if we do not need to generate an explanation
//     if (!s.learning && curlb > ub) {
//         Clause* r = Clause_new(reason);
//         s.addInactiveClause(r);
//         return r;
//     }
//     return NO_REASON;
// }
//
// /* Find a maximal set of disjoint components contained in the residual
//    graph and then a clique cover of the graph induced by the remaining
//    (uncovered) vertices
//  */
// Clause *cons_vc::lb_ccache_disjoint(minicsp::Solver& s)
// {
//     static const bool trace_cc{false};
//
//     static int run{0};
//     ++run;
//     ++num_cc_calls[s.decisionLevel()];
//
//     if (trace_cc) {
//         std::cout << "\n------------------------------\n";
//         std::cout << "dual start run = " << run << " @dlvl "
//                   << s.decisionLevel() << " decisions = " << s.decisions
//                   << " conflicts = " << s.conflicts << " with "
//                   << rG.node_set.size()
//                   << " available vertices = " << rG.node_set
//                   << " cache.size = " << ccache.cache.size() << "\n";
//         std::cout << "cover nclq = " << dual.dual_cost << "\n";
//     }
//
//     std::sort(begin(ccache.cache), end(ccache.cache), [&](auto& a, auto& b) {
//         return std::make_tuple(-(a->lb - a->nkilledv), (a->size - a->nkilledv))
//             < std::make_tuple(-(b->lb - b->nkilledv), (b->size - b->nkilledv));
//     });
//
//     BitSet v(rG.node_set);
//     BitSet removedv(0, rG.capacity, BitSet::empt);
//
//     weight_type curlb{0};
//     std::vector<std::pair<component*, weight_type>> used;
//     for (auto& compptr : ccache.cache) {
//         auto& comp = *compptr;
//
//         weight_type elb{comp.lb - comp.nkilledv};
//
//         if (elb <= (comp.size - comp.nkilledv) / 3)
//             continue;
//         if (elb <= 0)
//             continue;
//
//         if (trace_cc) {
//             std::cout << "component " << comp.vertices << " @ " << compptr.get()
//                       << " size = " << comp.vertices.size();
//             BitSet diff(comp.vertices);
//             diff.setminus_with(v);
//             std::cout << " diff = " << diff << " diffsz = " << diff.size()
//                       << " esz = " << (comp.size - comp.nkilledv)
//                       << " lb = " << comp.lb << " elb = " << elb
//                       << " nkilledv = " << comp.nkilledv << std::endl;
//             assert(diff.size() == static_cast<unsigned>(comp.nkilledv));
//         }
//
//         curlb += elb;
//         used.push_back(std::make_pair(compptr.get(), elb));
//         for (auto u : comp.vertices)
//             if (v.fast_contain(u))
//                 ccache.remove_vertex(u);
//
//         v.setminus_with(comp.vertices);
//         removedv.union_with(comp.vertices);
//     }
//
//     if (trace_cc) {
//         std::cout << "used.size = " << used.size() << " lb = " << curlb << "\n";
//         std::cout << "Clique lb = " << rG.node.size << "-" << dual.dual_cost
//                   << " = " << rG.node.size - dual.dual_cost << "\n";
//         std::cout << "Remaining vertices = " << v << " sz = " << v.size()
//                   << "\n";
//     }
//
//     removedv.intersect_with(rG.node_set);
//
//     int oldlb = rG.node.size - dual.dual_cost;
//     int maxinc = std::max(static_cast<int>(v.size()) - 1, 0);
//     if (curlb + maxinc <= oldlb) {
//         if (trace_cc)
//             std::cout << "Remaining graph is too small, cannot defeat oldlb\n";
//         for (auto u : removedv)
//             ccache.readd_vertex(u);
//         return 0;
//     }
//
//     if (v.size() == 0) {
//         if (trace_cc)
//             std::cout
//                 << "Empty remaining graph, skipping clique cover computation\n";
//         for (auto u : removedv)
//             ccache.readd_vertex(u);
//         bufferdual.clear();
//     } else {
//         std::vector<int> removedv_vec;
//         removedv_vec.insert(end(removedv_vec), begin(removedv), end(removedv));
//         for (auto u : removedv_vec)
//             rG.rem_and_update(u);
//
//         rG.min_clique_cover_bitset(
//             &bufferdual, close_type::WEAK, options.greedy_order);
//
//         std::reverse(begin(removedv_vec), end(removedv_vec));
//         for (auto u : removedv_vec) {
//             ccache.readd_vertex(u);
//             rG.add_and_update(u);
//         }
//     }
//
//     int newlb = curlb + v.size() - bufferdual.dual_cost;
//
//     if (newlb > oldlb)
//         for (auto p : used)
//             ccache.bump_activity(*p.first);
//
//     cc_bound[s.decisionLevel()] += std::max(0, newlb - oldlb);
//     weight_type coverweight = s.deref<weight_type>(coverweightptr);
//     if (newlb + coverweight >= ub) {
//         ++cc_early_fails[s.decisionLevel()];
//     }
//
//     DO_OR_RETURN(handle_lb(newlb + coverweight));
//
//     if (trace_cc) {
//         std::cout << "clique cover size = " << bufferdual.dual_cost << "\n";
//         std::cout << "new lower bound = " << newlb << "\n";
//         std::cout << "difference = " << (newlb - oldlb) << "\n";
//
//         std::cout << "incl cover oldlb = " << (oldlb + coverweight)
//                   << " newlb = " << (newlb + coverweight) << " ub = " << ub;
//
//         if (newlb + coverweight >= ub) {
//             std::cout << "\tConflict -- s.conflicts = " << s.conflicts;
//         }
//         if (s.decisionLevel() == 0 && newlb > oldlb)
//             std::cout << "\tImproved lb @dlvl 0 -- s.conflicts = "
//                       << s.conflicts;
//         std::cout << "\n";
//         if (s.decisionLevel() == 0 && newlb > oldlb) {
//             if (newlb > bestlb)
//                 std::cout << "\tbestlb too\n";
//             else
//                 std::cout << "\tbut not best\n";
//         }
//     }
//
//     if (!s.learning && newlb > oldlb && coverweight + newlb < ub) {
//         for (auto p : used) {
//             auto comp = p.first;
//             auto elb = p.second;
//             bufferdual.dual_cost += 1;
//             bufferdual.sorted.push_back(bufferdual.dual_cost - 1);
//             bufferdual.partition[bufferdual.dual_cost - 1] = comp->vertices;
//             bufferdual.partition_sz[bufferdual.dual_cost - 1] = elb + 1;
//             bufferdual.candidate[bufferdual.dual_cost - 1]
//                 = BitSet(0, rG.capacity - 1, 0);
//             bufferdual.candidate_sz[bufferdual.dual_cost - 1] = 0;
//         }
//         if (trace_cc)
//             std::cout << "Pruning from components\n";
//         auto cl = prune(s, bufferdual);
//         if (cl) {
//             std::cout << "Unimplemented: backpruning in component reasoning\n";
//             assert(0);
//         }
//         if (trace_cc)
//             std::cout << "End pruning from components\n";
//     }
//     // only return the actual bound if we do not need to generate an explanation
//     if (!s.learning && coverweight + newlb > ub) {
//         Clause* r = Clause_new(reason);
//         s.addInactiveClause(r);
//         return r;
//     }
//     return NO_REASON;
// }
//
// void cons_vc::reduce_cache()
// {
//     std::sort(begin(ccache.cache), end(ccache.cache),
//         [&](auto& a, auto& b) { return a->activity < b->activity; });
//
//     auto midpoint = begin(ccache.cache) + ccache.cache.size() / 2;
//     for (auto i = midpoint, e = end(ccache.cache); i != e; ++i) {
//         (*i)->dead = true;
//         ccache.dead.push_back(std::move(*i));
//     }
//     ccache.cache.erase(midpoint, end(ccache.cache));
// }

void vc_explainer::explain(
    minicsp::Solver& s, minicsp::Lit p, vec<minicsp::Lit>& c)
{
    assert(shared);
    shared->cons->explain_pruning(s, v, vlb, c, shared->dual, this);
    c.push(shared->cons->_x[v]);
    std::swap(c[0], c.last());
    shared.reset();
}

void cons_vc::verify_watchers(const char* msg)
{
    BitSet wnodes(0, rG.capacity, BitSet::empt);
    BitSet allwnodes(0, rG.capacity, BitSet::empt);

    for (unsigned i = 0; i < rG.capacity; ++i) {
        int w1 = rG.node[i];

        if (watchers.watched_by[w1].size() != watchers.watched_index[w1].size()
            || watchers.watched_by[w1].size()
                != watchers.watched_other[w1].size()) {
            std::cout << msg
                      << ": inconsistency in size of the watched lists of "
                      << w1 << ": " << std::endl;
            std::cout << watchers.watched_by[w1] << std::endl;
            std::cout << watchers.watched_index[w1] << std::endl;
            std::cout << watchers.watched_other[w1] << std::endl;

            assert(0);
        }

        wnodes.clear();
        for (size_t j = 0; j < watchers.watched_by[w1].size(); ++j) {
            int x = watchers.watched_by[w1][j];

            if (wnodes.contain(x)) {
                std::cout << msg << " " << x << " watches " << w1 << " twice!"
                          << std::endl;

                std::cout << watchers.watched_by[w1] << std::endl;
                std::cout << watchers.watched_index[w1] << std::endl;
                std::cout << watchers.watched_other[w1] << std::endl;

                assert(0);
            }
            wnodes.add(x);
            allwnodes.add(x);

            if (!watchers.watched.contain(x)) {
                std::cout << msg << " " << x
                          << " is not marked as watched but is watched by "
                          << w1 << std::endl;
                std::cout << watchers.watched_by[w1] << std::endl;

                assert(0);
            }
        }
        assert(wnodes.size() == watchers.watched_by[w1].size());

        for (size_t ixw1 = 0; ixw1 < watchers.watched_by[w1].size(); ++ixw1) {
            int x = watchers.watched_by[w1][ixw1];
            int w2 = watchers.watched_other[w1][ixw1];
            int ixw2 = watchers.watched_index[w1][ixw1];

            if (watchers.watched_by[w2][ixw2] != x) {
                std::cout
                    << msg
                    << ": inconsistency in the watched lists (element) of " << x
                    << " (" << w1 << " & " << w2 << "): " << std::endl;
                std::cout << watchers.watched_by[w1] << " / "
                          << watchers.watched_by[w2] << std::endl;
                std::cout << watchers.watched_index[w1] << " / "
                          << watchers.watched_index[w2] << std::endl;
                std::cout << watchers.watched_other[w1] << " / "
                          << watchers.watched_other[w2] << std::endl;

                assert(0);
            }

            if (watchers.watched_other[w2][ixw2] != w1) {
                std::cout << msg
                          << ":inconsistency in the watched lists (other) of "
                          << x << " (" << w1 << " & " << w2
                          << "): " << std::endl;
                std::cout << watchers.watched_by[w1] << " / "
                          << watchers.watched_by[w2] << std::endl;
                std::cout << watchers.watched_index[w1] << " / "
                          << watchers.watched_index[w2] << std::endl;
                std::cout << watchers.watched_other[w1] << " / "
                          << watchers.watched_other[w2] << std::endl;

                assert(0);
            }

            if (static_cast<unsigned>(watchers.watched_index[w2][ixw2])
                != ixw1) {
                std::cout << msg
                          << ": inconsistency in the watched lists (index) of "
                          << x << " (" << w1 << " & " << w2
                          << "): " << std::endl;
                std::cout << watchers.watched_by[w1] << " / "
                          << watchers.watched_by[w2] << std::endl;
                std::cout << watchers.watched_index[w1] << " / "
                          << watchers.watched_index[w2] << std::endl;
                std::cout << watchers.watched_other[w1] << " / "
                          << watchers.watched_other[w2] << std::endl;

                assert(0);
            }
        }
    }

    for (size_t i = 0; i < watchers.watched.size; ++i) {
        if (!allwnodes.contain(watchers.watched[i])) {
            std::cout << msg << " " << watchers.watched[i]
                      << " is marked as watched but is not watched" << std::endl
                      << "marked: " << watchers.watched << std::endl
                      << "actual: " << allwnodes << std::endl;

            assert(0);
        }
    }
}

// void cons_vc::degeneracy_sort(Solver& s, const bool is_branching)
// {
//     int num_nodes = rG.size();
//
//     s.use_clause_callback(
//         [this, &s](auto const& cl, int) { this->backtrack_event = cl[0]; });
//
//     s.use_decision_callback(
//         [this, &s](minicsp::Lit l) { this->decision_event = l; });
//
//     while (rG.size()) {
//         auto v = rG.node_of_degree[rG.max_degree][0];
//         cliqueorder.push_back((is_branching ? ~_x[v] : _x[v]));
//         rG.rem_node(v);
//     }
//
//     for (int i = rG.size(); i < num_nodes; ++i) {
//         auto x = rG.node[i];
//         rG.add_node(x);
//     }
// }
//
// void cons_vc::clique_sort(Solver& s, const bool is_branching)
// {
//     // std::cout << "before = " << dual.dual_cost << std::endl;
//     //
//     // rG.min_clique_cover_bitset(&dual);
//     // auto clqcover = dual.dual_cost;
//     //
//     // std::cout << "greedy = " << dual.dual_cost << std::endl;
//     //
//     // switch_partition();
//     // rG.dsatur_cc(&dual);
//     // auto clqcoverd{dual.dual_cost};
//     //
//     // std::cout << "dsatur = " << dual.dual_cost << std::endl;
//     //
//     // if (clqcoverd >= clqcover) {
//     //     switch_partition();
//     // } else {
//     //     clqcover = clqcoverd;
//     // }
//
//     // if (is_branching) {
//     //     l_InIS = l_True;
//     // }
//
//     s.use_clause_callback(
//         [this, &s](auto const& cl, int) { this->backtrack_event = cl[0]; });
//
//     s.use_decision_callback(
//         [this, &s](minicsp::Lit l) { this->decision_event = l; });
//
//     monitor_tree = true;
//
//     std::cout << "base   = " << dual.dual_cost << std::endl;
//
//     dual.repair(util_set);
//
//     std::cout << "repair = " << dual.dual_cost << std::endl;
//
//     dual.sort();
//
//     dual.percolate(util_set);
//
//     dual.sort();
//
//     ms.compute_conflict_set(dual, options.maxsat >= vc_options::FULL_AC);
//     auto inc = ms.conflict.size();
//
//     auto is_ub = (dual.dual_cost - inc);
//
//     std::cout << "maxsat = " << is_ub << std::endl;
//
//     auto pcl{ms.clique.end() - 1};
//
//     // int idx = 0;
//     for (auto cft : ms.conflict) {
//
//         bool not_first = false;
//         while (pcl >= cft) {
//
//             auto start = cliqueorder.size();
//
//             for (auto v : dual.partition[*pcl]) {
//                 int i = cliqueorder.size();
//                 while (--i >= 0) {
//                     auto u{var(cliqueorder[i])};
//                     if (!rG.matrix[v].fast_contain(u)) {
//                         // sorted_non_neighbor[v].push_back(u);
//                         sorted_non_neighbor[u].push_back(v);
//                     }
//                 }
//
//                 cliqueorder.push_back((is_branching ? ~_x[v] : _x[v]));
//                 staticUBIS[v] = is_ub;
//                 vertexUBIS[v] = is_ub;
//             }
//
//             sort(begin(cliqueorder) + start, end(cliqueorder),
//                 [&](auto& a, auto& b) {
//                     return rG.degree(var(a)) > rG.degree(var(b));
//                 });
//
//             if (not_first)
//                 --is_ub;
//             not_first = true;
//
//             --pcl;
//         }
//     }
//
//     while (pcl >= ms.clique.begin()) {
//
//         auto start = cliqueorder.size();
//
//         for (auto v : dual.partition[*pcl]) {
//             int i = cliqueorder.size();
//             while (--i >= 0) {
//                 auto u{var(cliqueorder[i])};
//                 if (!rG.matrix[v].fast_contain(u)) {
//                     // sorted_non_neighbor[v].push_back(u);
//                     sorted_non_neighbor[u].push_back(v);
//                 }
//             }
//
//             cliqueorder.push_back((is_branching ? ~_x[v] : _x[v]));
//             staticUBIS[v] = is_ub;
//             vertexUBIS[v] = is_ub;
//             // vertexindex[v] = idx++;
//         }
//
//         sort(begin(cliqueorder) + start, end(cliqueorder),
//             [&](auto& a, auto& b) {
//                 return rG.degree(var(a)) > rG.degree(var(b));
//             });
//
//         --is_ub;
//         --pcl;
//     }
//
//     auto n = rG.size();
//
//     int count = 0;
//
//     for (auto v : cliqueorder) {
//         auto x{var(v)};
//
//         staticLBVC[x] = n-- - staticUBIS[x];
//
//         std::cout << std::right << std::setw(3) << ++count << " "
//                   << std::setw(3) << x << " " << std::setw(2) <<
//                   staticUBIS[x]
//                   << "|" << std::left << std::setw(3) << staticLBVC[x] << "
//                   ("
//                   << dual.partition_sz[ms.clique_of[x]] << "/" <<
//                   rG.degree(x)
//                   << "="
//                   <<
//                   (double)((int)((double)(dual.partition_sz[ms.clique_of[x]])
//                          * 100.0 / (double)(rG.degree(x))))
//                 / 100.0
//                   << ")\n";
//     }
//
//     int idx = 0;
//     for (int i = 0; i < cliqueorder.size(); ++i) {
//         auto v{var(cliqueorder[i])};
//
//         vertexindex[v] = idx++;
//
//         searchUBIS[v].push_back(staticUBIS[v]);
//     }
// }

// int cons_vc::dynprog_bound_sta(Solver& s, const int node)
// {
//     if (rG.num_edges == 0)
//         return rG.size();
//
//     int x = node;
//     auto stop = vertexindex[x];
//
//     while (stop < rG.capacity - 1 && s.value(x) == l_True) {
//         x = var(cliqueorder[++stop]);
//     }
//
//     auto ground = 0;
//     int i = rG.capacity - 1;
//     do {
//         auto v{var(cliqueorder[i])};
//
//         descendant.clear();
//
//         if (s.value(v) != l_True) {
//             for (auto u : sorted_non_neighbor[v]) {
//                 if (s.value(u) != l_True) {
//                     auto ubu = vertexUBIS[u];
//                     if (!descendant.contain(ubu - 1)) {
//                         descendant.add(ubu - 1);
//                     }
//                 }
//             }
//         }
//
//         vertexUBIS[v] = std::min(staticUBIS[v], (int)(1 + descendant.size));
//
//         if (s.value(v) == l_False) {
//             ++ground;
//         }
//
//     } while (i-- > stop);
//
//     return vertexUBIS[x] - ground;
// }

// #define _DEBUG_DYNPROG

void cons_vc::dual_sort(
#ifdef _DEBUG_DYNPROG
    Solver& s
#endif
    )
{
    sortedvertices.clear();
    auto is_ub = dual.dual_cost - ms.conflict.size();
    auto ub_is = 0;

    auto pcl{ms.clique.end() - 1};
    auto last{ms.clique.begin()};

    if (options.maxsat == vc_options::NOMAXSAT) {
        dual.sort();
        pcl = dual.sorted.end() - 1;
        last = dual.sorted.begin();
    }

    /* those oare the conflict sets, we treat them separately since the initial
     * UB is different */
    for (auto cft : ms.conflict) {

        bool not_first = false;
        while (pcl >= cft) {

#ifdef _DEBUG_DYNPROG
            if (_DEBUG_DYNPROG)
                std::cout << "clique " << (int)(pcl - ms.clique.begin()) + 1
                          << std::endl;
#endif

            if (pcl > cft)
                ++ub_is;

            for (auto v : dual.partition[*pcl]) {
                vertexindex[v] = sortedvertices.size();
                sortedvertices.push_back(v);
                vertexUBForward[v] = is_ub;
                vertexUBBackward[v] = ub_is;

#ifdef _DEBUG_DYNPROG
                if (_DEBUG_DYNPROG)
                    std::cout << v << " " << is_ub << " " << ub_is << std::endl;
#endif
            }

            if (not_first)
                --is_ub;
            not_first = true;

            --pcl;
        }

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << std::endl;
#endif
    }

    /* then the cliques that are not in conflict */
    while (pcl >= last) {

        ++ub_is;

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << "clique " << (int)(pcl - last) + 1 << "/"
                      << dual.dual_cost << std::endl;
#endif

        for (auto v : dual.partition[*pcl]) {
            vertexindex[v] = sortedvertices.size();
            sortedvertices.push_back(v);
            vertexUBForward[v] = is_ub;
            vertexUBBackward[v] = ub_is;

#ifdef _DEBUG_DYNPROG
            if (_DEBUG_DYNPROG)
                std::cout << v << " " << is_ub << " " << ub_is << std::endl;
#endif
        }

        --is_ub;
        --pcl;
    }
}

int cons_vc::dynprog_bound(Solver& s)
{
    if (options.problem == vc_options::MAXINDSET)
        return dynprog_bound_clc(s);
    else
        return dynprog_bound_col(s);
}

int cons_vc::dynprog_bound_clc(Solver& s)
{
    if (rG.num_edges == 0)
        return rG.size();
    if (rG.num_edges == (rG.size() * (rG.size() - 1) / 2))
        return (rG.size() ? 1 : 0);

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG) {

        std::cout << rG << std::endl;

        std::cout << std::endl
                  << dual.dual_cost << " cliques, " << ms.conflict.size()
                  << " conflicts" << std::endl;
    }
#endif

    // first order the vertices and initialise vertexUB
    dual_sort(
#ifdef _DEBUG_DYNPROG
        s
#endif
        );

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG)
        std::cout << "size: " << sortedvertices.size() << std::endl;
#endif

    int ncount{0};
    /* now the forward pass (O(|V|^2), would be O(|E|) on the dual graph) */
    for (int i = sortedvertices.size() - 1; i >= 0; --i) {
        auto v = sortedvertices[i];

        descendant.clear();

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << v << ":";
#endif

        auto max_vub{0};
        ncount = rG.capacity - rG.neighbor[v].size();
        for (size_t j = i + 1; j < sortedvertices.size() && ncount > 0; ++j) {

            auto u = sortedvertices[j];

            if (!rG.exists_edge(v, u)) {

                --ncount;
                auto ubu = vertexUBForward[u];
                if (max_vub < ubu)
                    max_vub = ubu;
                auto prt = dual.get_partition(u);
                if (!descendant.contain(prt)) {
                    descendant.add(prt);
                }

#ifdef _DEBUG_DYNPROG
                if (_DEBUG_DYNPROG)
                    std::cout << " " << u << "[" << ubu << "|" << prt << "]";
#endif
            }

#ifdef _DEBUG_DYNPROG
            else if (_DEBUG_DYNPROG)
                std::cout << " (" << u << ")";
#endif
        }

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << std::endl
                      << v << " min(" << vertexUBForward[v] << ", 1+|"
                      << descendant << "|=" << descendant.size + 1 << ", 1+"
                      << max_vub << ")";
#endif

        vertexUBForward[v]
            = std::min(vertexUBForward[v], (int)(1 + descendant.size));
        vertexUBForward[v] = std::min(vertexUBForward[v], (1 + max_vub));

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << " --> " << vertexUBForward[v] << std::endl;
#endif
    }

    /* the backward pass [we could store stuff during the forward pass to make
     * the backward pass faster]*/
    for (size_t i = 0; i < sortedvertices.size(); ++i) {
        auto v = sortedvertices[i];

        descendant.clear();

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << v << ":";
#endif

        ncount = rG.capacity - rG.neighbor[v].size();
        auto max_vub{0};
        for (int j = i - 1; j >= 0 && ncount > 0; --j) {
            auto u = sortedvertices[j];

            if (!rG.exists_edge(v, u)) {

                --ncount;
                auto ubu = vertexUBBackward[u];
                if (max_vub < ubu)
                    max_vub = ubu;
                auto prt = dual.get_partition(u);
                if (!descendant.contain(prt)) {
                    descendant.add(prt);
                }

#ifdef _DEBUG_DYNPROG
                if (_DEBUG_DYNPROG)
                    std::cout << " " << u << "[" << ubu << "|" << prt << "]";
#endif
            }

#ifdef _DEBUG_DYNPROG
            else if (_DEBUG_DYNPROG)
                std::cout << " (" << u << ")";
#endif
        }

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << std::endl
                      << v << " min(" << vertexUBBackward[v] << ", 1+|"
                      << descendant << "|=" << descendant.size + 1 << ", 1+"
                      << max_vub << ")";
#endif

        vertexUBBackward[v]
            = std::min(vertexUBBackward[v], (int)(1 + descendant.size));
        vertexUBBackward[v] = std::min(vertexUBBackward[v], (1 + max_vub));

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << " --> " << vertexUBBackward[v] << std::endl;
#endif
    }

    // return the smallest UB
    auto worstub = 0;
    for (auto v : sortedvertices) {

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG) {
            std::cout << " " << (vertexUBBackward[v] + vertexUBForward[v] - 1);
            if (s.deref<weight_type>(coverweightptr) + rG.size()
                    - (vertexUBBackward[v] + vertexUBForward[v] - 1)
                >= ub) {

                std::cout << "-(" << v << ")";
            }
        }
#endif

        if (worstub < get_vertexUB(v)) {
            worstub = get_vertexUB(v);
        }
    }

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG)
        std::cout << std::endl;
#endif

    return worstub;
}

int cons_vc::dynprog_bound_col(Solver& s)
{
    if (rG.num_edges == 0)
        return (rG.size() ? 1 : 0);
    if (rG.num_edges == (rG.size() * (rG.size() - 1) / 2))
        return rG.size();

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG)
        std::cout << std::endl
                  << dual.dual_cost << " is, " << ms.conflict.size()
                  << " conflicts" << std::endl;
#endif

    // first order the vertices and initialise vertexUB
    dual_sort(
#ifdef _DEBUG_DYNPROG
        s
#endif
        );

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG)
        std::cout << "size: " << sortedvertices.size() << std::endl;
#endif

    int ncount{0};
    auto max_vub{0};
    /* now the forward pass (O(|V|^2), would be O(|E|) on the dual graph) */
    for (int i = sortedvertices.size() - 1; i >= 0; --i) {
        auto v = sortedvertices[i];

        descendant.clear();

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << i << " " << v << ":";
#endif

        ncount = (sortedvertices.size() - i - 1);
        for (auto u : rG.neighbor[v]) {
            if (vertexindex[u] > vertexindex[v]) {
                if (--ncount < 0)
                    break;

                auto ubu = vertexUBForward[u];
                if (max_vub < ubu)
                    max_vub = ubu;
                auto prt = dual.get_partition(u);
                if (!descendant.contain(prt)) {
                    descendant.add(prt);
                }
            }
        }

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << std::endl
                      << v << " min(" << vertexUBForward[v] << ", 1+|"
                      << descendant << "|=" << descendant.size + 1 << ", 1+"
                      << max_vub << ")\n";
#endif

        vertexUBForward[v]
            = std::min(vertexUBForward[v], (int)(1 + descendant.size));
        vertexUBForward[v] = std::min(vertexUBForward[v], (1 + max_vub));
    }

    /* the backward pass */
    for (size_t i = 0; i < sortedvertices.size(); ++i) {
        auto v = sortedvertices[i];

        descendant.clear();

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << i << " " << v << ":";
#endif

        ncount = i;
        auto max_vub{0};
        for (auto u : rG.neighbor[v]) {
            if (vertexindex[u] < vertexindex[v]) {
                if (--ncount < 0)
                    break;

                auto ubu = vertexUBBackward[u];
                if (max_vub < ubu)
                    max_vub = ubu;
                auto prt = dual.get_partition(u);
                if (!descendant.contain(prt)) {
                    descendant.add(prt);
                }
            }
        }

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << std::endl
                      << v << " min(" << vertexUBBackward[v] << ", 1+|"
                      << descendant << "|=" << descendant.size + 1 << ", 1+"
                      << max_vub << ")\n";
#endif

        vertexUBBackward[v]
            = std::min(vertexUBBackward[v], (int)(1 + descendant.size));
        vertexUBBackward[v] = std::min(vertexUBBackward[v], (1 + max_vub));
    }

    // return the smallest UB
    auto worstub = 0;
    for (auto v : sortedvertices) {

#ifdef _DEBUG_DYNPROG
        if (_DEBUG_DYNPROG)
            std::cout << " " << (vertexUBBackward[v] + vertexUBForward[v] - 1);
#endif

        if (worstub < get_vertexUB(v)) {
            worstub = get_vertexUB(v);
        }
    }

#ifdef _DEBUG_DYNPROG
    if (_DEBUG_DYNPROG)
        std::cout << std::endl;
#endif

    return worstub;
}

int cons_vc::get_vertexUB(const int v)
{
    return vertexUBBackward[v] + vertexUBForward[v] - 1;
}

cons_vc* post_vc(Solver& s, DegreeSorted<Graph>& G, std::vector<Lit> const& v,
    vc_options opt,
    std::function<void(std::vector<int> const&, std::vector<int> const&)>
        solution_callback)
{
    cons_vc* con = new cons_vc(s, G, v, opt, solution_callback);
    s.addConstraint(con);

    // con->dominance_preprocessing();
    // exit(1);

    return con;
}
