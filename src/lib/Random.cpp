
#include <time.h>

#include <Random.hpp>

/**********************************************
 * Knuth's Random number generator (code from sp-1.4)
 **********************************************/
/**
   urand(), urand0() return uniformly distributed unsigned random ints
   all available bits are random, e.g. 32 bits on many platforms
   usrand(seed) initializes the generator
   a seed of 0 uses the current time as seed

   urand0() is the additive number generator "Program A" on p.27 of Knuth v.2
   urand() is urand0() randomized by shuffling, "Algorithm B" on p.32 of Knuth
   v.2
   urand0() is one of the fastest known generators that passes randomness tests
   urand() is somewhat slower, and is presumably better
*/

static unsigned knuth_rand_x[56], knuth_rand_y[256], knuth_rand_z;
static int knuth_rand_j, knuth_rand_k;
static int rand_initialised = false;

bool random_generator_is_ready() { return rand_initialised; }

void usrand(unsigned seed)
{
    int j;

    rand_initialised = true;

    knuth_rand_x[1] = 1;
    if (seed)
        knuth_rand_x[2] = seed;
    else
        knuth_rand_x[2] = time(NULL);
    for (j = 3; j < 56; ++j)
        knuth_rand_x[j] = knuth_rand_x[j - 1] + knuth_rand_x[j - 2];

    knuth_rand_j = 24;
    knuth_rand_k = 55;
    for (j = 255; j >= 0; --j)
        urand0();
    for (j = 255; j >= 0; --j)
        knuth_rand_y[j] = urand0();
    knuth_rand_z = urand0();
}

unsigned urand0(void)
{
    if (--knuth_rand_j == 0)
        knuth_rand_j = 55;
    if (--knuth_rand_k == 0)
        knuth_rand_k = 55;
    return knuth_rand_x[knuth_rand_k] += knuth_rand_x[knuth_rand_j];
}

unsigned urand(void)
{
    int j;

    j = knuth_rand_z >> 24;

    knuth_rand_z = knuth_rand_y[j];
    if (--knuth_rand_j == 0)
        knuth_rand_j = 55;
    if (--knuth_rand_k == 0)
        knuth_rand_k = 55;
    knuth_rand_y[j] = knuth_rand_x[knuth_rand_k] += knuth_rand_x[knuth_rand_j];
    return knuth_rand_z;
}

int randint(const int upto)
// gives a random integer uniformly in [0..upto-1]
{
    return (urand() % upto);
}

double randreal()
// gives a random integer uniformly in (0,1)
{
    return (urand() / (double)((double)MAX_URAND + 1.0));
}
