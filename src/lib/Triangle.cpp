#include <Triangle.hpp>
#include <minicsp/core/utils.hpp>

using namespace ::minicsp;

Clause* cons_triangle::wake(Solver& s, Lit e) {
	return cons_triangle::propagate(s);
}

/* Eij + Eik + Ejk != 2 (used for coloring triangle cons) */
Clause* cons_triangle::propagate(Solver &s)
{
	lbool a1 = s.value(e1);
	lbool a2 = s.value(e2);
	lbool a3 = s.value(e3);
	int ud = (a1==l_Undef) + (a2==l_Undef) + (a3==l_Undef);
	if (ud < 2) {
		r.clear();
			//passer en attribut de classe
		if (ud < 1) {
			if ((int)(a1 == l_True) + (int)(a2 == l_True) + (int)(a3 == l_True) == 2) {
				if (a1 == l_False) {
					r.push(e1);
					r.push(~e2);
					r.push(~e3);
				} else if (a2 == l_False) {
					r.push(e2);
					r.push(~e1);
					r.push(~e3);
				} else if (a3 == l_False) {
					r.push(e3);
					r.push(~e2);
					r.push(~e1);
				}
				Clause *c = Clause_new(r);
				return c;
			}
		} else {
			if (a1 == l_Undef) {
				if (a2 == l_True && a3 == l_True) {
					r.push(e1);
					r.push(~e2);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(e1, r));
				} else if (a2 == l_True && a3 == l_False) {
					r.push(~e1);
					r.push(~e2);
					r.push(e3);
					DO_OR_RETURN(s.enqueueFill(~e1, r));
				} else if (a2 == l_False && a3 == l_True) {
					r.push(~e1);
					r.push(e2);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(~e1, r));
				}
			} else
			if (a2 == l_Undef) {
				if (a1 == l_True && a3 == l_True) {
					r.push(e2);
					r.push(~e1);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(e2, r));
				} else if (a1 == l_True && a3 == l_False) {
					r.push(~e1);
					r.push(~e2);
					r.push(e3);
					DO_OR_RETURN(s.enqueueFill(~e2, r));
				} else if (a1 == l_False && a3 == l_True) {
					r.push(e1);
					r.push(~e2);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(~e2, r));
				}
			} else
			if (a3 == l_Undef) {
				if (a2 == l_True && a1 == l_True) {
					r.push(e3);
					r.push(~e2);
					r.push(~e1);
					DO_OR_RETURN(s.enqueueFill(e3, r));
				} else if (a2 == l_True && a1 == l_False) {
					r.push(e1);
					r.push(~e2);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(~e3, r));
				} else if (a2 == l_False && a1 == l_True) {
					r.push(~e1);
					r.push(e2);
					r.push(~e3);
					DO_OR_RETURN(s.enqueueFill(~e3, r));
				}
			}
		}
	}
	return 0L;
}
