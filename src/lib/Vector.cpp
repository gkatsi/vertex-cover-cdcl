

#include <Vector.hpp>

std::ostream& operator<<(std::ostream& os, const Pair* x)
{
    return (x ? x->display(os) : os);
}

std::ostream& operator<<(std::ostream& os, const Pair& x)
{
    return x.display(os);
}
